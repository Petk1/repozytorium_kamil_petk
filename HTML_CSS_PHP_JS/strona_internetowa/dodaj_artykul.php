<?php
session_start();
	if(!isset($_SESSION['zalogowany']) AND $_SESSION['user'] != "admin"){
		header('Location: index.php?page=glowna');
		exit();	
	}
	require_once "polaczenie_z_baza.php";

	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
			echo "Error:".$connect->connect_errno;
	}
	else
	{	
		if($_GET['mode'] == "add")
		{
			if($_FILES['fPlik']['tmp_name'] != '')
			{
				$name = $_POST['tytul'].".jpg";
				$fileName = $_FILES['fPlik']['tmp_name'];
						
				#sciezka katalogu na pliki	
				$dir = 'okladki';
				move_uploaded_file($fileName, $dir.'/'.$name);
			}
		
			$stmt = $connect->prepare("INSERT INTO filmy(tytul, data_produkcji, rezyser, budzet, zysk_brutto, opis) VALUES (?,?,?,?,?,?)");
			
			$stmt->bind_param("ssssss", $_POST['tytul'], $_POST['data'], $_POST['rezyser'], $_POST['budzet'], $_POST['zysk'], $_POST['opis']); 
			$stmt->execute();	
			$stmt->close();	
		}	
		
		if($_GET['mode'] == "delete")
		{
			$stmt = $connect->prepare("DELETE FROM filmy WHERE tytul = ?");
			$stmt->bind_param("s", $_POST['dtytul']);
			$stmt->execute();
			$stmt->close();	
			
			$stmt2 = $connect->prepare("DELETE FROM aktorzy WHERE film = ?");
			$stmt2->bind_param("s", $_POST['dtytul']);
			$stmt2->execute();
			$stmt2->close();	
			
			unlink('okladki/'.$_POST['dtytul'].".jpg");
		}
		
		$connect->close();
		header('Location: index.php?page=lista_artykulow');
	}
?>