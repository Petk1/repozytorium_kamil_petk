<?php
session_start();
	if(!isset($_SESSION['zalogowany']) AND $_SESSION['user'] != "admin"){
		header('Location: index.php?page=glowna');
		exit();	
	}
	require_once "polaczenie_z_baza.php";

	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
			echo "Error:".$connect->connect_errno; //zwraca kod bledu
	}
	else
	{
		if($_GET['mode'] == 'add'){
		
			if($_FILES['fPlik']['tmp_name'] != '')
			{
				$nameWithTime = time()."-".$_FILES['fPlik']['name'];
				$fileName = $_FILES['fPlik']['tmp_name'];
					
				#sciezka katalogu na pliki	
				$dir = 'galeria';
				move_uploaded_file($fileName, $dir.'/'.$nameWithTime);
				
				$stmt = $connect->prepare("INSERT INTO galeria(nazwa_pliku) VALUES (?)");
			
				$stmt->bind_param("s", $nameWithTime); 
				$stmt->execute();	
				$stmt->close();
				
				$_SESSION['error_galeria'] = '<span style = "color:red">Poprawnie dodano zdjecie</span>';
				header('Location: index.php?page=galeria');
				die();				
			}
			else{
				$_SESSION['error_galeria'] = '<span style = "color:red">Wybierz plik</span>';
				header('Location: index.php?page=galeria');		
				die();
			}
		
		}
		if($_GET['mode'] == 'delete'){
				$id_file = isset($_POST['id_file']) ? $_POST['id_file'] : '';
				$nazwa_pliku= array();
				$stmt = $connect->prepare("SELECT nazwa_pliku FROM galeria WHERE id = ?");
				$stmt->bind_param("i", $id_file); 
				$stmt->execute();
				$result = $stmt->get_result();
				
				while($wiersz = $result->fetch_assoc()){
					array_push($nazwa_pliku, array($wiersz['nazwa_pliku']));				
				}
				$stmt->close();
				
				$zdjecie = $nazwa_pliku[0][0];
				$name = 'galeria/'.$zdjecie;
				unlink($name);
				
				$stmt2 = $connect->prepare("DELETE FROM galeria WHERE id = ?");
				$stmt2->bind_param("i", $id_file); 
				$stmt2->execute();
				
				$stmt2->close();
				
				$_SESSION['delete_photo'] = '<span style = "color:red">Usunięto zdjęcie</span>';
				header('Location: index.php?page=galeria');
				die();
		
		}
		$connect->close();
	}
?>