
<?php

	session_start();

	if((!isset($_POST['login'])) || (!isset($_POST['password'])) || (!isset($_POST['password'])))
	{
		header('Location: index.php?page=rejestracja');
		exit();
	}

	require_once "polaczenie_z_baza.php"; 

	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
			echo "Error:".$connect->connect_errno; //zwraca kod bledu
	}
	else
	{
		
		$stmt = $connect->prepare("SELECT * FROM uzytkownicy WHERE nazwa_uzytkownika = ?");
		$stmt->bind_param("s", $_POST['login']); 
		$stmt->execute();
		if($wynik_zapytania = $stmt->get_result())
		{
			
			if($wynik_zapytania->num_rows > 0)
			{
				
				$_SESSION['error_rejestracja'] = '<span style = "color:red">Użytkownik o takim loginie już istnieje</span>';
				header('Location: index.php?page=rejestracja');
				exit();
			}
		}
		
		if($_POST['password'] == $_POST['password2'])
		{
			$date = date('Y-m-d');
			$stmt = $connect->prepare("INSERT INTO uzytkownicy(nazwa_uzytkownika, haslo, email, data_zalozenia_konta) VALUES (?, ?, ?, ?)");
			$stmt->bind_param("ssss", $_POST['login'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST['email'], $date); //dodanie uzytkownika z zahasowanym haslem
			$stmt->execute();	

			$stmt->close();
			$_SESSION['powitanie'] = '<span style = "color:red">Konto założono poprawnie. Możesz się zalogować: </span>';
			header('Location: index.php?page=logowanie');
			die();
			

		}
		else{
			$_SESSION['error_rejestracja'] = '<span style = "color:red">Hasła nie są takie same</span>';
			header('Location: index.php?page=rejestracja');
		}
	$connect->close();
	}
?>