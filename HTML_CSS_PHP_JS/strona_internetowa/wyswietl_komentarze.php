<?php

	require_once "polaczenie_z_baza.php";

	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
		echo "Error:".$connect->connect_errno; //zwraca kod bledu
	}
	else
	{
		$nazwa = isset($_GET['movie']) ? trim(strip_tags($_GET['movie'])) : '';

		$watek = array();
		$stmt = $connect->prepare("SELECT * FROM komentarze WHERE tytul_filmu = ?");
		$stmt->bind_param("s", $_GET['movie']); 
		$stmt->execute();
		$result = $stmt->get_result();
		$timestamp = time();

		$tytul_watku = $_GET['movie'];
		while($wiersz = $result->fetch_assoc()){
			array_push($watek, array($wiersz['data'], $wiersz['autor'], $wiersz['komentarz']));
			
		}	
		$stmt->close();
		
		$connect->close();
	
?>

	<script type="text/javascript">
		function sprawdzPoprawnosc(){
			var brakuje_danych=false;
			var formularz=document.forms[0];
			var napis="";

			if (formularz.fTresc.value == ""){
				napis += "Uzupełnij pole Tresc\n"
				brakuje_danych=true;
			}

			if(formularz.fTresc.value.length > 200){
				napis += "Pole Tresc zawiera za duzo znakow\n"
				brakuje_danych=true;
			}

			if (!brakuje_danych)
				formularz.submit();
			else
				alert ("Dane nie zostaly przeslane. Popraw:\n\n" + napis + "\nPopraw dane i sprobuj wyslac ponownie");
		}
			
	</script>

<?php
$numerPliku = 0;
	echo '<br/><br/>';
	echo '<h3>'.$tytul_watku.' - Komentarze użytkowników</h3>';
	echo '<br/><br/>';
	echo '<table class = "form watek">';
	for ($i=0; $i<count($watek); $i++){
		if ($i % 2 == 0) echo '<tr class="even">';
		else echo '<tr>';
		echo "<td><span class=\"data_tytul\">".$watek[$i][0].'&nbsp;&nbsp;&nbsp;';
		echo $watek[$i][1].'</span><br/>';
		echo $watek[$i][2];
		echo "<br/><br/>";
		echo "</td></tr>";
	}
}
	echo '</table>';
?>
