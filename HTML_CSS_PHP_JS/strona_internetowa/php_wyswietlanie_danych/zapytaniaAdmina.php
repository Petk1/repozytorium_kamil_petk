<?php

	if($_SESSION['user'] == "admin"){
	
		require_once "polaczenie_z_baza.php";
		
		$connect = new mysqli($host, $db_user, "", $db_name);	
		if($connect->connect_errno)
		{
			echo "Error:".$connect->connect_errno; 
		}
		else
		{	
			if($nrZapytania == 1){
				$uzytkownicy = array();
				$stmt = $connect->prepare("SELECT * FROM uzytkownicy");
				$stmt->execute();
				$result = $stmt->get_result();

				while($wiersz = $result->fetch_assoc()){
					array_push($uzytkownicy, array($wiersz['id'], $wiersz['nazwa_uzytkownika']));
					
				}		
				$stmt->close();
			}
			
			if($nrZapytania == 2){
				
				$stmt = $connect->prepare("UPDATE uzytkownicy SET reset_hasla = 1 WHERE id = ?");
				$stmt->bind_param("i", $nr_id_do_reset_hasla); 
				$stmt->execute();
				
				$stmt->close();
			}
			
			if($nrZapytania == 3){
				
				$stmt = $connect->prepare("DELETE FROM uzytkownicy WHERE id = ?");
				$stmt->bind_param("i", $nr_id_konta_do_usuniecia); 
				$stmt->execute();
				
				$stmt->close();
			}
			
			if($nrZapytania == 4){
				$komentarze = array();
				$stmt = $connect->prepare("SELECT id_komentarza, nazwa_uzytkownika, komentarz FROM uzytkownicy, komentarze 
											WHERE uzytkownicy.nazwa_uzytkownika = komentarze.autor AND uzytkownicy.id = ?");
				$stmt->bind_param("i", $nr_id_uzytkownika_komentarze); 
				$stmt->execute();
				
				$result = $stmt->get_result();

				while($wiersz = $result->fetch_assoc()){
					array_push($komentarze, array($wiersz['id_komentarza'], $wiersz['nazwa_uzytkownika'], $wiersz['komentarz']));
					
				}				
				$stmt->close();
			}
			
			if($nrZapytania == 5){

				$stmt = $connect->prepare("DELETE FROM komentarze WHERE id_komentarza = ?");
				$stmt->bind_param("i", $id_komentarza_usuniecie); 
				$stmt->execute();
				
				$stmt->close();
			}
			
			if($nrZapytania == 6){
				$uzytkownik = array();
				$stmt = $connect->prepare("SELECT nazwa_uzytkownika FROM uzytkownicy WHERE id = ?");
				$stmt->bind_param("i", $id_uzytkownika_zlicz_komentarze); 
				$stmt->execute();
				$result = $stmt->get_result();
				
				while($wiersz = $result->fetch_assoc()){
					array_push($uzytkownik, array($wiersz['nazwa_uzytkownika']));				
				}
				$stmt->close();

				$nComents = array();
				$stmt2 = $connect->prepare("SELECT COUNT(komentarz) AS ilosc_komentarzy FROM komentarze WHERE autor = ?");
				$user= $uzytkownik[0][0];
				$stmt2->bind_param("s", $user); 
				$stmt2->execute();
				$commentsNumber = $stmt2->get_result();
				
				while($wiersz = $commentsNumber->fetch_assoc()){
					array_push($nComents, array($wiersz['ilosc_komentarzy']));				
				}
				$stmt2->close();
			}
			
			if($nrZapytania == 7){
				$uzytkownik = array();
				$stmt = $connect->prepare("SELECT nazwa_uzytkownika FROM uzytkownicy WHERE id = ?");
				$stmt->bind_param("i", $id_uzytkownika_usun_wszystkie_komentarze); 
				$stmt->execute();
				$result = $stmt->get_result();
				
				while($wiersz = $result->fetch_assoc()){
					array_push($uzytkownik, array($wiersz['nazwa_uzytkownika']));				
				}
				$stmt->close();
				
				$user= $uzytkownik[0][0];
				$stmt2 = $connect->prepare("DELETE FROM komentarze WHERE autor = ?");
				$stmt2->bind_param("s", $user); 
				$stmt2->execute();
				
				$stmt2->close();
			}
			
			if($nrZapytania == 8){
				$filmy = array();
				$stmt = $connect->prepare("SELECT aktorzy.film, filmy.rezyser, filmy.data_produkcji, filmy.budzet, filmy.zysk_brutto FROM aktorzy
											INNER JOIN filmy ON aktorzy.film = filmy.tytul 
											WHERE aktorzy.imie = ? AND aktorzy.nazwisko = ?");
				$stmt->bind_param("ss", $aktor_filmy_imie, $aktor_filmy_nazwisko); 
				$stmt->execute();
				$result = $stmt->get_result();
				
				while($wiersz = $result->fetch_assoc()){
					array_push($filmy, array($wiersz['film'], $wiersz['rezyser'], $wiersz['data_produkcji'], $wiersz['budzet'], $wiersz['zysk_brutto']));				
				}
				$stmt->close();
		
			}
			
			if($nrZapytania == 9){
				$uzytkownicy_like = array();
				$like = "%{$_POST['wyszukaj_uzytkownika']}%";
				$stmt = $connect->prepare("SELECT id, nazwa_uzytkownika, email FROM uzytkownicy WHERE nazwa_uzytkownika LIKE ?");
				$stmt->bind_param("s", $like); 
				$stmt->execute();
				$result = $stmt->get_result();
				
				while($wiersz = $result->fetch_assoc()){
					array_push($uzytkownicy_like, array($wiersz['id'], $wiersz['nazwa_uzytkownika'], $wiersz['email']));				
				}
				$stmt->close();
		
			}
			
			if($nrZapytania == 10){
				$uzytkownicy_alfabetycznie = array();
				$stmt = $connect->prepare("SELECT id, nazwa_uzytkownika, email FROM uzytkownicy GROUP BY nazwa_uzytkownika");
				$stmt->execute();
				$result = $stmt->get_result();
				
				while($wiersz = $result->fetch_assoc()){
					array_push($uzytkownicy_alfabetycznie, array($wiersz['id'], $wiersz['nazwa_uzytkownika'], $wiersz['email']));				
				}
				$stmt->close();
		
			}
			
			if($nrZapytania == 11){
				$aktorzy_w_ilu_rolach = array();
				$stmt = $connect->prepare("SELECT imie, nazwisko, COUNT(*) as ilsc_rol 
											FROM aktorzy GROUP BY imie ORDER BY COUNT(*) DESC");
				$stmt->execute();
				$result = $stmt->get_result();
				
				while($wiersz = $result->fetch_assoc()){
					array_push($aktorzy_w_ilu_rolach, array($wiersz['imie'], $wiersz['nazwisko'], $wiersz['ilsc_rol']));				
				}
				$stmt->close();
		
			}
				
			$connect->close();
		}	
	}
	
	
?>
