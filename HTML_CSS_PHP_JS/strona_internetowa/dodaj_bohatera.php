<?php
session_start();
	if(!isset($_SESSION['zalogowany']) AND $_SESSION['user'] != "admin"){
		header('Location: index.php?page=glowna');
		exit();	
	}
	require_once "polaczenie_z_baza.php";

	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
			echo "Error:".$connect->connect_errno;
	}
	else
	{	
		if($_GET['mode'] == "add"){
	
			$stmt = $connect->prepare("INSERT INTO aktorzy(imie, nazwisko, film, postac) VALUES (?,?,?,?)");
			
			$stmt->bind_param("ssss", $_POST['imie'], $_POST['nazwisko'], $_GET['movie'], $_POST['postac']); 
			$stmt->execute();	
			$stmt->close();	
		}
		if($_GET['mode'] == "delete"){
			$stmt = $connect->prepare("DELETE FROM aktorzy WHERE imie = ? AND nazwisko = ? AND postac = ?");
			
			$stmt->bind_param("sss", $_POST['dimie'], $_POST['dnazwisko'], $_POST['dpostac']); 
			$stmt->execute();	
			$stmt->close();	
		}
		$connect->close();
		header('Location: index.php?page=artykul&movie='.$_GET['movie']);
	}
?>