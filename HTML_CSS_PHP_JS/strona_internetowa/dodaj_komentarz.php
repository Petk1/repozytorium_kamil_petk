<?php
session_start();
	if(!isset($_SESSION['zalogowany']) AND $_SESSION['user'] != "admin"){
		header('Location: index.php?page=glowna');
		exit();	
	}
	require_once "polaczenie_z_baza.php";

	$czyDodanoWpis = 0;
	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
			echo "Error:".$connect->connect_errno; //zwraca kod bledu
	}
	else
	{
		$mode = isset($_GET['mode']) ? $_GET['mode'] : '';
		if ($mode == 'new_answer') $parent_timestamp = time();
		$movieName = isset($_GET['movie']) ? trim(strip_tags($_GET['movie'])) : '';
		
		$stmt = $connect->prepare("INSERT INTO komentarze(autor, data, komentarz, tytul_filmu) VALUES (?,?,?,?)");

		$autor = isset($_SESSION['user']) ? substr(trim(strip_tags($_SESSION['user'])), 0, 35) : '';
		$data_i_czas = date("Y-m-d H:i:s",intval($parent_timestamp));
		$mess = isset($_GET['wiadomosc']) ? substr(trim(strip_tags($_GET['wiadomosc'])), 0, 200) : '';
		
		$stmt->bind_param("ssss", $autor, $data_i_czas, $mess, $movieName); 
		$stmt->execute();	
		$stmt->close();
		
		$connect->close();
	}
?>