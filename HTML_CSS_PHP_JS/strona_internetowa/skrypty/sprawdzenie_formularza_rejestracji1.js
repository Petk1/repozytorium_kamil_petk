function przetwarzaj_dane() {
	var brakuje_danych=false;
	var formularz=document.forms[0];
	var napis="";
	var poziom_hasla="";
	
	var reg_login = /^[A-Za-z0-9]{5,10}$/;
	var reg_email = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	
	if(!reg_login.test(formularz.login.value))
	{
		napis += "Pole Login nie zostało wypełnione poprawnie\n"
		brakuje_danych=true;
	}
	
	if(formularz.password.value.length < 5 || formularz.password.value.length > 20){
		napis += "Pole Hasło zawiera niepoprawną ilość znaków\n"
		brakuje_danych=true;
	}
	if(!formularz.password.value.match(/[0-9]/))
	{
		napis += "Hasło musi zawierać przynajmniej jedną cyfrę\n";
		brakuje_danych=true;
	}
	
	if(formularz.password.value.length > 5 && formularz.password.value.match(/[0-9]/))
	{
		poziom_hasla = "Słabe hasło\n";
	}
	if(formularz.password.value.length > 5 && formularz.password.value.match(/[0-9]/) && formularz.password.value.match(/[A-Z]/))
	{
		poziom_hasla = "Średnie hasło\n";
	}
	if(formularz.password.value.length > 5 && formularz.password.value.match(/[0-9]/) && formularz.password.value.match(/[A-Z]/) && formularz.password.value.match(/[!,@,#,$,%,^,&,*,?,_,~.]/))
	{
		poziom_hasla = "Mocne hasło\n";			
	}
	
	if(formularz.password2.value.length < 5 || formularz.password2.value.length > 20){
		napis += "Pole Powtórz hasło zawiera niepoprawną ilość znaków\n"
		brakuje_danych=true;
	}

	if(!reg_email.test(formularz.email.value))
	{
		napis += "Pole E-mail nie zostało wypełnione poprawnie\n";
		brakuje_danych=true;
	}
	
	if(formularz.regulamin.checked == false)
	{	
		napis += "Zaakcpetruj regulamin strony\n"
		brakuje_danych=true;
	}
	if (!brakuje_danych)
		formularz.submit();
	else
	{
		//wiadomosc_o_bledach.innerHTML = "<p>Przed wysłaniem proszę poprawić błędy:</p>";
       		alert ("Nie wypełniłeś następujących pól:\n\n" + napis + "\n" + poziom_hasla);
	}
		 //.innerHTML = <h3 class="form-error-title">Przed wysłaniem proszę poprawić błędy:</h3>;

}


function przetwarzaj_dane_logowania() { 
	var brakuje_danych=false;
	var formularz=document.forms[0];
	var napis="";

	
	if(formularz.login.value.length < 1){
		napis += "Pole Login zawiera niepoprawną ilość znaków\n"
		brakuje_danych=true;
	}
	if(formularz.haslo.value.length < 1){
		napis += "Pole Hasło zawiera niepoprawną ilość znaków\n"
		brakuje_danych=true;
	}
	if (!brakuje_danych)
		formularz.submit();
	else
	{
       	alert ("Nie wypełniłeś następujących pól:\n\n" + napis + "\n");
	}

}

function przetwarzaj_dane_noweHaslo() {
	var brakuje_danych=false;
	var formularz=document.forms[0];
	var napis="";
	var poziom_hasla="";
	
	if(formularz.login.value.length < 1){
		napis += "Pole Login zawiera niepoprawną ilość znaków\n"
		brakuje_danych=true;
	}
	if(formularz.password.value.length < 5 || formularz.password.value.length > 20){
		napis += "Pole Hasło zawiera niepoprawną ilość znaków\n"
		brakuje_danych=true;
	}
	if(!formularz.password.value.match(/[0-9]/))
	{
		napis += "Hasło musi zawierać przynajmniej jedną cyfrę\n";
		brakuje_danych=true;
	}
	
	if(formularz.password.value.length > 5 && formularz.password.value.match(/[0-9]/))
	{
		poziom_hasla = "Słabe hasło\n";
	}
	if(formularz.password.value.length > 5 && formularz.password.value.match(/[0-9]/) && formularz.password.value.match(/[A-Z]/))
	{
		poziom_hasla = "Średnie hasło\n";
	}
	if(formularz.password.value.length > 5 && formularz.password.value.match(/[0-9]/) && formularz.password.value.match(/[A-Z]/) && formularz.password.value.match(/[!,@,#,$,%,^,&,*,?,_,~.]/))
	{
		poziom_hasla = "Mocne hasło\n";			
	}
	if (!brakuje_danych)
		formularz.submit();
	else
	{
       	alert ("Nie wypełniłeś następujących pól:\n\n" + napis + "\n" + poziom_hasla);
	}

}

function przetwarzaj_dane_artykul() {
	var brakuje_danych=false;
	var formularz=document.forms[0];
	var napis="";
	if (formularz.tytul.value == "" || formularz.tytul.value.length > 40){
		napis += "Pole tytuł zawiera nieporawną ilość znaków (max. to 40)\n";
		brakuje_danych=true;
	}
	if(formularz.data.value == ""){
		napis += "Podaj datę produkcji\n";
		brakuje_danych=true;
	}
	if(formularz.rezyser.value == "" || formularz.rezyser.value.length > 45){
		napis += "Pole rezyser zawiera nieporawną ilość znaków (max. to 40)\n";
		brakuje_danych=true;
	}
	if(formularz.budzet.value == "" || formularz.budzet.value.length > 45){
		napis += "Pole budżet zawiera nieporawną ilość znaków (max. to 40)\n";
		brakuje_danych=true;
	}
	if(formularz.zysk.value == "" || formularz.zysk.value.length > 45){
		napis += "Pole zysk zawiera nieporawną ilość znaków (max. to 40)\n";
		brakuje_danych=true;
	}
	if(formularz.opis.value == "" || formularz.opis.value.length > 500){
		napis += "Pole opis zawiera nieporawną ilość znaków (max. to 500)\n";
		brakuje_danych=true;
	}
	if (!brakuje_danych)
		formularz.submit();
	else
		alert ("Nie wypełniłeś następujących pól:\n\n" + napis);
}

function przetwarzaj_dane_artykul_delete() {
	var brakuje_danych=false;
	var formularz=document.forms[1];
	var napis="";
	if (formularz.dtytul.value == "" || formularz.dtytul.value.length > 40){
		napis += "Podaj tytuł filmu, który chcesz usunąć (max. to 40 znaków)\n";
		brakuje_danych=true;
	}
	
	if (!brakuje_danych)
		formularz.submit();
	else
		alert ("Nie wypełniłeś następujących pól:\n\n" + napis);
}

function przetwarzaj_dane_artykul_bohater() {
	var brakuje_danych=false;
	var formularz=document.forms[1];
	var napis="";
	if (formularz.imie.value == "" || formularz.imie.value.length > 30){
		napis += "Pole imie zawiera nieporawną ilość znaków (max. to 30)\n";
		brakuje_danych=true;
	}
	if (formularz.nazwisko.value == "" || formularz.nazwisko.value.length > 45){
		napis += "Pole nazwisko zawiera nieporawną ilość znaków (max. to 45)\n";
		brakuje_danych=true;
	}
	if (formularz.postac.value == "" || formularz.postac.value.length > 45){
		napis += "Pole postac zawiera nieporawną ilość znaków (max. to 45)\n";
		brakuje_danych=true;
	}
	if (!brakuje_danych)
		formularz.submit();
	else
		alert ("Nie wypełniłeś następujących pól:\n\n" + napis);
}

function przetwarzaj_dane_artykul_bohater_delete() {
	var brakuje_danych=false;
	var formularz=document.forms[2];
	var napis="";
	if (formularz.dimie.value == "" || formularz.dimie.value.length > 30){
		napis += "Pole imie zawiera nieporawną ilość znaków (max. to 30)\n";
		brakuje_danych=true;
	}
	if (formularz.dnazwisko.value == "" || formularz.dnazwisko.value.length > 45){
		napis += "Pole nazwisko zawiera nieporawną ilość znaków (max. to 45)\n";
		brakuje_danych=true;
	}
	if (formularz.dpostac.value == "" || formularz.dpostac.value.length > 45){
		napis += "Pole postac zawiera nieporawną ilość znaków (max. to 45)\n";
		brakuje_danych=true;
	}
	if (!brakuje_danych)
		formularz.submit();
	else
		alert ("Nie wypełniłeś następujących pól:\n\n" + napis);
}