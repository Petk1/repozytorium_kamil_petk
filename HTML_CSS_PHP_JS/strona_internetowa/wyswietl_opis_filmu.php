<?php

	require_once "polaczenie_z_baza.php";

	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
		echo "Error:".$connect->connect_errno; 
	}
	else
	{	
		$glowni_aktorzy = array();
		$stmt2 = $connect->prepare("SELECT * FROM aktorzy WHERE film = ?");
		$stmt2->bind_param("s", $_GET['movie']); 
		$stmt2->execute();
		$result2 = $stmt2->get_result();
		while($wiersz = $result2->fetch_assoc()){
			array_push($glowni_aktorzy, array($wiersz['imie'], $wiersz['nazwisko'], $wiersz['postac']));	
		}
		$stmt2->close();
		
		$opis_filmu = array();
		$stmt3 = $connect->prepare("SELECT opis, rezyser, data_produkcji FROM filmy WHERE tytul = ?");
		$stmt3->bind_param("s", $_GET['movie']); 
		$stmt3->execute();
		$result3 = $stmt3->get_result();
		while($wiersz = $result3->fetch_assoc()){
			array_push($opis_filmu, array($wiersz['opis'], $wiersz['rezyser'], $wiersz['data_produkcji']));	
		}
		$stmt3->close();
		
		$connect->close();

		echo '<h1>'.$_GET['movie'].'</h1><br/>';
			
		echo '<h2>';
		for ($i=0; $i<count($opis_filmu); $i++){
			echo '<td><b>'.$opis_filmu[$i][0].' </td><br/><br/>';
			echo '<td><b>Reżyser: '.$opis_filmu[$i][1].' </td><br/><br/>';
			echo '<td><b>Data premiery: '.$opis_filmu[$i][2].' </td><br/><br/>';
		}
		echo '</h2>';

		echo '<h3>Główni bohaterowie:</h3>';
		echo '</tr><h4>';
		for ($i=0; $i<count($glowni_aktorzy); $i++){
			echo '<td><b>'.$glowni_aktorzy[$i][0].' </td>';
			echo '<td>'.$glowni_aktorzy[$i][1].'</td>';
			echo '<td> jako '.$glowni_aktorzy[$i][2].'</b></td><br/>';
		}
		echo '</h4></tr>';
		echo '<br/>';
}