<?php
	
	if((!isset($_POST['login'])) || (!isset($_POST['password'])) || (!isset($_POST['password2'])))
	{
		header('Location: resetHasla.php');
		exit();
	}

	require_once "polaczenie_z_baza.php"; 

	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
			echo "Error:".$connect->connect_errno; //zwraca kod bledu
	}
	else
	{
		$stmt = $connect->prepare("SELECT * FROM uzytkownicy WHERE nazwa_uzytkownika = ?");
		$stmt->bind_param("s", $_POST['login']); 
		$stmt->execute();	
		if($wynik_zapytania = $stmt->get_result())
		{
			if($wynik_zapytania->num_rows !== 0) //sprawdzam czy nie ma przypadkiem wiecej niz jednego uzytkownika
			{						//o takim loginie i zawsze powinien byc tylko 1 
			
				$wiersz = $wynik_zapytania->fetch_assoc();
				$czyHasloZresetowane = $wiersz['reset_hasla'];
				
				if($czyHasloZresetowane == '1')
				{	
					$stmt->close();				
					if($_POST['password'] == $_POST['password2'])
					{
					
					$stmt2 = $connect->prepare("UPDATE uzytkownicy SET haslo = ? WHERE nazwa_uzytkownika = ?");
					$newPassword = password_hash($_POST['password'], PASSWORD_DEFAULT);
					$stmt2->bind_param("ss", $newPassword, $_POST['login']);
					$stmt2->execute();
					
					$stmt2->close();
					
					$stmt3 = $connect->prepare("UPDATE uzytkownicy SET reset_hasla = 0 WHERE nazwa_uzytkownika = ?");
					$stmt3->bind_param("s", $_POST['login']); 
					$stmt3->execute();	
					$stmt3->close();
					$connect->close();
						
					header('Location: index.php?page=logowanie');
					
					}else{
						$_SESSION['error_zmiana_hasla'] = '<span style = "color:red">Hasła nie są takie same</span>';
						header('Location: index.php?page=logowanie');
					}
				}
				else echo "<p>Nie mozesz zresetwac hasla</p>";
			}
		}
	}
?>