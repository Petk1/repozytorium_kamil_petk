<?php
	
	session_start();//kazdy dokument majacy korzystac z sesji musi miec to na poczatku 
	
	//sprawdzenie czy uzytkownik jest zalogowany
	//inaczej: sprawdzenie czy ustawiony jest login i haslo
	if((!isset($_POST['login'])) || (!isset($_POST['haslo'])))
	{
		header('Location: index.php?page=logowanie');
		exit();
	}

	require_once "polaczenie_z_baza.php"; 
	
	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
			echo "Error:".$connect->connect_errno; //zwraca kod bledu
	}
	else
	{
	
		$stmt = $connect->prepare("SELECT * FROM uzytkownicy WHERE nazwa_uzytkownika = ?");
		$stmt->bind_param("s", $_POST['login']); 
		$stmt->execute();	
	
	
	if($wynik_zapytania = $stmt->get_result()) //w if zeby sprawdzic czy w zapytaniu nie ma bledu i czy moze byc wykonane
	{
		if($wynik_zapytania->num_rows !== 0) //sprawdzam czy nie ma przypadkiem wiecej niz jednego uzytkownika
		{						//o takim loginie i zawsze powinien byc tylko 1 
		
			$wiersz = $wynik_zapytania->fetch_assoc();
			$czyHasloZresetowane = $wiersz['reset_hasla'];
			if($czyHasloZresetowane == 0)
			{
				$hasloo = $_POST['haslo'];
				if(password_verify($hasloo, $wiersz['haslo']))
				{
					$_SESSION['zalogowany'] = true;//jesli wiem ze logowanie sie powiodlo
					
					//fetch_assoc - tworzy tablice asocjacyjna, fetch - przynies dane i wlóz je do tablicy asocjacyjnej
					$_SESSION['id'] = $wiersz['id'];
					$_SESSION['user'] = $wiersz['nazwa_uzytkownika'];
					$_SESSION['email'] = $wiersz['email'];
					$_SESSION['data_zalozenia_konta'] = $wiersz['data_zalozenia_konta'];
					
					unset($_SESSION['error']);//kiedy uda sie zalogowac to usuwam z sesji zmienna
					$stmt->close();//zwolnienie pamięci z wynikow zapytań ktore i tak nie są juz potrzebne			
					header('Location: index.php?page=ustawienia');//przekierowanie do innego dokumentu
				}else{
					$_SESSION['error'] = '<span style = "color:red">Nieprawidlowy login lub haslo</span>';
					header('Location: index.php?page=logowanie');				
				}
			}else{
				header('Location: index.php?page=logowanie&resetHasla=1');
			}
		}else{
			$_SESSION['error'] = '<span style = "color:red">Nieprawidlowy login lub haslo</span>';
			header('Location: index.php?page=logowanie');		
		}
	}
		
	$connect->close();
	}
?>