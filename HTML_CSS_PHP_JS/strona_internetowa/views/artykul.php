<main>
	<div id="left">
		<img id="okladki" src="okladki/<?php echo $_GET['movie'] ?>.jpg" alt="<?php echo $_GET['movie'] ?>">
	</div>
	
	<div id="right">
		<?php
			require_once "wyswietl_opis_filmu.php";
		?>
	</div>

	<div id="comments">
		KOMENTARZE UŻYTKOWNIKÓW
	</div>

	<div id="addComments">
	
		<?php
			if(isset($_SESSION['zalogowany']))
			{
		?>
				<br/><br/><br/>
				<h4>Dodaj komentarz</h4>

				<form action="" onsubmit="wyslij('fTresc'); return false;">
					<table class="fNowy">
						<tr class="fTresc" title="Treść">
							<td><label for="fTresc">Treść</label></td>
							<td><textarea id="fTresc" name="fTresc" cols="50" rows="8"></textarea><br>(max. 200 znaków)</td>
						</tr>
						<tr class="fAutor" title="Autor">
							<td>Autor</td>
							<td><?php echo $_SESSION['user']?></td>
						</tr>   
						<tr>
							<td>
								<input type="submit" name = "komentarz" value="Wyślij" onsubmit="wyslij('fTresc')" />
							</td>
						</tr>
					</table>	
				</form>
	</div>
	<div id = "admin">
		<?php
			}else echo "<h4><br/>Dodawanie komentarzy dostępne tylko dla zalogowanych użytkowników</h4>";
		if(isset($_SESSION['zalogowany']) AND $_SESSION['user'] == "admin"){
		?>
				<br/><br/>
				<h2>OPCJE ADMINA</h2>
				<h3>Dodaj bohatera:</h3>
				<form method = "POST" action = "edycja_artykulu.php?mode=add&movie=<?php echo $_GET['movie'] ?>" enctype="multipart/form-data">
					<table>
						<tr>
							<td>
								<label for = "imie">Podaj imię:</label>
							</td>
							<td>
								<input type = "text" id = "imie" name = "imie"/>
							</td>
						</tr>
						<tr>
							<td>
								<label for = "nazwisko">Podaj nazwisko:</label>
							</td>
							<td>
								<input type = "text" id = "nazwisko" name = "nazwisko"/>
							</td>
						</tr>
						<tr>
							<td>
								<label for = "postac">Jaką postać zagrał:</label>
							</td>
							<td>
								<input type = "text" id = "postac" name = "postac"/>
							</td>
						</tr>
						<tr>
							<td>
								<input type="submit" name = "addhero" value="Dodaj bohatera" onclick="przetwarzaj_dane_artykul_bohater(); return false"/>
							</td>
						</tr>
					</table>
				</form>
				<h3>Usuń bohatera:</h3>
				<form method = "POST" action = "edycja_artykulu.php?mode=delete&movie=<?php echo $_GET['movie'] ?>" enctype="multipart/form-data">
				<table>
						<tr>
							<td>
								<label for = "dimie">Podaj imię:</label>
							</td>
							<td>
								<input type = "text" id = "dimie" name = "dimie"/>
							</td>
						</tr>
						<tr>
							<td>
								<label for = "dnazwisko">Podaj nazwisko:</label>
							</td>
							<td>
								<input type = "text" id = "dnazwisko" name = "dnazwisko"/>
							</td>
						</tr>
						<tr>
							<td>
								<label for = "dpostac">Jaką postać zagrał:</label>
							</td>
							<td>
								<input type = "text" id = "dpostac" name = "dpostac"/>
							</td>
						</tr>
						<tr>
							<td>
								<input type="submit" name = "deletehero" value="Usuń bohatera" onclick="przetwarzaj_dane_artykul_bohater_delete(); return false"/>
							</td>
						</tr>
					</table>
				</form>
				<h3>Zmień okładkę:</h3>
				<form method = "POST" action = "edycja_artykulu.php?mode=cover&movie=<?php echo $_GET['movie'] ?>" enctype="multipart/form-data">
					<label for = "fPlik">Wybierz okładkę:</label>
					<input type="file" id="fPlik" name="fPlik" accept = ".jpg, .png"/><br/>
					<input type="submit" name = "updatecover" value="Zmień okładkę"/>
				</form>
				<h3>Zmień opis:</h3>
				<form method = "POST" action = "edycja_artykulu.php?mode=description&movie=<?php echo $_GET['movie'] ?>" enctype="multipart/form-data">
					<label for = "uopis">Opis:</label>	
					<textarea id = "uopis" name = "uopis" rows="4" cols="50"></textarea>
					<br/>
					<input type="submit" name = "updatedescription" value="Zmień opis"/>
				</form>
				<h3>Zmień reżysera:</h3>
				<form method = "POST" action = "edycja_artykulu.php?mode=director&movie=<?php echo $_GET['movie'] ?>" enctype="multipart/form-data">
					<label for = "udirector">Reżyser:</label>	
					<input type = "text" id = "udirector" name = "udirector"/>
					<br/>
					<input type="submit" name = "updatedirector" value="Zmień reżysera"/>
				</form>
				<br/>
				<h3>Zmień datę:</h3>
				<form method = "POST" action = "edycja_artykulu.php?mode=date&movie=<?php echo $_GET['movie'] ?>" enctype="multipart/form-data">
					<label for = "udate">Data produkcji:</label>	
					<input type = "date" id = "udate" name = "udate"/>
					<br/>
					<input type="submit" name = "updatedate" value="Zmień datę"/>
				</form>
				<br/>
		<?php
			}
		?>
	</div>
</main>
