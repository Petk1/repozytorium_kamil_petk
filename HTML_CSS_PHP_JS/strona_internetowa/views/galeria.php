<main>
    <div id = "galeria">			
		<?php 	
			include "php_wyswietlanie_danych/galeria.php";			
			for ($i=0; $i<$count; $i++){ 		
		?>
				<img src="<?php echo $filesDir.$files[$i] ?>" alt = "Zdjęcie"  id = "galeria_zdjecia">				
		<?php 	
				if(isset($_SESSION['zalogowany']) AND $_SESSION['user'] == "admin"){
					echo $id_zdjec[$i][0];		
				}
			}	
		?>	
	</div>
		
	<div id = "dodaj_zdjecie">
		<?php
			if(isset($_SESSION['zalogowany']) AND $_SESSION['user'] == "admin"){
				echo "<p>Dodaj zdjęcie do galerii:</p>";
		?>
			<form class = "form" action="dodaj_zdjecie.php?mode=add" method = "POST" enctype="multipart/form-data">
				<table class="fNowy">
					<tr>
						<td><label for = "fPlik">Wybierz zdjęcie</label></td>
						<td><input type="file" id="fPlik" name="fPlik" accept = ".jpg, .png"/><br/></td>
					</tr>
					<tr>
						<td>
							<input id="fSubmit" name="fSubmit" class="submit" type="submit" value="Dodaj zdjęcie"/>
						</td>
					</tr>
				</table>
			</form>
				
			<?php
				if(isset($_SESSION['error_galeria'])){
					echo"<p>".$_SESSION['error_galeria'];
					unset($_SESSION['error_galeria']);
				}else echo "<p></p>";
			?>			
			<br/>	
			<form class = "form" action="dodaj_zdjecie.php?mode=delete" method = "POST">
				<label for="id_file">Podaj id pliku:</label>
				<input type="number" name="id_file" id="id_file"/>
				<input id="fSubmit2" name="fSubmit" class="submit" type="submit" value="Usuń zdjęcie"/>	
			</form>	
				<br/>			
		<?php	
			if(isset($_SESSION['delete_photo'])){
				echo"<p>".$_SESSION['delete_photo'];
				unset($_SESSION['delete_photo']);
			}else echo "<p></p>";
		}		
		?>
    </div>
</main>