<main>
	<div id = "artykuly">
		<h1>Lista artykułów:</h1>
		<br/>
				
	<?php
		include 'php_wyswietlanie_danych/lista_artykulow.php';
		if(isset($_SESSION['zalogowany']) AND $_SESSION['user'] == "admin"){
	?>
			<h2>Dodaj artykuł:</h2>
			<form method = "POST" action = "dodaj_artykul.php?mode=add" enctype="multipart/form-data">
				<table>
					<tr>
						<td>
							<label for = "tytul">Podaj tytuł filmu:</label>
						</td>
						<td>
							<input type = "text" id = "tytul" name = "tytul"/>
						</td>
					</tr>
					<tr>
						<td>
							<label for = "fPlik">Wybierz okładkę:</label>
						</td>
						<td>
							<input type="file" id="fPlik" name="fPlik" accept = ".jpg, .png"/><br/>
						</td>
					</tr>
					<tr>
						<td>
							<label for = "data">Data produkcji:</label>
						</td>
						<td>
							<input type = "date" id = "data" name = "data"/>
						</td>
					</tr>
					<tr>
						<td>
							<label for = "rezyser">Reżyser:</label>
						</td>
						<td>
							<input type = "text" id = "rezyser" name = "rezyser"/>
						</td>
					</tr>
					<tr>
						<td>
							<label for = "budzet">Budżet (w USD):</label>
						</td>
						<td>
							<input type = "text" id = "budzet" name = "budzet"/>
						</td>
					</tr>
					<tr>
						<td>
							<label for = "zysk">Zysk brutto (w USD):</label>
						</td>
						<td>
							<input type = "text" id = "zysk" name = "zysk"/>
						</td>
					</tr>
					<tr>
						<td>
							<label for = "opis">Opis:</label>
						</td>
						<td>
							<textarea id = "opis" name = "opis" rows="4" cols="50"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" value="Dodaj artykuł" name="add_article" onclick="przetwarzaj_dane_artykul(); return false"/>
						</td>
					</tr>
				</table>
			</form>
			<br/>
			<h2>Usuń artykuł:</h2>
			<form method = "POST" action = "dodaj_artykul.php?mode=delete">
				<table>
					<tr>
						<td>
							<label for = "dtytul">Podaj tytuł filmu:</label>
						</td>
						<td>
							<input type = "text" id = "dtytul" name = "dtytul"/>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" value="Usuń artykuł" name="delete_article" onclick="przetwarzaj_dane_artykul_delete(); return false"/>
						</td>
					</tr>
				</table>
			</form>
			<br/>
		<?php
		}
		?>
	</div>
</main>