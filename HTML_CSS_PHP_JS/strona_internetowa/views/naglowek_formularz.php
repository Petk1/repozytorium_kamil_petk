<!DOCTYPE html>
<?php
session_start();
?>
<html lang="pl-PL">
<head>
    <title>MARVEL MOVIES</title>
    <meta charset="utf-8"/>  	
    <link rel="stylesheet" href="style/style2.css" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>	
	<script src="skrypty/sprawdzenie_formularza_rejestracji1.js"></script>
	<script>
	function wykonaj() {
		var x = document.getElementById("gorne_menu");
		if (x.className === "pasek_menu") {
			x.className += " responsive";
		}else {
			x.className = "pasek_menu";
		}
	}
	
	
	var XMLHttpRequestObject = false;

	if(window.XMLHttpRequest){
	  XMLHttpRequestObject = new XMLHttpRequest();
	}
	else if(window.ActiveXObject){
	  XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	}

	function wyslij(wiadomosc){

		if(XMLHttpRequestObject){
			var div = document.getElementById('save');
			
			var mess = document.getElementById(wiadomosc).value;
			
			if(mess != "" && mess.length <= 200){
				var url = "dodaj_komentarz.php?mode=new_answer&movie=" + "<?php echo $_GET['movie']; ?>" + "&wiadomosc=" + mess;
				XMLHttpRequestObject.open("GET", url);
				XMLHttpRequestObject.onreadystatechange = function()
				{
				  if (XMLHttpRequestObject.readyState == 4 && 
					  XMLHttpRequestObject.status == 200){
						div.innerHTML = XMLHttpRequestObject.responseText;	
				  }
						else{
							document.getElementById(wiadomosc).value = '';
							div.innerHTML = "Czekaj...";
					}
				}
			
			XMLHttpRequestObject.send(null);
			}else alert("Popraw komentarz");
		}
	}

	period = 1000; 
	setInterval(pobierz_komentarze, period)
	function pobierz_komentarze(){
		var comments;  

		comments = new XMLHttpRequest();
		comments.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			  document.getElementById("comments").innerHTML = this.responseText;
			}
		  };

		  comments.open("GET", "wyswietl_komentarze.php?movie=" + "<?php echo $_GET['movie']; ?>", true);
		  comments.send();
	}
	</script>

</head>

<body>

	<header>
		<h1>MARVEL</h1>
	
        <div class="pasek_menu" id="gorne_menu">
			<a href="index.php?page=glowna">Home</a>
			<a href="index.php?page=lista_artykulow">Lista filmów</a>
            <a href="index.php?page=logowanie">Logowanie</a>
			<a href="index.php?page=rejestracja">Rejestracja</a>
			<a href="index.php?page=galeria">Galeria</a>
			<a href="index.php?page=about">About</a>
			<a href="javascript:void(0);" class="icon" onClick="wykonaj()">
            <i class="fa fa-bars"></i></a>
        </div>  
	</header>