<!DOCTYPE html>
<?php
session_start();
?>
<html lang="pl-PL">
<head>
    <title>MARVEL MOVIES</title>
    <meta charset="utf-8"/>  	
    <link rel="stylesheet" href="style/style2.css" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
	<script src="skrypty/sprawdzenie_formularza_rejestracji1.js"></script>
	
	<script>
		function wykonaj() {
		  var x = document.getElementById("gorne_menu");
		  if (x.className === "pasek_menu") {
			x.className += " responsive";
		  } else {
			x.className = "pasek_menu";
		  }
		}
	</script>
</head>

<body>
	<header>
		<h1>MARVEL</h1>
	
        <div class="pasek_menu" id="gorne_menu">
			<a href="index.php?page=glowna">Home</a>
			<a href="index.php?page=lista_artykulow">Lista filmów</a>
            <a href="index.php?page=logowanie">Logowanie</a>
			<a href="index.php?page=rejestracja">Rejestracja</a>
			<a href="index.php?page=galeria">Galeria</a>
			<a href="index.php?page=about">About</a>
			<a href="javascript:void(0);" class="icon" onClick="wykonaj()">
            <i class="fa fa-bars"></i></a>
        </div>  
	</header>