<main>
	<?php
		if(!isset($_SESSION['zalogowany']))
		{
	?>
		<div id = "rejestracja">
			Wypełnij poniższy formularz
			<br />
			<em>(Wszystkie pola muszą zostac wypełnione)</em>
			<br /><br />
			<form class = "form" id="form" method="POST" action="rejestracja.php"> 
			<table>
				<tr>
					<td><label for="log">Login:</label></td>
					<td><input type="text" name="login" id="log"/></td>
				</tr>
				<tr>
					<td><label for="pass">Hasło:</label></td>
					<td><input type="password" name="password" id="pass"/></td>
				</tr>
				<tr>
					<td><label for="pass2">Powtórz hasło:</label></td>
					<td><input type="password" name="password2" id="pass2"/></td>
				</tr>
				<tr>
					<td><label for="mail">E-mail:</label></td>
					<td><input type="text" name="email" id="mail"/></td>
				</tr>
				<tr>
					<td><label for="reg">Akceptuję regulamin</label></td>
					<td><input type="checkbox" name="regulamin" id="reg"/>
					<p id="info_regulamin" style="display:none">Zaakceptuj regulamin</p>
					 </td>	
				</tr>
			</table>
				<div class="bledy"></div>
				<input type="submit" value="  Zarejestruj się  " onclick="przetwarzaj_dane(); return false"/>
			</form>
<?php		
		}else{
?>
			<br/><br/>
			<h2><b>Posiadasz już konto. Jesteś zalogowany.</b></h2>
			<br/><br/>
<?php
		}
		if(isset($_SESSION['error_rejestracja']))
		echo"<p>".$_SESSION['error_rejestracja'];
		unset($_SESSION['error_rejestracja']);
?>
	</div>
</main>
