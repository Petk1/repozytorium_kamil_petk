<?php
	class Widok{
		
		public function naglowek(){
			include 'naglowek.php';
		}
		
		public function srodek(){
			include 'srodek.php';
		}
		
		public function lista_artykulow(){
			include 'lista_artykulow.php';
		}
		
		public function artykul(){
			include 'artykul.php';
		}
		
		public function naglowek_formularz(){
			include 'naglowek_formularz.php';
		}
		
		public function panel_logowania(){
			include 'panel_logowania.php';
		}
		
		public function reset_hasla(){
			include 'resetHasla.php';
		}
		
		public function panel_uzytkownika(){
			include 'panel_uzytkownika.php';
		}
		
		public function panel_rejestracji(){
				include 'panel_rejestracji.php';
		}
		
		public function srodek_glowna(){
			include 'srodek_glowna.php';
		}
		
		public function about(){
			include 'about.php';
		}
		
		public function galeria(){
			include 'galeria.php';
		}
		
		public function stopka(){
			include 'stopka.php';
		}		
	}
?>