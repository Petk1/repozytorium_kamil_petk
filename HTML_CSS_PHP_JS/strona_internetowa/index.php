<?php	
	$page = isset($_GET['page']) ? $_GET['page'] : 'glowna';

	include('views/Widok.php');
	$wyglad = new Widok();

	switch($page){
		
		case 'glowna':
			$wyglad->naglowek();
			$wyglad->srodek_glowna();
			break;
		case 'lista_artykulow':
			$wyglad->naglowek();
			$wyglad->lista_artykulow();
			break;
		case 'artykul':
			$wyglad->naglowek_formularz();
			$wyglad->artykul();
			break;
		case 'logowanie':
			$wyglad->naglowek();	
			$wyglad->panel_logowania();
			break;
		case 'rejestracja':
			$wyglad->naglowek();	
			$wyglad->panel_rejestracji();
			break;
		case 'ustawienia':
			$wyglad->naglowek();	
			$wyglad->panel_uzytkownika();
			break;
		case 'reset':
			$wyglad->naglowek();	
			$wyglad->reset_hasla();
			break;
		case 'galeria':
			$wyglad->naglowek();
			$wyglad->galeria();
			break;
		case 'about':
			$wyglad->naglowek();
			$wyglad->about();			
			break;

		default:
		
			die("Taka strona nie istnieje");
			
	}
	$wyglad->stopka();

?>