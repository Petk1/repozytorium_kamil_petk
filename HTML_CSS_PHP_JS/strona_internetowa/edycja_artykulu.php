<?php
session_start();
	if(!isset($_SESSION['zalogowany']) AND $_SESSION['user'] != "admin"){
		header('Location: index.php?page=glowna');
		exit();	
	}
	require_once "polaczenie_z_baza.php";

	$connect = new mysqli($host, $db_user, "", $db_name);	
	if($connect->connect_errno)
	{
			echo "Error:".$connect->connect_errno;
	}
	else
	{	
		if($_GET['mode'] == "add"){
	
			$stmt = $connect->prepare("INSERT INTO aktorzy(imie, nazwisko, film, postac) VALUES (?,?,?,?)");
			
			$stmt->bind_param("ssss", $_POST['imie'], $_POST['nazwisko'], $_GET['movie'], $_POST['postac']); 
			$stmt->execute();	
			$stmt->close();	
		}
		if($_GET['mode'] == "delete"){
			$stmt = $connect->prepare("DELETE FROM aktorzy WHERE imie = ? AND nazwisko = ? AND postac = ?");
			
			$stmt->bind_param("sss", $_POST['dimie'], $_POST['dnazwisko'], $_POST['dpostac']); 
			$stmt->execute();	
			$stmt->close();	
		}
		if($_GET['mode'] == "cover"){
			unlink('okladki/'.$_GET['movie'].".jpg");
			$fileName = $_FILES['fPlik']['tmp_name'];
			move_uploaded_file($fileName, 'okladki/'.$_GET['movie'].".jpg");
		}
		if($_GET['mode'] == "description"){
			$stmt = $connect->prepare("UPDATE filmy SET opis = ? WHERE tytul = ?");
			
			$stmt->bind_param("ss", $_POST['uopis'], $_GET['movie']); 
			$stmt->execute();	
			$stmt->close();	
		}
		if($_GET['mode'] == "director"){
			$stmt = $connect->prepare("UPDATE filmy SET rezyser = ? WHERE tytul = ?");
			
			$stmt->bind_param("ss", $_POST['udirector'], $_GET['movie']); 
			$stmt->execute();	
			$stmt->close();	
		}
		if($_GET['mode'] == "date"){
			$stmt = $connect->prepare("UPDATE filmy SET data_produkcji = ? WHERE tytul = ?");
			
			$stmt->bind_param("ss", $_POST['udate'], $_GET['movie']); 
			$stmt->execute();	
			$stmt->close();	
		}
		$connect->close();
		header('Location: index.php?page=artykul&movie='.$_GET['movie']);
	}
?>