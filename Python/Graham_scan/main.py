import matplotlib.pyplot as plt  # pyplot umozliwa tworzenie wykresow
from random import randint  # do wylosowania punktow
from math import atan2  # do obliczenia katow

def utworzenie_punktow(ilosc_pkt, min=0, max=500):
    return [[randint(min, max), randint(min, max)] for _ in range(ilosc_pkt)]  # ZWRACA LOSOWE LICZBY W LISCIE

# print(utworzenie_punktow(5)) #TEST CZY WYSWIETLA PKT POPRAWNIE

#WYKRES
def wykres(wspolrzedne, otoczka_wypukla=0):
    # print(wspolrzedne) #SPRAWDZENIE ZAWARTOSCI LISTY
    xs, ys = zip(*wspolrzedne)  # PRZYPISUJE WSPOLRZEDNE POBIERAJAC DANE Z LISTY ZA POMOCA zip() która zwraca liste krotek po kolei; krotka - niezmienna lista
    plt.scatter(xs, ys)  # WYSWIETLENIE PUNKTOW GRAFICZNIE NA WYKRESIE

    if otoczka_wypukla != None:
        for i in range(1, len(otoczka_wypukla) + 1):
            if i == len(otoczka_wypukla): i = 0
            c0 = otoczka_wypukla[i - 1]
            c1 = otoczka_wypukla[i]
            plt.plot((c0[0], c1[0]), (c0[1], c1[1]), 'r')

    plt.show()

#wykres(utworzenie_punktow(10))  # TEST WYSWIETLANIA PKT GRAFICZNIE

#wykorzystywane kiedy kat jest jednakowy, wtedy sprawdzana jest odl i na tej podstwie nastepuje sortowanie
def odleglosc(p0,p1=None):
   if p1==None: p1=anchor
   y_span=p0[1]-p1[1]
   x_span=p0[0]-p1[0]
   return y_span**2 + x_span**2

def kat(p0, p1=0):  # zwraca kat w radianach
    if p1 == 0: p1 = anchor  # anchor - zmienna globalna wiec mozna ja wykorzystac zarowno wewnatrz jak i na zewnatrz funkcji
    y_span = p0[1] - p1[1]  # obliczenie odleglosci w kierunku y
    x_span = p0[0] - p1[0]  # obliczenie odleglosci w kierunku x
    return atan2(y_span, x_span)  # atan2 pochodzace z math zwraca atan(y/x). arctg poniewaz chcemy uzyskac wartosc w radianach

def det(p1, p2, p3):  # wyznacznik macierzy
    return p1[0] * p2[1] + p1[1] * p3[0] + p2[0] * p3[1] - (p2[1] * p3[0] + p1[0] * p3[1] + p1[1] * p2[0])

def quicksort(a):
    #print(len((a)))

    if len(a) <= 1: return a # jesli tylko jeden pkt badz mniej to zwroci ten pkt

    mniejszy, rowny, wiekszy = [], [], []

    piv_kat = kat(a[randint(0, len(a) - 1)])  # wybranie losowego pkt i obliczenie kata
    #print(piv_ang)
    for i in a: #i - kolejne pkt z a
        #print(i)
        #print(piv_ang)
        pkt_kat = kat(i)  # licze kat dla danego pkt
        if pkt_kat < piv_kat:
            mniejszy.append(i)
        elif pkt_kat == piv_kat:
            rowny.append(i)
        else:
            wiekszy.append(i)
    return quicksort(mniejszy) + sorted(rowny, key=odleglosc) + quicksort(wiekszy) #wazny parametr key, ktory okresla jaka funkcja ma byc wywolana na kazdym elemencie listy zanim zostanie dokonane porownanie

def graham_scan(punkty):
    global anchor  #zmienna globalna ktora posluzy do ustawienia pkt (x,y) z najmniejsza wartoscia y

 # szukanie pkt o najmniejszej wartosci y; w przypadku znalezienia wiecej niz 1 pkt wybierany jest
 # ten o najmniejszej wartosci x
    min_idx = 0 #poczatkowo ustawiany na 0 a w czasie wykonywania petli bedzie odpowiednio zmieniana jego wartosc
    for i, (x, y) in enumerate(punkty): # wykorzystanie petli wraz z numeracja
        if min_idx == 0 or y < punkty[min_idx][1]: # point[min_idx][1] - pkt o w wspolrzednych (0,y); jesli zostanie spelniony warunek to min_idx = i czyli odpowiedni pkt z wylosowanego na poczatku zbioru pkt
            min_idx = i
            # print(min_idx)
        if y == punkty[min_idx][1] and x < punkty[min_idx][0]: # wykonywane gdy pojawi sie wiecej pkt o takim samym min y; analogicznie po spelnieniu warunku min_idx = i czyli odpowiedni pkt z wylosowanego na poczatku zbioru pkt
            min_idx = i
           # print(min_idx)
        ##else:  ## TESTY
          ##  min_idx = i
          ##  print(min_idx)


    anchor = punkty[min_idx] # wykorzystujac znaleziony min_idx ustawiam zmienna globalną -> PKT POCZATKOWY
    #print(anchor)
    #print(punkty)
    #print(len(punkty)) ## TESTOWE SPRAWDZENIE
    posortowane_pkt = quicksort(punkty) # sortowanie pkt pod wzgledem kata wykorzystujac quicksort
    #print(posortowane_pkt)
    del posortowane_pkt[posortowane_pkt.index(anchor)] #usuniecie anchor z listy punktow
    #print(posortowane_pkt)

    otoczka = [anchor, posortowane_pkt[0]] # ustawienie jako otoczke dwoch pierwszych punktow
    #print(otoczka)

    for s in posortowane_pkt[1:]: #petla przegladajaca kolejne elementy listy punktow
        #print(s)
        #print(otoczka)
        #print(otoczka[-2] , otoczka[-1])
        #print(det(otoczka[-2], otoczka[-1], s))
        while det(otoczka[-2], otoczka[-1], s) <= 0: #dopoki wyznacznik macierzy <=0 usun ostatni element listy otoczka
            #print(otoczka[-1])
            del otoczka[-1]
        #print(s)
        otoczka.append(s) # w innym wypadku dodaj s czyli kolejny element listy punktow dla ktorego wyznacznik >1 do listy otoczka
    #print(otoczka)
    return otoczka # zwroc pkt otoczki wypuklej

pkt = utworzenie_punktow(12)
print("Współrzędne punktów:", pkt)
otoczka = graham_scan(pkt)
print("Współrzędne punktów otoczki:", otoczka)
wykres(pkt, otoczka)