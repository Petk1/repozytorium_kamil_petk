class Event{
    constructor(eventName, numberDay, monthName){
        this.eventName = eventName;
        this.numberDay = numberDay;
        this.monthName = monthName;
        this.description = "Dodaj opis";
    }

    returnDate(){
        return `${this.numberDay} ${this.monthName}`;
    }

    uptadeDescription(description){
        if(description !== ""){
            this.description = description;
            //zamkniecie opcji edycji i wyswietlenie nowego podanego opisu
            const placeForDescription = document.querySelector('.descriptionEvent'); 
            placeForDescription.innerHTML = "";

            const descriptionEventSpan = document.createElement('span');
            descriptionEventSpan.textContent = `Opis: ${this.description}`; //zaktualizowanie opisu   
            placeForDescription.appendChild(descriptionEventSpan);       
        }else alert("Podaj opis");        
    }

    newEvent(id){
        const newEvent = document.createElement('li');
        newEvent.innerHTML = `<p data-id = ${id}>${this.eventName}</p> <button>Usuń</button> <button class="showDescription">Opis</button>`;
        return newEvent;
    }

    returnAllDescription(numberDay, number, monthName, id){
        document.querySelector('.numberDay span').innerHTML = `Data: <p>${numberDay}</p> <p>${monthName}</p>`; 
        document.querySelector('.nameEvent span').innerHTML = `Nazwa wydarzenia: <p>${this.eventName}</p>`; 
        document.querySelector('.nrEvent span').innerHTML = `Nr wydarzenia: <p data-id = ${id}>${number}</p>`;
        document.querySelector('.descriptionEvent span').textContent = this.description;
    }
    
    removeAllDescription(){
        document.querySelector('.numberDay span').innerHTML = "";
        document.querySelector('.nameEvent span').innerHTML = "";
        document.querySelector('.nrEvent span').innerHTML = "";
        document.querySelector('.descriptionEvent').innerHTML = "";
        const descriptionEventSpan = document.createElement('span');
        document.querySelector('.descriptionEvent').appendChild(descriptionEventSpan);
    }

    returnDescription(){
        return this.description;
    }
}