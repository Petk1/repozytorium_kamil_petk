class Render{
    constructor(whichYear){
        this.whichYear = whichYear;
        this.daysInMonth = []; 
        this.savedEvents = []; //zapisane wydarzenia

        for(let i = 0; i < 12; i++){ //zapianie ile dni ma kazdy miesiac
            this.daysInMonth.push(this.checkHowManyDaysMonth(i));
        }

        this.months = []; // data kolejnych miesiecy z ich pierwszymi dniami
        this.numberMonth = 0;
        
        let firstDay = new Date();
        for(let i = 0; i < 12; i++){
            firstDay = new Date();
            firstDay.setFullYear(this.whichYear,i,1);
            this.months.push(firstDay);
        }
        this.monthsName = []; //nazwy kolejnych miesiecy
        for(let i = 0; i < 12; i++){
            this.monthsName.push(this.months[i].toLocaleString('default', {month: 'long'}));
        }
        this.year = new Year(this.daysInMonth, this.whichYear, this.monthsName); //utworzenie 12 miesiecy wraz z odpowiednia iloscia dni

        const btnPreviousMonth = document.querySelector('.previousMonth');
        btnPreviousMonth.addEventListener('click', this.showMonth.bind(this));
        btnPreviousMonth.myParam = "-";

        const btnNextMonth = document.querySelector('.nextMonth');
        btnNextMonth.addEventListener('click', this.showMonth.bind(this));
        btnNextMonth.myParam = "+";

        window.addEventListener('keydown', this.showMonth.bind(this));
        this.addElementsOnPage(this.daysInMonth[0]);    
        document.querySelector('button.addEvent').addEventListener('click', this.addEvent.bind(this));

        this.editing = false; //przekazywane do kontroli czy aktualnie obecna jest juz edycja opisu wydarzenia aby nie moc 
        //drugi raz aktywowac metody addDescription do momentu zapisania edycji
        const addDescriptionClickOnText = document.querySelector('.descriptionEvent'); //umozliwia edycje po kliknieciu na obecny opis
        addDescriptionClickOnText.addEventListener('click', this.addDescription.bind(this));

        const addDescriptionBtn = document.querySelector('.addDescription button#add'); 
        addDescriptionBtn.addEventListener('click', this.addDescription.bind(this));

        const saveDescriptionBtn = document.querySelector('.addDescription button#save');
        saveDescriptionBtn.addEventListener('click', this.saveDescription.bind(this));

        //Zapisanie na wydarzenie
        const signUpBtn = document.querySelector('.addDescription button#signUp');
        signUpBtn.addEventListener('click', this.signUp.bind(this));
    }
    
    addElementsOnPage(howManyDays){
        //dodanie do listy rozwijanej odpowiedniej dla danego miesiaca ilosci dni
        const select = document.querySelector('select#day');
        const options = select.querySelectorAll('option');
        options.forEach( option => option.remove());
        for(let i = 1; i <= howManyDays; i++){
            const option = document.createElement("option");
            option.text = i;
            select.add(option)
        }
    }

    showEvents(numberMonth){ //Wyswietlanie zapisanych wydarzen w danym dniu
        const months = this.year.returnMonths();     
        const month = months[numberMonth];
        const days = month.showDays();
        days.forEach(day => day.uptadeListEvents());
    }

    checkHowManyDaysMonth(numberMonth){
        return new Date(this.whichYear, ++numberMonth, 0).getDate(); //preinkrementuje numberMonth poniewaz potrzebuje numeracji od 1 do 12 
    }

    showMonth(e){
        const nameMonth = document.querySelector('[data-month = "month"]');
        
        if(e.keyCode === 37 || e.keyCode === 39 || e.currentTarget.myParam === "-" || e.currentTarget.myParam === "+"){    
            if(document.querySelector('textarea#textDescription') !== null){ //sprawdzenie czy obecnie nie jest aktualna edycja opisu wydarzenia
                alert('Najpierw dokończ edycję opisu wydarzenia')
            }else{
                e.keyCode === 37 || e.currentTarget.myParam === "-" ? this.numberMonth-- : this.numberMonth++;
    
                //wyczyszczenie wyswietlania mozliwego otwartego opisu jakiegos wydarzenia z poprzedniego miesiaca
                document.querySelector('.numberDay span').textContent = "";
                document.querySelector('.nameEvent span').textContent = "";
                document.querySelector('.nrEvent span').textContent = "";
                document.querySelector('.descriptionEvent span').textContent = "";

                if(this.numberMonth === 12) this.numberMonth = 0;
                else if(this.numberMonth === -1) this.numberMonth = 11;

                nameMonth.textContent = this.months[this.numberMonth].toLocaleString('default', {month: 'long'});
                const date = this.months[this.numberMonth];
        
                this.addElementsOnPage(this.daysInMonth[this.numberMonth]);
                console.log(this.daysInMonth[this.numberMonth])
                this.year.displayManagement(date, this.daysInMonth[this.numberMonth]);
                this.showEvents(date.getMonth());
            }
        }else console.log('Przełączanie <- lub ->')
    }

    addEvent(){
        const months = this.year.returnMonths();
        const month = months[this.numberMonth];
        const days = month.showDays();
        const nameEvent = document.querySelector('input.nameEvent').value;
        if(nameEvent.length === 0){
            alert("Podaj nazwę wydarzenia")
        }else if(nameEvent.length > 0 && nameEvent.length <= 20){
            days[document.querySelector('select#day').value-1].createEvent(nameEvent);
            document.querySelector('input.nameEvent').value = "";
        }else alert("Przekroczyłeś liczbę znaków dla nazwy wydarzenia (max to 20)")
    }

    addDescription(e){ //uruchamia mozliwosc edycji opisu wydarzenia
        if(this.editing === false){ //jesli nie jest obecnie prowadzona zadna edycja wydarzenia zezwol na edycje
            const placeForDescription = document.querySelector('.descriptionEvent');

            const months = this.year.returnMonths();
            const month = months[this.numberMonth];
            const days = month.showDays();

            //pobranie potrzebnych danych do dodania nowego opisu - nr dnia, nr wydarzenia, tekst opisu
            const numberDay = document.querySelector('.numberDay span p');
            let currentDescription = ""; 
            if(numberDay === null){
                alert('Najpierw wybierz wydarzenie');
            }else {
                let numberEvent = document.querySelector('.nrEvent span p').textContent;
                numberEvent-- //z uwagi na zwiekszony o 1 wyswietlany nr wydarzenia -- aby powrocic do numeracji od 0
                console.log(numberEvent);
                placeForDescription.innerHTML = "";
                const areaForDescription = document.createElement("textarea");
                areaForDescription.id = "textDescription";
                areaForDescription.rows = 9;
                areaForDescription.cols = 138;
                areaForDescription.autofocus;
                console.log(numberEvent);
                currentDescription = days[numberDay.textContent-1].returnDescription(numberEvent); //zwrocenie obecnego opisu
                areaForDescription.value = currentDescription;          
                this.editing = true; //informacja ze prowadzona jest edycja, zmieni się na false kiedy zapisane zostana zmiany
                placeForDescription.appendChild(areaForDescription);
            }
        }
    }

    saveDescription(){ //zapisanie nowego opisu wydarzenia
        if(document.querySelector('textarea#textDescription') !== null){ //sprawdzenie czy aktywna jest edycja opisu wydarzenia
            const months = this.year.returnMonths();
            const month = months[this.numberMonth];
            const days = month.showDays();

            //pobranie potrzebnych danych do dodania nowego opisu - nr dnia, nr wydarzenia, tekst opisu
            const numberDay = document.querySelector('.numberDay span p');
            if(numberDay === null){
                alert('Najpierw wybierz wydarzenie');
            }else {
                let numberEvent = document.querySelector('.nrEvent span p').textContent;
                numberEvent-- //z uwagi na zwiekszony o 1 wyswietlany nr wydarzenia -- aby powrocic do numeracji od 0
                const text = document.querySelector('textarea#textDescription').value;
                if(text.length > 500){
                    alert(`Maksymalna długość opisu to 500 znaków. Podany przez Ciebie opis zawiera ${text.length} znaków.`);
                }else{
                    days[numberDay.textContent-1].createDescription(numberEvent, text); //dodanie nowego opisu  
                    this.editing = false; //informacja o zakonczeniu edycji opisu wydarzenia
                }         
            }
        }
    }

    showDescriptionSavedEvent(e){
        const li = e.target.parentNode.parentNode; //w celu odczytania daty tego wydarzenia, li - element na stronie zawierajacy element p w ktorym znajduje sie data
        const data = li.querySelectorAll('p'); //odczytanie daty, p - element na stronie zawierajacy date
        let numberEvent = li.querySelector('span').dataset.numberevent;
        numberEvent--;
        let numberDay= data[0].textContent
        let nameMonth = data[1].textContent;
        numberDay--; //numeracj na stronie od 1, w tablicy indeksy od 0 dlatego dekrementacja
        const numberMonth = this.monthsName.findIndex(month => month === nameMonth);
 
        const months = this.year.returnMonths();
        const month = months[numberMonth];
        const days = month.showDays();
        const description = days[numberDay].returnDescription(numberEvent);
        document.querySelector('div.descriptionSavedEvent').textContent = description;
    }
    
    uptadeListEventsSaved(){ //metoda aktualizująca obecnie wyświetlane wydarzenia
        const savedEvemts = document.querySelector(`.listEvent ul`); 

        savedEvemts.textContent = ""; 
        this.savedEvents.forEach( (event, key) => {
            event.dataset.key = key;
            savedEvemts.appendChild(event); 
        })
    }

    canceledEvent(numberEvent){ //usuniecie zapisanego wydarzenia w przypadku gdy usunieto wydarzenie z kalendarza
        console.log(numberEvent);
        this.savedEvents.splice(numberEvent, 1);
        document.querySelector('div.descriptionSavedEvent').textContent = "";
        this.uptadeListEventsSaved();
    }

    removeSavedEvent(e){
        let numberEvent = document.querySelector(`.listEvent ul`).dataset.key;
        document.querySelector('div.descriptionSavedEvent').textContent = "";
        this.savedEvents.splice(numberEvent, 1);
        this.uptadeListEventsSaved();
    }

    signUp(){
        const data = document.querySelectorAll('.numberDay p'); 
        const nameEvent = document.querySelector('.nameEvent p').textContent; 
        let numberEvent = document.querySelector('.nrEvent p').textContent; 
        console.log(numberEvent)
        let idEvent = document.querySelector('.nrEvent p').dataset.id; //id potrzebne w razie gdyby usunieto wydarzenie z kalendarza - potrzebny jeden staly identyfikator
   
        //sprawdzenie czy juz nie zapisano sie na to wydarzenie
        const dataWithNumberEvent = data[0].textContent + " " + data [1].textContent + " " + idEvent;
        const savedEventsInfo = this.returnSavedEvents();
        const nrIndexEvent = savedEventsInfo.findIndex( savedEvent => savedEvent === dataWithNumberEvent)
        if(nrIndexEvent === -1){
            const savedEvents = document.querySelector('.listEvent ul');
            const newSavedEvent = document.createElement('li');
            newSavedEvent.innerHTML = `<p>${data[0].textContent}</p> <p>${data[1].textContent}</p> <span data-numberevent = "${numberEvent}"  data-id = "${idEvent}">${nameEvent} <button id = "showDescriptionSavedEvent">Opis</button> <button id = "resignation">Zrezygnuj</button></span>`;
                                                                                                                                        // data-numbereventcalendar - w razie gdy z kalendarza zostanie usuniete wydarzenie nalezy tu takze zmienic numeracje
            this.savedEvents.push(newSavedEvent); //dodanie do listy zapisanego wydarzenia
            this.uptadeListEventsSaved(); //zaktualizowanie wyswietlanej listy wydarzen
            
            newSavedEvent.querySelector('button#showDescriptionSavedEvent').addEventListener('click', this.showDescriptionSavedEvent.bind(this));
            newSavedEvent.querySelector('button#resignation').addEventListener('click', this.removeSavedEvent.bind(this));
        }else alert("Zapisałeś się już na to wydarzenie")
    }

    returnSavedEvents(){
        let savedEventsInfo = []; //odczytanie informacji o datach i nr wydarzen
        let dataWithNumberEvent = ''; // do przechowania daty i nr wydarzenia
        for(const date of this.savedEvents){
            dataWithNumberEvent = date.querySelectorAll('p')[0].textContent + " " + date.querySelectorAll('p')[1].textContent + " " + date.querySelector('span').dataset.id;
            savedEventsInfo.push(dataWithNumberEvent);
         }
        return savedEventsInfo;
    }
}