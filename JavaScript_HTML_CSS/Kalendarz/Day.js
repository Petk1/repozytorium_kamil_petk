class Day{
    constructor(numberDay, monthName){
        this.events = [];
        this.objectEvents = [];
        this.numberDay = numberDay;
        this.monthName = monthName;
        this.nrIdEvent = 0; //do numeracji kolejnych wydarzen danego dnia
    }

    uptadeListEvents(){ //metoda aktualizująca obecnie wyświetlane wydarzenia
        const boxDay = document.querySelector(`[data-day="${this.numberDay}"] ul`); //-1 bo dni podaje od 1 a indeksy sa ponumerowane od 0

        boxDay.textContent = ""; 
        this.events.forEach( (event, key) => {
            event.dataset.key = key;
            boxDay.appendChild(event); 
        })
    }

    removeEvent(e){
        e.preventDefault();
        if(document.querySelector('textarea#textDescription') === null){ //sprawdzenie czy nie jest otwarta edycja opisu wydarzenia
            const indexElement = e.target.parentNode.dataset.key;
            const numberDay = e.target.parentNode.parentNode.parentNode.dataset.day; //pobranie numeru dnia
            let numberEventOnPage = indexElement;
            numberEventOnPage++; //poniewaz na stronie nr wydarzen wyswietlane są od 1 a indexElement liczone jest od 0

            //sprawdzenie czy aktualnie wyswietlany jest opis tego wydarzenia, jeśli tak to wyczysc te pola
            let numberEventInDescription = null;
            let numberDayInDescription = null;
            if(document.querySelector('.nrEvent p') !== null){
                numberEventInDescription = document.querySelector('.nrEvent p').textContent;
                numberDayInDescription = document.querySelector('.numberDay p').textContent;
            }

            //sprawdzenie czy zapisano sie na to wydarzenie, jesli tak usun je z listy zapisanych
            const savedEventsImfo = newYear.returnSavedEvents(); //pobranie dat i nr wydarzen na ktore sie zapisano
            console.log(e.target.parentNode.querySelector('p'))
            const infoRemoveEvent = this.objectEvents[indexElement].returnDate() + " " + e.target.parentNode.querySelector('p').dataset.id//pobieram date usuwanego wydarzenia i nr tego wydarzenia
            const nrIndexEvent = savedEventsImfo.findIndex( savedEvent => savedEvent === infoRemoveEvent) //sprawdzenie czy zapisano sie na to wydarzenie
            console.log(savedEventsImfo, infoRemoveEvent)
            if(nrIndexEvent !== -1){ //jesli zapisano sie na usuwane wydarzenie
                    newYear.canceledEvent(nrIndexEvent);
            }

            if(numberEventInDescription !== null){
                if(numberEventInDescription == numberEventOnPage &&
                    numberDayInDescription === numberDay)
                    {       
                        this.objectEvents[indexElement].removeAllDescription();
                        this.objectEvents.splice(indexElement, 1);
                    }
            }
            this.events.splice(indexElement, 1);
            this.uptadeListEvents(); //po usunieciu elementu nalezy zaktualizowac wyswietlane dane
        }else alert("Przed usunięciem musisz zakończyć edycję opisu wydarzenia. Zapisz edycję i ponów próbę usunięcia.");
    }

    showDescription(e){
        e.preventDefault();
        //sprawdzenie czy nie jest otwarta aktualnie edycja opisu jakiegos wydarzenia
        if(document.querySelector('textarea#textDescription') !== null){
            alert('Najpierw dokończ edycję opisu wydarzenia')
        }else{
            const indexElement = e.target.parentNode.dataset.key;
            const nameEvent = e.target.parentNode.querySelector('p').textContent; //pobieram nazwe wydarzenia
            const id = e.target.parentNode.querySelector('p').dataset.id; //pobieram id ogloszenia
            //wyswietlam informacje o tym wydarzenie w oknie gdzie moge dodac opis
            let number = indexElement;
            this.objectEvents[indexElement].returnAllDescription(this.numberDay,  ++number, this.monthName, id);
        }
    }

    createEvent(eventName){
        const createEvent = new Event(eventName, this.numberDay, this.monthName);
        console.log(this.numberDay, this.monthName)
        this.objectEvents.push(createEvent);

        const newEvent = createEvent.newEvent(this.nrIdEvent); //utworzenie wydarzenia wraz z nr id
        this.nrIdEvent++;
        console.log(createEvent.returnDescription());
        this.events.push(newEvent);
        this.uptadeListEvents();

        newEvent.querySelector('button').addEventListener('click', this.removeEvent.bind(this));
        newEvent.querySelector('button.showDescription').addEventListener('click', this.showDescription.bind(this));
    }

    returnDescription(numberEvent){
        return this.objectEvents[numberEvent].returnDescription();
    }

    createDescription(numberEvent, textDescription){
        this.objectEvents[numberEvent].uptadeDescription(textDescription);
    }
}