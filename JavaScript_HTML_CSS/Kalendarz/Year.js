class Year{
    constructor(daysInMonth, year, monthsName){
        this.monthsName = monthsName;
        this.months = []; //indexy odpowiadaja miesiaca 0-styczen, 1-luty...,11-grudzien
        this.daysInMonth = daysInMonth;
        this.year = year;
        this.fistData = new Date(`January 1, ${year}`)//data pierwszego miesiaca ktory zostanie wyswietlony jako pierwszy
        this.numberDay = 1;
        this.displayManagement();
        this.createMonths();    
    }
    //utworzenie 12 miesięcy
    createMonths(){
        for(let i = 0; i < 12; i++){
            this.months.push(new Month(this.daysInMonth[i], this.monthsName[i])); //0 - january, 1 - february..., 11-december
        }
    }

    returnMonths(){
        return this.months;
    }

    showDays(numnberDay, firstDay = 0, daysInMonth = 31){ //Wyswietlanie okienek dni na stronie   
        const calendar = document.querySelector('.calendar');
        const newDay = document.createElement('div');
        newDay.className = 'day';    

        if(firstDay > 0){ //Dopelnienie wyswietlenia pierwszego tygodnia koncowymi dniami z poprzedniego miesiaca
            newDay.dataset.day= 'previousMonth';      
            newDay.innerHTML = `<span>${daysInMonth-numnberDay+1}</span>`;
            newDay.style.backgroundColor = "gray";
            newDay.style.color = "black";
            newDay.querySelector('span').style.backgroundColor = "#F0FFFF";
        }else{
            newDay.dataset.day= numnberDay;
            newDay.innerHTML = `<span>${numnberDay}</span><ul></ul>`;
        }

        //zaznaczenie weekendow innym kolorem
        if(this.numberDay === 6 || this.numberDay === 7){
            newDay.querySelector('span').style.backgroundColor = "red";
            if(this.numberDay === 7)
                this.numberDay = 0;
        }
        this.numberDay++;

        calendar.appendChild(newDay);
    }

    daysInPreviousMonth(whichMonth){ //Zwrocenie liczby dni w poprzednim miesiacu
        if(whichMonth === 0) whichMonth = 11
        else whichMonth--;
        return this.daysInMonth[whichMonth];
    }

    displayManagement(whichMonth = this.fistData, days = this.daysInMonth[this.fistData.getMonth()]){ //Zarzadzanie wyswietlaniem na stronie w zaleznosci od wybranego miesiaca i ilosci dni jakie zawiera ten miesiac
        let firstDay = whichMonth.getDay()-1; //ktorego dnia zaczyna sie miesiac
        
        if(firstDay === -1) firstDay = 6; //przypadek w ktorym pierwszy dzien miesiaca to niedziela (oibuerajac getDay() niedziela to 0)
  
        let daysInPreviousMonth = 0;
        if(firstDay > 0){
            daysInPreviousMonth = this.daysInPreviousMonth(whichMonth.getMonth())
        }

        //czyszczenie
        const elements = document.querySelectorAll('div.day');
        elements.forEach(element => element.remove())
        this.numberDay = 1; //ustawienia na 1 - pon. dzieki czemu po przelaczeniu na inny miesiac mozna odliczac kolejne dni w celu zaznaczenia weekendow innym kolorem

        //Wyswietlanie tresci na stronie
        for(let i = firstDay; i > 0 ; i--){ //-1 poniewaz 0-niedziela, a chce liczyc od poniedzialku
            this.showDays(i, firstDay, daysInPreviousMonth);
        }
        for(let i = 1; i <= days; i++){ // bede musial podac jeszcze ile dni ma ten miesiac
            this.showDays(i);
        }
    }
}