class Month{
    constructor(daysInMonth, monthName) {
        this.days = []; //tablica przechowujaca wszystkie dni danego miesiaca

        //Tworzenie obiektow wszystkich dni danego miesiaca
        this.createDaysObject(daysInMonth, monthName);
    }

    createDaysObject(daysInMonth, monthName){    
        for(let i = 1; i <= daysInMonth; i++){ 
            this.days.push(new Day(i, monthName));
        }
    }

    showDays(){
        return this.days;
    }
}