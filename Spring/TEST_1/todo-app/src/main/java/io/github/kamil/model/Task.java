package io.github.kamil.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.lang.NonNull;

@Entity
@Table(name = "tasks")
public class Task {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotBlank(message = "Task's description must be not null and not be empty")
	private String description;
	private boolean done;
	
	Task(){
		
	}
	
	public int getId() {
		return id;
	}
	void setId(final int id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	void setDescription(final String description) {
		this.description = description;
	}
	
	public boolean isDone() {
		return done;
	}
	void setDone(final boolean done) {
		this.done = done;
	}
}
