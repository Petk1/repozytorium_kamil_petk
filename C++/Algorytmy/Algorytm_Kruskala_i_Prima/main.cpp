#include <iostream>
#include <map>
#include <vector>
#include <algorithm> //w celu uzycia std::sort
#include <ctime>
#include <cstdio>

using namespace std;

typedef  pair<int, int> wierzcholki_krawedzi;

class Graf{
    public:
        void dodaj_wierzcholki(int ilosc_wierzcholkow);

        Graf(int ilosc_wierzcholkow){
            this->ilosc_wierzcholkow = ilosc_wierzcholkow;
            dodaj_wierzcholki(ilosc_wierzcholkow);
        }

        void dodaj_krawedz_z_jej_waga(int waga, int wierzochlekA, int wierzcholekB);

        void algorytm_Kruskala();
        void algorytm_Prima(int poczatkowy_wierzcholek);
        int sprawdz(int i);
        void sprawdz_Prim();
        void posortuj_po_wagach();
        void wyswietl_graf(char ktory_graf_wyswietlic);
        int zlicz_koszt(char ktory_graf);

    private:
        vector<pair<int, wierzcholki_krawedzi>> K; //z tego startuje
        vector<pair<int, wierzcholki_krawedzi>> L; //tutaj chce uzyskac minimalne drzewo rozpinajace dla A.Kruskala
        vector<pair<int, wierzcholki_krawedzi>> P; //tutaj chce uzyskac minimalne drzewo rozpinajace dla A.Prima
        vector<int> wierzcholki; //tutaj przechowuje wszystkie wierzcholki (0,1,2...)
        vector<int> wierzcholki_dodane;
        int ilosc_wierzcholkow;
        int nr_kolejnego_wierzcholka;
        bool czy_wierzcholek_A_juz_dodano = false;
        bool czy_wierzcholek_B_juz_dodano = false;
        int koszt_calego_drzewa = 0;
};

void Graf::dodaj_wierzcholki(int ilosc_wierzcholkow){

    for(int i = 0; i < ilosc_wierzcholkow; i++)
        wierzcholki.push_back(i);
}

void Graf::dodaj_krawedz_z_jej_waga(int waga, int wierzcholekA, int wierzcholekB){
    K.push_back( {waga, {wierzcholekA, wierzcholekB} } );

}

void Graf::posortuj_po_wagach(){

    sort(K.begin(), K.end() );

}

int Graf::zlicz_koszt(char ktory_graf){
    if(ktory_graf == 'P'){
    for(vector<pair<int, wierzcholki_krawedzi>>::iterator it=P.begin(); it!=P.end(); ++it)
         koszt_calego_drzewa += it->first ;
    }
    if(ktory_graf == 'L'){
    for(vector<pair<int, wierzcholki_krawedzi>>::iterator it=L.begin(); it!=L.end(); ++it)
         koszt_calego_drzewa += it->first ;
    }
    return koszt_calego_drzewa;
}

int Graf::sprawdz(int ktory_wierzcholek){
    if(ktory_wierzcholek == wierzcholki[ktory_wierzcholek])
        return ktory_wierzcholek;
    else
        return sprawdz(wierzcholki[ktory_wierzcholek]);
}

void Graf::algorytm_Kruskala(){
    for(int i = 0; i < K.size(); i++)
    {
       if( sprawdz(K[i].second.first) != sprawdz(K[i].second.second) ) //sprawdzenie czy krawdedz laczy dwa rozne drzewa
       {
           L.push_back(K[i]);
           wierzcholki[sprawdz(K[i].second.first)] = wierzcholki[sprawdz(K[i].second.second)];
       }
    }
}

void Graf::sprawdz_Prim(){
    czy_wierzcholek_A_juz_dodano = false; //jesli oba wierzcholki krawedzi znajduja sie juz w wierzcholki_dodane to nie tworz tego polaczenia
    czy_wierzcholek_B_juz_dodano = false;

    for(int j = 0; j < wierzcholki_dodane.size(); j++)
    {
        if(wierzcholki_dodane[j] == L[0].second.first)
        {
            nr_kolejnego_wierzcholka = L[0].second.second;
            czy_wierzcholek_A_juz_dodano = true;
        }
        if(wierzcholki_dodane[j] == L[0].second.second)
        {
            nr_kolejnego_wierzcholka = L[0].second.first;
            czy_wierzcholek_B_juz_dodano = true;
        }
    }

    if(czy_wierzcholek_A_juz_dodano == true && czy_wierzcholek_B_juz_dodano == true)
    {
        auto it = L.begin();
        L.erase(it); //usunie poniewaz dodanie tego polaczenia spowoduje powstanie zapetlenia
    }
}

void Graf::algorytm_Prima(int poczatkowy_wierzcholek){
    vector<pair<int, wierzcholki_krawedzi>> vector_pomocniczy;
    nr_kolejnego_wierzcholka = poczatkowy_wierzcholek; //poczatkowy wierzcholek
    wierzcholki_dodane.push_back(nr_kolejnego_wierzcholka);

    do{
        for(int i = 0; i < K.size(); i++){
            if(K[i].second.first == nr_kolejnego_wierzcholka || K[i].second.second == nr_kolejnego_wierzcholka) //znajdz sasiedztwo
                L.push_back(K[i]);
            else vector_pomocniczy.push_back(K[i]);
            }
        sort( L.begin(), L.end() );

        do{  //Sprawdza czy polaczenie nie spowoduje powstania zapetlenia
            sprawdz_Prim();
        }while(czy_wierzcholek_A_juz_dodano == true && czy_wierzcholek_B_juz_dodano == true);

        P.push_back(L[0]);
        wierzcholki_dodane.push_back(nr_kolejnego_wierzcholka);

        auto it = L.begin();
        L.erase(it); //usuniecie z L wybranej juz krawedzi

        K.clear();
        for(int i = 0; i < vector_pomocniczy.size(); i++)
            K.push_back(vector_pomocniczy[i]); //pozostawienie w K pozostalych krawedzi
        vector_pomocniczy.clear();

    }while(wierzcholki_dodane.size() != ilosc_wierzcholkow); //do momentu utworzenia polaczec ze wszystkimi wierzcholkami

}

void Graf::wyswietl_graf(char ktory_graf_wyswietlic){

    if(ktory_graf_wyswietlic == 'K')
    {
        for(vector<pair<int, wierzcholki_krawedzi>>::iterator it=K.begin(); it!=K.end(); ++it)
        cout << "waga: " << it->first << " krawedz {" << it->second.first << ", " << it->second.second <<  "}\n";
    }
    else if(ktory_graf_wyswietlic == 'L')
    {
        for(vector<pair<int, wierzcholki_krawedzi>>::iterator it=L.begin(); it!=L.end(); ++it)
        cout << "waga: " << it->first << " krawedz {" << it->second.first << ", " << it->second.second <<  "}\n";
    }
    else if(ktory_graf_wyswietlic == 'P')
    {
        for(vector<pair<int, wierzcholki_krawedzi>>::iterator it=P.begin(); it!=P.end(); ++it)
        cout << "waga: " << it->first << " krawedz {" << it->second.first << ", " << it->second.second <<  "}\n";
    }
    else
        cout << "nie ma takiego grafu \n";

};

int main()
{
    Graf testowyGraf(5); //tworze nowy graf podajac ilosc wierzcholkow

    //WIERZCHOLKI NUMEROWANE OD 0 WZWYZ
    testowyGraf.dodaj_krawedz_z_jej_waga(1,0,1); //waga, wierzcholekA, wierzcholekB
    testowyGraf.dodaj_krawedz_z_jej_waga(2,0,2);
    testowyGraf.dodaj_krawedz_z_jej_waga(2,0,3);
    testowyGraf.dodaj_krawedz_z_jej_waga(6,0,4);
    testowyGraf.dodaj_krawedz_z_jej_waga(3,2,3);
    testowyGraf.dodaj_krawedz_z_jej_waga(4,3,1);
    testowyGraf.dodaj_krawedz_z_jej_waga(5,4,1);
    testowyGraf.dodaj_krawedz_z_jej_waga(3,3,4);
    //wynik sprawdzono http://dbserver.mif.pg.gda.pl/st/l3.pdf

    testowyGraf.posortuj_po_wagach(); //najpierw posortowanie po wagach

    cout << "Wszystkie krawedzi z wagami:\n";
    testowyGraf.wyswietl_graf('K');

    //Algorytm Kruskala
    testowyGraf.algorytm_Kruskala();
    cout << "Krawedzie ktore tworza drzewo:\n";
    testowyGraf.wyswietl_graf('L');
    cout << "Koszt calego drzewa: " <<  testowyGraf.zlicz_koszt('L') << endl;

    /*
    //Algorytm Prima
    testowyGraf.algorytm_Prima(0); //podac poczatkowy wierzcholek
    cout << "Krawedzie ktore tworza drzewo:\n";
    testowyGraf.wyswietl_graf('P');
    cout << "Koszt calego drzewa: " <<  testowyGraf.zlicz_koszt('P') << endl;
    */
    return 0;
}

/*

    //Testowano na:

    testowyGraf.dodaj_krawedz_z_jej_waga(5,0,1); //waga, wierzcholekA, wierzcholekB
    testowyGraf.dodaj_krawedz_z_jej_waga(9,1,2);
    testowyGraf.dodaj_krawedz_z_jej_waga(3,2,7);
    testowyGraf.dodaj_krawedz_z_jej_waga(9,7,6);
    testowyGraf.dodaj_krawedz_z_jej_waga(6,5,6);
    testowyGraf.dodaj_krawedz_z_jej_waga(9,0,3);
    testowyGraf.dodaj_krawedz_z_jej_waga(3,0,6);
    testowyGraf.dodaj_krawedz_z_jej_waga(8,3,6);
    testowyGraf.dodaj_krawedz_z_jej_waga(9,2,3);
    testowyGraf.dodaj_krawedz_z_jej_waga(4,2,4);
    testowyGraf.dodaj_krawedz_z_jej_waga(2,4,5);
    testowyGraf.dodaj_krawedz_z_jej_waga(5,2,6);
    testowyGraf.dodaj_krawedz_z_jej_waga(8,1,4);
    testowyGraf.dodaj_krawedz_z_jej_waga(1,4,6);
    testowyGraf.dodaj_krawedz_z_jej_waga(6,1,5);
    testowyGraf.dodaj_krawedz_z_jej_waga(7,1,7);
    //wynik sprawdzono https://eduinf.waw.pl/inf/alg/001_search/0141.php


    testowyGraf.dodaj_krawedz_z_jej_waga(4,0,1); //waga, wierzcholekA, wierzcholekB
    testowyGraf.dodaj_krawedz_z_jej_waga(2,2,1);
    testowyGraf.dodaj_krawedz_z_jej_waga(2,0,5);
    testowyGraf.dodaj_krawedz_z_jej_waga(1,0,4);
    testowyGraf.dodaj_krawedz_z_jej_waga(2,1,4);
    testowyGraf.dodaj_krawedz_z_jej_waga(8,2,3);
    testowyGraf.dodaj_krawedz_z_jej_waga(3,3,4);
    testowyGraf.dodaj_krawedz_z_jej_waga(6,3,5);
    testowyGraf.dodaj_krawedz_z_jej_waga(7,4,5);
    //wynik sprawdzono http://www.algorytm.org/algorytmy-grafowe/algorytm-prima.html



*/
