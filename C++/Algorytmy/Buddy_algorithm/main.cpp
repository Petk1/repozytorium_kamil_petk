#include "std_lib_facilities.hpp"
#include <string>
#include <cctype>
using namespace std;

map<int, string> memory_blocks;
map<int, string> copy_memory_blocks;
/////////////////////////////DANE///////////////////////////////////////////////
#define LICZBA_PROCESOW 6
#define ILE_PROCESOW_USUNAC 4
string name_process[LICZBA_PROCESOW] = {"P1", "P2", "P3", "P4", "P5", "P6"};
int wielkosci_procesow[LICZBA_PROCESOW] = {5,60,70,50,250,10};
////////////////////////////////////////////////////////////////////////////////

vector<int> required_memory;
void calculateRequiredMemory(){
    for(int i = 0; i < LICZBA_PROCESOW; i++)
        required_memory.push_back(pow(2,ceil(log(wielkosci_procesow[i])/log(2))));
}

int findIndex(int wielkosc_procesu){  //wyszukanie pierwszego indexu do ktorego nie jest przypisany juz proces i
                                    //ktory bedzie w stanie pomiescic proces
    int index = -1;
    for(int i = 0; i < memory_blocks.size(); i++){
        for(string process : name_process){
            if(memory_blocks[i] == process){
                index = -1;
                break;
            }else if(memory_blocks[i] != process){
                if(isdigit(memory_blocks[i][0]) && stoi(memory_blocks[i]) > wielkosc_procesu)
                {
                    index = i;
                }else{
                    index = -1;
                    break;
                }
            }
        }
        if(index != -1)
            break;
    }
    return index;
}

int findBlockIndex(int index, int nr_process){   //sprawdzenie czy juz istnieje taki blok pamieci
                            //ktory posiada wymagane miejsce pamieci dla tego procesu
                            //jesli tak nie bedzie trzeba dokonywac dalszych podzialow
                            //a wystarczy tylko umiescic proces w odpowiednim bloku pamieci
    int block_index = -1; //jesli -1 to nie istnieje taki blok w pamieci
    bool saved_process = false;

    for(int i = index; i < memory_blocks.size(); i++){
        for(string process : name_process){
            if(memory_blocks[i] == process){
                saved_process = true;
            }
        }
        if(saved_process == false && required_memory[nr_process] == stoi(memory_blocks[i])){
            block_index = i; //to pobierz jego ndexu
            break;
        }
        saved_process = false;
    }

    return block_index;
}

map<int, string>::iterator it;
void zapisDoPamieci(){
     memory_blocks.insert(pair<int, string>(0, to_string(2048)));
    calculateRequiredMemory();

    int how_much_blocks = 1;
    vector<string> previous_memory_blocks;
    bool saved = false;

    for(int nr_process = 0; nr_process < LICZBA_PROCESOW; nr_process++){
        int wielkosc_procesu = wielkosci_procesow[nr_process];

        cout << endl;
        cout << "Numer procesu " << nr_process+1 << endl;
        int index = findIndex(wielkosc_procesu);


        if(index != -1){
            copy_memory_blocks.insert(pair<int, string>(0, memory_blocks[index]));
            int wielkosc_pierwszej_wolnej_komorki_pamieci = stoi(copy_memory_blocks[0]);

            int block_index = findBlockIndex(index, nr_process);


            if(block_index == -1){
                    //wykonanie podzialu pamieci az do momentu osiagniecia takiego bloku pamieci ktory
                    //bedzie w stanie przechowac proces np. proces wymagaj¹cy 64 jednostki pamieci
                    //nalezy umiescic w bloku pamieci o wielkosci 64 jednostek pamieci
                while(wielkosc_pierwszej_wolnej_komorki_pamieci > wielkosci_procesow[nr_process]){
                    how_much_blocks++;
                    wielkosc_pierwszej_wolnej_komorki_pamieci = wielkosc_pierwszej_wolnej_komorki_pamieci/2;

                    copy_memory_blocks.erase(copy_memory_blocks.begin(), copy_memory_blocks.end());

                    for(int block_number = 0; block_number < how_much_blocks; block_number++){

                        if(block_number <= 1){
                            if(wielkosc_pierwszej_wolnej_komorki_pamieci == required_memory[nr_process] && saved == false){

                                 copy_memory_blocks.insert(pair<int, string>(block_number, name_process[nr_process]));

                                 saved = true;
                            }else
                                copy_memory_blocks.insert(pair<int, string>(block_number, to_string(wielkosc_pierwszej_wolnej_komorki_pamieci)));
                        }else{
                            copy_memory_blocks.insert(pair<int, string>(block_number, previous_memory_blocks[block_number-2]));
                        }
                    }

                    previous_memory_blocks.clear();
                    for(auto it = copy_memory_blocks.find(1); it != copy_memory_blocks.end(); ++it){
                        previous_memory_blocks.push_back(it->second);
                    }

                    if(saved == true)
                        break;
                }
                if(saved == true){
                    if(nr_process == 0){
                        memory_blocks.erase(memory_blocks.begin(), memory_blocks.end());

                        for(int i = 0; i < copy_memory_blocks.size(); i++){
                            memory_blocks.insert(pair<int, string>(i, copy_memory_blocks[i]));
                        }
                        copy_memory_blocks.erase(copy_memory_blocks.begin(), copy_memory_blocks.end());
                        saved = false;
                    }else{
                        map<int, string> copy_memory_blocks_before_index; //pomocniczy map do przechowania danych z blokow pamieci
                        map<int, string> copy_memory_blocks_after_index; //pomocniczy map do przechowania danych z blokow pamieci

                        for(int i = 0; i < index; i++){
                            copy_memory_blocks_before_index.insert(pair<int, string>(i, memory_blocks[i]));
                        }

                        index++;
                        for(int i = 0; (i + index) < memory_blocks.size(); i++){
                            copy_memory_blocks_after_index.insert(pair<int, string>(i, memory_blocks[i+index]));

                        }
                        memory_blocks.erase(memory_blocks.begin(), memory_blocks.end());

                        for(int i = 0; i < copy_memory_blocks_before_index.size(); i++)
                            memory_blocks.insert(pair<int, string>(i, copy_memory_blocks_before_index[i]));

                        int memory_blocks_size = memory_blocks.size();
                        for(int i = memory_blocks_size; i < memory_blocks_size + copy_memory_blocks.size(); i++)
                            memory_blocks.insert(pair<int, string>(i, copy_memory_blocks[i-memory_blocks_size]));

                        memory_blocks_size = memory_blocks.size();
                        for(int i = 0; i < copy_memory_blocks_after_index.size(); i++)
                            memory_blocks.insert(pair<int, string>(i+memory_blocks_size, copy_memory_blocks_after_index[i]));

                        copy_memory_blocks.erase(copy_memory_blocks.begin(), copy_memory_blocks.end());
                        saved = false;
                    }
                }
                how_much_blocks = 1;
            }else{
                int index_nr = 0;
                for (auto it = memory_blocks.begin(); it != memory_blocks.end(); ++it){
                    if (it->second == to_string(required_memory[nr_process])){
                        index_nr = it->first;
                        break;
                    }
                }
                saved = false;
                block_index = -1;
                memory_blocks[index_nr] = name_process[nr_process];
                copy_memory_blocks.erase(copy_memory_blocks.begin(), copy_memory_blocks.end());
            }
        }
            cout << endl;
            for(it = memory_blocks.begin(); it != memory_blocks.end(); ++it)
                cout << it->second << "\n";
            cout << endl;
    }
}

void zwalnianiePamieci(){
    vector<string> procesy_do_zwolnienia;

    srand(time(NULL));
    while(procesy_do_zwolnienia.size() != ILE_PROCESOW_USUNAC){
        bool flag = false;
        int losowy_nr_procesu =(rand() % 6 );
        for(int j = 0; j < procesy_do_zwolnienia.size(); j++){ //sprawdzenie czy taki nie zostal dodany juz do listy procesow do zwolnienia
            if(procesy_do_zwolnienia[j] == name_process[losowy_nr_procesu])
                flag = true;
        }
        if(flag == false)
            procesy_do_zwolnienia.push_back(name_process[losowy_nr_procesu]);
    }
    cout << "Procesy wylosowane do zwolnienia: ";
    for(int i = 0; i < procesy_do_zwolnienia.size();i++)
    cout << procesy_do_zwolnienia[i] << ", ";
    cout << endl;
    //trzeba znalezc ten proces
    for(int nr_process = 0; nr_process < procesy_do_zwolnienia.size(); nr_process++){
        cout << "Zwalniany proces: " << procesy_do_zwolnienia[nr_process] << endl;
        int index_zwalnianego_procesu;
        int counter = 0;
          for(it = memory_blocks.begin(); it != memory_blocks.end(); ++it){
                if(it->second == procesy_do_zwolnienia[nr_process]){
                    index_zwalnianego_procesu = counter;
                    break;
                }
                counter++;
        }

        //teraz musze pobrac ile miejsca zajmuje
        map<string, int> nazwy_procesow_z_zajmowana_pamiecia;
        for(int i = 0; i < LICZBA_PROCESOW; i++){
             nazwy_procesow_z_zajmowana_pamiecia.insert(pair<string, int>(name_process[i], required_memory[i]));
        }

        //szukam ile ten proces zajmuje miejsca
        int zajmowane_miejsce;
        map<string, int>::iterator itr;
        for(itr= nazwy_procesow_z_zajmowana_pamiecia.begin(); itr != nazwy_procesow_z_zajmowana_pamiecia.end(); ++itr){
            if(itr->first == procesy_do_zwolnienia[nr_process]){
                zajmowane_miejsce = itr->second;
                break;
                }
        }
        cout << "Proces zajmuje " << zajmowane_miejsce << " jednostek pamieci" << endl;
        //sprawdzam czy po prawej od mojego indexu jest taka sama ilosc jednostek pamieci
        if(isdigit(memory_blocks[index_zwalnianego_procesu + 1][0]) && stoi(memory_blocks[index_zwalnianego_procesu + 1]) == zajmowane_miejsce){

            while(isdigit(memory_blocks[index_zwalnianego_procesu + 1][0]) && stoi(memory_blocks[index_zwalnianego_procesu + 1]) == zajmowane_miejsce){
                cout << "blok obok zajmuje tyle samo miejsca" << endl;

                //zapisuje sobie kopie elementow przed indexem oraz kopie elementow po index+1
                map<int, string> copy_memory_blocks_before_index; //pomocniczy map do przechowania danych z blokow pamieci
                map<int, string> copy_memory_blocks_after_index; //pomocniczy map do przechowania danych z blokow pamieci
                for(int i = 0; i < index_zwalnianego_procesu; i++){
                    copy_memory_blocks_before_index.insert(pair<int, string>(i, memory_blocks[i]));
                }

                for(int i = 1; i < memory_blocks.size()-(index_zwalnianego_procesu + 1); i++){
                    copy_memory_blocks_after_index.insert(pair<int, string>(i, memory_blocks[index_zwalnianego_procesu + 1 + i]));
                }

                //łącze dwa bloki pamieci w jeden
                memory_blocks.erase(memory_blocks.begin(), memory_blocks.end());
                for(int i = 0; i < copy_memory_blocks_before_index.size(); i++)
                    memory_blocks.insert(pair<int, string>(i, copy_memory_blocks_before_index[i]));

                int size_memory_blocks = memory_blocks.size();
                memory_blocks.insert(pair<int, string>(memory_blocks.size() > 0 ? memory_blocks.size() : 0, to_string(zajmowane_miejsce*2)));

                for(int i = 0; i < copy_memory_blocks_after_index.size(); i++){
                    memory_blocks.insert(pair<int, string>(size_memory_blocks+i, copy_memory_blocks_after_index[i]));
                }

                cout << endl << "Pamiec po zwolnieniu procesu lub scaleniu dwoch sasiednich komorek pamieci o tym samym rozmiarze w jedna(jesli po prawej komorka ma taki sam rozmiar to nastepuje scalenie): " << endl;

                for(it = memory_blocks.begin(); it != memory_blocks.end(); ++it)
                    cout << it->second << "\n";
                cout << endl;
                zajmowane_miejsce = zajmowane_miejsce*2;
            }
        }else{
             cout << "Blok obok zajmuje inna ilosc pamieci albo jest zajety" << endl;
                //zapisuje sobie kopie elementow przed indexem oraz kopie elementow po index+1
                map<int, string> copy_memory_blocks_before_index; //pomocniczy map do przechowania danych z blokow pamieci
                map<int, string> copy_memory_blocks_after_index; //pomocniczy map do przechowania danych z blokow pamieci

                for(int i = 0; i < index_zwalnianego_procesu; i++){
                    copy_memory_blocks_before_index.insert(pair<int, string>(i, memory_blocks[i]));
                }

                for(int i = 1; i < memory_blocks.size()-index_zwalnianego_procesu; i++){
                    copy_memory_blocks_after_index.insert(pair<int, string>(i, memory_blocks[index_zwalnianego_procesu + i]));
                }

                memory_blocks.erase(memory_blocks.begin(), memory_blocks.end());

                for(int i = 0; i < copy_memory_blocks_before_index.size(); i++)
                    memory_blocks.insert(pair<int, string>(i, copy_memory_blocks_before_index[i]));

                int size_memory_blocks = memory_blocks.size();
                memory_blocks.insert(pair<int, string>(memory_blocks.size() > 0 ? memory_blocks.size() : 0, to_string(zajmowane_miejsce)));

                for(int i = 0; i < copy_memory_blocks_after_index.size(); i++){
                    memory_blocks.insert(pair<int, string>(size_memory_blocks+i, copy_memory_blocks_after_index[i]));
                }

                cout << endl << "Pamiec po zwolnieniu procesu: " << endl;

                for(it = memory_blocks.begin(); it != memory_blocks.end(); ++it)
                    cout << it->second << "\n";
                cout << endl;
        }
    }
}

int main()
{
    cout << "Nazwy procesow z ich rozmiarami: " << endl;
    int i = 0;
    for(string process : name_process){
        cout << process << "(" << wielkosci_procesow[i] << "), ";
        i++;
    }
    cout << "\n\nZapis procesow: " << endl;
    zapisDoPamieci();

    cout << "Pamiec po dodaniu procesow:" << endl;
    for(it = memory_blocks.begin(); it != memory_blocks.end(); ++it){
        cout << it->second << "\n";
    }

    cout << endl << "Zwalnianie procesow:" << endl << endl;
    zwalnianiePamieci();

    return 0;
}
