#include "winsock2.h"
#define HAVE_REMOTE
#include "pcap.h"

//ethernet
typedef struct ethernet_header
{
	UCHAR dest[6]; //Adres MAC odbiorcy
	UCHAR source[6]; //Adres MAC nadawcy
	USHORT type;
}ethernet_header;

//warstwa sieciowa
//IP header
typedef struct ip_header{
	unsigned char ip_header_len:4; //dlugosc naglowka
	unsigned char ip_version :4; //wersja (4, 6 dla IPv6)
	unsigned char ip_tos; //typ uslugi
	unsigned short ip_total_length; //dlugosc calego pakietu(naglowek oraz dane)
	unsigned short ip_id; //numer identyfikacyjny - do okreslenia przynaleznosci pofragmentowanych datagramow

	unsigned char ip_frag_offset :5; //przesuniecie - ile bajtow od podczatku pakietu

	//Flagi - czy datagram jest w calosci czy jest to fragment
	unsigned char ip_more_fragment :1;
	unsigned char ip_dont_fragment :1;
	unsigned char ip_reserved_zero :1;

	unsigned char ip_frag_offset1; //fragment offset

	unsigned char ip_ttl; // Time to live - ilosc przejsc, np. po kazdym przejsci przez router odejmowane jest 1
	unsigned char ip_protocol; //Protokol
	unsigned short ip_checksum; //Header Checksum
	unsigned int ip_srcaddr; //Adres zrodlowy
	unsigned int ip_destaddr; //Adres docelowy
}ip_header;

//warstwa transportowa
//UDP header
typedef struct udp_header{
	unsigned short source_port; //Port zrodlowy
	unsigned short dest_port; // Port docelowy
	unsigned short udp_length; //Dlugosc pakietu
	unsigned short udp_checksum; //Checksum
}udp_header;

//warstwa transportowa
//TCP header
typedef struct tcp_header{
	unsigned short source_port; //Port zrodlowy
	unsigned short dest_port; //Port docelowy
	unsigned int sequence; //Numer sekwencji (32 bity) - umozliwia ulozenie danych w poprawnej kolejnosci
	unsigned int acknowledge; //Przyznany numer

	unsigned char ns :1; //flaga dodana w RFC 3540.
	unsigned char reserved_part1:3; //Zarezerwowane na przyszlosc
	unsigned char data_offset:4; //Okresla gdzie w pakiecie zaczynaja sie przyslane dane

	unsigned char fin :1; //Finish Flag
	unsigned char syn :1; //Synchronise Flag
	unsigned char rst :1; //Reset Flag
	unsigned char psh :1; //Push Flag
	unsigned char ack :1; //Acknowledgement Flag
	unsigned char urg :1; //Urgent Flag

	unsigned char ecn :1; //ECN-Echo Flag
	unsigned char cwr :1; //Congestion Window Reduced Flag

	unsigned short window; //liczba bajtow danych, ktore nadwaca zgodzi sie przyjac
	unsigned short checksum; //Checksum
	unsigned short urgent_pointer; //Pilne dane - interpretowany tylko przy ustawionej fladze URG
}tcp_header;

//warstwa sieciowa
//ICMP header
typedef struct icmp_header
{
	BYTE type; //typ wiadomosci
	BYTE code; //kod
	USHORT checksum;
	USHORT id; //identyfikator
	USHORT seq; //numer sekwencji
}icmp_header;

struct sockaddr_in source;
struct sockaddr_in dest;

void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data);

char separator[50]="*************************************************";

void print_ethernet(u_char *pkt_data){
    ethernet_header *ethernet = (ethernet_header *)pkt_data;

    printf("Ethernet header:\n");

    printf("Dest Addr: \t\t%.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", ethernet->dest[0], ethernet->dest[1], ethernet->dest[2], ethernet->dest[3], ethernet->dest[4], ethernet->dest[5]);
    printf("Source Addr: \t\t%.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", ethernet->source[0], ethernet->source[1], ethernet->source[2], ethernet->source[3], ethernet->source[4], ethernet->source[5]);
    printf("Type: \t\t\t0x% 4x \n", ntohs(ethernet->type));

    printf("%s\n", separator);
}

void print_udp(u_char *pkt_data){
    ip_header *ih;
    udp_header *uh;
    int ip_header_len = 0;
    int udp_header_len = 0;

    ih = (ip_header *)(pkt_data + sizeof(ethernet_header));
    ip_header_len = ih->ip_header_len*4;

    uh = (udp_header *)(pkt_data + ip_header_len + sizeof(ethernet_header));

    printf("UDP header:\n");

	printf("Source Port No : \t%d\n", ntohs(uh->source_port));
	printf("Destination Port No : \t%d\n", ntohs(uh->dest_port));
	printf("Length : \t\t%d\n", ntohs(uh->udp_length));
	printf("Checksum : \t\t%d\n", ntohs(uh->udp_checksum));

    printf("%s\n", separator);

    struct pcap_pkthdr *header;
    u_char *UDPdata = (u_char*)uh;
    unsigned char dataDump;
    int sizeData;
    u_char *data;

    data = (pkt_data + sizeof(ethernet_header) + ip_header_len + sizeof(udp_header));
    sizeData = (header->caplen - sizeof(ethernet_header) - ip_header_len - sizeof(udp_header));

    printf("\nUDP HEADER:\n");
    for(int i = 0; i < sizeof(udp_header); i++)
    {
        dataDump = UDPdata[i];
        printf(" %.2x", (unsigned int)dataDump);

        if(i%6==0 && i != 0)
        printf("\n");
    }
    printf("\n");

    printf("%s\nDATA PAYLOAD\n", separator);
    unsigned char dataPayload;

    for(int i = 0; i < sizeData; i++)
    {
        dataPayload = data[i];
        printf(" %.2x", (unsigned int)dataPayload);

        if(i%6==0 && i != 0)
        printf("\n");
    }
    printf("\n");
}

void print_tcp(u_char *pkt_data){
    tcp_header *th;
    ip_header *ih;
    int ihlen;
    int thlen;


    ih = (ip_header *)(pkt_data + sizeof(ethernet_header));
    th = (tcp_header *)(pkt_data + sizeof(ethernet_header));

    ihlen = ih->ip_header_len*4;
    thlen = th->data_offset*4;

    printf("TCP header:\n");

    printf("Source Port : \t\t%u\n",ntohs(th->source_port));
	printf("Destination Port : \t%u\n",ntohs(th->dest_port));
    printf("Sequence Number : \t%u\n",ntohl(th->sequence));
	printf("Acknowledge Number : \t%u\n",ntohl(th->acknowledge));
	printf("Header Length : \t%d DWORDS or %d BYTES\n", (unsigned int)th->data_offset,(unsigned int)th->data_offset*4);
	printf("CWR Flag : \t\t%d\n",(unsigned int)th->cwr);
	printf("ECN Flag : \t\t%d\n",(unsigned int)th->ecn);
	printf("Urgent Flag : \t\t%d\n",(unsigned int)th->urg);
	printf("Acknowledgement Flag : \t%d\n",(unsigned int)th->ack);
	printf("Push Flag : \t\t%d\n",(unsigned int)th->psh);
	printf("Reset Flag : \t\t%d\n",(unsigned int)th->rst);
	printf("Synchronise Flag : \t%d\n",(unsigned int)th->syn);
	printf("Finish Flag : \t\t%d\n",(unsigned int)th->fin);
	printf("Window : \t\t%d\n",ntohs(th->window));
	printf("Checksum : \t\t%d\n",ntohs(th->checksum));
	printf("Urgent Pointer : \t%d\n",th->urgent_pointer);

    printf("%s\n", separator);

    struct pcap_pkthdr *header;
    u_char *TCPdata = (u_char*)th;
    unsigned char dataDump;
    int sizeData;
    u_char *data;

    data = (pkt_data + sizeof(ethernet_header) + ihlen + sizeof(tcp_header));
    sizeData = (header->caplen - sizeof(ethernet_header) - ihlen - sizeof(tcp_header));

    printf("\nTCP HEADER:\n");
    for(int i = 0; i < thlen; i++)
    {
        dataDump = TCPdata[i];
        printf(" %.2x", (unsigned int)dataDump);

        if(i%6==0 && i != 0)
        printf("\n");
    }
    printf("\n");

    printf("%s\nDATA PAYLOAD\n", separator);
    unsigned char dataPayload;

    for(int i = 0; i < sizeData; i++)
    {
        dataPayload = data[i];
        printf(" %.2x", (unsigned int)dataPayload);

        if(i%6==0 && i != 0)
        printf("\n");
    }
    printf("\n");

}

void print_icmp(u_char *pkt_data){
    ip_header *ih;
    icmp_header *icmph;
    int ip_header_len = 0;
    int udp_header_len = 0;

    ih = (ip_header *)(pkt_data + sizeof(ethernet_header));
    ip_header_len = ih->ip_header_len*4;

    icmph = (icmp_header *)(pkt_data + ip_header_len + sizeof(ethernet_header));

    printf("ICMP header:\n");

	printf("Type : \t\t\t%d\n", (unsigned int)(icmph->type));
    printf("Code : \t\t\t%d\n", (unsigned int)(icmph->code));
	printf("Checksum : \t\t%d\n", ntohs(icmph->checksum));
	printf("ID : \t\t\t%d\n", ntohs(icmph->id));
	printf("Sequence : \t\t%d\n", ntohs(icmph->seq));

    printf("%s\n", separator);

    struct pcap_pkthdr *header;
    u_char *ICMPdata = (u_char*)icmph;
    unsigned char dataDump;
    int sizeData;
    u_char *data;

    data = (pkt_data + sizeof(ethernet_header) + ip_header_len + sizeof(icmp_header));
    sizeData = (header->caplen - sizeof(ethernet_header) - ip_header_len - sizeof(icmp_header));

    printf("\nICMP HEADER:\n");
    for(int i = 0; i < sizeof(icmp_header); i++)
    {
        dataDump = ICMPdata[i];
        printf(" %.2x", (unsigned int)dataDump);

        if(i%6==0 && i != 0)
        printf("\n");
    }
    printf("\n");

    printf("%s\nDATA PAYLOAD\n", separator);
    unsigned char dataPayload;

    for(int i = 0; i < sizeData; i++)
    {
        dataPayload = data[i];
        printf(" %.2x", (unsigned int)dataPayload);

        if(i%6==0 && i != 0)
        printf("\n");
    }
    printf("\n");
}

void print_ip(u_char *pkt_data){
    ip_header *ih;
    int ip_header_len = 0;

    ih = (ip_header *)(pkt_data + sizeof(ethernet_header));
    source.sin_addr.s_addr = ih->ip_srcaddr;
    dest.sin_addr.s_addr = ih->ip_destaddr;

    ip_header_len = ih->ip_header_len*4;
    printf("IP header:\n");

    printf("IP version: \t\t%d\n", (unsigned int)ih->ip_version);
    printf("Header length (bytes): \t%d\n", ((unsigned int)(ih->ip_header_len))*4);
    printf("TOS: \t\t\t%d\n", (unsigned int)ih->ip_tos);
	printf("IP Total Length : \t%d Bytes(Size of Packet)\n", ntohs(ih->ip_total_length));
	printf("Identification : \t%d\n", ntohs(ih->ip_id));
	printf("Reserved ZERO Field : \t%d\n", (unsigned int)ih->ip_reserved_zero);
	printf("Dont Fragment Field : \t%d\n", (unsigned int)ih->ip_dont_fragment);
	printf("More Fragment Field : \t%d\n", (unsigned int)ih->ip_more_fragment);
	printf("TTL : \t\t\t%d\n", (unsigned int)ih->ip_ttl);

	printf("Checksum : \t\t%d\n", ntohs(ih->ip_checksum));
	printf("Source IP : \t\t%s\n",inet_ntoa(source.sin_addr));
	printf("Destination IP : \t%s\n", inet_ntoa(dest.sin_addr));

    if((ih->ip_protocol)==IPPROTO_TCP){
        printf("Packet : \t\tTCP\n");
        printf("%s\n", separator);
        print_tcp(pkt_data);
    }
    if((ih->ip_protocol)==IPPROTO_UDP){
        printf("Packet : \t\tUDP\n");
        printf("%s\n", separator);
        print_udp(pkt_data);
    }
    if((ih->ip_protocol)==IPPROTO_ICMP){
         printf("Packet : \t\tICMP\n");
         printf("%s\n", separator);
         print_icmp(pkt_data);
    }


    printf("%s\n", separator);
    printf("IP HEADER\n");
    u_char *IPdata = (u_char*)ih;
    unsigned char dataDump;

    for(int i = 0; i < ip_header_len; i++)
    {
        dataDump = IPdata[i];
        printf(" %.2x", (unsigned int)dataDump);

        if(i%6==0 && i != 0)
        printf("\n");
    }
    printf("\n");
}

int main(){
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int inum;
    int i=0;
    pcap_t *fp;
    char errbuf[PCAP_ERRBUF_SIZE];
    u_int netmask;
    //char packet_filter[] = "ip and icmp";
    char packet_filter[] = "src net 192.168.0.114 and icmp";
    //char packet_filter[] = "src net 192.168.0.1"; //router
    struct bpf_program fcode;
    WSADATA wsd ;
    //Do konwersji adresu IP
    WSAStartup(MAKEWORD(1,1),&wsd);

    printf("Wybierz urzadzenie:\n");
    if (pcap_findalldevs_ex(PCAP_SRC_IF_STRING, NULL, &alldevs, errbuf) == -1) {
        fprintf(stderr,"Blad w pcap_findalldevs.ex: %s\n", errbuf);
        exit(1);
    }

    //Lista urzadzen
    for(d=alldevs; d; d=d->next) {
        printf("%d. %s\n   ", ++i, d->name);

        if (d->description) {
            printf(" (%s)\n", d->description);
        }else {
            printf("(Brak opisu)\n");
        }
    }

    if(i==0){
        printf("\nBrak urzadzen(interfejsow).\n");
        return -1;
    }

    printf("Wprowadz numer urzadzenia (1-%d):",i);
    scanf("%d", &inum);

    if(inum < 1 || inum > i){
        printf("\nNumer z poza przedzialu.\n");
        pcap_freealldevs(alldevs);
        return -1;
    }

    //Wybor sprzetu
    for(d=alldevs, i=0; i < inum-1 ;d=d->next, i++);

    //Otwarcie urzadzenia
    if ((fp = pcap_open(d->name,      //nazwa urzadzenia
                             65536,   //wielkosc pakietu, 65536 gwarancja
                             //ze przetwarzamy caly pakiet
                             PCAP_OPENFLAG_PROMISCUOUS, //promiscuous mode
                             1000,    //czas oczekiwania
                             NULL, errbuf )) == NULL)

    {
        fprintf(stderr,"\nNie mozna otworzyc urzadenia. %s nie obslugiwane przez WinPcap\n");
        pcap_freealldevs(alldevs);
        return -1;
    }

    //Wybor tylko sieci ethernet (dla uproszczenia)
    if(pcap_datalink(fp) != DLT_EN10MB) {
        fprintf(stderr,"\nTylko dla sieci Ethernet.\n");
        pcap_freealldevs(alldevs);
        return -1;
    }

    if(d->addresses != NULL) {
        //pobranie maski pierwszego adresu ip urzadzenia
        netmask=((struct sockaddr_in *)(d->addresses->netmask))->sin_addr.S_un.S_addr;
    } else {
        //jesli nie, zakladamy, ze jestesmy w klasie C sieci (Klasa C - liczba hostow 254)
        netmask=0xffffff;
    }

    //Kompilacja filtru, np. ip and tc, ip and udp
    if (pcap_compile(fp, &fcode, packet_filter, 1, netmask) <0 ){
        fprintf(stderr,"\nNie można skompilować. Sprawdź składnię.\n");
        pcap_freealldevs(alldevs);
        return -1;
    }

    //Ustawienie filtru
    if (pcap_setfilter(fp, &fcode)<0){
        fprintf(stderr,"\nBlad podczas ustawiania filtru.\n");
        pcap_freealldevs(alldevs);
        return -1;
    }

    printf("\nListening on %s\n", d->description);

    pcap_freealldevs(alldevs);
    pcap_loop(fp, 5, packet_handler, NULL); //przetwarza pakiety z przechwytywania
    //pcap_loop(fp, 0, packet_handler, NULL);
    WSACleanup();
    return 0;
}
int cout = 1;
//Funkcja zwrotna
void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data){

    printf("\n%s\n", separator);
    printf("Pakiet%d\n",cout);
    printf("%s\n", separator);
    print_ethernet(pkt_data);
    print_ip(pkt_data);

    cout++;

}

