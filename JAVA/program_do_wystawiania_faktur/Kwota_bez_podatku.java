package pl.programfaktury.first;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Kwota_bez_podatku {
	private double kwota23;
	private double kwota8;
	private double kwota5;
	private double kwota0;
	
	public Kwota_bez_podatku(double kwota23, double kwota8, double kwota5, double kwota0) {
		this.kwota23 = kwota23;
		this.kwota8 = kwota8;
		this.kwota5 = kwota5;
		this.kwota0 = kwota0;
	}
	
	public double oblicz_23() {
		double wynik23;

		wynik23=BigDecimal.valueOf(kwota23).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik23=BigDecimal.valueOf(wynik23).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik23=BigDecimal.valueOf(wynik23).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik23=(BigDecimal.valueOf(wynik23).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik23=Math.round(wynik23);
		return wynik23;
		
	}
	
	public double oblicz_8() {
		double wynik8;

		wynik8=BigDecimal.valueOf(kwota8).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik8=BigDecimal.valueOf(wynik8).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik8=BigDecimal.valueOf(wynik8).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik8=(BigDecimal.valueOf(wynik8).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik8=Math.round(wynik8);
		return wynik8;
		
	}
	
	public double oblicz_5() {
		double wynik5;

		wynik5=BigDecimal.valueOf(kwota5).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik5=BigDecimal.valueOf(wynik5).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik5=BigDecimal.valueOf(wynik5).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik5=(BigDecimal.valueOf(wynik5).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik5=Math.round(wynik5);
		return wynik5;
		
	}
	
	public double oblicz_0() {
		double wynik0;

		wynik0=BigDecimal.valueOf(kwota0).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik0=BigDecimal.valueOf(wynik0).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik0=BigDecimal.valueOf(wynik0).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik0=(BigDecimal.valueOf(wynik0).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik0=Math.round(wynik0);
		return wynik0;
		
	}
}