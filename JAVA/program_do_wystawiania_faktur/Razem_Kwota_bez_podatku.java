package pl.programfaktury.first;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Razem_Kwota_bez_podatku {
	private double razem_kwota_bez_podatku;

	public Razem_Kwota_bez_podatku(double razem_kwota_bez_podatku) {
		this.razem_kwota_bez_podatku = razem_kwota_bez_podatku;

	}
	
	public double oblicz_razem() {
		double wynik;

		wynik=BigDecimal.valueOf(razem_kwota_bez_podatku).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik=BigDecimal.valueOf(wynik).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik=BigDecimal.valueOf(wynik).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik=(BigDecimal.valueOf(wynik).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik=Math.round(wynik);
		return wynik;
		
	}
}
