package pl.programfaktury.first;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Kwota_wraz_z_podatkiem_sumowanie_poszczegolnych {
	private double kwota_wraz_z_podatkiem23;
	private double kwota_wraz_z_podatkiem8;
	private double kwota_wraz_z_podatkiem5;
	private double kwota_wraz_z_podatkiem0;
	
	public Kwota_wraz_z_podatkiem_sumowanie_poszczegolnych(double kwota_wraz_z_podatkiem23, double kwota_wraz_z_podatkiem8, double kwota_wraz_z_podatkiem5, double kwota_wraz_z_podatkiem0) {
		this.kwota_wraz_z_podatkiem23 = kwota_wraz_z_podatkiem23;
		this.kwota_wraz_z_podatkiem8 = kwota_wraz_z_podatkiem8;
		this.kwota_wraz_z_podatkiem5 = kwota_wraz_z_podatkiem5;
		this.kwota_wraz_z_podatkiem0 = kwota_wraz_z_podatkiem0;
	}
	
	public double oblicz_23() {
		double wynik23;

		wynik23=BigDecimal.valueOf(kwota_wraz_z_podatkiem23).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik23=BigDecimal.valueOf(wynik23).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik23=BigDecimal.valueOf(wynik23).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik23=(BigDecimal.valueOf(wynik23).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik23=Math.round(wynik23);
		return wynik23;	
	}
	
	public double oblicz_8() {
		double wynik8;

		wynik8=BigDecimal.valueOf(kwota_wraz_z_podatkiem8).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik8=BigDecimal.valueOf(wynik8).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik8=BigDecimal.valueOf(wynik8).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik8=(BigDecimal.valueOf(wynik8).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik8=Math.round(wynik8);
		return wynik8;	
	}
	
	public double oblicz_5() {
		double wynik5;

		wynik5=BigDecimal.valueOf(kwota_wraz_z_podatkiem5).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik5=BigDecimal.valueOf(wynik5).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik5=BigDecimal.valueOf(wynik5).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik5=(BigDecimal.valueOf(wynik5).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik5=Math.round(wynik5);
		return wynik5;	
	}
	
	public double oblicz_0() {
		double wynik0;

		wynik0=BigDecimal.valueOf(kwota_wraz_z_podatkiem0).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik0=BigDecimal.valueOf(wynik0).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik0=BigDecimal.valueOf(wynik0).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik0=(BigDecimal.valueOf(wynik0).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik0=Math.round(wynik0);
		return wynik0;	
	}
}
