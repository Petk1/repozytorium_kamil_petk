package pl.programfaktury.first;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Kwota_podatku {

	private double Kwota_podatku;

	public Kwota_podatku(double  Kwota_podatku) {
		this.Kwota_podatku =  Kwota_podatku;

	}
	
	public double oblicz_podatek() {
		double wynik;
		
		//wykonanie niezbednych zaokraglen
	   	wynik=BigDecimal.valueOf(Kwota_podatku).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	wynik=BigDecimal.valueOf(wynik).setScale(4, RoundingMode.HALF_UP).doubleValue();
	   	wynik=BigDecimal.valueOf(wynik).setScale(3, RoundingMode.HALF_UP).doubleValue();
	   	wynik=(BigDecimal.valueOf(wynik).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		
		return wynik;//zwrocenie zaokraglonej wartosci GR podatku
		
	}

}