package pl.programfaktury.first;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Menu_Glowne extends JFrame implements ActionListener {
	
	private JButton bWystawFakture;
	private Jednorazowa_faktura oknoWystawFakture;
	
	private JButton bDodajDaneFirmy;
	private Dodaj_nowe_dane_firmy oknoDodajDaneFirmy;
	
	private JButton bDodajDaneProduktu;
	private Dodaj_nowe_dane_produktu oknoDodajDaneProduktu;
	
	private JButton bHistoriaFaktur;
	
	
	public Menu_Glowne() {

//PARAMETRY G��WNEGO MENU		
		setTitle("Menu g��wne");
		setSize(400, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);

		
//PARAMETRY PRZYCISK�W			
		bWystawFakture = new JButton("Wystaw jednorazow� faktur� VAT");
		bWystawFakture.setBounds(40,60,300,20);
		add(bWystawFakture);
		bWystawFakture.addActionListener(this);
		
		bDodajDaneFirmy = new JButton("Dodaj nowe dane firmy");
		bDodajDaneFirmy.setBounds(40,100,300,20);
		add(bDodajDaneFirmy);
		bDodajDaneFirmy.addActionListener(this);
		
		bDodajDaneProduktu = new JButton("Dodaj nowe dane produktu");
		bDodajDaneProduktu.setBounds(40,140,300,20);
		add(bDodajDaneProduktu);
		bDodajDaneProduktu.addActionListener(this);
		
		bHistoriaFaktur = new JButton("Historia faktur");
		bHistoriaFaktur.setBounds(40,180,300,20);
		add(bHistoriaFaktur);
		bHistoriaFaktur.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent g) {
		Object zrodlo = g.getSource();
		
//PRZYCISKI G��WNEGO MENU		
		//PRZYCISK OPCJI WYSTAW FAKTURE
		if(zrodlo == bWystawFakture) 
		{
		if (oknoWystawFakture==null)
			oknoWystawFakture = new Jednorazowa_faktura();
		
		oknoWystawFakture.setVisible(true);
		}	
		
		//PRZYCISK OPCJI DODAJ NOWE DANE FIRMY
		if(zrodlo == bDodajDaneFirmy) 
		{
		if (oknoDodajDaneFirmy==null)
			oknoDodajDaneFirmy = new Dodaj_nowe_dane_firmy();

		oknoDodajDaneFirmy.setVisible(true);
		}
		
		//PRZYCISK OPCJI DODAJ NOWE DANE PRODUKTU
		if(zrodlo == bDodajDaneProduktu) 
		{
		if (oknoDodajDaneProduktu==null)
			oknoDodajDaneProduktu = new Dodaj_nowe_dane_produktu();
		
		oknoDodajDaneProduktu.setVisible(true);
		}
		
		if(zrodlo == bHistoriaFaktur) 
		{
			try 
			{
				Desktop.getDesktop().open(new File("Historia Faktur"));
			}
			catch (Exception e)
			{
				System.out.println(e);
			}
			
		}
	}
	
	public static void main(String[] args) 
	{
		new Menu_Glowne();
	}
}