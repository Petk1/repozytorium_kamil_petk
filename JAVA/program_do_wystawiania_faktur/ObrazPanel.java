package pl.programfaktury.first;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class ObrazPanel extends JPanel {

	Image back;	
 
    public ObrazPanel() {

        Toolkit kit = Toolkit.getDefaultToolkit();
        back = kit.getImage("faktura.jpg");
        setLayout(null);
       
    }
    
public void paintComponent(Graphics comp) {
	
	Graphics2D comp2D = (Graphics2D)comp;
	comp2D.drawImage(back, 0, 0, 598, 824, this);
	}



}
