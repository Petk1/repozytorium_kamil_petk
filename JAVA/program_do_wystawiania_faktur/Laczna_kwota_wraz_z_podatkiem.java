package pl.programfaktury.first;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Laczna_kwota_wraz_z_podatkiem {
	private double laczna_kwota_wraz_z_podatkiem;

	public Laczna_kwota_wraz_z_podatkiem(double laczna_kwota_wraz_z_podatkiem) {
		this.laczna_kwota_wraz_z_podatkiem = laczna_kwota_wraz_z_podatkiem;

	}
	
	public double oblicz_laczna_wartosc_wraz_z_podatekiem() {
		double wynik;

		wynik=BigDecimal.valueOf(laczna_kwota_wraz_z_podatkiem).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik=BigDecimal.valueOf(wynik).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik=BigDecimal.valueOf(wynik).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik=(BigDecimal.valueOf(wynik).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik=Math.round(wynik);
		return wynik;
		
	}
}
