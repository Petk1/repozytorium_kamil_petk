package pl.programfaktury.first;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Kwota_podatku_sumowanie_poszczegolnych {	
		private double kwota_podatku23;
		private double kwota_podatku8;
		private double kwota_podatku5;
		
		public Kwota_podatku_sumowanie_poszczegolnych(double kwota_podatku23, double kwota_podatku8, double kwota_podatku5) {
			this.kwota_podatku23 = kwota_podatku23;
			this.kwota_podatku8 = kwota_podatku8;
			this.kwota_podatku5 = kwota_podatku5;
		}
		
		public double oblicz_23() {
			double wynik23;

			wynik23=BigDecimal.valueOf(kwota_podatku23).setScale(5, RoundingMode.HALF_UP).doubleValue();
			wynik23=BigDecimal.valueOf(wynik23).setScale(4, RoundingMode.HALF_UP).doubleValue();
			wynik23=BigDecimal.valueOf(wynik23).setScale(3, RoundingMode.HALF_UP).doubleValue();
			wynik23=(BigDecimal.valueOf(wynik23).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
			wynik23=Math.round(wynik23);
			return wynik23;	
		}
		
		public double oblicz_8() {
			double wynik8;

			wynik8=BigDecimal.valueOf(kwota_podatku8).setScale(5, RoundingMode.HALF_UP).doubleValue();
			wynik8=BigDecimal.valueOf(wynik8).setScale(4, RoundingMode.HALF_UP).doubleValue();
			wynik8=BigDecimal.valueOf(wynik8).setScale(3, RoundingMode.HALF_UP).doubleValue();
			wynik8=(BigDecimal.valueOf(wynik8).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
			wynik8=Math.round(wynik8);
			return wynik8;	
		}
		
		public double oblicz_5() {
			double wynik5;

			wynik5=BigDecimal.valueOf(kwota_podatku5).setScale(5, RoundingMode.HALF_UP).doubleValue();
			wynik5=BigDecimal.valueOf(wynik5).setScale(4, RoundingMode.HALF_UP).doubleValue();
			wynik5=BigDecimal.valueOf(wynik5).setScale(3, RoundingMode.HALF_UP).doubleValue();
			wynik5=(BigDecimal.valueOf(wynik5).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
			wynik5=Math.round(wynik5);
			return wynik5;	
		}
}
