package pl.programfaktury.first;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Razem_Kwota_podatku {
	private double razem_kwota_podatku;

	public Razem_Kwota_podatku(double razem_kwota_podatku) {
		this.razem_kwota_podatku = razem_kwota_podatku;

	}
	
	public double oblicz_razem_podatek() {
		double wynik;

		wynik=BigDecimal.valueOf(razem_kwota_podatku).setScale(5, RoundingMode.HALF_UP).doubleValue();
		wynik=BigDecimal.valueOf(wynik).setScale(4, RoundingMode.HALF_UP).doubleValue();
		wynik=BigDecimal.valueOf(wynik).setScale(3, RoundingMode.HALF_UP).doubleValue();
		wynik=(BigDecimal.valueOf(wynik).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		wynik=Math.round(wynik);
		return wynik;
		
	}
}
