package pl.programfaktury.first;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Wybierz_zapisana_firme extends JFrame{
	
//LISTA ROZWIJANA WYBIERZ ZPISANA FAKTURE
		JComboBox wyborFirmy;
		
//OPCJE WPROWADZANIA DANYCH
		//NR FAKTURY I DATA
		JLabel nrFakturyTekst;
		JLabel dataTekst;
		JTextField nrFaktury;
		JTextField data;
		
	//DANE WYSTAWIAJACEGO FAKTURE
		JLabel miejscowoscWystawieniaTekst;
		JLabel miaraProduktuTekst;
		JLabel sposobZaplatyTekst;
		JLabel terminZaplatyTekst;
		JLabel bankTekst;
		JLabel nrKontaTekst;		
		JTextField miejscowoscWystawienia;
		JTextField miaraProduktu;
		JTextField sposobZaplaty;
		JTextField terminZaplaty;
		JTextField bank;
		JTextField nrKonta;
		
	//PIERWSZY PRODUKT//
		JComboBox listaProdukt1;
		JLabel pierwszyProdukt;
		JLabel podajNazweProdukt1Tekst;
		JLabel podajIloscProdukt1Tekst;
		JLabel podajMiareProduktu1Tekst;
		JLabel podajCeneProdukt1Tekst;
		JLabel stawkaPodatku1Tekst;
		JTextField podajNazweProdukt1;
		JTextField podajIloscProdukt1;
		JTextField podajMiareProduktu1;
		JTextField podajCeneProdukt1;
		JTextField stawkaPodatku1;
		   
	//DRUGI PRODUKT// 
		JComboBox listaProdukt2;
		JLabel drugiProdukt;
		JLabel podajNazweProdukt2Tekst;
		JLabel podajIloscProdukt2Tekst;
		JLabel podajMiareProduktu2Tekst;
		JLabel podajCeneProdukt2Tekst;
		JLabel stawkaPodatku2Tekst;
		JTextField podajNazweProdukt2;
		JTextField podajIloscProdukt2;
		JTextField podajMiareProduktu2;
		JTextField podajCeneProdukt2;
		JTextField stawkaPodatku2;
		   
	 //TRZECI PRODUKT// 
		JComboBox listaProdukt3;
		JLabel trzeciProdukt;
		JLabel podajNazweProdukt3Tekst;
		JLabel podajIloscProdukt3Tekst;
		JLabel podajMiareProduktu3Tekst;
		JLabel podajCeneProdukt3Tekst;
		JLabel stawkaPodatku3Tekst;
		JTextField podajNazweProdukt3;
		JTextField podajIloscProdukt3;
		JTextField podajMiareProduktu3;
		JTextField podajCeneProdukt3;
		JTextField stawkaPodatku3;
		   
	//CZWARTY PRODUKT// 
		JComboBox listaProdukt4;
		JLabel czwartyProdukt;
		JLabel podajNazweProdukt4Tekst;
		JLabel podajIloscProdukt4Tekst;
		JLabel podajMiareProduktu4Tekst;
		JLabel podajCeneProdukt4Tekst;
		JLabel stawkaPodatku4Tekst;
		JTextField podajNazweProdukt4;
		JTextField podajIloscProdukt4;
		JTextField podajMiareProduktu4;
		JTextField podajCeneProdukt4;
		JTextField stawkaPodatku4;
		   
	//PIATY PRODUKT// 
		JComboBox listaProdukt5;
		JLabel piatyProdukt;
		JLabel podajNazweProdukt5Tekst;
		JLabel podajIloscProdukt5Tekst;
		JLabel podajMiareProduktu5Tekst;
		JLabel podajCeneProdukt5Tekst;
		JLabel stawkaPodatku5Tekst;
		JTextField podajNazweProdukt5;
		JTextField podajIloscProdukt5;
		JTextField podajMiareProduktu5;
		JTextField podajCeneProdukt5;
		JTextField stawkaPodatku5;

    public Wybierz_zapisana_firme() {

//PARAMETRY OKNA
		setTitle("Wybierz zapisan� firm�");
		ObrazPanel obrazPanel = new ObrazPanel();
	   	add(obrazPanel);
    	obrazPanel.setBounds(0,0,700,900);
    	setSize(1600, 1040);
		setLayout(null);
		
		
//LISTA ROZWIJANA WYBIERZ ZPISANA FAKTURE
		wyborFirmy = new JComboBox();
		wyborFirmy.setBounds(700,25,125,20);
		wyborFirmy.addItem("WYBIERZ FIRME");
		wyborFirmy.addItem("Firma 1");
		wyborFirmy.addItem("Firma 2");
		wyborFirmy.addItem("Firma 3");
		add(wyborFirmy);		
				
				
		//OPCJE WPROWADZANIA DANYCH	
				//NR FAKTURY I DATA
				//--------------------------------------------------------------//
				nrFakturyTekst = new JLabel("Podaj numer faktury VAT:");
				nrFakturyTekst.setBounds(700,75,200,20);
			    add(nrFakturyTekst);
			    	
			    nrFaktury = new JTextField("");
			    nrFaktury.setBounds(910,75,200,20);
			    add(nrFaktury);
				//--------------------------------------------------------------//
			    
				//--------------------------------------------------------------//
				dataTekst = new JLabel("Podaj dat�:");
				dataTekst.setBounds(700,100,200,20);
			    add(dataTekst);
			    	
			    data = new JTextField("");
			    data.setBounds(910,100,200,20);
			    add(data);
				//--------------------------------------------------------------//

			    
			    
		//DANE WYSTAWIAJACEGO FAKTURE	    
			    
				//--------------------------------------------------------------//
			    miejscowoscWystawieniaTekst = new JLabel("Miejscowo�� wystawienia faktury:");
			    miejscowoscWystawieniaTekst.setBounds(700,125,200,20);
			    add(miejscowoscWystawieniaTekst);
			    	
			    miejscowoscWystawienia = new JTextField("");
			    miejscowoscWystawienia.setBounds(910,125,200,20);
			    add(miejscowoscWystawienia);
			    //--------------------------------------------------------------//
				
				//--------------------------------------------------------------//
			    bankTekst = new JLabel("Nazwa Twojego banku:");
			    bankTekst.setBounds(700,150,150,20);
			    add(bankTekst);
			    	
			    bank = new JTextField("");
			    bank.setBounds(910,150,200,20);
			    add(bank);
			    //--------------------------------------------------------------//
			    
			  //--------------------------------------------------------------//
			    nrKontaTekst = new JLabel("Tw�j numer konta bankowego:");
			    nrKontaTekst.setBounds(700,175,200,20);
			    add(nrKontaTekst);
			    	
			    nrKonta = new JTextField("");
			    nrKonta.setBounds(910,175,200,20);
			    add(nrKonta);
			    //--------------------------------------------------------------//
			    
		//DANE ZAPLATY
				//--------------------------------------------------------------//
			    sposobZaplatyTekst = new JLabel("Spos�b zap�aty:");
			    sposobZaplatyTekst.setBounds(700,200,150,20);
			    add(sposobZaplatyTekst);
			    	
			    sposobZaplaty = new JTextField("");
			    sposobZaplaty.setBounds(910,200,200,20);
			    add(sposobZaplaty);
			    //--------------------------------------------------------------//
			    
				//--------------------------------------------------------------//
			    terminZaplatyTekst = new JLabel("Termin zap�aty:");
			    terminZaplatyTekst.setBounds(700,225,150,20);
			    add(terminZaplatyTekst);
			    	
			    terminZaplaty = new JTextField("");
			    terminZaplaty.setBounds(910,225,200,20);
			    add(terminZaplaty);
			    //--------------------------------------------------------------//
				
			  //----------------------------PIERWSZY PRODUKT---------------------------------------------//
				pierwszyProdukt = new JLabel("Dane pierwszego produku");
				pierwszyProdukt.setBounds(1150,25,200,20);
			    add(pierwszyProdukt);
			    
				//LISTA PRODUKTOW
			    listaProdukt1 = new JComboBox();
			    listaProdukt1.setBounds(1360,25,200,20);
			    listaProdukt1.addItem("WYBIERZ PIERWSZY PRODUKT");
			    listaProdukt1.addItem("Produkt 1");
			    listaProdukt1.addItem("Produkt 2");
			    listaProdukt1.addItem("Produkt 3");
				add(listaProdukt1);	
				
				//--------------------------------------------------------------//
				podajNazweProdukt1Tekst = new JLabel("Podaj nazw� pierwszego produktu:");
				podajNazweProdukt1Tekst.setBounds(1150,50,200,20);
			    add(podajNazweProdukt1Tekst);
			    	
			    podajNazweProdukt1 = new JTextField("");
			    podajNazweProdukt1.setBounds(1360,50,200,20);
			    add(podajNazweProdukt1);
			    //--------------------------------------------------------------//
			    	
			    	
			    //--------------------------------------------------------------//
				podajIloscProdukt1Tekst = new JLabel("Podaj ilo�� pierwszego produktu:");
			    podajIloscProdukt1Tekst.setBounds(1150,75,230,20);
			    add(podajIloscProdukt1Tekst);
			    	
			    podajIloscProdukt1 = new JTextField("");
			    podajIloscProdukt1.setBounds(1360,75,200,20);
			    add(podajIloscProdukt1);
			    //--------------------------------------------------------------//
			    
			    //--------------------------------------------------------------//
			    podajMiareProduktu1Tekst = new JLabel("Podaj miar� pierwszego produktu:");
			    podajMiareProduktu1Tekst.setBounds(1150,100,230,20);
			    add(podajMiareProduktu1Tekst);
			    	
			    podajMiareProduktu1 = new JTextField("");
			    podajMiareProduktu1.setBounds(1360,100,200,20);
			    add(podajMiareProduktu1);
			    //--------------------------------------------------------------//		
			    	
			    //--------------------------------------------------------------//
			    podajCeneProdukt1Tekst = new JLabel("Podaj cen� pierwszego produktu:");
			    podajCeneProdukt1Tekst.setBounds(1150,125,230,20);
			    add(podajCeneProdukt1Tekst);
			    	
			    podajCeneProdukt1 = new JTextField("");
			    podajCeneProdukt1.setBounds(1360,125,200,20);
			    add(podajCeneProdukt1);
			    //--------------------------------------------------------------//
			    
			    //--------------------------------------------------------------//
			    stawkaPodatku1Tekst = new JLabel("Podaj stawk� podatku:");
			    stawkaPodatku1Tekst.setBounds(1150,150,230,20);
			    add(stawkaPodatku1Tekst);
			    	
			    stawkaPodatku1 = new JTextField("");
			    stawkaPodatku1.setBounds(1360,150,200,20);
			    add(stawkaPodatku1);
			    //--------------------------------------------------------------//
			    
			   	    	
			    
	//----------------------------DRUGI PRODUKT---------------------------------------------//
			    drugiProdukt = new JLabel("Dane drugiego produku");
			    drugiProdukt.setBounds(1150,175,200,20);
			    add(drugiProdukt);
			    
				//LISTA PRODUKTOW
			    listaProdukt2 = new JComboBox();
			    listaProdukt2.setBounds(1360,175,200,20);
			    listaProdukt2.addItem("WYBIERZ DRUGI PRODUKT");
			    listaProdukt2.addItem("Produkt 1");
			    listaProdukt2.addItem("Produkt 2");
			    listaProdukt2.addItem("Produkt 3");
				add(listaProdukt2);
				
			    //--------------------------------------------------------------//
			    podajNazweProdukt2Tekst = new JLabel("Podaj nazw� drugiego produktu:");
			    podajNazweProdukt2Tekst.setBounds(1150,200,200,20);
			    add(podajNazweProdukt2Tekst);
			    	    	
			    podajNazweProdukt2 = new JTextField("");
			    podajNazweProdukt2.setBounds(1360,200,200,20);
			    add(podajNazweProdukt2);
			    //--------------------------------------------------------------//
			    	    	
			    	    	
			    //--------------------------------------------------------------//
			    podajIloscProdukt2Tekst = new JLabel("Podaj ilo�� drugiego produktu:");
			    podajIloscProdukt2Tekst.setBounds(1150,225,230,20);
			    add(podajIloscProdukt2Tekst);
			    	    	
			    podajIloscProdukt2 = new JTextField("");
			   	podajIloscProdukt2.setBounds(1360,225,200,20);
			   	add(podajIloscProdukt2);
			   	//--------------------------------------------------------------//
			   	
			    //--------------------------------------------------------------//
			    podajMiareProduktu2Tekst = new JLabel("Podaj miar� drugiego produktu:");
			    podajMiareProduktu2Tekst.setBounds(1150,250,230,20);
			    add(podajMiareProduktu2Tekst);
			    	
			    podajMiareProduktu2 = new JTextField("");
			    podajMiareProduktu2.setBounds(1360,250,200,20);
			    add(podajMiareProduktu2);
			    //--------------------------------------------------------------//
			    	    	
			   	//--------------------------------------------------------------//
		    	podajCeneProdukt2Tekst = new JLabel("Podaj cen� drugiego produktu:");
		    	podajCeneProdukt2Tekst.setBounds(1150,275,230,20);
			    add(podajCeneProdukt2Tekst);
			    	    	
			    podajCeneProdukt2 = new JTextField("");
			    podajCeneProdukt2.setBounds(1360,275,200,20);
			    add(podajCeneProdukt2);
			     //--------------------------------------------------------------//
			  
			    //--------------------------------------------------------------//
			    stawkaPodatku2Tekst = new JLabel("Podaj stawk� podatku:");
			    stawkaPodatku2Tekst.setBounds(1150,300,230,20);
			    add(stawkaPodatku2Tekst);
			    	
			    stawkaPodatku2 = new JTextField("");
			    stawkaPodatku2.setBounds(1360,300,200,20);
			    add(stawkaPodatku2);
			    //--------------------------------------------------------------//
			    	
	//----------------------------TRZECI PRODUKT---------------------------------------------//
			    trzeciProdukt = new JLabel("Dane trzeciego produku");
			    trzeciProdukt.setBounds(1150,325,200,20);
			    add(trzeciProdukt);
			    
				//LISTA PRODUKTOW
			    listaProdukt3 = new JComboBox();
			    listaProdukt3.setBounds(1360,325,200,20);
			    listaProdukt3.addItem("WYBIERZ TRZECI PRODUKT");
			    listaProdukt3.addItem("Produkt 1");
			    listaProdukt3.addItem("Produkt 2");
			    listaProdukt3.addItem("Produkt 3");
				add(listaProdukt3);				
			    //--------------------------------------------------------------//
			    podajNazweProdukt3Tekst = new JLabel("Podaj nazw� trzeciego produktu:");
			    podajNazweProdukt3Tekst.setBounds(1150,350,200,20);
			    add(podajNazweProdukt3Tekst);
			    	    	
			    podajNazweProdukt3 = new JTextField("");
			   	podajNazweProdukt3.setBounds(1360,350,200,20);
			   	add(podajNazweProdukt3);
			   	//--------------------------------------------------------------//
			    	    	
			    	    	
			   	//--------------------------------------------------------------//
		    	podajIloscProdukt3Tekst = new JLabel("Podaj ilo�� trzeciego produktu:");
		    	podajIloscProdukt3Tekst.setBounds(1150,375,230,20);
			   	add(podajIloscProdukt3Tekst);
			   	    	
			    podajIloscProdukt3 = new JTextField("");
			    podajIloscProdukt3.setBounds(1360,375,200,20);
			    add(podajIloscProdukt3);
			    //--------------------------------------------------------------//
			    			
			    //--------------------------------------------------------------//
			    podajMiareProduktu3Tekst = new JLabel("Podaj miar� trzeciego produktu:");
			    podajMiareProduktu3Tekst.setBounds(1150,400,230,20);
			    add(podajMiareProduktu3Tekst);
			    	
			    podajMiareProduktu3 = new JTextField("");
			    podajMiareProduktu3.setBounds(1360,400,200,20);
			    add(podajMiareProduktu3);
			    //--------------------------------------------------------------//
			    
			    //--------------------------------------------------------------//
			    podajCeneProdukt3Tekst = new JLabel("Podaj cen� trzeciego produktu:");
			    podajCeneProdukt3Tekst.setBounds(1150,425,230,20);
			    add(podajCeneProdukt3Tekst);
			    	    	
			    podajCeneProdukt3 = new JTextField("");
			    podajCeneProdukt3.setBounds(1360,425,200,20);
			    add(podajCeneProdukt3);
			     //--------------------------------------------------------------//
			    
			    //--------------------------------------------------------------//
			    stawkaPodatku3Tekst = new JLabel("Podaj stawk� podatku:");
			    stawkaPodatku3Tekst.setBounds(1150,450,230,20);
			    add(stawkaPodatku3Tekst);
			    	
			    stawkaPodatku3 = new JTextField("");
			    stawkaPodatku3.setBounds(1360,450,200,20);
			    add(stawkaPodatku3);
			    //--------------------------------------------------------------//
			    
			    	
	//---------------------------CZWARTY PRODUKT---------------------------------------------//
			    czwartyProdukt = new JLabel("Dane czwartego produku");
			    czwartyProdukt.setBounds(1150,475,200,20);
			    add(czwartyProdukt);
			    
				//LISTA PRODUKTOW
			    listaProdukt4 = new JComboBox();
			    listaProdukt4.setBounds(1360,475,200,20);
			    listaProdukt4.addItem("WYBIERZ CZWARTY PRODUKT");
			    listaProdukt4.addItem("Produkt 1");
			    listaProdukt4.addItem("Produkt 2");
			    listaProdukt4.addItem("Produkt 3");
				add(listaProdukt4);	
				
			   	//--------------------------------------------------------------//
			   	podajNazweProdukt4Tekst = new JLabel("Podaj nazw� czwartego produktu:");
			   	podajNazweProdukt4Tekst.setBounds(1150,500,200,20);
			   	add(podajNazweProdukt4Tekst);
			       	
			   	podajNazweProdukt4 = new JTextField("");
			   	podajNazweProdukt4.setBounds(1360,500,200,20);
			   	add(podajNazweProdukt4);
			   	//--------------------------------------------------------------//
			    	    	
			    	    	
			    //--------------------------------------------------------------//
			   	podajIloscProdukt4Tekst = new JLabel("Podaj ilo�� czwartego produktu:");
			   	podajIloscProdukt4Tekst.setBounds(1150,525,230,20);
			   	add(podajIloscProdukt4Tekst);
			    	    	
			   	podajIloscProdukt4 = new JTextField("");
			   	podajIloscProdukt4.setBounds(1360,525,200,20);
			   	add(podajIloscProdukt4);
			   	//--------------------------------------------------------------//
			    
			   	//--------------------------------------------------------------//
			    podajMiareProduktu4Tekst = new JLabel("Podaj miar� czwartego produktu:");
			    podajMiareProduktu4Tekst.setBounds(1150,550,230,20);
			    add(podajMiareProduktu4Tekst);
			    	
			    podajMiareProduktu4 = new JTextField("");
			    podajMiareProduktu4.setBounds(1360,550,200,20);
			    add(podajMiareProduktu4);
			    //--------------------------------------------------------------//	
			    	    	
			   	//--------------------------------------------------------------//
			   	podajCeneProdukt4Tekst = new JLabel("Podaj cen� czwartego produktu:");
			   	podajCeneProdukt4Tekst.setBounds(1150,575,230,20);
			   	add(podajCeneProdukt4Tekst);
			   	    	
			   	podajCeneProdukt4 = new JTextField("");
			   	podajCeneProdukt4.setBounds(1360,575,200,20);
			   	add(podajCeneProdukt4);
			   	 //--------------------------------------------------------------//
			   	
			    //--------------------------------------------------------------//
			    stawkaPodatku4Tekst = new JLabel("Podaj stawk� podatku:");
			    stawkaPodatku4Tekst.setBounds(1150,600,230,20);
			    add(stawkaPodatku4Tekst);
			    	
			    stawkaPodatku4 = new JTextField("");
			    stawkaPodatku4.setBounds(1360,600,200,20);
			    add(stawkaPodatku4);
			    //--------------------------------------------------------------//
			    
			    	
	//---------------------------PIATY PRODUKT---------------------------------------------//
			    piatyProdukt = new JLabel("Dane pi�tego produku");
			    piatyProdukt.setBounds(1150,625,200,20);
			    add(piatyProdukt);
			    
				//LISTA PRODUKTOW
			    listaProdukt5 = new JComboBox();
			    listaProdukt5.setBounds(1360,625,200,20);
			    listaProdukt5.addItem("WYBIERZ PIATY PRODUKT");
			    listaProdukt5.addItem("Produkt 1");
			    listaProdukt5.addItem("Produkt 2");
			    listaProdukt5.addItem("Produkt 3");
				add(listaProdukt5);			
			   	//--------------------------------------------------------------//
			   	podajNazweProdukt5Tekst = new JLabel("Podaj nazw� pi�tego produktu:");
			   	podajNazweProdukt5Tekst.setBounds(1150,650,200,20);
			   	add(podajNazweProdukt5Tekst);
			    	    	
			   	podajNazweProdukt5 = new JTextField("");
			   	podajNazweProdukt5.setBounds(1360,650,200,20);
			   	add(podajNazweProdukt5);
			    //--------------------------------------------------------------//
			    	    	
			    	    	
			    //--------------------------------------------------------------//
			   	podajIloscProdukt5Tekst = new JLabel("Podaj ilo�� pi�tego produktu:");
			    podajIloscProdukt5Tekst.setBounds(1150,675,230,20);
			    add(podajIloscProdukt5Tekst);
			    	    	
			    podajIloscProdukt5 = new JTextField("");
			    podajIloscProdukt5.setBounds(1360,675,200,20);
			    add(podajIloscProdukt5);
			    //--------------------------------------------------------------//
			    			
			    //--------------------------------------------------------------//
			    podajMiareProduktu5Tekst = new JLabel("Podaj miar� pi�tego produktu:");
			    podajMiareProduktu5Tekst.setBounds(1150,700,230,20);
			    add(podajMiareProduktu5Tekst);
			    	
			    podajMiareProduktu5 = new JTextField("");
			    podajMiareProduktu5.setBounds(1360,700,200,20);
			    add(podajMiareProduktu5);
			    //--------------------------------------------------------------//
			    	    	
			   	//--------------------------------------------------------------//
			   	podajCeneProdukt5Tekst = new JLabel("Podaj cen� pi�tego produktu:");
			   	podajCeneProdukt5Tekst.setBounds(1150,725,230,20);
			    add(podajCeneProdukt5Tekst);
			    	    	
			    podajCeneProdukt5 = new JTextField("");
			    podajCeneProdukt5.setBounds(1360,725,200,20); 
			    add(podajCeneProdukt5);
			    //--------------------------------------------------------------//
			    
			    //--------------------------------------------------------------//
			    stawkaPodatku5Tekst = new JLabel("Podaj stawk� podatku:");
			    stawkaPodatku5Tekst.setBounds(1150,750,230,20);
			    add(stawkaPodatku5Tekst);
			    	
			    stawkaPodatku5 = new JTextField("");
			    stawkaPodatku5.setBounds(1360,750,200,20);
			    add(stawkaPodatku5);
			    //--------------------------------------------------------------//
    }
}