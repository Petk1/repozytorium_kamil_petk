package pl.programfaktury.first;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Jednorazowa_faktura extends JFrame implements Printable, ActionListener{

	JButton bSave;
	
	//DRUKOWANIE
	JPanel panelToPrint;
	JButton bDrukuj;
	
	
	JButton bOblicz;
	double razem_Kwota_bez_podatku;
	
	JLabel wyswietlRazemWartoscTowarow1ZL3;
	JLabel wyswietlRazemWartoscTowarow1GR3;
	
	JLabel wyswietlRazemWartoscTowarow1ZLKopia3;
	JLabel wyswietlRazemWartoscTowarow1GRKopia3;
	
//KWOTA PODATKU W ZALENOSCI OD OPODATKOWANIA	
	JLabel wyswietlWartoscPodatkuZL23;
	JLabel wyswietlWartoscPodatkuZL8;
	JLabel wyswietlWartoscPodatkuZL5;
	
	JLabel wyswietlWartoscPodatkuGR23;
	JLabel wyswietlWartoscPodatkuGR8;
	JLabel wyswietlWartoscPodatkuGR5;
	
	JLabel wyswietlWartoscPodatkuZL23Kopia;
	JLabel wyswietlWartoscPodatkuZL8Kopia;
	JLabel wyswietlWartoscPodatkuZL5Kopia;
	
	JLabel wyswietlWartoscPodatkuGR23Kopia;
	JLabel wyswietlWartoscPodatkuGR8Kopia;
	JLabel wyswietlWartoscPodatkuGR5Kopia;

	//LACZNA KWOTA PODATKU
	double razem_Kwota_podatku;
	
	JLabel wyswietlRazemWartoscPodatkuZL;
	JLabel wyswietlRazemWartoscPodatkuZLKopia;
	
	JLabel wyswietlRazemWartoscPodatkuGR;
	JLabel wyswietlRazemWartoscPodatkuGRKopia;
	
	//KWOTA WRAZ Z PODATKIE
	double wartoscZPodatkiem1Zl;
	double wartoscZPodatkiem1GR;
	
	double wartoscZPodatkiem2Zl;
	double wartoscZPodatkiem2GR;
	
	double wartoscZPodatkiem3Zl;
	double wartoscZPodatkiem3GR;
	
	double wartoscZPodatkiem4Zl;
	double wartoscZPodatkiem4GR;
	
	double wartoscZPodatkiem5Zl;
	double wartoscZPodatkiem5GR;
	
	JLabel wyswietlWartoscZPodatkiem1ZL;
	JLabel wyswietlWartoscZPodatkiem2ZL;
	JLabel wyswietlWartoscZPodatkiem3ZL;
	JLabel wyswietlWartoscZPodatkiem4ZL;
	JLabel wyswietlWartoscZPodatkiem5ZL;
	
	JLabel wyswietlWartoscZPodatkiem1GR;
	JLabel wyswietlWartoscZPodatkiem2GR;
	JLabel wyswietlWartoscZPodatkiem3GR;
	JLabel wyswietlWartoscZPodatkiem4GR;
	JLabel wyswietlWartoscZPodatkiem5GR;
	
	//WARTOSC WRAZ Z PODATKIEM KOPIA
	JLabel wyswietlWartoscZPodatkiem1ZLKopia;
	JLabel wyswietlWartoscZPodatkiem2ZLKopia;
	JLabel wyswietlWartoscZPodatkiem3ZLKopia;
	JLabel wyswietlWartoscZPodatkiem4ZLKopia;
	JLabel wyswietlWartoscZPodatkiem5ZLKopia;
	
	JLabel wyswietlWartoscZPodatkiem1GRKopia;
	JLabel wyswietlWartoscZPodatkiem2GRKopia;
	JLabel wyswietlWartoscZPodatkiem3GRKopia;
	JLabel wyswietlWartoscZPodatkiem4GRKopia;
	JLabel wyswietlWartoscZPodatkiem5GRKopia;
	
	//WARTOSC WRAZ Z PODATKIEM W ZALEZNOCI OD PODATKU
	double wartoscZPodatkiemZl23 = 0;
	double wartoscZPodatkiemZl8 = 0;
	double wartoscZPodatkiemZl5 = 0;
	double wartoscZPodatkiemZl0 = 0;

	
	JLabel wyswietlRazemWartoscPodatkuZL23;
	JLabel wyswietlRazemWartoscPodatkuZL8;
	JLabel wyswietlRazemWartoscPodatkuZL5;
	JLabel wyswietlRazemWartoscPodatkuZL0;
	
	JLabel wyswietlRazemWartoscPodatkuGR23;
	JLabel wyswietlRazemWartoscPodatkuGR8;
	JLabel wyswietlRazemWartoscPodatkuGR5;
	JLabel wyswietlRazemWartoscPodatkuGR0;
	
	JLabel wyswietlRazemWartoscPodatkuZL23Kopia;
	JLabel wyswietlRazemWartoscPodatkuZL8Kopia;
	JLabel wyswietlRazemWartoscPodatkuZL5Kopia;
	JLabel wyswietlRazemWartoscPodatkuZL0Kopia;
	
	JLabel wyswietlRazemWartoscPodatkuGR23Kopia;
	JLabel wyswietlRazemWartoscPodatkuGR8Kopia;
	JLabel wyswietlRazemWartoscPodatkuGR5Kopia;
	JLabel wyswietlRazemWartoscPodatkuGR0Kopia;
	
	//LACZNA WARTOSC WRAZ Z PODATKIEM
	double laczna_Kwota_Wraz_Z_Podatkiem = 0;
	double obliczona_laczna_kwota_wraz_z_podatkiem = 0;
	
	JLabel wyswietlLacznaWartoscWrazZPodatkiemZl;
	JLabel wyswietlLacznaWartoscWrazZPodatkiemGR;

	JLabel wyswietlLacznaWartoscWrazZPodatkiemZlKopia;
	JLabel wyswietlLacznaWartoscWrazZPodatkiemGRKopia;
	
	//DO ZAPLATY-----------//
	JLabel doZaplatyZl;
	JLabel doZaplatyGR;
	
	JLabel doZaplatyZlKopia;
	JLabel doZaplatyGRKopia;
	
	//LiczbyNaSlowa 
	Font wygladDane = new Font("Arial", Font.BOLD, 10);
	
		int zlotowkiJednosci;     
		int zlotowkiJednosciDwochProduktow;  
			String[] jednosci = { "", "jeden ", "dwa ", "trzy ", "cztery ",           
					"pi ", "sze ", "siedem ", "osiem ", "dziewi ", };           
		JLabel slownieZlotowkiJednosci;                                           
			                                                                          
			                                                                          
		int zlotowkiDziesiatkiV1;  
		int zlotowkiDziesiatkiV1DwochProduktow; 
			String[] dziesiatkiV1= {"", "jedenacie ", "dwanacie ", "trzynacie ",   
					"czternacie ", "pitnacie ", "szesnacie ", "siedemnacie ",    
					"osiemnacie ", "dziewitnacie ", };                             
		JLabel slownieZlotowkiDziesiatki;                                         
			                                                                          
		int zlotowkiDziesiatki;            
		int zlotowkiDziesiatkiDwochProduktow;     
			String[] dziesiatki = { "", "dziesi ", "dwadziecia ",                  
					"trzydzieci ", "czterdzieci ", "pidziesit ",                 
					"szedziesit ", "siedemdziesit ", "osiemdziesit ",            
					"dziewidziesit ", };                                           
			                                                                          
		int zlotowkiSetki;  
		int zlotowkiSetkiDwochProduktow;
			String[] setki = { "", "sto ", "dwiecie ", "trzysta ", "czterysta ",     
					"piset ", "szeset ", "siedemset ", "osiemset ",               
					"dziewiset ", };                                                
		JLabel slownieZlotowkiSetki;                                              
		                                                                              
		int zlotowkiTysiace;             
		int zlotowkiTysiaceDwochProduktow;     
		JLabel slownieZlotowkiTysiace;                                            
			                                                                          
			String[] tysiaceKoncowki = { "", "tysic" , "tysice", "tysicy"};        
		JLabel slownieTysiaceKoncowki;                                            
			                                                                          
		JLabel zlote;                                                             
		//GROSZE                                                                      
		JLabel groszeSlownie;
		
		//LiczbyNaSlowaKopia                    
		JLabel zloteKopia;                  
		JLabel slownieZlotowkiJednosciKopia;
		JLabel slownieZlotowkiDziesiatkiKopia;
		JLabel slownieZlotowkiSetkiKopia;   
		JLabel slownieZlotowkiTysiaceKopia; 
		JLabel slownieTysiaceKoncowkiKopia; 
		JLabel groszeSlownieKopia;   
	
	
//OPCJE WPROWADZANIA DANYCH
	
		//NR FAKTURY I DATA
			JLabel nrFakturyTekst;
			JLabel dataTekst;
			JTextField nrFaktury;
			JTextField dataDzien;
			JTextField dataMiesiac;
			JTextField dataRok;
			JLabel dataPrzyklad;
			
		//DANE FIRMY
			JLabel nazwaFirmyTekst;
			JLabel miejscowoscWystawieniaTekst;
			JLabel adresFirmyTekst;
			JLabel nipTekst;
			JLabel miaraProduktuTekst;
			JLabel sposobZaplatyTekst;
			JLabel terminZaplatyTekst;
			JLabel bankTekst;
			JLabel nrKontaTekst;	
			JTextField nazwaFirmy;
			JTextField miejscowoscWystawienia;
			JTextField adresFirmy;
			JTextField nip;
			JTextField miaraProduktu;
			JTextField sposobZaplaty;
			JTextField terminZaplaty;
			JTextField bank;
			JTextField nrKonta;
			
		//PIERWSZY PRODUKT//
			JComboBox listaProdukt1;
			JLabel pierwszyProdukt;
			JLabel podajNazweProdukt1Tekst;
			JLabel podajIloscProdukt1Tekst;
			JLabel podajMiareProduktu1Tekst;
			JLabel podajCeneProdukt1Tekst;
			JLabel stawkaPodatku1Tekst;
			JTextField podajNazweProdukt1;
			JTextField podajIloscProdukt1;
			JTextField podajMiareProduktu1;
			JTextField podajCeneProdukt1Zl;
			JTextField podajCeneProdukt1GR;
			
			JTextField stawkaPodatku1;
			
			int stawkaPodatku1Wartosc;
			//WARTO TOWARW
			int iloscProduktu1;
			double wartoscTowarow1ZL;
			double wartoscTowarow1GR;
			JLabel wyswietlWartoscTowarow1ZL;
			JLabel wyswietlWartoscTowarow1GR;
			
			JLabel wyswietlWartoscTowarow1ZL23;
			JLabel wyswietlWartoscTowarow1ZL8;
			JLabel wyswietlWartoscTowarow1ZL5;
			JLabel wyswietlWartoscTowarow1ZL0;
			
			JLabel wyswietlWartoscTowarow1GR23;
			JLabel wyswietlWartoscTowarow1GR8;
			JLabel wyswietlWartoscTowarow1GR5;
			JLabel wyswietlWartoscTowarow1GR0;
			
			//WARTOSC PODATKU
			double wartoscPodatku1ZL;
			double wartoscPodatku1GR;
			
			JLabel wyswietlWartoscPodatku1ZL;
			JLabel wyswietlWartoscPodatku1GR;
			
			//WARTO TOWARW KOPIA
			JLabel wyswietlWartoscTowarow1ZLKopia;
			JLabel wyswietlWartoscTowarow1GRKopia;
			
			JLabel wyswietlWartoscTowarow1ZLKopia23;
			JLabel wyswietlWartoscTowarow1ZLKopia8;
			JLabel wyswietlWartoscTowarow1ZLKopia5;
			JLabel wyswietlWartoscTowarow1ZLKopia0;
			
			JLabel wyswietlWartoscTowarow1GRKopia23;
			JLabel wyswietlWartoscTowarow1GRKopia8;
			JLabel wyswietlWartoscTowarow1GRKopia5;
			JLabel wyswietlWartoscTowarow1GRKopia0;
			
			//WARTOSC PODATKU KOPIA
			JLabel wyswietlWartoscPodatku1ZLKopia;
			JLabel wyswietlWartoscPodatku1GRKopia;
			
		//DRUGI PRODUKT// 
			JComboBox listaProdukt2;
			JLabel drugiProdukt;
			JLabel podajNazweProdukt2Tekst;
			JLabel podajIloscProdukt2Tekst;
			JLabel podajMiareProduktu2Tekst;
			JLabel podajCeneProdukt2Tekst;
			JLabel stawkaPodatku2Tekst;
			JTextField podajNazweProdukt2;
			JTextField podajIloscProdukt2;
			JTextField podajMiareProduktu2;
			JTextField podajCeneProdukt2Zl;
			JTextField podajCeneProdukt2GR;
			JTextField stawkaPodatku2;
			
			int stawkaPodatku2Wartosc;
			//WARTO TOWARW
			int iloscProduktu2;
			double wartoscTowarow2ZL;
			double wartoscTowarow2GR;
			JLabel wyswietlWartoscTowarow2ZL;
			JLabel wyswietlWartoscTowarow2GR;
			
			//WARTOSC PODATKU 
			double wartoscPodatku2ZL;
			double wartoscPodatku2GR;
			
			JLabel wyswietlWartoscPodatku2ZL;
			JLabel wyswietlWartoscPodatku2GR;
			
			//WARTO TOWARW KOPIA
			JLabel wyswietlWartoscTowarow2ZLKopia;
			JLabel wyswietlWartoscTowarow2GRKopia;
			
			//WARTOSC PODATKU KOPIA
			JLabel wyswietlWartoscPodatku2ZLKopia;
			JLabel wyswietlWartoscPodatku2GRKopia;
			   
		 //TRZECI PRODUKT// 
			JComboBox listaProdukt3;
			JLabel trzeciProdukt;
			JLabel podajNazweProdukt3Tekst;
			JLabel podajIloscProdukt3Tekst;
			JLabel podajMiareProduktu3Tekst;
			JLabel podajCeneProdukt3Tekst;
			JLabel stawkaPodatku3Tekst;
			JTextField podajNazweProdukt3;
			JTextField podajIloscProdukt3;
			JTextField podajMiareProduktu3;
			JTextField podajCeneProdukt3Zl;
			JTextField podajCeneProdukt3GR;
			JTextField stawkaPodatku3;
			
			int stawkaPodatku3Wartosc;
			//WARTO TOWARW
			int iloscProduktu3;			
			double wartoscTowarow3ZL;
			double wartoscTowarow3GR;
			JLabel wyswietlWartoscTowarow3ZL;
			JLabel wyswietlWartoscTowarow3GR;
			
			//WARTOSC PODATKU 
			double wartoscPodatku3ZL;
			double wartoscPodatku3GR;
			
			JLabel wyswietlWartoscPodatku3ZL;
			JLabel wyswietlWartoscPodatku3GR;
			
			//WARTO TOWARW KOPIA
			JLabel wyswietlWartoscTowarow3ZLKopia;
			JLabel wyswietlWartoscTowarow3GRKopia;
			
			//WARTOSC PODATKU
			JLabel wyswietlWartoscPodatku3ZLKopia;
			JLabel wyswietlWartoscPodatku3GRKopia;
			   
		//CZWARTY PRODUKT// 
			JComboBox listaProdukt4;
			JLabel czwartyProdukt;
			JLabel podajNazweProdukt4Tekst;
			JLabel podajIloscProdukt4Tekst;
			JLabel podajMiareProduktu4Tekst;
			JLabel podajCeneProdukt4Tekst;
			JLabel stawkaPodatku4Tekst;
			JTextField podajNazweProdukt4;
			JTextField podajIloscProdukt4;
			JTextField podajMiareProduktu4;
			JTextField podajCeneProdukt4Zl;
			JTextField podajCeneProdukt4GR;
			JTextField stawkaPodatku4;
			
			int stawkaPodatku4Wartosc;
			//WARTO TOWARW
			int iloscProduktu4;
			double wartoscTowarow4ZL;
			double wartoscTowarow4GR;
			JLabel wyswietlWartoscTowarow4ZL;
			JLabel wyswietlWartoscTowarow4GR;
			
			//WARTOSC PODATKU
			double wartoscPodatku4ZL;
			double wartoscPodatku4GR;
			
			JLabel wyswietlWartoscPodatku4ZL;
			JLabel wyswietlWartoscPodatku4GR;
			
			//WARTO TOWARW KOPIA
			JLabel wyswietlWartoscTowarow4ZLKopia;
			JLabel wyswietlWartoscTowarow4GRKopia;
			
			//WARTOSC PODATKU KOPIA
			JLabel wyswietlWartoscPodatku4ZLKopia;
			JLabel wyswietlWartoscPodatku4GRKopia;
			
		//PIATY PRODUKT// 
			JComboBox listaProdukt5;
			JLabel piatyProdukt;
			JLabel podajNazweProdukt5Tekst;
			JLabel podajIloscProdukt5Tekst;
			JLabel podajMiareProduktu5Tekst;
			JLabel podajCeneProdukt5Tekst;
			JLabel stawkaPodatku5Tekst;
			JTextField podajNazweProdukt5;
			JTextField podajIloscProdukt5;
			JTextField podajMiareProduktu5;
			JTextField podajCeneProdukt5Zl;
			JTextField podajCeneProdukt5GR;
			JTextField stawkaPodatku5;
			
			int stawkaPodatku5Wartosc;
			//WARTO TOWARW
			int iloscProduktu5;
			double wartoscTowarow5ZL;
			double wartoscTowarow5GR;
			JLabel wyswietlWartoscTowarow5ZL;
			JLabel wyswietlWartoscTowarow5GR;
			
			//WARTOSC PODATKU
			double wartoscPodatku5ZL;
			double wartoscPodatku5GR;
			
			JLabel wyswietlWartoscPodatku5ZL;
			JLabel wyswietlWartoscPodatku5GR;
			
			//WARTO TOWARW KOPIA
			JLabel wyswietlWartoscTowarow5ZLKopia;
			JLabel wyswietlWartoscTowarow5GRKopia;
			
			//WARTOSC PODATKU
			JLabel wyswietlWartoscPodatku5ZLKopia;
			JLabel wyswietlWartoscPodatku5GRKopia;
			
			//WARTOSCI W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA - KWOTA BEZ PODATKU
			double Kwota_bez_podatku_23 = 0;
			double Kwota_bez_podatku_8 = 0;
			double Kwota_bez_podatku_5 = 0;
			double Kwota_bez_podatku_0 = 0;
			
			//WARTOSCI W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA - KWOTA PODATKU
			double Kwota_podatku_23 = 0;
			double Kwota_podatku_8 = 0;
			double Kwota_podatku_5 = 0;
			double Kwota_podatku_0 = 0;


	//--------------------------------------------------------------//
	//----------------WYWIETLANIE NA PODGLADZIE--------------------//
	//--------------------------------------------------------------//
			
	//-----------------------------DANE-----------------------------//
			JButton bWyswietlDane;	
			JComboBox zapisaneFirmy;
			
			JLabel wNrFaktury;
			JLabel wDataDzien;
			JLabel wDataMiesiac;
			JLabel wDataRok;
			JLabel wNazwaFirmy;
			JLabel wAdres;
			JLabel wNipFirmy;
			JLabel wMiejscowosc;
			JLabel wBank;
			JLabel wNrKontaBankowego;
			JLabel wSposobZaplaty;
			JLabel wTerminZaplaty;
			JLabel data2;
			
			JLabel wNrFakturyKopia;
			JLabel wDataDzienKopia;
			JLabel wDataMiesiacKopia;
			JLabel wDataRokKopia;
			JLabel wNazwaFirmyKopia;
			JLabel wAdresKopia;
			JLabel wNipFirmyKopia;
			JLabel wMiejscowoscKopia;
			JLabel wBankKopia;
			JLabel wNrKontaBankowegoKopia;
			JLabel wSposobZaplatyKopia;
			JLabel wTerminZaplatyKopia;
			JLabel data2Kopia;
	//--------------------------------------------------------------//		
			JButton bWyswietl1;		

			JLabel lp1;
			JLabel wNazwa1;
			JLabel wMiara1;
			JLabel wIlosc1;
			JLabel wCena1;	
			JLabel wCena1GR;
			JLabel wStawka1;
			JLabel przecinek;
			
			JLabel lp1Kopia;
			JLabel wNazwa1Kopia;
			JLabel wMiara1Kopia;
			JLabel wIlosc1Kopia;
			JLabel wCena1Kopia;	
			JLabel wCena1GRKopia;
			JLabel wStawka1Kopia;

		//--------------------------------------------------------------//
			JButton bWyswietl2;
			
			JLabel lp2;
			JLabel wNazwa2;
			JLabel wMiara2;
			JLabel wIlosc2;
			JLabel wCena2;	
			JLabel wCena2GR;
			JLabel wStawka2;
			
			JLabel lp2Kopia;
			JLabel wNazwa2Kopia;
			JLabel wMiara2Kopia;
			JLabel wIlosc2Kopia;
			JLabel wCena2Kopia;	
			JLabel wCena2GRKopia;
			JLabel wStawka2Kopia;
			
		//--------------------------------------------------------------//
			JButton bWyswietl3;
			
			JLabel lp3;
			JLabel wNazwa3;
			JLabel wMiara3;
			JLabel wIlosc3;
			JLabel wCena3;	
			JLabel wCena3GR;
			JLabel wStawka3;
			
			JLabel lp3Kopia;
			JLabel wNazwa3Kopia;
			JLabel wMiara3Kopia;
			JLabel wIlosc3Kopia;
			JLabel wCena3Kopia;	
			JLabel wCena3GRKopia;
			JLabel wStawka3Kopia;
			
		//--------------------------------------------------------------//
			JButton bWyswietl4;
			
			JLabel lp4;
			JLabel wNazwa4;
			JLabel wMiara4;
			JLabel wIlosc4;
			JLabel wCena4;	
			JLabel wCena4GR;
			JLabel wStawka4;
			
			JLabel lp4Kopia;
			JLabel wNazwa4Kopia;
			JLabel wMiara4Kopia;
			JLabel wIlosc4Kopia;
			JLabel wCena4Kopia;	
			JLabel wCena4GRKopia;
			JLabel wStawka4Kopia;

		//--------------------------------------------------------------//
			JButton bWyswietl5;
			
			JLabel lp5;
			JLabel wNazwa5;
			JLabel wMiara5;
			JLabel wIlosc5;
			JLabel wCena5;	
			JLabel wCena5GR;
			JLabel wStawka5;
			
			JLabel lp5Kopia;
			JLabel wNazwa5Kopia;
			JLabel wMiara5Kopia;
			JLabel wIlosc5Kopia;
			JLabel wCena5Kopia;	
			JLabel wCena5GRKopia;
			JLabel wStawka5Kopia;

		    JLabel BDO;
		    JLabel BDOkopia;
			
    public Jednorazowa_faktura() {
//PARAMETRY OKNA
		setTitle("Jednorazowa faktura VAT");
		ObrazPanel obrazPanel = new ObrazPanel();
	   	add(obrazPanel);
    	obrazPanel.setBounds(0,0,700,900);
    	setSize(1700, 1040);
		setLayout(null);

				
//OPCJE WPROWADZANIA DANYCH	
		//----------------------------ZAPISANE FIRMY---------------------------------------------//    
		//LISTA FIRM
		zapisaneFirmy = new JComboBox();
		zapisaneFirmy.setBounds(700,25,200,20);
		zapisaneFirmy.addItem("WYBIERZ PIERWSZY PRODUKT");
	    
	    File firma = new File("Firmy\\Licznik\\licznik.txt");
	    Scanner wybrana = null;
		try {
			wybrana = new Scanner(firma);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	    String wybranaFirmaTekst = wybrana.nextLine();
	    int licznik = Integer.valueOf(wybranaFirmaTekst);
	    //System.out.println(licznik); //TEST
		
		for(int i = 1; i<licznik; i++) {
			zapisaneFirmy.addItem("Firma " + i);
		}
		add(zapisaneFirmy);
		zapisaneFirmy.addActionListener(this);
	    		
		//--------------------------------------------------------------//
		//NR BDO	
		
    	BDO = new JLabel("BDO: 000325517");
    	BDO.setBounds(340,374,200,20);
    	obrazPanel.add(BDO);
    	
    	BDOkopia = new JLabel("BDO: 000325517");
    	BDOkopia.setBounds(340,795,200,20);
    	obrazPanel.add(BDOkopia);
		
		//NR FAKTURY I DATA
		//--------------------------------------------------------------//
		nrFakturyTekst = new JLabel("Podaj numer faktury VAT:");
		nrFakturyTekst.setBounds(700,50,200,20);
	    add(nrFakturyTekst);
	    	
	    nrFaktury = new JTextField("");
	    nrFaktury.setBounds(910,50,200,20);
	    add(nrFaktury);
		//--------------------------------------------------------------//
	    
		//--------------------------------------------------------------//
		dataTekst = new JLabel("Podaj dat:");
		dataTekst.setBounds(700,75,200,20);
	    add(dataTekst);
	    	
	    dataDzien = new JTextField("");
	    dataDzien.setBounds(910,75,30,20);
	    add(dataDzien);
	    
	    dataMiesiac = new JTextField("");
	    dataMiesiac.setBounds(945,75,30,20);
	    add(dataMiesiac);
	    
	    dataRok = new JTextField("");
	    dataRok.setBounds(980,75,30,20);
	    add(dataRok);
	    
		dataPrzyklad = new JLabel("dd-mm-rr");
		dataPrzyklad.setBounds(1015,75,200,20);
	    add(dataPrzyklad);
		//--------------------------------------------------------------//

	    
//DANE FIRMY
		//--------------------------------------------------------------//
	    nazwaFirmyTekst = new JLabel("Podaj nazw firmy:");
	    nazwaFirmyTekst.setBounds(700,100,150,20);
	    add(nazwaFirmyTekst);
	    	
	    nazwaFirmy = new JTextField("");
	    nazwaFirmy.setBounds(910,100,200,20);
	    add(nazwaFirmy);
	    //--------------------------------------------------------------//
	    
	    //--------------------------------------------------------------//
	    nipTekst = new JLabel("Podaj NIP firmy:");
	    nipTekst.setBounds(700,125,150,20);
	    add(nipTekst);
	    	
	    nip= new JTextField("");
	    nip.setBounds(910,125,200,20);
	    add(nip);
	    //--------------------------------------------------------------//
	  
	    //--------------------------------------------------------------//    
	    adresFirmyTekst = new JLabel("Podaj adres firmy:");
	    adresFirmyTekst.setBounds(700,150,150,20);
	    add(adresFirmyTekst);

	    adresFirmy= new JTextField("");
	    adresFirmy.setBounds(910,150,200,20);
	    add(adresFirmy);
	    //--------------------------------------------------------------//
	    
//DANE WYSTAWIAJACEGO FAKTURE	    
	    
		//--------------------------------------------------------------//
	    miejscowoscWystawieniaTekst = new JLabel("Miejscowo wystawienia faktury:");
	    miejscowoscWystawieniaTekst.setBounds(700,175,200,20);
	    add(miejscowoscWystawieniaTekst);
	    	
	    miejscowoscWystawienia = new JTextField("");
	    miejscowoscWystawienia.setBounds(910,175,200,20);
	    add(miejscowoscWystawienia);
	    //--------------------------------------------------------------//
		
		//--------------------------------------------------------------//
	    bankTekst = new JLabel("Nazwa Twojego banku:");
	    bankTekst.setBounds(700,200,150,20);
	    add(bankTekst);
	    	
	    bank = new JTextField("");
	    bank.setBounds(910,200,200,20);
	    add(bank);
	    //--------------------------------------------------------------//
	    
	  //--------------------------------------------------------------//
	    nrKontaTekst = new JLabel("Twj numer konta bankowego:");
	    nrKontaTekst.setBounds(700,225,200,20);
	    add(nrKontaTekst);
	    	
	    nrKonta = new JTextField("");
	    nrKonta.setBounds(910,225,200,20);
	    add(nrKonta);
	    //--------------------------------------------------------------//
	    
//DANE ZAPLATY
		//--------------------------------------------------------------//
	    sposobZaplatyTekst = new JLabel("Sposb zapaty:");
	    sposobZaplatyTekst.setBounds(700,250,150,20);
	    add(sposobZaplatyTekst);
	    	
	    sposobZaplaty = new JTextField("");
	    sposobZaplaty.setBounds(910,250,200,20);
	    add(sposobZaplaty);
	    //--------------------------------------------------------------//
	    
		//--------------------------------------------------------------//
	    terminZaplatyTekst = new JLabel("Termin zapaty:");
	    terminZaplatyTekst.setBounds(700,275,150,20);
	    add(terminZaplatyTekst);
	    	
	    terminZaplaty = new JTextField("");
	    terminZaplaty.setBounds(910,275,200,20);
	    add(terminZaplaty);
	    //--------------------------------------------------------------//
	    
	    //-----------------------PRZYCISK-------------------------------//
	    bWyswietlDane = new JButton("Akceptuj Dane");
	    bWyswietlDane.setBounds(910,310,150,20);
	    add(bWyswietlDane);
	    bWyswietlDane.addActionListener(this);
		
//----------------------------PIERWSZY PRODUKT---------------------------------------------//
			pierwszyProdukt = new JLabel("Dane pierwszego produku");
			pierwszyProdukt.setBounds(1150,25,200,20);
		    add(pierwszyProdukt);
		    
			//LISTA PRODUKTOW
		    listaProdukt1 = new JComboBox();
		    listaProdukt1.setBounds(1360,25,200,20);
		    listaProdukt1.addItem("WYBIERZ PIERWSZY PRODUKT");
		    
		    File file = new File("Produkty\\Licznik\\licznik.txt");
		    Scanner in = null;
			try {
				in = new Scanner(file);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		    String zawartosc = in.nextLine();
		    int numer = Integer.valueOf(zawartosc);
		    //System.out.println(numer); //TEST
			
			for(int i = 1; i<numer; i++) {
				listaProdukt1.addItem("Produkt " + i);
			}
			add(listaProdukt1);
			listaProdukt1.addActionListener(this);
		    		
			//--------------------------------------------------------------//
			podajNazweProdukt1Tekst = new JLabel("Podaj nazw pierwszego produktu:");
			podajNazweProdukt1Tekst.setBounds(1150,50,200,20);
		    add(podajNazweProdukt1Tekst);
		    	
		    podajNazweProdukt1 = new JTextField("");
		    podajNazweProdukt1.setBounds(1360,50,200,20);
		    add(podajNazweProdukt1);
		    //--------------------------------------------------------------//
		    	    	
		    //--------------------------------------------------------------//
			podajIloscProdukt1Tekst = new JLabel("Podaj ilo pierwszego produktu:");
		    podajIloscProdukt1Tekst.setBounds(1150,75,230,20);
		    add(podajIloscProdukt1Tekst);
		    	
		    podajIloscProdukt1 = new JTextField("");
		    podajIloscProdukt1.setBounds(1360,75,200,20);
		    add(podajIloscProdukt1);
		    //--------------------------------------------------------------//
		    
		    //--------------------------------------------------------------//
		    podajMiareProduktu1Tekst = new JLabel("Podaj miar pierwszego produktu:");
		    podajMiareProduktu1Tekst.setBounds(1150,100,230,20);
		    add(podajMiareProduktu1Tekst);
		    	
		    podajMiareProduktu1 = new JTextField("");
		    podajMiareProduktu1.setBounds(1360,100,200,20);
		    add(podajMiareProduktu1);
		    //--------------------------------------------------------------//		
		    	
		    //--------------------------------------------------------------//
		    podajCeneProdukt1Tekst = new JLabel("Podaj cen pierwszego produktu:");
		    podajCeneProdukt1Tekst.setBounds(1150,125,230,20);
		    add(podajCeneProdukt1Tekst);
		    	
		    podajCeneProdukt1Zl = new JTextField("");
		    podajCeneProdukt1Zl.setBounds(1360,125,40,20);
		    add(podajCeneProdukt1Zl);
		    
		    przecinek = new JLabel(",");
		    przecinek.setBounds(1403,125,40,20);
		    add(przecinek);
		    
		    podajCeneProdukt1GR = new JTextField("");
		    podajCeneProdukt1GR.setBounds(1407,125,40,20);
		    add(podajCeneProdukt1GR);
		    //--------------------------------------------------------------//
		    
		    //--------------------------------------------------------------//
		    stawkaPodatku1Tekst = new JLabel("Podaj stawk podatku:");
		    stawkaPodatku1Tekst.setBounds(1150,150,230,20);
		    add(stawkaPodatku1Tekst);
		    	
		    stawkaPodatku1 = new JTextField("");
		    stawkaPodatku1.setBounds(1360,150,200,20);
		    add(stawkaPodatku1);
		    //--------------------------------------------------------------//
		    
		    
		    bWyswietl1 = new JButton("Akceptuj");
		    bWyswietl1.setBounds(1575,150,90,20);
		    add(bWyswietl1);
		    bWyswietl1.addActionListener(this);
		    
//----------------------------DRUGI PRODUKT---------------------------------------------//
		    drugiProdukt = new JLabel("Dane drugiego produku");
		    drugiProdukt.setBounds(1150,175,200,20);
		    add(drugiProdukt);
		    
			//LISTA PRODUKTOW
		    listaProdukt2 = new JComboBox();
		    listaProdukt2.setBounds(1360,175,200,20);
		    listaProdukt2.addItem("WYBIERZ DRUGI PRODUKT");		    
			
			for(int i = 1; i<numer; i++) {
				listaProdukt2.addItem("Produkt " + i);
			}
			add(listaProdukt2);
			listaProdukt2.addActionListener(this);
			
		    //--------------------------------------------------------------//
		    podajNazweProdukt2Tekst = new JLabel("Podaj nazw drugiego produktu:");
		    podajNazweProdukt2Tekst.setBounds(1150,200,200,20);
		    add(podajNazweProdukt2Tekst);
		    	    	
		    podajNazweProdukt2 = new JTextField("");
		    podajNazweProdukt2.setBounds(1360,200,200,20);
		    add(podajNazweProdukt2);
		    //--------------------------------------------------------------//
		    	    	
		    	    	
		    //--------------------------------------------------------------//
		    podajIloscProdukt2Tekst = new JLabel("Podaj ilo drugiego produktu:");
		    podajIloscProdukt2Tekst.setBounds(1150,225,230,20);
		    add(podajIloscProdukt2Tekst);
		    	    	
		    podajIloscProdukt2 = new JTextField("");
		   	podajIloscProdukt2.setBounds(1360,225,200,20);
		   	add(podajIloscProdukt2);
		   	//--------------------------------------------------------------//
		   	
		    //--------------------------------------------------------------//
		    podajMiareProduktu2Tekst = new JLabel("Podaj miar drugiego produktu:");
		    podajMiareProduktu2Tekst.setBounds(1150,250,230,20);
		    add(podajMiareProduktu2Tekst);
		    	
		    podajMiareProduktu2 = new JTextField("");
		    podajMiareProduktu2.setBounds(1360,250,200,20);
		    add(podajMiareProduktu2);
		    //--------------------------------------------------------------//
		    	    	
		   	//--------------------------------------------------------------//
	    	podajCeneProdukt2Tekst = new JLabel("Podaj cen drugiego produktu:");
	    	podajCeneProdukt2Tekst.setBounds(1150,275,230,20);
		    add(podajCeneProdukt2Tekst);
		    
		    podajCeneProdukt2Zl = new JTextField("");
		    podajCeneProdukt2Zl.setBounds(1360,275,40,20);
		    add(podajCeneProdukt2Zl);
		    
		    przecinek = new JLabel(",");
		    przecinek.setBounds(1403,275,40,20);
		    add(przecinek);
		    
		    podajCeneProdukt2GR = new JTextField("");
		    podajCeneProdukt2GR.setBounds(1407,275,40,20);
		    add(podajCeneProdukt2GR);
		     //--------------------------------------------------------------//
		  
		    //--------------------------------------------------------------//
		    stawkaPodatku2Tekst = new JLabel("Podaj stawk podatku:");
		    stawkaPodatku2Tekst.setBounds(1150,300,230,20);
		    add(stawkaPodatku2Tekst);
		    	
		    stawkaPodatku2 = new JTextField("");
		    stawkaPodatku2.setBounds(1360,300,200,20);
		    add(stawkaPodatku2);
		    //--------------------------------------------------------------//
		    
		    bWyswietl2 = new JButton("Akceptuj");
		    bWyswietl2.setBounds(1575,300,90,20);
		    add(bWyswietl2);
		    bWyswietl2.addActionListener(this);
		    	
//----------------------------TRZECI PRODUKT---------------------------------------------//
		    trzeciProdukt = new JLabel("Dane trzeciego produku");
		    trzeciProdukt.setBounds(1150,325,200,20);
		    add(trzeciProdukt);
		    
			//LISTA PRODUKTOW
		    listaProdukt3 = new JComboBox();
		    listaProdukt3.setBounds(1360,325,200,20);
		    listaProdukt3.addItem("WYBIERZ TRZECI PRODUKT");		    
			
			for(int i = 1; i<numer; i++) {
				listaProdukt3.addItem("Produkt " + i);
			}
			add(listaProdukt3);
			listaProdukt3.addActionListener(this);
		    //--------------------------------------------------------------//
		    podajNazweProdukt3Tekst = new JLabel("Podaj nazw trzeciego produktu:");
		    podajNazweProdukt3Tekst.setBounds(1150,350,200,20);
		    add(podajNazweProdukt3Tekst);
		    	    	
		    podajNazweProdukt3 = new JTextField("");
		   	podajNazweProdukt3.setBounds(1360,350,200,20);
		   	add(podajNazweProdukt3);
		   	//--------------------------------------------------------------//
		    	    	
		    	    	
		   	//--------------------------------------------------------------//
	    	podajIloscProdukt3Tekst = new JLabel("Podaj ilo trzeciego produktu:");
	    	podajIloscProdukt3Tekst.setBounds(1150,375,230,20);
		   	add(podajIloscProdukt3Tekst);
		   	    	
		    podajIloscProdukt3 = new JTextField("");
		    podajIloscProdukt3.setBounds(1360,375,200,20);
		    add(podajIloscProdukt3);
		    //--------------------------------------------------------------//
		    			
		    //--------------------------------------------------------------//
		    podajMiareProduktu3Tekst = new JLabel("Podaj miar trzeciego produktu:");
		    podajMiareProduktu3Tekst.setBounds(1150,400,230,20);
		    add(podajMiareProduktu3Tekst);
		    	
		    podajMiareProduktu3 = new JTextField("");
		    podajMiareProduktu3.setBounds(1360,400,200,20);
		    add(podajMiareProduktu3);
		    //--------------------------------------------------------------//
		    
		    //--------------------------------------------------------------//
		    podajCeneProdukt3Tekst = new JLabel("Podaj cen trzeciego produktu:");
		    podajCeneProdukt3Tekst.setBounds(1150,425,230,20);
		    add(podajCeneProdukt3Tekst);
		    	
		    podajCeneProdukt3Zl = new JTextField("");
		    podajCeneProdukt3Zl.setBounds(1360,425,40,20);
		    add(podajCeneProdukt3Zl);
		    
		    przecinek = new JLabel(",");
		    przecinek.setBounds(1403,425,40,20);
		    add(przecinek);
		    
		    podajCeneProdukt3GR = new JTextField("");
		    podajCeneProdukt3GR.setBounds(1407,425,40,20);
		    add(podajCeneProdukt3GR);
		     //--------------------------------------------------------------//
		    
		    //--------------------------------------------------------------//
		    stawkaPodatku3Tekst = new JLabel("Podaj stawk podatku:");
		    stawkaPodatku3Tekst.setBounds(1150,450,230,20);
		    add(stawkaPodatku3Tekst);
		    	
		    stawkaPodatku3 = new JTextField("");
		    stawkaPodatku3.setBounds(1360,450,200,20);
		    add(stawkaPodatku3);
		    //--------------------------------------------------------------//
		    
		    bWyswietl3 = new JButton("Akceptuj");
		    bWyswietl3.setBounds(1575,450,90,20);
		    add(bWyswietl3);
		    bWyswietl3.addActionListener(this);		    
		    	
//---------------------------CZWARTY PRODUKT---------------------------------------------//
		    czwartyProdukt = new JLabel("Dane czwartego produku");
		    czwartyProdukt.setBounds(1150,475,200,20);
		    add(czwartyProdukt);
		    
			//LISTA PRODUKTOW
		    listaProdukt4 = new JComboBox();
		    listaProdukt4.setBounds(1360,475,200,20);
		    listaProdukt4.addItem("WYBIERZ CZWARTY PRODUKT");
			
			for(int i = 1; i<numer; i++) {
				listaProdukt4.addItem("Produkt " + i);
			}
			add(listaProdukt4);
			listaProdukt4.addActionListener(this);
			
			
		   	//--------------------------------------------------------------//
		   	podajNazweProdukt4Tekst = new JLabel("Podaj nazw czwartego produktu:");
		   	podajNazweProdukt4Tekst.setBounds(1150,500,200,20);
		   	add(podajNazweProdukt4Tekst);
		       	
		   	podajNazweProdukt4 = new JTextField("");
		   	podajNazweProdukt4.setBounds(1360,500,200,20);
		   	add(podajNazweProdukt4);
		   	//--------------------------------------------------------------//
		    	    	
		    	    	
		    //--------------------------------------------------------------//
		   	podajIloscProdukt4Tekst = new JLabel("Podaj ilo czwartego produktu:");
		   	podajIloscProdukt4Tekst.setBounds(1150,525,230,20);
		   	add(podajIloscProdukt4Tekst);
		    	    	
		   	podajIloscProdukt4 = new JTextField("");
		   	podajIloscProdukt4.setBounds(1360,525,200,20);
		   	add(podajIloscProdukt4);
		   	//--------------------------------------------------------------//
		    
		   	//--------------------------------------------------------------//
		    podajMiareProduktu4Tekst = new JLabel("Podaj miar czwartego produktu:");
		    podajMiareProduktu4Tekst.setBounds(1150,550,230,20);
		    add(podajMiareProduktu4Tekst);
		    	
		    podajMiareProduktu4 = new JTextField("");
		    podajMiareProduktu4.setBounds(1360,550,200,20);
		    add(podajMiareProduktu4);
		    //--------------------------------------------------------------//	
		    	    	
		   	//--------------------------------------------------------------//
		   	podajCeneProdukt4Tekst = new JLabel("Podaj cen czwartego produktu:");
		   	podajCeneProdukt4Tekst.setBounds(1150,575,230,20);
		   	add(podajCeneProdukt4Tekst);
		   	    	
		    podajCeneProdukt4Zl = new JTextField("");
		    podajCeneProdukt4Zl.setBounds(1360,575,40,20);
		    add(podajCeneProdukt4Zl);
		    
		    przecinek = new JLabel(",");
		    przecinek.setBounds(1403,575,40,20);
		    add(przecinek);
		    
		    podajCeneProdukt4GR = new JTextField("");
		    podajCeneProdukt4GR.setBounds(1407,575,40,20);
		    add(podajCeneProdukt4GR);
		    //--------------------------------------------------------------//

		   	//--------------------------------------------------------------//
		    stawkaPodatku4Tekst = new JLabel("Podaj stawk podatku:");
		    stawkaPodatku4Tekst.setBounds(1150,600,230,20);
		    add(stawkaPodatku4Tekst);
		    	
		    stawkaPodatku4 = new JTextField("");
		    stawkaPodatku4.setBounds(1360,600,200,20);
		    add(stawkaPodatku4);
		    //--------------------------------------------------------------//
		    
		    bWyswietl4 = new JButton("Akceptuj");
		    bWyswietl4.setBounds(1575,600,90,20);
		    add(bWyswietl4);
		    bWyswietl4.addActionListener(this);
		    	
//---------------------------PIATY PRODUKT---------------------------------------------//
		    piatyProdukt = new JLabel("Dane pitego produku");
		    piatyProdukt.setBounds(1150,625,200,20);
		    add(piatyProdukt);
		    
			//LISTA PRODUKTOW
		    listaProdukt5 = new JComboBox();
		    listaProdukt5.setBounds(1360,625,200,20);
		    listaProdukt5.addItem("WYBIERZ PIATY PRODUKT");
			
			for(int i = 1; i<numer; i++) {
				listaProdukt5.addItem("Produkt " + i);
			}
			add(listaProdukt5);
			listaProdukt5.addActionListener(this);
		   	//--------------------------------------------------------------//
		   	podajNazweProdukt5Tekst = new JLabel("Podaj nazw pitego produktu:");
		   	podajNazweProdukt5Tekst.setBounds(1150,650,200,20);
		   	add(podajNazweProdukt5Tekst);
		    	    	
		   	podajNazweProdukt5 = new JTextField("");
		   	podajNazweProdukt5.setBounds(1360,650,200,20);
		   	add(podajNazweProdukt5);
		    //--------------------------------------------------------------//
		    	    	
		    	    	
		    //--------------------------------------------------------------//
		   	podajIloscProdukt5Tekst = new JLabel("Podaj ilo pitego produktu:");
		    podajIloscProdukt5Tekst.setBounds(1150,675,230,20);
		    add(podajIloscProdukt5Tekst);
		    	    	
		    podajIloscProdukt5 = new JTextField("");
		    podajIloscProdukt5.setBounds(1360,675,200,20);
		    add(podajIloscProdukt5);
		    //--------------------------------------------------------------//
		    			
		    //--------------------------------------------------------------//
		    podajMiareProduktu5Tekst = new JLabel("Podaj miar pitego produktu:");
		    podajMiareProduktu5Tekst.setBounds(1150,700,230,20);
		    add(podajMiareProduktu5Tekst);
		    	
		    podajMiareProduktu5 = new JTextField("");
		    podajMiareProduktu5.setBounds(1360,700,200,20);
		    add(podajMiareProduktu5);
		    //--------------------------------------------------------------//
		    	    	
		   	//--------------------------------------------------------------//
		   	podajCeneProdukt5Tekst = new JLabel("Podaj cen pitego produktu:");
		   	podajCeneProdukt5Tekst.setBounds(1150,725,230,20);
		    add(podajCeneProdukt5Tekst);
		    	    	
		    podajCeneProdukt5Zl = new JTextField("");
		    podajCeneProdukt5Zl.setBounds(1360,725,40,20);
		    add(podajCeneProdukt5Zl);
		    
		    przecinek = new JLabel(",");
		    przecinek.setBounds(1403,725,40,20);
		    add(przecinek);
		    
		    podajCeneProdukt5GR = new JTextField("");
		    podajCeneProdukt5GR.setBounds(1407,725,40,20);
		    add(podajCeneProdukt5GR);
		    //--------------------------------------------------------------//
		    
		    //--------------------------------------------------------------//
		    stawkaPodatku5Tekst = new JLabel("Podaj stawk podatku:");
		    stawkaPodatku5Tekst.setBounds(1150,750,230,20);
		    add(stawkaPodatku5Tekst);
		    	
		    stawkaPodatku5 = new JTextField("");
		    stawkaPodatku5.setBounds(1360,750,200,20);
		    add(stawkaPodatku5);
		    //--------------------------------------------------------------//
		  
		    bWyswietl5 = new JButton("Akceptuj");
		    bWyswietl5.setBounds(1575,750,90,20);
		    add(bWyswietl5);
		    bWyswietl5.addActionListener(this);
		    
		    //--------------------------------------------------------------//
		    bOblicz = new JButton("Oblicz");
		    bOblicz.setBounds(1575,775,90,20);
		    add(bOblicz);
		    bOblicz.addActionListener(this);	
		    
//-------------------------------------------------------------------------------------------------------------------------------------//		  
//-------------------------------------------------------------------------------------------------------------------------------------//		    
//-------------------------------------------------------------------------------------------------------------------------------------//
		    //--------------------------------------------------------------//
		    //------------------WYWIETLANIE NA PODGLDZIE------------------//
		    //--------------------------------------------------------------//
		    
		    //------------------------------DANE----------------------------//
		    wNrFaktury= new JLabel("");
		    wNrFaktury.setBounds(260,38,100,20);
		    wNrFaktury.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wNrFaktury);		
	    	
	    	wDataDzien= new JLabel("");
	    	wDataDzien.setBounds(459,27,40,20);
	    	wDataDzien.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wDataDzien);		
	    	
	    	wDataMiesiac= new JLabel("");
	    	wDataMiesiac.setBounds(488,27,40,20);
	    	wDataMiesiac.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wDataMiesiac);		
	    	
	    	wDataRok= new JLabel("");
	    	wDataRok.setBounds(545,27,40,20);
	    	wDataRok.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wDataRok);	
	    	
	    	data2= new JLabel("");
	    	data2.setBounds(495,47,90,20);
	    	data2.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(data2);	
	    	
	    	wNazwaFirmy= new JLabel("");
	    	wNazwaFirmy.setBounds(170,74,290,20);
	    	wNazwaFirmy.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wNazwaFirmy);
	    		    	
	    	wAdres= new JLabel("");
	    	wAdres.setBounds(110,94,330,20);
	    	wAdres.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wAdres);  	
	    	
	    	wNipFirmy= new JLabel("");
	    	wNipFirmy.setBounds(450,94,100,20);
	    	wNipFirmy.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wNipFirmy);
	    	
	    	wMiejscowosc= new JLabel("");
	    	wMiejscowosc.setBounds(415,8,160,20);
	    	wMiejscowosc.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wMiejscowosc);
	    	
	    	wBank= new JLabel("");
	    	wBank.setBounds(90,253,180,20);
	    	wBank.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wBank);
	    	
	    	wNrKontaBankowego= new JLabel("");
	    	wNrKontaBankowego.setBounds(90,272,260,20);
	    	wNrKontaBankowego.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wNrKontaBankowego);
	    	
	    	wSposobZaplaty= new JLabel("");
	    	wSposobZaplaty.setBounds(90,235,100,20);
	    	wSposobZaplaty.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wSposobZaplaty);
	    	
	    	wTerminZaplaty= new JLabel("");
	    	wTerminZaplaty.setBounds(270,235,100,20);
	    	wTerminZaplaty.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wTerminZaplaty);
	    	
		    //------------------------DANE KOPIA-----------------------------//
		    wNrFakturyKopia= new JLabel("");
		    wNrFakturyKopia.setBounds(260,459,100,20);
		    wNrFakturyKopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wNrFakturyKopia);		
	    	
	    	wDataDzienKopia= new JLabel("");
	    	wDataDzienKopia.setBounds(459,448,40,20);
	    	wDataDzienKopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wDataDzienKopia);		
	    	
	    	wDataMiesiacKopia= new JLabel("");
	    	wDataMiesiacKopia.setBounds(488,448,40,20);
	    	wDataMiesiacKopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wDataMiesiacKopia);		
	    	
	    	wDataRokKopia= new JLabel("");
	    	wDataRokKopia.setBounds(545,448,40,20);
	    	wDataRokKopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wDataRokKopia);	
	    	
	    	data2Kopia= new JLabel("");
	    	data2Kopia.setBounds(495,468,90,20);
	    	data2Kopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(data2Kopia);	
	    	
	    	wNazwaFirmyKopia= new JLabel("");
	    	wNazwaFirmyKopia.setBounds(170,495,290,20);
	    	wNazwaFirmyKopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wNazwaFirmyKopia);
	    		    	
	    	wAdresKopia= new JLabel("");
	    	wAdresKopia.setBounds(110,514,330,20);
	    	wAdresKopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wAdresKopia);  	
	    	
	    	wNipFirmyKopia= new JLabel("");
	    	wNipFirmyKopia.setBounds(450,514,100,20);
	    	wNipFirmyKopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wNipFirmyKopia);
	    	
	    	wMiejscowoscKopia= new JLabel("");
	    	wMiejscowoscKopia.setBounds(415,428,160,20);
	    	wMiejscowoscKopia.setFont(new Font("Arial", Font.BOLD, 14));
	    	obrazPanel.add(wMiejscowoscKopia);
	    	
	    	wBankKopia= new JLabel("");
	    	wBankKopia.setBounds(90,673,180,20);
	    	wBankKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wBankKopia);
	    	
	    	wNrKontaBankowegoKopia= new JLabel("");
	    	wNrKontaBankowegoKopia.setBounds(90,692,250,20);
	    	wNrKontaBankowegoKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wNrKontaBankowegoKopia);
	    	
	    	wSposobZaplatyKopia= new JLabel("");
	    	wSposobZaplatyKopia.setBounds(90,655,100,20);
	    	wSposobZaplatyKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wSposobZaplatyKopia);
	    	
	    	wTerminZaplatyKopia= new JLabel("");
	    	wTerminZaplatyKopia.setBounds(270,655,100,20);
	    	wTerminZaplatyKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wTerminZaplatyKopia);
		    
		    //-----------------------PIERWSZY PRODUKT-----------------------//	    
	    	lp1 = new JLabel("");
	    	lp1.setBounds(38,148,20,20);
	    	lp1.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp1);		  	    
   	
	    	wNazwa1 = new JLabel("");
	    	wNazwa1.setBounds(50,148,190,20);
	    	wNazwa1.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa1);
	    	
	    	wMiara1 = new JLabel("");
	    	wMiara1.setBounds(240,148,70,20);
	    	wMiara1.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara1);
	    	
	    	wIlosc1 = new JLabel("");
	    	wIlosc1.setBounds(271,148,70,20);
	    	wIlosc1.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc1);
	    	
	    	wCena1 = new JLabel("");
	    	wCena1.setBounds(310,148,70,20);
	    	wCena1.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena1);
	    	
	    	wCena1GR = new JLabel("");
	    	wCena1GR.setBounds(345,148,70,20);
	    	wCena1GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena1GR);
	    		    	
	    	wStawka1 = new JLabel("");
	    	wStawka1.setBounds(429,148,70,20);
	    	wStawka1.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka1);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow1ZL = new JLabel("");
	    	wyswietlWartoscTowarow1ZL.setBounds(367,148,35,20);
	    	wyswietlWartoscTowarow1ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZL);
	    	
	    	wyswietlWartoscTowarow1GR = new JLabel("");
	    	wyswietlWartoscTowarow1GR.setBounds(405,148,20,20);
	    	wyswietlWartoscTowarow1GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GR);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO KWOTY PODATKU
	   //--------------------------------------------------------------//
	    	wyswietlWartoscPodatku1ZL = new JLabel("");
	    	wyswietlWartoscPodatku1ZL.setBounds(458,148,35,20);
	    	wyswietlWartoscPodatku1ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatku1ZL);
	    	
	    	wyswietlWartoscPodatku1GR = new JLabel("");
	    	wyswietlWartoscPodatku1GR.setBounds(490,148,20,20);
	    	wyswietlWartoscPodatku1GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatku1GR);
	    	
	    //--------------------------------------------------------------//
	    	//WARTO KWOTY Z PODATKIEM
	   //--------------------------------------------------------------//
	    	wyswietlWartoscZPodatkiem1ZL = new JLabel("");
	    	wyswietlWartoscZPodatkiem1ZL.setBounds(515,148,35,20);
	    	wyswietlWartoscZPodatkiem1ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscZPodatkiem1ZL);
	    	
	    	wyswietlWartoscZPodatkiem1GR = new JLabel("");
	    	wyswietlWartoscZPodatkiem1GR.setBounds(558,148,20,20);
	    	wyswietlWartoscZPodatkiem1GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscZPodatkiem1GR);
	   //-----------------//WARTO KWOTY BEZ PODATKU W ZALEZNSCI	OD OPODATKOWANIA---------------------//	
	    	wyswietlWartoscTowarow1ZL23 = new JLabel("");
	    	wyswietlWartoscTowarow1ZL23.setBounds(367,230,35,20);
	    	wyswietlWartoscTowarow1ZL23.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZL23);
	    	
	    	wyswietlWartoscTowarow1ZL8 = new JLabel("");
	    	wyswietlWartoscTowarow1ZL8.setBounds(367,247,35,20);
	    	wyswietlWartoscTowarow1ZL8.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZL8);
	    	
	    	wyswietlWartoscTowarow1ZL5 = new JLabel("");
	    	wyswietlWartoscTowarow1ZL5.setBounds(367,264,35,20);
	    	wyswietlWartoscTowarow1ZL5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZL5);
	    	
	    	wyswietlWartoscTowarow1ZL0 = new JLabel("");
	    	wyswietlWartoscTowarow1ZL0.setBounds(367,281,35,20);
	    	wyswietlWartoscTowarow1ZL0.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZL0);
	   //--------------------------------------------------------------// 
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow1GR23 = new JLabel("");
	    	wyswietlWartoscTowarow1GR23.setBounds(405,230,20,20);
	    	wyswietlWartoscTowarow1GR23.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GR23);
	    	
	    	wyswietlWartoscTowarow1GR8 = new JLabel("");
	    	wyswietlWartoscTowarow1GR8.setBounds(405,247,20,20);
	    	wyswietlWartoscTowarow1GR8.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GR8);
	    	
	    	wyswietlWartoscTowarow1GR5 = new JLabel("");
	    	wyswietlWartoscTowarow1GR5.setBounds(405,264,20,20);
	    	wyswietlWartoscTowarow1GR5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GR5);
	    	
	    	wyswietlWartoscTowarow1GR0 = new JLabel("");
	    	wyswietlWartoscTowarow1GR0.setBounds(405,281,20,20);
	    	wyswietlWartoscTowarow1GR0.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GR0);
	   //--------------------------------------------------------------// 
	   //--------------------------------------------------------------//
	    	wyswietlRazemWartoscTowarow1ZL3 = new JLabel("");
	    	wyswietlRazemWartoscTowarow1ZL3.setBounds(367,328,35,20);
	    	wyswietlRazemWartoscTowarow1ZL3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlRazemWartoscTowarow1ZL3);
	    	
	    	wyswietlRazemWartoscTowarow1GR3 = new JLabel("");
	    	wyswietlRazemWartoscTowarow1GR3.setBounds(405,328,20,20);
	    	wyswietlRazemWartoscTowarow1GR3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlRazemWartoscTowarow1GR3);

	    	
	    	//---------------------------KOPIA---------------------------//	
	    	lp1Kopia = new JLabel("");
	    	lp1Kopia.setBounds(38,568,20,20);
	    	lp1Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp1Kopia);		  	    
   	
	    	wNazwa1Kopia = new JLabel("");
	    	wNazwa1Kopia.setBounds(50,568,190,20);
	    	wNazwa1Kopia.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa1Kopia);
	    	
	    	wMiara1Kopia = new JLabel("");
	    	wMiara1Kopia.setBounds(240,568,70,20);
	    	wMiara1Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara1Kopia);
	    	
	    	wIlosc1Kopia = new JLabel("");
	    	wIlosc1Kopia.setBounds(271,568,70,20);
	    	wIlosc1Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc1Kopia);
	    	
	    	wCena1Kopia = new JLabel("");
	    	wCena1Kopia.setBounds(310,568,70,20);
	    	wCena1Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena1Kopia);
	    	
	    	wCena1GRKopia = new JLabel("");
	    	wCena1GRKopia.setBounds(345,568,70,20);
	    	wCena1GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena1GRKopia);
	    		    	
	    	wStawka1Kopia = new JLabel("");
	    	wStawka1Kopia.setBounds(429,568,70,20);
	    	wStawka1Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka1Kopia);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU KOPIA	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow1ZLKopia = new JLabel("");
	    	wyswietlWartoscTowarow1ZLKopia.setBounds(367,568,38,20);
	    	wyswietlWartoscTowarow1ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZLKopia);
	    	
	    	wyswietlWartoscTowarow1GRKopia = new JLabel("");
	    	wyswietlWartoscTowarow1GRKopia.setBounds(405,568,38,20);
	    	wyswietlWartoscTowarow1GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GRKopia);
	   //--------------------------------------------------------------//
	   //--------------------------------------------------------------//
	    	
	   //--------------------------------------------------------------// 	
	   //-------------------WARTO PODATKU KOPIA----------------------//
	    	wyswietlWartoscPodatku1ZLKopia = new JLabel("");
	    	wyswietlWartoscPodatku1ZLKopia.setBounds(458,568,35,20);
	    	wyswietlWartoscPodatku1ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatku1ZLKopia);
	    	
	    	wyswietlWartoscPodatku1GRKopia = new JLabel("");
	    	wyswietlWartoscPodatku1GRKopia.setBounds(490,568,20,20);
	    	wyswietlWartoscPodatku1GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatku1GRKopia);
	   //--------------------------------------------------------------//	
	   	
	   //--------------------------------------------------------------//
		   //WARTO KWOTY Z PODATKIEM
	   //--------------------------------------------------------------//
			 wyswietlWartoscZPodatkiem1ZLKopia = new JLabel("");
			 wyswietlWartoscZPodatkiem1ZLKopia.setBounds(515,568,35,20);
			 wyswietlWartoscZPodatkiem1ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem1ZLKopia);
		    	
		     wyswietlWartoscZPodatkiem1GRKopia = new JLabel("");
		     wyswietlWartoscZPodatkiem1GRKopia.setBounds(558,568,20,20);
		     wyswietlWartoscZPodatkiem1GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem1GRKopia);	
	   //--------------------------------------------------------------//     
	    	wyswietlWartoscTowarow1ZLKopia23 = new JLabel("");
	    	wyswietlWartoscTowarow1ZLKopia23.setBounds(367,650,38,20);
	    	wyswietlWartoscTowarow1ZLKopia23.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZLKopia23);
	    	
	    	wyswietlWartoscTowarow1ZLKopia8 = new JLabel("");
	    	wyswietlWartoscTowarow1ZLKopia8.setBounds(367,667,38,20);
	    	wyswietlWartoscTowarow1ZLKopia8.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZLKopia8);
	    	
	    	wyswietlWartoscTowarow1ZLKopia5 = new JLabel("");
	    	wyswietlWartoscTowarow1ZLKopia5.setBounds(367,684,38,20);
	    	wyswietlWartoscTowarow1ZLKopia5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZLKopia5);
	    	
	    	wyswietlWartoscTowarow1ZLKopia0 = new JLabel("");
	    	wyswietlWartoscTowarow1ZLKopia0.setBounds(367,701,38,20);
	    	wyswietlWartoscTowarow1ZLKopia0.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1ZLKopia0);
	   //--------------------------------------------------------------//
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow1GRKopia23 = new JLabel("");
	    	wyswietlWartoscTowarow1GRKopia23.setBounds(405,650,20,20);
	    	wyswietlWartoscTowarow1GRKopia23.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GRKopia23);
	  
	    	wyswietlWartoscTowarow1GRKopia8 = new JLabel("");
	    	wyswietlWartoscTowarow1GRKopia8.setBounds(405,667,20,20);
	    	wyswietlWartoscTowarow1GRKopia8.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GRKopia8);
	    	
	    	wyswietlWartoscTowarow1GRKopia5 = new JLabel("");
	    	wyswietlWartoscTowarow1GRKopia5.setBounds(405,684,20,20);
	    	wyswietlWartoscTowarow1GRKopia5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GRKopia5);
	    	
	    	wyswietlWartoscTowarow1GRKopia0 = new JLabel("");
	    	wyswietlWartoscTowarow1GRKopia0.setBounds(405,701,20,20);
	    	wyswietlWartoscTowarow1GRKopia0.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow1GRKopia0);			    	
	    //--------------------------------------------------------------//
	    //--------------------------------------------------------------//	    	
	    	wyswietlRazemWartoscTowarow1ZLKopia3 = new JLabel("");
	    	wyswietlRazemWartoscTowarow1ZLKopia3.setBounds(367,748,38,20);
	    	wyswietlRazemWartoscTowarow1ZLKopia3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlRazemWartoscTowarow1ZLKopia3);
	    	
	    	wyswietlRazemWartoscTowarow1GRKopia3 = new JLabel("");
	    	wyswietlRazemWartoscTowarow1GRKopia3.setBounds(405,748,38,20);
	    	wyswietlRazemWartoscTowarow1GRKopia3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlRazemWartoscTowarow1GRKopia3);
	    //--------------------------------------------------------------//
	 	//--------------------------------------------------------------//
	    	
	    	//-----------------------DRUGI PRODUKT-----------------------//		
	    	lp2 = new JLabel("");
	    	lp2.setBounds(38,165,20,20);
	    	lp2.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp2);		    
   	
	    	wNazwa2 = new JLabel("");
	    	wNazwa2.setBounds(50,165,190,20);
	    	wNazwa2.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa2);
	    	
	    	wMiara2 = new JLabel("");
	    	wMiara2.setBounds(240,165,70,20);
	    	wMiara2.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara2);
	    	
	    	wIlosc2 = new JLabel("");
	    	wIlosc2.setBounds(271,165,70,20);
	    	wIlosc2.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc2);
	    	
	    	wCena2 = new JLabel("");
	    	wCena2.setBounds(310,165,70,20);
	    	wCena2.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena2);
	    	
	    	wCena2GR = new JLabel("");
	    	wCena2GR.setBounds(345,165,70,20);
	    	wCena2GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena2GR);
	    		    	
	    	wStawka2 = new JLabel("");
	    	wStawka2.setBounds(429,165,70,20);
	    	wStawka2.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka2);
	    	
	 	//--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow2ZL = new JLabel("");
	    	wyswietlWartoscTowarow2ZL.setBounds(367,165,35,20);
	    	wyswietlWartoscTowarow2ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow2ZL);
	    	
	    	wyswietlWartoscTowarow2GR = new JLabel("");
	    	wyswietlWartoscTowarow2GR.setBounds(405,165,20,20);
	    	wyswietlWartoscTowarow2GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow2GR);
	    	
	    	
	   //--------------------------------------------------------------//	
	   //----------------------WARTO PODATKU ------------------------//	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscPodatku2ZL = new JLabel("");
	    	wyswietlWartoscPodatku2ZL.setBounds(458,165,35,20);
	    	wyswietlWartoscPodatku2ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatku2ZL);
	    	
	    	wyswietlWartoscPodatku2GR = new JLabel("");
	    	wyswietlWartoscPodatku2GR.setBounds(490,165,20,20);
	    	wyswietlWartoscPodatku2GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatku2GR);
	   //--------------------------------------------------------------//
	    	
	   //--------------------------------------------------------------//
	    	//WARTO KWOTY Z PODATKIEM
	   //--------------------------------------------------------------//
	    	wyswietlWartoscZPodatkiem2ZL = new JLabel("");
	    	wyswietlWartoscZPodatkiem2ZL.setBounds(515,165,35,20);
	    	wyswietlWartoscZPodatkiem2ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscZPodatkiem2ZL);
	    	
	    	wyswietlWartoscZPodatkiem2GR = new JLabel("");
	    	wyswietlWartoscZPodatkiem2GR.setBounds(558,165,20,20);
	    	wyswietlWartoscZPodatkiem2GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscZPodatkiem2GR);
	    	
	    	//---------------------------KOPIA---------------------------//	
	    	lp2Kopia = new JLabel("");
	    	lp2Kopia.setBounds(38,585,20,20);
	    	lp2Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp2Kopia);		    
   	
	    	wNazwa2Kopia = new JLabel("");
	    	wNazwa2Kopia.setBounds(50,585,190,20);
	    	wNazwa2Kopia.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa2Kopia);
	    	
	    	wMiara2Kopia = new JLabel("");
	    	wMiara2Kopia.setBounds(240,585,70,20);
	    	wMiara2Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara2Kopia);
	    	
	    	wIlosc2Kopia = new JLabel("");
	    	wIlosc2Kopia.setBounds(271,585,70,20);
	    	wIlosc2Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc2Kopia);
	    	
	    	wCena2Kopia = new JLabel("");
	    	wCena2Kopia.setBounds(310,585,70,20);
	    	wCena2Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena2Kopia);
	    	
	    	wCena2GRKopia = new JLabel("");
	    	wCena2GRKopia.setBounds(345,585,70,20);
	    	wCena2GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena2GRKopia);
	    		    	
	    	wStawka2Kopia = new JLabel("");
	    	wStawka2Kopia.setBounds(429,585,70,20);
	    	wStawka2Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka2Kopia);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU KOPIA	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow2ZLKopia = new JLabel("");
	    	wyswietlWartoscTowarow2ZLKopia.setBounds(367,585,35,20);
	    	wyswietlWartoscTowarow2ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow2ZLKopia);
	    	
	    	wyswietlWartoscTowarow2GRKopia = new JLabel("");
	    	wyswietlWartoscTowarow2GRKopia.setBounds(405,585,20,20);
	    	wyswietlWartoscTowarow2GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow2GRKopia);
	    	
	 	//--------------------------------------------------------------// 	
	 	//-------------------WARTO PODATKU KOPIA----------------------//
	 	    wyswietlWartoscPodatku2ZLKopia = new JLabel("");
	 	    wyswietlWartoscPodatku2ZLKopia.setBounds(458,585,35,20);
	 	    wyswietlWartoscPodatku2ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku2ZLKopia);
	 	    	
	 	    wyswietlWartoscPodatku2GRKopia = new JLabel("");
	 	    wyswietlWartoscPodatku2GRKopia.setBounds(490,585,20,20);
	 	    wyswietlWartoscPodatku2GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku2GRKopia);
	 	    
	    //--------------------------------------------------------------//
		    //WARTO KWOTY Z PODATKIEM
		//--------------------------------------------------------------//
			 wyswietlWartoscZPodatkiem2ZLKopia = new JLabel("");
			 wyswietlWartoscZPodatkiem2ZLKopia.setBounds(515,585,35,20);
			 wyswietlWartoscZPodatkiem2ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem2ZLKopia);
		    	
		     wyswietlWartoscZPodatkiem2GRKopia = new JLabel("");
		     wyswietlWartoscZPodatkiem2GRKopia.setBounds(558,585,20,20);
		     wyswietlWartoscZPodatkiem2GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem2GRKopia);	    
	 	//--------------------------------------------------------------//	
	    	
	    	//-----------------------TRZECI PRODUKT-----------------------//		
	    	lp3 = new JLabel("");
	    	lp3.setBounds(38,180,20,20);
	    	lp3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp3);		    
   	
	    	wNazwa3 = new JLabel("");
	    	wNazwa3.setBounds(50,180,190,20);
	    	wNazwa3.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa3);
	    	
	    	wMiara3 = new JLabel("");
	    	wMiara3.setBounds(240,180,70,20);
	    	wMiara3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara3);
	    	
	    	wIlosc3 = new JLabel("");
	    	wIlosc3.setBounds(271,180,70,20);
	    	wIlosc3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc3);
	    	
	    	wCena3 = new JLabel("");
	    	wCena3.setBounds(310,180,70,20);
	    	wCena3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena3);
	    	
	    	wCena3GR = new JLabel("");
	    	wCena3GR.setBounds(345,180,70,20);
	    	wCena3GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena3GR);
	    		    	
	    	wStawka3 = new JLabel("");
	    	wStawka3.setBounds(429,180,70,20);
	    	wStawka3.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka3);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow3ZL = new JLabel("");
	    	wyswietlWartoscTowarow3ZL.setBounds(367,180,35,20);
	    	wyswietlWartoscTowarow3ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow3ZL);
	    	
	    	wyswietlWartoscTowarow3GR = new JLabel("");
	    	wyswietlWartoscTowarow3GR.setBounds(405,180,20,20);
	    	wyswietlWartoscTowarow3GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow3GR);
	    //--------------------------------------------------------------//
	 	//----------------------WARTO PODATKU ------------------------//	
	    //--------------------------------------------------------------//
	 	    wyswietlWartoscPodatku3ZL = new JLabel("");
	 	    wyswietlWartoscPodatku3ZL.setBounds(458,180,35,20);
	 	    wyswietlWartoscPodatku3ZL.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku3ZL);
	 	    	
	 	    wyswietlWartoscPodatku3GR = new JLabel("");
	 	    wyswietlWartoscPodatku3GR.setBounds(490,180,20,20);
	 	    wyswietlWartoscPodatku3GR.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku3GR);
	   //--------------------------------------------------------------//
	    	//WARTO KWOTY Z PODATKIEM
	   //--------------------------------------------------------------//
	    	wyswietlWartoscZPodatkiem3ZL = new JLabel("");
	    	wyswietlWartoscZPodatkiem3ZL.setBounds(515,180,35,20);
	    	wyswietlWartoscZPodatkiem3ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscZPodatkiem3ZL);
	    	
	    	wyswietlWartoscZPodatkiem3GR = new JLabel("");
	    	wyswietlWartoscZPodatkiem3GR.setBounds(558,180,20,20);
	    	wyswietlWartoscZPodatkiem3GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscZPodatkiem3GR);
	 	//--------------------------------------------------------------//	
	    	
	    	//---------------------------KOPIA---------------------------//	
	    	lp3Kopia = new JLabel("");
	    	lp3Kopia.setBounds(38,602,20,20);
	    	lp3Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp3Kopia);		    
   	
	    	wNazwa3Kopia = new JLabel("");
	    	wNazwa3Kopia.setBounds(50,602,190,20);
	    	wNazwa3Kopia.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa3Kopia);
	    	
	    	wMiara3Kopia = new JLabel("");
	    	wMiara3Kopia.setBounds(240,602,70,20);
	    	wMiara3Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara3Kopia);
	    	
	    	wIlosc3Kopia = new JLabel("");
	    	wIlosc3Kopia.setBounds(271,602,70,20);
	    	wIlosc3Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc3Kopia);
	    	
	    	wCena3Kopia = new JLabel("");
	    	wCena3Kopia.setBounds(310,602,70,20);
	    	wCena3Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena3Kopia);
	    	
	    	wCena3GRKopia = new JLabel("");
	    	wCena3GRKopia.setBounds(345,602,70,20);
	    	wCena3GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena3GRKopia);
	    		    	
	    	wStawka3Kopia = new JLabel("");
	    	wStawka3Kopia.setBounds(429,602,70,20);
	    	wStawka3Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka3Kopia);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU KOPIA	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow3ZLKopia = new JLabel("");
	    	wyswietlWartoscTowarow3ZLKopia.setBounds(367,602,35,20);
	    	wyswietlWartoscTowarow3ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow3ZLKopia);
	    	
	    	wyswietlWartoscTowarow3GRKopia = new JLabel("");
	    	wyswietlWartoscTowarow3GRKopia.setBounds(405,602,20,20);
	    	wyswietlWartoscTowarow3GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow3GRKopia);
	    	
	    //--------------------------------------------------------------// 	
	 	//-------------------WARTO PODATKU KOPIA----------------------//
	 	    wyswietlWartoscPodatku3ZLKopia = new JLabel("");
	 	    wyswietlWartoscPodatku3ZLKopia.setBounds(458,602,35,20);
	 	    wyswietlWartoscPodatku3ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku3ZLKopia);
	 	    	
	 	    wyswietlWartoscPodatku3GRKopia = new JLabel("");
	 	    wyswietlWartoscPodatku3GRKopia.setBounds(490,602,20,20);
	 	    wyswietlWartoscPodatku3GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku3GRKopia);
	 	//--------------------------------------------------------------//	
	 	    
		//--------------------------------------------------------------//
		    //WARTO KWOTY Z PODATKIEM
		//--------------------------------------------------------------//
			 wyswietlWartoscZPodatkiem3ZLKopia = new JLabel("");
			 wyswietlWartoscZPodatkiem3ZLKopia.setBounds(515,602,35,20);
			 wyswietlWartoscZPodatkiem3ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem3ZLKopia);
		    	
		     wyswietlWartoscZPodatkiem3GRKopia = new JLabel("");
		     wyswietlWartoscZPodatkiem3GRKopia.setBounds(558,602,20,20);
		     wyswietlWartoscZPodatkiem3GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem3GRKopia);	    
	    	
	    //-----------------------CZWARTY PRODUKT-----------------------//		
	    	lp4 = new JLabel("");
	    	lp4.setBounds(38,195,20,20);
	    	lp4.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp4);		    
   	
	    	wNazwa4 = new JLabel("");
	    	wNazwa4.setBounds(50,195,190,20);
	    	wNazwa4.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa4);
	    	
	    	wMiara4 = new JLabel("");
	    	wMiara4.setBounds(240,195,70,20);
	    	wMiara4.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara4);
	    	
	    	wIlosc4 = new JLabel("");
	    	wIlosc4.setBounds(271,195,70,20);
	    	wIlosc4.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc4);
	    	
	    	wCena4 = new JLabel("");
	    	wCena4.setBounds(310,195,70,20);
	    	wCena4.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena4);
	    	
	    	wCena4GR = new JLabel("");
	    	wCena4GR.setBounds(345,195,70,20);
	    	wCena4GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena4GR);
	    		    	
	    	wStawka4 = new JLabel("");
	    	wStawka4.setBounds(429,195,70,20);
	    	wStawka4.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka4);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow4ZL = new JLabel("");
	    	wyswietlWartoscTowarow4ZL.setBounds(367,195,35,20);
	    	wyswietlWartoscTowarow4ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow4ZL);
	    	
	    	wyswietlWartoscTowarow4GR = new JLabel("");
	    	wyswietlWartoscTowarow4GR.setBounds(405,195,20,20);
	    	wyswietlWartoscTowarow4GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow4GR);
	    	
		//--------------------------------------------------------------//
		//----------------------WARTO PODATKU ------------------------//	
		//--------------------------------------------------------------//
		 	wyswietlWartoscPodatku4ZL = new JLabel("");
		 	wyswietlWartoscPodatku4ZL.setBounds(458,195,35,20);
		 	wyswietlWartoscPodatku4ZL.setFont(new Font("Arial", Font.BOLD, 11));
		 	obrazPanel.add(wyswietlWartoscPodatku4ZL);
		 	    	
		 	wyswietlWartoscPodatku4GR = new JLabel("");
		 	wyswietlWartoscPodatku4GR.setBounds(490,195,20,20);
		    wyswietlWartoscPodatku4GR.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlWartoscPodatku4GR);
		    
	   //--------------------------------------------------------------//
	    	//WARTO KWOTY Z PODATKIEM
	   //--------------------------------------------------------------//
	    	wyswietlWartoscZPodatkiem4ZL = new JLabel("");
	    	wyswietlWartoscZPodatkiem4ZL.setBounds(515,195,35,20);
	    	wyswietlWartoscZPodatkiem4ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscZPodatkiem4ZL);
	    	
	    	wyswietlWartoscZPodatkiem4GR = new JLabel("");
	    	wyswietlWartoscZPodatkiem4GR.setBounds(558,195,20,20);
	    	wyswietlWartoscZPodatkiem4GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscZPodatkiem4GR);
		 //--------------------------------------------------------------//
	    	
	     //---------------------------KOPIA------------------------------//	
	    	lp4Kopia = new JLabel("");
	    	lp4Kopia.setBounds(38,617,20,20);
	    	lp4Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp4Kopia);		    
   	
	    	wNazwa4Kopia = new JLabel("");
	    	wNazwa4Kopia.setBounds(50,617,190,20);
	    	wNazwa4Kopia.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa4Kopia);
	    	
	    	wMiara4Kopia = new JLabel("");
	    	wMiara4Kopia.setBounds(240,617,70,20);
	    	wMiara4Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara4Kopia);
	    	
	    	wIlosc4Kopia = new JLabel("");
	    	wIlosc4Kopia.setBounds(271,617,70,20);
	    	wIlosc4Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc4Kopia);
	    	
	    	wCena4Kopia = new JLabel("");
	    	wCena4Kopia.setBounds(310,617,70,20);
	    	wCena4Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena4Kopia);
	    	
	    	wCena4GRKopia = new JLabel("");
	    	wCena4GRKopia.setBounds(345,617,70,20);
	    	wCena4GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena4GRKopia);
	    		    	
	    	wStawka4Kopia = new JLabel("");
	    	wStawka4Kopia.setBounds(429,617,70,20);
	    	wStawka4Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka4Kopia);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU KOPIA	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow4ZLKopia = new JLabel("");
	    	wyswietlWartoscTowarow4ZLKopia.setBounds(367,617,35,20);
	    	wyswietlWartoscTowarow4ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow4ZLKopia);
	    	
	    	wyswietlWartoscTowarow4GRKopia = new JLabel("");
	    	wyswietlWartoscTowarow4GRKopia.setBounds(405,617,20,20);
	    	wyswietlWartoscTowarow4GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow4GRKopia);
	 	//--------------------------------------------------------------// 	
	 	//-------------------WARTO PODATKU KOPIA----------------------//
	 	    wyswietlWartoscPodatku4ZLKopia = new JLabel("");
	 	    wyswietlWartoscPodatku4ZLKopia.setBounds(458,617,35,20);
	 	    wyswietlWartoscPodatku4ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku4ZLKopia);
	 	    
	 	    wyswietlWartoscPodatku4GRKopia = new JLabel("");
	 	    wyswietlWartoscPodatku4GRKopia.setBounds(490,617,20,20);
	 	    wyswietlWartoscPodatku4GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku4GRKopia);
	 	//--------------------------------------------------------------//	
		//--------------------------------------------------------------//
		    //WARTO KWOTY Z PODATKIEM
		//--------------------------------------------------------------//
			 wyswietlWartoscZPodatkiem4ZLKopia = new JLabel("");
			 wyswietlWartoscZPodatkiem4ZLKopia.setBounds(515,617,35,20);
			 wyswietlWartoscZPodatkiem4ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem4ZLKopia);
		    	
		     wyswietlWartoscZPodatkiem4GRKopia = new JLabel("");
		     wyswietlWartoscZPodatkiem4GRKopia.setBounds(558,617,20,20);
		     wyswietlWartoscZPodatkiem4GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem4GRKopia);	        
	 	    
	 	    
	    	//-----------------------PITY PRODUKT-----------------------//		
	    	lp5 = new JLabel("");
	    	lp5.setBounds(38,212,20,20);
	    	lp5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp5);		    
   	
	    	wNazwa5 = new JLabel("");
	    	wNazwa5.setBounds(50,212,190,20);
	    	wNazwa5.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa5);
	    	
	    	wMiara5 = new JLabel("");
	    	wMiara5.setBounds(240,212,70,20);
	    	wMiara5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara5);
	    	
	    	wIlosc5 = new JLabel("");
	    	wIlosc5.setBounds(271,212,70,20);
	    	wIlosc5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc5);
	    	
	    	wCena5 = new JLabel("");
	    	wCena5.setBounds(310,212,70,20);
	    	wCena5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena5);
	    	
	    	wCena5GR = new JLabel("");
	    	wCena5GR.setBounds(345,212,70,20);
	    	wCena5GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena5GR);
	    		    	
	    	wStawka5 = new JLabel("");
	    	wStawka5.setBounds(429,212,70,20);
	    	wStawka5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka5);
	    	
	    //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow5ZL = new JLabel("");
	    	wyswietlWartoscTowarow5ZL.setBounds(367,212,35,20);
	    	wyswietlWartoscTowarow5ZL.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow5ZL);
	    	
	    	wyswietlWartoscTowarow5GR = new JLabel("");
	    	wyswietlWartoscTowarow5GR.setBounds(405,212,20,20);
	    	wyswietlWartoscTowarow5GR.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow5GR);
	    	
		//--------------------------------------------------------------//
		//----------------------WARTO PODATKU ------------------------//	
		//--------------------------------------------------------------//
			 wyswietlWartoscPodatku5ZL = new JLabel("");
			 wyswietlWartoscPodatku5ZL.setBounds(458,212,35,20);
			 wyswietlWartoscPodatku5ZL.setFont(new Font("Arial", Font.BOLD, 11));
			 obrazPanel.add(wyswietlWartoscPodatku5ZL);
			 	    	
			 wyswietlWartoscPodatku5GR = new JLabel("");
			 wyswietlWartoscPodatku5GR.setBounds(490,212,20,20);
			 wyswietlWartoscPodatku5GR.setFont(new Font("Arial", Font.BOLD, 11));
			 obrazPanel.add(wyswietlWartoscPodatku5GR);
		//--------------------------------------------------------------//
		//--------------------------------------------------------------//
		    //WARTO KWOTY Z PODATKIEM
		//--------------------------------------------------------------//
			 wyswietlWartoscZPodatkiem5ZL = new JLabel("");
			 wyswietlWartoscZPodatkiem5ZL.setBounds(515,212,35,20);
			 wyswietlWartoscZPodatkiem5ZL.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem5ZL);
		    	
		     wyswietlWartoscZPodatkiem5GR = new JLabel("");
		     wyswietlWartoscZPodatkiem5GR.setBounds(558,212,20,20);
		     wyswietlWartoscZPodatkiem5GR.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem5GR);
	    	
	    	//---------------------------KOPIA---------------------------//	
	    	lp5Kopia = new JLabel("");
	    	lp5Kopia.setBounds(38,633,20,20);
	    	lp5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(lp5Kopia);		    
   	
	    	wNazwa5Kopia = new JLabel("");
	    	wNazwa5Kopia.setBounds(50,633,190,20);
	    	wNazwa5Kopia.setFont(new Font("Arial", Font.BOLD, 9));
	    	obrazPanel.add(wNazwa5Kopia);
	    	
	    	wMiara5Kopia = new JLabel("");
	    	wMiara5Kopia.setBounds(240,633,70,20);
	    	wMiara5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wMiara5Kopia);
	    	
	    	wIlosc5Kopia = new JLabel("");
	    	wIlosc5Kopia.setBounds(271,633,70,20);
	    	wIlosc5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wIlosc5Kopia);
	    	
	    	wCena5Kopia = new JLabel("");
	    	wCena5Kopia.setBounds(310,633,70,20);
	    	wCena5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena5Kopia);
	    	
	    	wCena5GRKopia = new JLabel("");
	    	wCena5GRKopia.setBounds(345,633,70,20);
	    	wCena5GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wCena5GRKopia);
	    		    	
	    	wStawka5Kopia = new JLabel("");
	    	wStawka5Kopia.setBounds(429,633,70,20);
	    	wStawka5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wStawka5Kopia);
	    	
	   //--------------------------------------------------------------//
	    	//WARTO TOWARW BEZ PODATKU KOPIA	
	   //--------------------------------------------------------------//
	    	wyswietlWartoscTowarow5ZLKopia = new JLabel("");
	    	wyswietlWartoscTowarow5ZLKopia.setBounds(367,633,35,20);
	    	wyswietlWartoscTowarow5ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow5ZLKopia);
	    	
	    	wyswietlWartoscTowarow5GRKopia = new JLabel("");
	    	wyswietlWartoscTowarow5GRKopia.setBounds(405,633,20,20);
	    	wyswietlWartoscTowarow5GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscTowarow5GRKopia);
	    	
	  //--------------------------------------------------------------// 	
	  //-------------------WARTO PODATKU KOPIA----------------------//
	 	    wyswietlWartoscPodatku5ZLKopia = new JLabel("");
	 	    wyswietlWartoscPodatku5ZLKopia.setBounds(458,633,35,20);
	 	    wyswietlWartoscPodatku5ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku5ZLKopia);
	 	    	
	 	    wyswietlWartoscPodatku5GRKopia = new JLabel("");
	 	    wyswietlWartoscPodatku5GRKopia.setBounds(490,633,20,20);
	 	    wyswietlWartoscPodatku5GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	 	    obrazPanel.add(wyswietlWartoscPodatku5GRKopia);
		//--------------------------------------------------------------//
		    //WARTO KWOTY Z PODATKIEM
		//--------------------------------------------------------------//
			 wyswietlWartoscZPodatkiem5ZLKopia = new JLabel("");
			 wyswietlWartoscZPodatkiem5ZLKopia.setBounds(515,633,35,20);
			 wyswietlWartoscZPodatkiem5ZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem5ZLKopia);
		    	
		     wyswietlWartoscZPodatkiem5GRKopia = new JLabel("");
		     wyswietlWartoscZPodatkiem5GRKopia.setBounds(558,633,20,20);
		     wyswietlWartoscZPodatkiem5GRKopia.setFont(new Font("Arial", Font.BOLD, 11));
		     obrazPanel.add(wyswietlWartoscZPodatkiem5GRKopia);	    
	  //-----------------------------------------------------------------------//	
	  //----------------------Laczna kwota podatku-----------------------------//
	  //----------------w zaleznosci od wysokosci opodatkowania----------------//	
	 	    wyswietlWartoscPodatkuZL23 = new JLabel("");
	    	wyswietlWartoscPodatkuZL23.setBounds(458,230,35,20);
	    	wyswietlWartoscPodatkuZL23.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuZL23);
	    	
	    	wyswietlWartoscPodatkuZL8 = new JLabel("");
	    	wyswietlWartoscPodatkuZL8.setBounds(458,247,35,20);
	    	wyswietlWartoscPodatkuZL8.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuZL8);
	    	
	    	wyswietlWartoscPodatkuZL5 = new JLabel("");
	    	wyswietlWartoscPodatkuZL5.setBounds(458,264,35,20);
	    	wyswietlWartoscPodatkuZL5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuZL5);
	   //-------------------------------GROSZE---------------------------------//
	 	    wyswietlWartoscPodatkuGR23 = new JLabel("");
	    	wyswietlWartoscPodatkuGR23.setBounds(490,230,35,20);
	    	wyswietlWartoscPodatkuGR23.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuGR23);
	    	
	    	wyswietlWartoscPodatkuGR8 = new JLabel("");
	    	wyswietlWartoscPodatkuGR8.setBounds(490,247,35,20);
	    	wyswietlWartoscPodatkuGR8.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuGR8);
	    	
	    	wyswietlWartoscPodatkuGR5 = new JLabel("");
	    	wyswietlWartoscPodatkuGR5.setBounds(490,264,35,20);
	    	wyswietlWartoscPodatkuGR5.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuGR5);

	 //----------------------Laczna kwota podatku-----------------------------//
	 //----------------w zaleznosci od wysokosci opodatkowania----------------//	
	 //------------------------------KOPIA------------------------------------//
	  	 	wyswietlWartoscPodatkuZL23Kopia = new JLabel("");
	  	    wyswietlWartoscPodatkuZL23Kopia.setBounds(458,650,35,20);
	  	    wyswietlWartoscPodatkuZL23Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlWartoscPodatkuZL23Kopia);
	  	    
	  	    wyswietlWartoscPodatkuZL8Kopia = new JLabel("");
	  	    wyswietlWartoscPodatkuZL8Kopia.setBounds(458,667,35,20);
	  	    wyswietlWartoscPodatkuZL8Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlWartoscPodatkuZL8Kopia);
	  	    	
	  	    wyswietlWartoscPodatkuZL5Kopia = new JLabel("");
	  	    wyswietlWartoscPodatkuZL5Kopia.setBounds(458,684,35,20);
	  	    wyswietlWartoscPodatkuZL5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlWartoscPodatkuZL5Kopia);
	  	    
	 //-------------------------------GROSZE---------------------------------//
	 	    wyswietlWartoscPodatkuGR23Kopia = new JLabel("");
	    	wyswietlWartoscPodatkuGR23Kopia.setBounds(490,650,35,20);
	    	wyswietlWartoscPodatkuGR23Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuGR23Kopia);
	    	
	    	wyswietlWartoscPodatkuGR8Kopia = new JLabel("");
	    	wyswietlWartoscPodatkuGR8Kopia.setBounds(490,667,35,20);
	    	wyswietlWartoscPodatkuGR8Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuGR8Kopia);
	    	
	    	wyswietlWartoscPodatkuGR5Kopia = new JLabel("");
	    	wyswietlWartoscPodatkuGR5Kopia.setBounds(490,684,35,20);
	    	wyswietlWartoscPodatkuGR5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlWartoscPodatkuGR5Kopia);
	    	
		  //-----------------------------------------------------------------------//	
		  //----------------------Laczna kwota podatku-----------------------------//
		  //-----------------------------------------------------------------------//	    	   	
		    wyswietlRazemWartoscPodatkuZL = new JLabel("");
		    wyswietlRazemWartoscPodatkuZL.setBounds(458,330,38,20);
		    wyswietlRazemWartoscPodatkuZL.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlRazemWartoscPodatkuZL);
		    	
		    wyswietlRazemWartoscPodatkuGR = new JLabel("");
		    wyswietlRazemWartoscPodatkuGR.setBounds(492,330,38,20);
		    wyswietlRazemWartoscPodatkuGR.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlRazemWartoscPodatkuGR);	
	    	
	  	  //-----------------------------------------------------------------------//	
	  	  //--------------------Laczna kwota podatku KOPIA-------------------------//
	  	  //-----------------------------------------------------------------------//	
	    	wyswietlRazemWartoscPodatkuZLKopia = new JLabel("");
	    	wyswietlRazemWartoscPodatkuZLKopia.setBounds(458,748,38,20);
	    	wyswietlRazemWartoscPodatkuZLKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlRazemWartoscPodatkuZLKopia);
	    	
	    	wyswietlRazemWartoscPodatkuGRKopia = new JLabel("");
	    	wyswietlRazemWartoscPodatkuGRKopia.setBounds(492,748,38,20);
	    	wyswietlRazemWartoscPodatkuGRKopia.setFont(new Font("Arial", Font.BOLD, 11));
	    	obrazPanel.add(wyswietlRazemWartoscPodatkuGRKopia);
	    	
	      //--------------------------------------------------------------------------------//	
	  	  //------------------------Laczna kwota wraz z podatkiem---------------------------//
	  	  //----------------w zaleznosci od wysokosci opodatkowania-------------------------//	
	    	wyswietlRazemWartoscPodatkuZL23 = new JLabel("");
	    	wyswietlRazemWartoscPodatkuZL23.setBounds(515,230,35,20);
	    	wyswietlRazemWartoscPodatkuZL23.setFont(new Font("Arial", Font.BOLD, 11));
	      	obrazPanel.add(wyswietlRazemWartoscPodatkuZL23);
	  	    	
	      	wyswietlRazemWartoscPodatkuZL8 = new JLabel("");
	      	wyswietlRazemWartoscPodatkuZL8.setBounds(515,247,35,20);
	      	wyswietlRazemWartoscPodatkuZL8.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlRazemWartoscPodatkuZL8);
	  	    	
	  	    wyswietlRazemWartoscPodatkuZL5 = new JLabel("");
	  	    wyswietlRazemWartoscPodatkuZL5.setBounds(515,264,35,20);
	  	    wyswietlRazemWartoscPodatkuZL5.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlRazemWartoscPodatkuZL5);
	  	    
	  	    wyswietlRazemWartoscPodatkuZL0 = new JLabel("");
	  	    wyswietlRazemWartoscPodatkuZL0.setBounds(515,280,35,20);
	  	    wyswietlRazemWartoscPodatkuZL0.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlRazemWartoscPodatkuZL0);
	  	   //-------------------------------GROSZE---------------------------------//
	  	    wyswietlRazemWartoscPodatkuGR23 = new JLabel("");
	  	    wyswietlRazemWartoscPodatkuGR23.setBounds(558,230,35,20);
	  	    wyswietlRazemWartoscPodatkuGR23.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlRazemWartoscPodatkuGR23);
	  	    	
	  	    wyswietlRazemWartoscPodatkuGR8 = new JLabel("");
	  	    wyswietlRazemWartoscPodatkuGR8.setBounds(558,247,35,20);
	  	    wyswietlRazemWartoscPodatkuGR8.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlRazemWartoscPodatkuGR8);
	  	    	
	  	    wyswietlRazemWartoscPodatkuGR5 = new JLabel("");
	  	    wyswietlRazemWartoscPodatkuGR5.setBounds(558,264,35,20);
	  	    wyswietlRazemWartoscPodatkuGR5.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlRazemWartoscPodatkuGR5);
	  	    
	  	    wyswietlRazemWartoscPodatkuGR0 = new JLabel("");
	  	    wyswietlRazemWartoscPodatkuGR0.setBounds(558,280,35,20);
	  	    wyswietlRazemWartoscPodatkuGR0.setFont(new Font("Arial", Font.BOLD, 11));
	  	    obrazPanel.add(wyswietlRazemWartoscPodatkuGR0);
	  	    
	  	  //----------------------------------KOPIA----------------------------------------//	
		  //------------------------Laczna kwota wraz z podatkiem---------------------------//
		  //----------------w zaleznosci od wysokosci opodatkowania-------------------------//	
		    wyswietlRazemWartoscPodatkuZL23Kopia = new JLabel("");
		    wyswietlRazemWartoscPodatkuZL23Kopia.setBounds(515,649,35,20);
		    wyswietlRazemWartoscPodatkuZL23Kopia.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlRazemWartoscPodatkuZL23Kopia);
		  	    	
		    wyswietlRazemWartoscPodatkuZL8Kopia = new JLabel("");
		    wyswietlRazemWartoscPodatkuZL8Kopia.setBounds(515,666,35,20);
		   	wyswietlRazemWartoscPodatkuZL8Kopia.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlRazemWartoscPodatkuZL8Kopia);
		  	    	
		  	wyswietlRazemWartoscPodatkuZL5Kopia = new JLabel("");
		    wyswietlRazemWartoscPodatkuZL5Kopia.setBounds(515,683,35,20);
		  	wyswietlRazemWartoscPodatkuZL5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
		  	obrazPanel.add(wyswietlRazemWartoscPodatkuZL5Kopia);
		  	    
		    wyswietlRazemWartoscPodatkuZL0Kopia = new JLabel("");
		  	wyswietlRazemWartoscPodatkuZL0Kopia.setBounds(515,701,35,20);
		  	wyswietlRazemWartoscPodatkuZL0Kopia.setFont(new Font("Arial", Font.BOLD, 11));
		  	obrazPanel.add(wyswietlRazemWartoscPodatkuZL0Kopia);
		  	//-------------------------------GROSZE---------------------------------//
		  	wyswietlRazemWartoscPodatkuGR23Kopia = new JLabel("");
		  	wyswietlRazemWartoscPodatkuGR23Kopia.setBounds(558,649,35,20);
		  	wyswietlRazemWartoscPodatkuGR23Kopia.setFont(new Font("Arial", Font.BOLD, 11));
		  	obrazPanel.add(wyswietlRazemWartoscPodatkuGR23Kopia);
		  	    	
		  	wyswietlRazemWartoscPodatkuGR8Kopia = new JLabel("");
		  	wyswietlRazemWartoscPodatkuGR8Kopia.setBounds(558,666,35,20);
		  	wyswietlRazemWartoscPodatkuGR8Kopia.setFont(new Font("Arial", Font.BOLD, 11));
		  	obrazPanel.add(wyswietlRazemWartoscPodatkuGR8Kopia);
		  	    	
		  	wyswietlRazemWartoscPodatkuGR5Kopia = new JLabel("");
		  	wyswietlRazemWartoscPodatkuGR5Kopia.setBounds(558,683,35,20);
		  	wyswietlRazemWartoscPodatkuGR5Kopia.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlRazemWartoscPodatkuGR5Kopia);
		  	    
		  	wyswietlRazemWartoscPodatkuGR0Kopia = new JLabel("");
		  	wyswietlRazemWartoscPodatkuGR0Kopia.setBounds(558,701,35,20);
		  	wyswietlRazemWartoscPodatkuGR0Kopia.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlRazemWartoscPodatkuGR0Kopia);
		  	//--------------------------------------------------------------------------------//	
			//------------------------Laczna kwota wraz z podatkiem---------------------------//
		  	//--------------------------------------------------------------------------------//	
		    wyswietlLacznaWartoscWrazZPodatkiemZl = new JLabel("");
		    wyswietlLacznaWartoscWrazZPodatkiemZl.setBounds(518,330,38,20);
		    wyswietlLacznaWartoscWrazZPodatkiemZl.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlLacznaWartoscWrazZPodatkiemZl);
		    	
		    wyswietlLacznaWartoscWrazZPodatkiemGR = new JLabel("");
		    wyswietlLacznaWartoscWrazZPodatkiemGR.setBounds(558,330,38,20);
		    wyswietlLacznaWartoscWrazZPodatkiemGR.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlLacznaWartoscWrazZPodatkiemGR);
		    
		    //DO ZAPLATY--------------------/
		    doZaplatyZl = new JLabel("");
		    doZaplatyZl.setBounds(90,296,100,20);
		    doZaplatyZl.setFont(new Font("Arial", Font.BOLD, 11));
        	obrazPanel.add(doZaplatyZl);
        	
        	doZaplatyGR = new JLabel("");
  		    doZaplatyGR.setBounds(153,296,100,20);
  		    doZaplatyGR.setFont(new Font("Arial", Font.BOLD, 11));
          	obrazPanel.add(doZaplatyGR);
		    	
		    //--------------------------------------------------------------------------------//
		    
		    
		  	//---------------------------------KOPIA------------------------------------------//	
			//------------------------Laczna kwota wraz z podatkiem---------------------------//
		  	//--------------------------------------------------------------------------------//	
		    wyswietlLacznaWartoscWrazZPodatkiemZlKopia = new JLabel("");
		    wyswietlLacznaWartoscWrazZPodatkiemZlKopia.setBounds(518,748,38,20);
		    wyswietlLacznaWartoscWrazZPodatkiemZlKopia.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlLacznaWartoscWrazZPodatkiemZlKopia);
		    	
		    wyswietlLacznaWartoscWrazZPodatkiemGRKopia = new JLabel("");
		    wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setBounds(558,748,38,20);
		    wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setFont(new Font("Arial", Font.BOLD, 11));
		    obrazPanel.add(wyswietlLacznaWartoscWrazZPodatkiemGRKopia);
		    
		  //DO ZAPLATY--------------------/
		    doZaplatyZlKopia = new JLabel("");
		    doZaplatyZlKopia.setBounds(90,715,100,20);
		    doZaplatyZlKopia.setFont(new Font("Arial", Font.BOLD, 11));
        	obrazPanel.add(doZaplatyZlKopia);
        	
        	doZaplatyGRKopia = new JLabel("");
  		    doZaplatyGRKopia.setBounds(153,715,100,20);
  		    doZaplatyGRKopia.setFont(new Font("Arial", Font.BOLD, 11));
          	obrazPanel.add(doZaplatyGRKopia);
		    	
		    //--------------------------------------------------------------------------------//	
   
		    
		    
		  //LiczbyNaSlowa   
	    	
			//SOWO ZOTE                                                       
			zlote=new JLabel("");                                               
			zlote.setBounds(260,309,150,30);    
			zlote.setFont(wygladDane); 
			obrazPanel.add(zlote);                                                         
			                                                                    
			zloteKopia=new JLabel("");                                          
			zloteKopia.setBounds(260,729,150,30);   
			zloteKopia.setFont(wygladDane);
			obrazPanel.add(zloteKopia);    
			
			//JEDNOSCI		                                                    
			slownieZlotowkiJednosci=new JLabel("");                             
			slownieZlotowkiJednosci.setBounds(200,309,150,30);   
			slownieZlotowkiJednosci.setFont(wygladDane);
			obrazPanel.add(slownieZlotowkiJednosci);                                       
			                                                                    
			slownieZlotowkiJednosciKopia=new JLabel("");                        
			slownieZlotowkiJednosciKopia.setBounds(200,729,150,30);   
			slownieZlotowkiJednosciKopia.setFont(wygladDane);
			obrazPanel.add(slownieZlotowkiJednosciKopia);                                  
			                                                                    
			//DZIESIATKI 11-19                                                  
			slownieZlotowkiDziesiatki=new JLabel("");                           
			slownieZlotowkiDziesiatki.setBounds(110,309,150,30); 
			slownieZlotowkiDziesiatki.setFont(wygladDane);
			obrazPanel.add(slownieZlotowkiDziesiatki);                                     
	                                                                            
			slownieZlotowkiDziesiatkiKopia=new JLabel("");                      
			slownieZlotowkiDziesiatkiKopia.setBounds(110,729,150,30);  
			slownieZlotowkiDziesiatkiKopia.setFont(wygladDane);
			obrazPanel.add(slownieZlotowkiDziesiatkiKopia);                                
			                                                                    
			//SETKI                                                             
			slownieZlotowkiSetki=new JLabel("");                                
			slownieZlotowkiSetki.setBounds(45,309,150,30); 
			slownieZlotowkiSetki.setFont(wygladDane);
			obrazPanel.add(slownieZlotowkiSetki);                                          
			                                                                    
			slownieZlotowkiSetkiKopia=new JLabel("");                           
			slownieZlotowkiSetkiKopia.setBounds(45,729,150,30); 
			slownieZlotowkiSetkiKopia.setFont(wygladDane);
			obrazPanel.add(slownieZlotowkiSetkiKopia);                                     
			                                                                    
			//TYSICE                                                           
			slownieZlotowkiTysiace=new JLabel("");                              
			slownieZlotowkiTysiace.setBounds(240,289,150,30);     
			slownieZlotowkiTysiace.setFont(wygladDane);
			obrazPanel.add(slownieZlotowkiTysiace);                                        
			                                                                    
			slownieZlotowkiTysiaceKopia=new JLabel("");                         
			slownieZlotowkiTysiaceKopia.setBounds(240,709,150,30); 
			slownieZlotowkiTysiaceKopia.setFont(wygladDane);           
			obrazPanel.add(slownieZlotowkiTysiaceKopia);                                   
	                                                                            
			//TYSICE KONCWKI                                                  
			slownieTysiaceKoncowki=new JLabel("");                              
			slownieTysiaceKoncowki.setBounds(288,289,150,30);  
			slownieTysiaceKoncowki.setFont(wygladDane);
			obrazPanel.add(slownieTysiaceKoncowki);                                        
			                                                                    
			slownieTysiaceKoncowkiKopia=new JLabel("");                         
			slownieTysiaceKoncowkiKopia.setBounds(288,709,150,30);   
			slownieTysiaceKoncowkiKopia.setFont(wygladDane);
			obrazPanel.add(slownieTysiaceKoncowkiKopia);                                   
			                                                                    
			//GROSZE                                                            
			groszeSlownie=new JLabel("");                                       
			groszeSlownie.setBounds(275,309,150,30);
			groszeSlownie.setFont(wygladDane);
			obrazPanel.add(groszeSlownie);                                                 
			                                                                    
			groszeSlownieKopia=new JLabel("");                                  
			groszeSlownieKopia.setBounds(275,729,150,30); 
			groszeSlownieKopia.setFont(wygladDane);
			obrazPanel.add(groszeSlownieKopia); 
			
			
			//DRUKUJ
		   	bDrukuj = new JButton("Drukuj");
	    	bDrukuj.setBounds(50,900,100,20);
	    	add(bDrukuj);
	    	bDrukuj.addActionListener(new Jednorazowa_faktura(obrazPanel));
		    
			//ZAPISZ
		   	bSave = new JButton("Zapisz");
	    	bSave.setBounds(150,900,100,20);
	    	add(bSave);
	    	bSave.addActionListener(this);
    } 

    
    
    
    public int print(Graphics g, PageFormat pf, int page) throws
                                                        PrinterException {

    	
        if (page > 0) { /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }
 
        /* User (0,0) is typically outside the imageable area, so we must
         * translate by the X and Y values in the PageFormat to avoid clipping
         */
        Graphics2D g2d = (Graphics2D)g;
        g2d.translate(pf.getImageableX()-4, pf.getImageableY());
        
 
        /*  */
     
        panelToPrint.printAll(g);
 
        /* tell the caller that this page is part of the printed document */
        return PAGE_EXISTS;
    	}   
    
    
    
    
    
    
    public void actionPerformed(ActionEvent e) {  	 

   	 Object zrodlo = e.getSource();
   	 if(zrodlo == zapisaneFirmy) {
   		 String wybranaFirma = zapisaneFirmy.getSelectedItem().toString(); //ODCZYTANIE WYNRANEJ OPCJI Z COMBOBOXA
		
	    File firma = new File("Firmy\\Licznik\\licznik.txt");
	    Scanner in = null;
		try {
			in = new Scanner(firma);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	    String zawartosc = in.nextLine();
	    int numer = Integer.valueOf(zawartosc);
	    //System.out.println(numer); //TEST				
	    
	   
	    
	    for(int i = 1; i<numer; i++) {
		if(wybranaFirma.equals("Firma " + i))
		{
			File daneFirmy = new File("Firmy\\" + i +"-firma.txt");
			
			Scanner dane = null;
			try {
				in = new Scanner(daneFirmy);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		    String daneTekstNazwaFirmy = in.nextLine();
		    String daneTekstNipFirmy = in.nextLine();
		    String daneTekstAdresFirmy = in.nextLine();
		    //System.out.println(daneTekstAdresFirmy);

		    wNazwaFirmy.setText(daneTekstNazwaFirmy);	      
		    wNipFirmy.setText(daneTekstNipFirmy);
		    wAdres.setText(daneTekstAdresFirmy);
		    
			wNazwaFirmyKopia.setText(daneTekstNazwaFirmy);	      
			wNipFirmyKopia.setText(daneTekstNipFirmy);
			wAdresKopia.setText(daneTekstAdresFirmy);
		}
	    }
		
   	 }
   	 
   	 else if(zrodlo == bWyswietlDane) {
   		
   		
   		 
   		int liczbaZnakowNrFaktury = nrFaktury.getText().length();

   		if(liczbaZnakowNrFaktury>0 && liczbaZnakowNrFaktury<=10) {
   		wNrFaktury.setText(nrFaktury.getText());
   		wNrFakturyKopia.setText(nrFaktury.getText());
   		}
   		else
   		{
   			wNrFaktury.setText("popraw dane");
   			wNrFakturyKopia.setText("popraw dane");
   		}
   		
   		//----------------------------------------------------------------------------//
   		
   		int liczbaZnakowDzien = dataDzien.getText().length();
   		
   		String sprawdzenieDataDzien = dataDzien.getText();
   		boolean testDataDzien = sprawdzenieDataDzien.chars().allMatch( Character::isDigit );
   		if(testDataDzien == true) {
   		wDataDzien.setText(dataDzien.getText());
   		wDataDzienKopia.setText(dataDzien.getText());
   		}
   		else
   		{
   	   		wDataDzien.setText("xx");
   	   		wDataDzienKopia.setText("xx");
   		}
   		
   		//----------------------------------------------------------------------------//
   		
  		int liczbaZnakowMiesiac = dataMiesiac.getText().length();
  		
   		String sprawdzenieDataMiesiac = dataMiesiac.getText();
   		boolean testDataMiesiac = sprawdzenieDataMiesiac.chars().allMatch( Character::isDigit );
   		if(testDataMiesiac == true && liczbaZnakowMiesiac<=2 && liczbaZnakowMiesiac>0) {
   		wDataMiesiac.setText(dataMiesiac.getText());
   		wDataMiesiacKopia.setText(dataMiesiac.getText());
   		}
   		else
   		{
   			wDataMiesiac.setText("xx");
   	   		wDataMiesiacKopia.setText("xx");
   		}
   		
   		//----------------------------------------------------------------------------//
   		
  		int liczbaZnakowRok = dataRok.getText().length();
  		
   		String sprawdzenieDataRok = dataRok.getText();
   		boolean testDataRok = sprawdzenieDataRok.chars().allMatch( Character::isDigit );
   		if(testDataRok == true && liczbaZnakowRok<=2 && liczbaZnakowRok>0) {
   	   	wDataRok.setText(dataRok.getText());
   	   	wDataRokKopia.setText(dataRok.getText());
   		}
   		else
   		{
   			wDataRok.setText("xx");
   	   		wDataRokKopia.setText("xx");
   		}
   		
   		//----------------------------------------------------------------------------//
   		
   		data2.setText(dataDzien.getText()+"."+dataMiesiac.getText()+"."+"20"+dataRok.getText());
   		data2Kopia.setText(dataDzien.getText()+"."+dataMiesiac.getText()+"."+"20"+dataRok.getText());
   		
   		//----------------------------------------------------------------------------//
   		
   		int liczbaZnakowNazwaFirmy = nazwaFirmy.getText().length();
   		if(liczbaZnakowNazwaFirmy>0 && liczbaZnakowNazwaFirmy<=45) {
   		wNazwaFirmy.setText(nazwaFirmy.getText());
   		wNazwaFirmyKopia.setText(nazwaFirmy.getText());
   		}
   		else
   		{
   			wNazwaFirmy.setText("popraw dane");
   	   		wNazwaFirmyKopia.setText("popraw dane");
   		}
   		
   		//----------------------------------------------------------------------------//
   		
   		//int liczbaZnakowNipFirmy = nip.getText().length();
   		//String sprawdzenieNipFirmy = nip.getText();
   		//boolean testNipFirmy = sprawdzenieNipFirmy.chars().allMatch( Character::isDigit );
   		//if(liczbaZnakowNipFirmy==10) {
   		wNipFirmy.setText(nip.getText());
   	   	wNipFirmyKopia.setText(nip.getText());
   		//}
   		//else
   		//{
   	   		//wNipFirmy.setText("popraw dane");
   	   	   //wNipFirmyKopia.setText("popraw dane");
   		//}
   		
   		//----------------------------------------------------------------------------//
   		
   		int liczbaZnakowAdresFirmy = adresFirmy.getText().length();
   		if(liczbaZnakowAdresFirmy > 0 && liczbaZnakowAdresFirmy < 30) {
   		wAdres.setText(adresFirmy.getText());
   	   	wAdresKopia.setText(adresFirmy.getText());
   		}
   		else
   		{
   	   		wAdres.setText("popraw dane");
   	   	   	wAdresKopia.setText("popraw dane");
   		}
   		
   		//----------------------------------------------------------------------------// 		
   		
   		//int liczbaZnakowMiejscowosc = miejscowoscWystawienia.getText().length();
   		
   		//String sprawdzenieMiejscowosc = miejscowoscWystawienia.getText();
   		//char kolejnyZnak;
   		//boolean[] tablica = new boolean[100];
   		//for(int i = 0; i<liczbaZnakowMiejscowosc; i++)
   		//{		
   		//kolejnyZnak = sprawdzenieMiejscowosc.charAt(i);
   		//boolean testMiejscowosc = Character.isLetter(kolejnyZnak);
   		//tablica[i] = testMiejscowosc;
   		//System.out.println("Kolejna komrka to: " + tablica[i]);
   		//}

   		//szukanie w tablicy true
   		//for(int j=0; j<liczbaZnakowMiejscowosc; j++)
   		//{
   			//System.out.println(tablica[j]);
   		//	if(tablica[j] == false)
   		//	{
 		//   	 	wMiejscowosc.setText("popraw dane");
 		//   	 	wMiejscowoscKopia.setText("popraw dane");
   		//		return;
   		//	}
   		//}
   		//if(liczbaZnakowMiejscowosc > 0 && liczbaZnakowMiejscowosc <= 18) {

   		wMiejscowosc.setText("Miejscowosc");
   		wMiejscowoscKopia.setText("Miejscowosc");
   		
   		//}
   		//else
   		//{
		//   	 wMiejscowosc.setText("popraw dane");
		//   	 wMiejscowoscKopia.setText("popraw dane");
   		//}
   		
   		
   		//----------------------------------------------------------------------------// 		
   		
   		//int liczbaZnakowBank = bank.getText().length();
   		//if(liczbaZnakowBank>0 && liczbaZnakowBank<20) {
   		wBank.setText("Bank");
   		wBankKopia.setText("Bank");
   		//}
   		//else
   		//{
   	   	//	wBank.setText("popraw dane");
   	   	//	wBankKopia.setText("popraw dane");
   		//}
   		 		
   		//----------------------------------------------------------------------------// 
   		
   		//int liczbaZnakowNrKontaBankowego = nrKonta.getText().length();
   		//String sprawdzenieNrKontaBankowego = nrKonta.getText();
   		//boolean testNrKontaBankowego = sprawdzenieNrKontaBankowego.chars().allMatch( Character::isDigit );
   		//if(testNrKontaBankowego == true && liczbaZnakowNrKontaBankowego==26) {
   		wNrKontaBankowego.setText("Nr konta");
   		wNrKontaBankowegoKopia.setText("Nr konta");
   		//}
   		//else
   		//{
   		//	wNrKontaBankowego.setText("popraw dane");
   		//	wNrKontaBankowegoKopia.setText("popraw dane");
   		//}

  		//----------------------------------------------------------------------------// 
   		//int liczbaZnakowSposobZaplaty = sposobZaplaty.getText().length();
   		
   		//String sprawdzenieSposobZaplaty = sposobZaplaty.getText();
   		//char kolejnyZnakSposobZaplaty;
   		//boolean[] tablicaSposobZaplaty = new boolean[100];
   		//for(int i = 0; i<liczbaZnakowSposobZaplaty; i++)
   		//{		
   		//kolejnyZnak = sprawdzenieSposobZaplaty.charAt(i);
   		//boolean testSposobZaplaty = Character.isLetter(kolejnyZnak);
   		//tablicaSposobZaplaty[i] = testSposobZaplaty;
   		//System.out.println("Kolejna komrka to: " + tablicaSposobZaplaty[i]);
   		//}

   		//szukanie w tablicy true
   		//for(int j=0; j<liczbaZnakowSposobZaplaty; j++)
   		//{
   			//System.out.println(tablica[j]);
   		//	if(tablicaSposobZaplaty[j] == false)
   		//	{
   		//  		wSposobZaplaty.setText("popraw dane");
   		//   		wSposobZaplatyKopia.setText("popraw dane");
   		//		return;
   		//	}
   		//}
   		//if(liczbaZnakowSposobZaplaty > 0 && liczbaZnakowSposobZaplaty <= 18) {

   	   		wSposobZaplaty.setText("przelew");
   	   		wSposobZaplatyKopia.setText("przelew");
   		
   		//}
   		//else
   		//{
   		//	wSposobZaplaty.setText("popraw dane");
   		//	wSposobZaplatyKopia.setText("popraw dane");
   		//}
   		
  		//----------------------------------------------------------------------------// 
   		//int liczbaZnakowTerminZaplaty = terminZaplaty.getText().length();
   		//if(liczbaZnakowTerminZaplaty>0 && liczbaZnakowTerminZaplaty<10) {
   		wTerminZaplaty.setText("X dni");
   		wTerminZaplatyKopia.setText("X dni");	
   		//}
   		//else
   		//{
   	   	//	wTerminZaplaty.setText("popraw dane");
   	   	//	wTerminZaplatyKopia.setText("popraw dane");	
   		//}
   	 } 
   	 else if(zrodlo == listaProdukt1){
 		String pierwszyWybranyProdukt = listaProdukt1.getSelectedItem().toString(); //ODCZYTANIE WYNRANEJ OPCJI Z COMBOBOXA
 		
	    File wartosc = new File("Produkty\\Licznik\\licznik.txt");
	    Scanner in = null;
		try {
			in = new Scanner(wartosc);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	    String zawartosc = in.nextLine();
	    int numer = Integer.valueOf(zawartosc);
	    //System.out.println(numer); //TEST				
	    
	   
	    
	    for(int i = 1; i<numer; i++) {
 		if(pierwszyWybranyProdukt.equals("Produkt " + i))
 		{
 			File daneProduktu = new File("Produkty\\" + i +"-produkt.txt");
 			
 			Scanner dane = null;
 			try {
 				in = new Scanner(daneProduktu);
 			} catch (FileNotFoundException e2) {
 				// TODO Auto-generated catch block
 				e2.printStackTrace();
 			}

 		    String daneTekstNazwa = in.nextLine();
 		    String daneTekstMiara = in.nextLine();
 		    //System.out.println(daneTekst2);

 			wNazwa1.setText(daneTekstNazwa);	      
 			wMiara1.setText(daneTekstMiara);
 			
 			wNazwa1Kopia.setText(daneTekstNazwa);	      
 			wMiara1Kopia.setText(daneTekstMiara);
 		}
	    }
 		
   	 }
   	 
   else if(zrodlo == bWyswietl1) 
   {
	   lp1.setText("1");
	   lp1Kopia.setText("1");  
	   
	   
	   //----------------------------------------------------------------------------// 
	
  		int liczbaZnakowNazwa1 = podajNazweProdukt1.getText().length();
  		if(liczbaZnakowNazwa1>0 && liczbaZnakowNazwa1<70) {	
  		   wNazwa1.setText(podajNazweProdukt1.getText());
  		   wNazwa1Kopia.setText(podajNazweProdukt1.getText());
  		}
  		else
  		{
		   wNazwa1.setText("popraw dane");
		   wNazwa1Kopia.setText("popraw dane");
  		}
  		
  		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowMiara1 = podajMiareProduktu1.getText().length();
   		if(liczbaZnakowMiara1 > 0 && liczbaZnakowMiara1 <= 4) {
   		wMiara1.setText(podajMiareProduktu1.getText());
   		wMiara1Kopia.setText(podajMiareProduktu1.getText());		
   		}
   		else
   		{
   			wMiara1.setText("xxx");
		   	wMiara1Kopia.setText("xxx");
   		}	
	    
   		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowIlosc1 = podajIloscProdukt1.getText().length();
   		String sprawdzenieIlosc1 = podajIloscProdukt1.getText();
   		boolean testIlosc1 = sprawdzenieIlosc1.chars().allMatch( Character::isDigit );
   		if(testIlosc1 == true && liczbaZnakowIlosc1<=6) {
   		   wIlosc1.setText(String.valueOf(podajIloscProdukt1.getText()));
   		   wIlosc1Kopia.setText(String.valueOf(podajIloscProdukt1.getText())); 
   		}
   		else
   		{
   			wIlosc1.setText("xxxx");
   		 	wIlosc1Kopia.setText("xxxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena1 = podajCeneProdukt1Zl.getText().length();
   		String sprawdzenieCena1 = podajCeneProdukt1Zl.getText();
   		boolean testCena1 = sprawdzenieCena1.chars().allMatch( Character::isDigit );
   		if(testCena1 == true && liczbaZnakowCena1 > 0 && liczbaZnakowCena1 <= 5) {
   		   wCena1.setText(String.valueOf(podajCeneProdukt1Zl.getText()));
   		   wCena1Kopia.setText(String.valueOf(podajCeneProdukt1Zl.getText()));
   		}
   		else
   		{
   			wCena1.setText("xxx");
   			wCena1Kopia.setText("xxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena1GR =podajCeneProdukt1GR.getText().length();
   		String sprawdzenieCena1GR = podajCeneProdukt1GR.getText();
   		boolean testCena1GR = sprawdzenieCena1GR.chars().allMatch( Character::isDigit );
   		if(testCena1GR == true && liczbaZnakowCena1GR == 2) {
   		   wCena1GR.setText(String.valueOf(podajCeneProdukt1GR.getText()));
   		   wCena1GRKopia.setText(String.valueOf(podajCeneProdukt1GR.getText()));
   		}
   		else
   		{
   			wCena1GR.setText("xx");
   			wCena1GRKopia.setText("xx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowStawka1 =stawkaPodatku1.getText().length();
 		//System.out.print(liczbaZnakowStawka1);
   		int sprawdzenieStawka1 = Integer.valueOf(stawkaPodatku1.getText());
  
   		if(sprawdzenieStawka1 == 0 || sprawdzenieStawka1 == 5 || sprawdzenieStawka1 == 8 || sprawdzenieStawka1 == 23) {
   		   wStawka1.setText(String.valueOf(stawkaPodatku1.getText()));	   
   		   wStawka1Kopia.setText(String.valueOf(stawkaPodatku1.getText()));
   		}
   		else
   		{
   			wStawka1.setText("xx");
   			wStawka1Kopia.setText("xx");
   		} 

	  
	   //--------------------------------------------------------------//
	   //--------------------------------------------------------------//
	   //OBLICZANIE KWOT
	   //--------------------------------------------------------------//
	   //--------------------------------------------------------------//
	   //KOLUMNA WARTO TOWARW	
	   	iloscProduktu1 = Integer.parseInt(podajIloscProdukt1.getText());
	   
	   	wartoscTowarow1ZL=iloscProduktu1*((Double.parseDouble(podajCeneProdukt1Zl.getText()))+(Double.parseDouble(podajCeneProdukt1GR.getText())/100));
	  
	  //--------------------------------------------------------------//
	  //--------------------------------------------------------------//
	   						//AKTUALIZACJA 02.09.2020 
	   	wartoscTowarow1ZL=BigDecimal.valueOf(wartoscTowarow1ZL).setScale(5, RoundingMode.HALF_UP).doubleValue(); //dodane dla wszystkich produktow ponizej
	   	//System.out.print(wartoscTowarow1ZL);
	  //--------------------------------------------------------------//
	  //--------------------------------------------------------------//
	   	
	   	wartoscTowarow1GR=BigDecimal.valueOf(wartoscTowarow1ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow1GR=BigDecimal.valueOf(wartoscTowarow1GR).setScale(4, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow1GR=BigDecimal.valueOf(wartoscTowarow1GR).setScale(3, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow1GR=(BigDecimal.valueOf(wartoscTowarow1GR).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
	
	   	wyswietlWartoscTowarow1ZL.setText(String.valueOf((int)wartoscTowarow1ZL));
		wyswietlWartoscTowarow1ZLKopia.setText(String.valueOf((int)wartoscTowarow1ZL));
		
		stawkaPodatku1Wartosc = Integer.parseInt(stawkaPodatku1.getText()); //w celu ustalenia wyswietlania z uzwglednieniem stawki podatku		
  	
	   	
	   	wartoscTowarow1GR=Math.round(wartoscTowarow1GR); //zaokraglenie
	   	

		if(wartoscTowarow1GR==1) {
			wyswietlWartoscTowarow1GR.setText("01");
			wyswietlWartoscTowarow1GRKopia.setText("01");		
				
		}else if(wartoscTowarow1GR==2) {
			wyswietlWartoscTowarow1GR.setText("02");						
			wyswietlWartoscTowarow1GRKopia.setText("02");

		}else if(wartoscTowarow1GR==3) {
			wyswietlWartoscTowarow1GR.setText("03");	
			wyswietlWartoscTowarow1GRKopia.setText("03");

		}else if(wartoscTowarow1GR==4) {
			wyswietlWartoscTowarow1GR.setText("04");		
			wyswietlWartoscTowarow1GRKopia.setText("04");

		}else if(wartoscTowarow1GR==5) {
			wyswietlWartoscTowarow1GR.setText("05");			
			wyswietlWartoscTowarow1GRKopia.setText("05");
			
		}else if(wartoscTowarow1GR==6) {
			wyswietlWartoscTowarow1GR.setText("06");
			wyswietlWartoscTowarow1GRKopia.setText("06");
			
		}else if(wartoscTowarow1GR==7) {
			wyswietlWartoscTowarow1GR.setText("07");
			wyswietlWartoscTowarow1GRKopia.setText("07");
			
		}else if(wartoscTowarow1GR==8) {
			wyswietlWartoscTowarow1GR.setText("08");
			wyswietlWartoscTowarow1GRKopia.setText("08");
			
		}else if(wartoscTowarow1GR==9) {
			wyswietlWartoscTowarow1GR.setText("09");	
			wyswietlWartoscTowarow1GRKopia.setText("09");
			
		}else if(wartoscTowarow1GR==0) {
			wyswietlWartoscTowarow1GR.setText(" -");						
			wyswietlWartoscTowarow1GRKopia.setText(" -");
			
		}else {
			wyswietlWartoscTowarow1GR.setText(String.valueOf(Math.round(wartoscTowarow1GR)));			
			wyswietlWartoscTowarow1GRKopia.setText(String.valueOf(Math.round(wartoscTowarow1GR)));
		}	
		
//---------SUMOWANIE KWOTY BEZ PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//			
		if(stawkaPodatku1Wartosc==23)
		{					
			Kwota_bez_podatku_23 = (int)wartoscTowarow1ZL + (wartoscTowarow1GR/100);	
		}
		else if(stawkaPodatku1Wartosc==8)
		{
			Kwota_bez_podatku_8 = (int)wartoscTowarow1ZL + (wartoscTowarow1GR/100);
		}
		else if(stawkaPodatku1Wartosc==5)
		{
			Kwota_bez_podatku_5 = (int)wartoscTowarow1ZL + (wartoscTowarow1GR/100);
		}
		else if(stawkaPodatku1Wartosc==0)
		{
			Kwota_bez_podatku_0 = (int)wartoscTowarow1ZL + (wartoscTowarow1GR/100);
		}
		
		//--------------------------------------------------------------// 
		//KOLUMNA WARTOSC PODATKU
		//--------------------------------------------------------------//
		if(stawkaPodatku1Wartosc==23)
		{					
			wartoscPodatku1ZL= wartoscTowarow1ZL * 0.23;	
		}
		else if(stawkaPodatku1Wartosc==8)
		{
			wartoscPodatku1ZL= wartoscTowarow1ZL * 0.08;	
		}
		else if(stawkaPodatku1Wartosc==5)
		{
			wartoscPodatku1ZL= wartoscTowarow1ZL * 0.05;	
		}
		else if(stawkaPodatku1Wartosc==0)
		{
			wartoscPodatku1ZL= 0;	
		}
	   	
	   	wyswietlWartoscPodatku1ZL.setText(String.valueOf((int)wartoscPodatku1ZL));
		wyswietlWartoscPodatku1ZLKopia.setText(String.valueOf((int)wartoscPodatku1ZL));
		
		Kwota_podatku kwota_podatku_1 = new Kwota_podatku(wartoscPodatku1ZL);
		wartoscPodatku1GR = kwota_podatku_1.oblicz_podatek();
	   	
	   	wartoscPodatku1GR=Math.round(wartoscPodatku1GR); //zaokraglenie
	   	
	   	if(wartoscPodatku1GR==1) {
			wyswietlWartoscPodatku1GR.setText("01");
			wyswietlWartoscPodatku1GRKopia.setText("01");		
				
		}else if(wartoscPodatku1GR==2) {
			wyswietlWartoscPodatku1GR.setText("02");						
			wyswietlWartoscPodatku1GRKopia.setText("02");

		}else if(wartoscPodatku1GR==3) {
			wyswietlWartoscPodatku1GR.setText("03");	
			wyswietlWartoscPodatku1GRKopia.setText("03");

		}else if(wartoscPodatku1GR==4) {
			wyswietlWartoscPodatku1GR.setText("04");		
			wyswietlWartoscPodatku1GRKopia.setText("04");

		}else if(wartoscPodatku1GR==5) {
			wyswietlWartoscPodatku1GR.setText("05");			
			wyswietlWartoscPodatku1GRKopia.setText("05");
			
		}else if(wartoscPodatku1GR==6) {
			wyswietlWartoscPodatku1GR.setText("06");
			wyswietlWartoscPodatku1GRKopia.setText("06");
			
		}else if(wartoscPodatku1GR==7) {
			wyswietlWartoscPodatku1GR.setText("07");
			wyswietlWartoscPodatku1GRKopia.setText("07");
			
		}else if(wartoscPodatku1GR==8) {
			wyswietlWartoscPodatku1GR.setText("08");
			wyswietlWartoscPodatku1GRKopia.setText("08");
			
		}else if(wartoscPodatku1GR==9) {
			wyswietlWartoscPodatku1GR.setText("09");	
			wyswietlWartoscPodatku1GRKopia.setText("09");
			
		}else if(wartoscPodatku1GR==0) {
			wyswietlWartoscPodatku1GR.setText(" -");						
			wyswietlWartoscPodatku1GRKopia.setText(" -");
			
		}else {
			wyswietlWartoscPodatku1GR.setText(String.valueOf(Math.round(wartoscPodatku1GR)));			
			wyswietlWartoscPodatku1GRKopia.setText(String.valueOf(Math.round(wartoscPodatku1GR)));
		}
	  //------------------------------------------------------------------------------------//
	  //---------SUMOWANIE KWOTY PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//	

	  		if(stawkaPodatku1Wartosc==23)
	  		{					
	  			Kwota_podatku_23 = (int)wartoscPodatku1ZL + (wartoscPodatku1GR/100);
	  		}
	  		else if(stawkaPodatku1Wartosc==8)
	  		{
	  			Kwota_podatku_8 = (int)wartoscPodatku1ZL + (wartoscPodatku1GR/100);
	  		}
	  		else if(stawkaPodatku1Wartosc==5)
	  		{
	  			Kwota_podatku_5 = (int)wartoscPodatku1ZL + (wartoscPodatku1GR/100);
	  		}
	  		else if(stawkaPodatku1Wartosc==0)
	  		{
	  			Kwota_podatku_0 = (int)wartoscPodatku1ZL + (wartoscPodatku1GR/100);
	  		}
	  	 //------------------------------------------------------------------------------------//
	  	 //------------------------------KWOTA WRAZ PODATKIEM----------------------------------//		
	  		wartoscZPodatkiem1Zl = (int)wartoscTowarow1ZL + (wartoscTowarow1GR/100) + (int)wartoscPodatku1ZL + (wartoscPodatku1GR/100);
	  		wyswietlWartoscZPodatkiem1ZL.setText(String.valueOf((int)wartoscZPodatkiem1Zl));
	  		wyswietlWartoscZPodatkiem1ZLKopia.setText(String.valueOf((int)wartoscZPodatkiem1Zl));
	  		
			Kwota_wraz_z_podatkiem kwota_wraz_z_podatkiem = new Kwota_wraz_z_podatkiem(wartoscZPodatkiem1Zl);
			wartoscZPodatkiem1GR = kwota_wraz_z_podatkiem.oblicz_kwote_wraz_z_podatekiem();
		   	
			wartoscZPodatkiem1GR=Math.round(wartoscZPodatkiem1GR); //zaokraglenie
		   	
		   	if(wartoscZPodatkiem1GR==1) {
		   		wyswietlWartoscZPodatkiem1GR.setText("01");
		   		wyswietlWartoscZPodatkiem1GRKopia.setText("01");		
					
			}else if(wartoscZPodatkiem1GR==2) {
				wyswietlWartoscZPodatkiem1GR.setText("02");						
				wyswietlWartoscZPodatkiem1GRKopia.setText("02");

			}else if(wartoscZPodatkiem1GR==3) {
				wyswietlWartoscZPodatkiem1GR.setText("03");	
				wyswietlWartoscZPodatkiem1GRKopia.setText("03");

			}else if(wartoscZPodatkiem1GR==4) {
				wyswietlWartoscZPodatkiem1GR.setText("04");		
				wyswietlWartoscZPodatkiem1GRKopia.setText("04");

			}else if(wartoscZPodatkiem1GR==5) {
				wyswietlWartoscZPodatkiem1GR.setText("05");			
				wyswietlWartoscZPodatkiem1GRKopia.setText("05");
				
			}else if(wartoscZPodatkiem1GR==6) {
				wyswietlWartoscZPodatkiem1GR.setText("06");
				wyswietlWartoscZPodatkiem1GRKopia.setText("06");
				
			}else if(wartoscZPodatkiem1GR==7) {
				wyswietlWartoscZPodatkiem1GR.setText("07");
				wyswietlWartoscZPodatkiem1GRKopia.setText("07");
				
			}else if(wartoscZPodatkiem1GR==8) {
				wyswietlWartoscZPodatkiem1GR.setText("08");
				wyswietlWartoscZPodatkiem1GRKopia.setText("08");
				
			}else if(wartoscZPodatkiem1GR==9) {
				wyswietlWartoscZPodatkiem1GR.setText("09");	
				wyswietlWartoscZPodatkiem1GRKopia.setText("09");
				
			}else if(wartoscZPodatkiem1GR==0) {
				wyswietlWartoscZPodatkiem1GR.setText(" -");						
				wyswietlWartoscZPodatkiem1GRKopia.setText(" -");
				
			}else {
				wyswietlWartoscZPodatkiem1GR.setText(String.valueOf(Math.round(wartoscZPodatkiem1GR)));			
				wyswietlWartoscZPodatkiem1GRKopia.setText(String.valueOf(Math.round(wartoscZPodatkiem1GR)));
			}
		   	
			//---------SUMOWANIE KWOTY WRAZ Z PODATKIEM W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
			if(stawkaPodatku1Wartosc==23)
			{					
				wartoscZPodatkiemZl23 = (int)wartoscZPodatkiem1Zl + (wartoscZPodatkiem1GR/100);	
			}
			else if(stawkaPodatku1Wartosc==8)
			{
				wartoscZPodatkiemZl8 = (int)wartoscZPodatkiem1Zl + (wartoscZPodatkiem1GR/100);	
			}
			else if(stawkaPodatku1Wartosc==5)
			{
				wartoscZPodatkiemZl5 = (int)wartoscZPodatkiem1Zl + (wartoscZPodatkiem1GR/100);	
			}
			else if(stawkaPodatku1Wartosc==0)
			{
				wartoscZPodatkiemZl0 = (int)wartoscZPodatkiem1Zl + (wartoscZPodatkiem1GR/100);	
			}
   }
   	 
   else if(zrodlo == listaProdukt2){
		String drugiWybranyProdukt = listaProdukt2.getSelectedItem().toString(); //ODCZYTANIE WYNRANEJ OPCJI Z COMBOBOXA
		
	    File wartosc = new File("Produkty\\Licznik\\licznik.txt");
	    Scanner in = null;
		try {
			in = new Scanner(wartosc);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	    String zawartosc = in.nextLine();
	    int numer = Integer.valueOf(zawartosc);
	    //System.out.println(numer); //TEST				
	    
	   
	    
	    for(int i = 1; i<numer; i++) {
		if(drugiWybranyProdukt.equals("Produkt " + i))
		{
			File daneProduktu = new File("Produkty\\" + i +"-produkt.txt");
			
			Scanner dane = null;
			try {
				in = new Scanner(daneProduktu);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		    String daneTekstNazwa = in.nextLine();
		    String daneTekstMiara = in.nextLine();
		    //System.out.println(daneTekst2);

			wNazwa2.setText(daneTekstNazwa);	      
			wMiara2.setText(daneTekstMiara);
			
			wNazwa2Kopia.setText(daneTekstNazwa);	      
			wMiara2Kopia.setText(daneTekstMiara);
		}
	    }
		
  	 }
   	 
   else if(zrodlo == bWyswietl2) 
   {
	   lp2.setText("2");
	   lp2Kopia.setText("2");  
	   
	   //----------------------------------------------------------------------------// 
	  
  		int liczbaZnakowNazwa2 = podajNazweProdukt2.getText().length();
  		if(liczbaZnakowNazwa2>0 && liczbaZnakowNazwa2<70) {	
  		   wNazwa2.setText(podajNazweProdukt2.getText());
  		   wNazwa2Kopia.setText(podajNazweProdukt2.getText());
  		}
  		else
  		{
		   wNazwa2.setText("popraw dane");
		   wNazwa2Kopia.setText("popraw dane");
  		}
	   
  		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowMiara2 = podajMiareProduktu2.getText().length();
   		if(liczbaZnakowMiara2 > 0 && liczbaZnakowMiara2 <= 4) {
   		wMiara2.setText(podajMiareProduktu2.getText());
   		wMiara2Kopia.setText(podajMiareProduktu2.getText());		
   		}
   		else
   		{
   			wMiara2.setText("xxx");
		   	wMiara2Kopia.setText("xxx");
   		}	
	    
   		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowIlosc2 = podajIloscProdukt2.getText().length();
   		String sprawdzenieIlosc2 = podajIloscProdukt2.getText();
   		boolean testIlosc2 = sprawdzenieIlosc2.chars().allMatch( Character::isDigit );
   		if(testIlosc2 == true && liczbaZnakowIlosc2<=6) {
   		   wIlosc2.setText(String.valueOf(podajIloscProdukt2.getText()));
   		   wIlosc2Kopia.setText(String.valueOf(podajIloscProdukt2.getText())); 
   		}
   		else
   		{
   			wIlosc2.setText("xxxx");
   		 	wIlosc2Kopia.setText("xxxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena2 =podajCeneProdukt2Zl.getText().length();
   		String sprawdzenieCena2 = podajCeneProdukt2Zl.getText();
   		boolean testCena2 = sprawdzenieCena2.chars().allMatch( Character::isDigit );
   		if(testCena2 == true && liczbaZnakowCena2 > 0 && liczbaZnakowCena2 <= 5) {
   		   wCena2.setText(String.valueOf(podajCeneProdukt2Zl.getText()));
   		   wCena2Kopia.setText(String.valueOf(podajCeneProdukt2Zl.getText()));
   		}
   		else
   		{
   			wCena2.setText("xxx");
   			wCena2Kopia.setText("xxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena2GR =podajCeneProdukt2GR.getText().length();
   		String sprawdzenieCena2GR = podajCeneProdukt2GR.getText();
   		boolean testCena2GR = sprawdzenieCena2GR.chars().allMatch( Character::isDigit );
   		if(testCena2GR == true && liczbaZnakowCena2GR == 2) {
   		   wCena2GR.setText(String.valueOf(podajCeneProdukt2GR.getText()));
   		   wCena2GRKopia.setText(String.valueOf(podajCeneProdukt2GR.getText()));
   		}
   		else
   		{
   			wCena2GR.setText("xx");
   			wCena2GRKopia.setText("xx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowStawka2 =stawkaPodatku2.getText().length();
 		//System.out.print(liczbaZnakowStawka2);
   		int sprawdzenieStawka2 = Integer.valueOf(stawkaPodatku2.getText());
  
   		if(sprawdzenieStawka2 == 0 || sprawdzenieStawka2 == 5 || sprawdzenieStawka2 == 8 || sprawdzenieStawka2 == 23) {
   		   wStawka2.setText(String.valueOf(stawkaPodatku2.getText()));	   
   		   wStawka2Kopia.setText(String.valueOf(stawkaPodatku2.getText()));
   		}
   		else
   		{
   			wStawka2.setText("xx");
   			wStawka2Kopia.setText("xx");
   		} 
  
	   	iloscProduktu2 = Integer.parseInt(podajIloscProdukt2.getText());
		   
	   	wartoscTowarow2ZL=iloscProduktu2*((Double.parseDouble(podajCeneProdukt2Zl.getText()))+(Double.parseDouble(podajCeneProdukt2GR.getText())/100));
	   	wartoscTowarow2ZL=BigDecimal.valueOf(wartoscTowarow2ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	
	   	wartoscTowarow2GR=BigDecimal.valueOf(wartoscTowarow2ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow2GR=BigDecimal.valueOf(wartoscTowarow2GR).setScale(4, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow2GR=BigDecimal.valueOf(wartoscTowarow2GR).setScale(3, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow2GR=(BigDecimal.valueOf(wartoscTowarow2GR).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
	
	   	wyswietlWartoscTowarow2ZL.setText(String.valueOf((int)wartoscTowarow2ZL));
		wyswietlWartoscTowarow2ZLKopia.setText(String.valueOf((int)wartoscTowarow2ZL));
	   	
		stawkaPodatku2Wartosc = Integer.parseInt(stawkaPodatku2.getText());
		
	   	wartoscTowarow2GR=Math.round(wartoscTowarow2GR); //zaokraglenie
	   	
		if(wartoscTowarow2GR==1) {
			wyswietlWartoscTowarow2GR.setText("01");
			wyswietlWartoscTowarow2GRKopia.setText("01");
			
		}else if(wartoscTowarow2GR==2) {
			wyswietlWartoscTowarow2GR.setText("02");
			wyswietlWartoscTowarow2GRKopia.setText("02");
						
		}else if(wartoscTowarow2GR==3) {
			wyswietlWartoscTowarow2GR.setText("03");
			wyswietlWartoscTowarow2GRKopia.setText("03");
						
		}else if(wartoscTowarow2GR==4) {
			wyswietlWartoscTowarow2GR.setText("04");
			wyswietlWartoscTowarow2GRKopia.setText("04");
			
		}else if(wartoscTowarow2GR==5) {
			wyswietlWartoscTowarow2GR.setText("05");
			wyswietlWartoscTowarow2GRKopia.setText("05");
			
		}else if(wartoscTowarow2GR==6) {
			wyswietlWartoscTowarow2GR.setText("06");
			wyswietlWartoscTowarow2GRKopia.setText("06");
			
		}else if(wartoscTowarow2GR==7) {
			wyswietlWartoscTowarow2GR.setText("07");
			wyswietlWartoscTowarow2GRKopia.setText("07");

		}else if(wartoscTowarow2GR==8) {
			wyswietlWartoscTowarow2GR.setText("08");
			wyswietlWartoscTowarow2GRKopia.setText("08");
	
		}else if(wartoscTowarow2GR==9) {
			wyswietlWartoscTowarow2GR.setText("09");
			wyswietlWartoscTowarow2GRKopia.setText("09");
			
		}else if(wartoscTowarow2GR==0) {
			wyswietlWartoscTowarow2GR.setText(" -");
			wyswietlWartoscTowarow2GRKopia.setText(" -");
			
		}else {
			wyswietlWartoscTowarow2GR.setText(String.valueOf(Math.round(wartoscTowarow2GR)));
			wyswietlWartoscTowarow2GRKopia.setText(String.valueOf(Math.round(wartoscTowarow2GR)));
			
		}
//---------SUMOWANIE KWOTY BEZ PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
		if(stawkaPodatku2Wartosc==23)
		{					
			Kwota_bez_podatku_23 += (int)wartoscTowarow2ZL + (wartoscTowarow2GR/100);	
		}
		else if(stawkaPodatku2Wartosc==8)
		{
			Kwota_bez_podatku_8 += (int)wartoscTowarow2ZL + (wartoscTowarow2GR/100);	
		}
		else if(stawkaPodatku2Wartosc==5)
		{
			Kwota_bez_podatku_5 += (int)wartoscTowarow2ZL + (wartoscTowarow2GR/100);	
		}
		else if(stawkaPodatku2Wartosc==0)
		{
			Kwota_bez_podatku_0 += (int)wartoscTowarow2ZL + (wartoscTowarow2GR/100);	
		}

		//--------------------------------------------------------------// 
		//KOLUMNA WARTOSC PODATKU
		//--------------------------------------------------------------//
		if(stawkaPodatku2Wartosc==23)
		{					
			wartoscPodatku2ZL= wartoscTowarow2ZL * 0.23;	
		}
		else if(stawkaPodatku2Wartosc==8)
		{
			wartoscPodatku2ZL= wartoscTowarow2ZL * 0.08;	
		}
		else if(stawkaPodatku2Wartosc==5)
		{
			wartoscPodatku2ZL= wartoscTowarow2ZL * 0.05;	
		}
		else if(stawkaPodatku2Wartosc==0)
		{
			wartoscPodatku2ZL= 0;	
		}
	   	
		wyswietlWartoscPodatku2ZL.setText(String.valueOf((int)wartoscPodatku2ZL));
		wyswietlWartoscPodatku2ZLKopia.setText(String.valueOf((int)wartoscPodatku2ZL));		
		
		Kwota_podatku kwota_podatku_2 = new Kwota_podatku(wartoscPodatku2ZL);
		wartoscPodatku2GR = kwota_podatku_2.oblicz_podatek();
			   	
		wartoscPodatku2GR=Math.round(wartoscPodatku2GR); //zaokraglenie
			   	
		if(wartoscPodatku2GR==1) {
			wyswietlWartoscPodatku2GR.setText("01");
			wyswietlWartoscPodatku2GRKopia.setText("01");		
						
		}else if(wartoscPodatku2GR==2) {
			wyswietlWartoscPodatku2GR.setText("02");						
			wyswietlWartoscPodatku2GRKopia.setText("02");

		}else if(wartoscPodatku2GR==3) {
			wyswietlWartoscPodatku2GR.setText("03");	
			wyswietlWartoscPodatku2GRKopia.setText("03");

		}else if(wartoscPodatku2GR==4) {
			wyswietlWartoscPodatku2GR.setText("04");		
			wyswietlWartoscPodatku2GRKopia.setText("04");

		}else if(wartoscPodatku2GR==5) {
			wyswietlWartoscPodatku2GR.setText("05");			
			wyswietlWartoscPodatku2GRKopia.setText("05");
					
		}else if(wartoscPodatku2GR==6) {
			wyswietlWartoscPodatku2GR.setText("06");
			wyswietlWartoscPodatku2GRKopia.setText("06");
					
		}else if(wartoscPodatku2GR==7) {
			wyswietlWartoscPodatku2GR.setText("07");
			wyswietlWartoscPodatku2GRKopia.setText("07");
					
		}else if(wartoscPodatku2GR==8) {
			wyswietlWartoscPodatku2GR.setText("08");
			wyswietlWartoscPodatku2GRKopia.setText("08");
					
		}else if(wartoscPodatku2GR==9) {
			wyswietlWartoscPodatku2GR.setText("09");	
			wyswietlWartoscPodatku2GRKopia.setText("09");
					
		}else if(wartoscPodatku2GR==0) {
			wyswietlWartoscPodatku2GR.setText(" -");						
			wyswietlWartoscPodatku2GRKopia.setText(" -");
					
		}else {
			wyswietlWartoscPodatku2GR.setText(String.valueOf(Math.round(wartoscPodatku2GR)));			
			wyswietlWartoscPodatku2GRKopia.setText(String.valueOf(Math.round(wartoscPodatku2GR)));
		}
		//------------------------------------------------------------------------------------//
		//---------SUMOWANIE KWOTY PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//			
		  	if(stawkaPodatku2Wartosc==23)
		  	{					
		  		Kwota_podatku_23 += (int)wartoscPodatku2ZL + (wartoscPodatku2GR/100) ;	
		  	}
		  	else if(stawkaPodatku2Wartosc==8)
		  	{
		  		Kwota_podatku_8 += (int)wartoscPodatku2ZL + (wartoscPodatku2GR/100);
		  	}
		  	else if(stawkaPodatku2Wartosc==5)
		  	{
		  		Kwota_podatku_5 += (int)wartoscPodatku2ZL + (wartoscPodatku2GR/100);
		  	}
		  	else if(stawkaPodatku2Wartosc==0)
		  	{
		  		Kwota_podatku_0 += (int)wartoscPodatku2ZL + (wartoscPodatku2GR/100);
		  	}
		  	
		  //------------------------------------------------------------------------------------//
		  //------------------------------KWOTA WRAZ PODATKIEM----------------------------------//		
		  	wartoscZPodatkiem2Zl = (int)wartoscTowarow2ZL + (wartoscTowarow2GR/100) + (int)wartoscPodatku2ZL + (wartoscPodatku2GR/100);
		  	wyswietlWartoscZPodatkiem2ZL.setText(String.valueOf((int)wartoscZPodatkiem2Zl));
		  	wyswietlWartoscZPodatkiem2ZLKopia.setText(String.valueOf((int)wartoscZPodatkiem2Zl));
		  		
			Kwota_wraz_z_podatkiem kwota_wraz_z_podatkiem = new Kwota_wraz_z_podatkiem(wartoscZPodatkiem2Zl);
			wartoscZPodatkiem2GR = kwota_wraz_z_podatkiem.oblicz_kwote_wraz_z_podatekiem();
			   	
			wartoscZPodatkiem2GR=Math.round(wartoscZPodatkiem2GR); //zaokraglenie
			   	
			if(wartoscZPodatkiem2GR==1) {
			   	wyswietlWartoscZPodatkiem2GR.setText("01");
			   	wyswietlWartoscZPodatkiem2GRKopia.setText("01");		
						
			}else if(wartoscZPodatkiem2GR==2) {
				wyswietlWartoscZPodatkiem2GR.setText("02");						
				wyswietlWartoscZPodatkiem2GRKopia.setText("02");

			}else if(wartoscZPodatkiem2GR==3) {
				wyswietlWartoscZPodatkiem2GR.setText("03");	
				wyswietlWartoscZPodatkiem2GRKopia.setText("03");

			}else if(wartoscZPodatkiem2GR==4) {
				wyswietlWartoscZPodatkiem2GR.setText("04");		
				wyswietlWartoscZPodatkiem2GRKopia.setText("04");

			}else if(wartoscZPodatkiem2GR==5) {
				wyswietlWartoscZPodatkiem2GR.setText("05");			
				wyswietlWartoscZPodatkiem2GRKopia.setText("05");
					
			}else if(wartoscZPodatkiem2GR==6) {
				wyswietlWartoscZPodatkiem2GR.setText("06");
				wyswietlWartoscZPodatkiem2GRKopia.setText("06");
					
			}else if(wartoscZPodatkiem2GR==7) {
				wyswietlWartoscZPodatkiem2GR.setText("07");
				wyswietlWartoscZPodatkiem2GRKopia.setText("07");
					
			}else if(wartoscZPodatkiem2GR==8) {
				wyswietlWartoscZPodatkiem2GR.setText("08");
				wyswietlWartoscZPodatkiem2GRKopia.setText("08");
					
			}else if(wartoscZPodatkiem2GR==9) {
				wyswietlWartoscZPodatkiem2GR.setText("09");	
				wyswietlWartoscZPodatkiem2GRKopia.setText("09");
					
			}else if(wartoscZPodatkiem2GR==0) {
				wyswietlWartoscZPodatkiem2GR.setText(" -");						
				wyswietlWartoscZPodatkiem2GRKopia.setText(" -");
					
			}else {
				wyswietlWartoscZPodatkiem2GR.setText(String.valueOf(Math.round(wartoscZPodatkiem2GR)));			
				wyswietlWartoscZPodatkiem2GRKopia.setText(String.valueOf(Math.round(wartoscZPodatkiem2GR)));
			}
			
		//---------SUMOWANIE KWOTY WRAZ Z PODATKIEM W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
			if(stawkaPodatku2Wartosc==23)
			{					
				wartoscZPodatkiemZl23 += (int)wartoscZPodatkiem2Zl + (wartoscZPodatkiem2GR/100);	
			}
			else if(stawkaPodatku2Wartosc==8)
			{
				wartoscZPodatkiemZl8 += (int)wartoscZPodatkiem2Zl + (wartoscZPodatkiem2GR/100);	
			}
			else if(stawkaPodatku2Wartosc==5)
			{
				wartoscZPodatkiemZl5 += (int)wartoscZPodatkiem2Zl + (wartoscZPodatkiem2GR/100);	
			}
			else if(stawkaPodatku2Wartosc==0)
			{
				wartoscZPodatkiemZl0 += (int)wartoscZPodatkiem2Zl + (wartoscZPodatkiem2GR/100);	
			}
   }
   
 	 else if(zrodlo == listaProdukt3){
		String trzeciWybranyProdukt = listaProdukt3.getSelectedItem().toString(); //ODCZYTANIE WYNRANEJ OPCJI Z COMBOBOXA
		
	    File wartosc = new File("Produkty\\Licznik\\licznik.txt");
	    Scanner in = null;
		try {
			in = new Scanner(wartosc);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	    String zawartosc = in.nextLine();
	    int numer = Integer.valueOf(zawartosc);
	    //System.out.println(numer); //TEST				
	    
   
	    
	    for(int i = 1; i<numer; i++) {
		if(trzeciWybranyProdukt.equals("Produkt " + i))
		{
			File daneProduktu = new File("Produkty\\" + i +"-produkt.txt");
			
			Scanner dane = null;
			try {
				in = new Scanner(daneProduktu);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		    String daneTekstNazwa = in.nextLine();
		    String daneTekstMiara = in.nextLine();
		    //System.out.println(daneTekst2);

			wNazwa3.setText(daneTekstNazwa);	      
			wMiara3.setText(daneTekstMiara);
			
			wNazwa3Kopia.setText(daneTekstNazwa);	      
			wMiara3Kopia.setText(daneTekstMiara);
		}
	    }
		
 	 }
   	 
   else if(zrodlo == bWyswietl3) 
   {
	   lp3.setText("3");
	   lp3Kopia.setText("3");  
	   
	   //----------------------------------------------------------------------------// 
	  
  		int liczbaZnakowNazwa3 = podajNazweProdukt3.getText().length();
  		if(liczbaZnakowNazwa3>0 && liczbaZnakowNazwa3<70) {	
  		   wNazwa3.setText(podajNazweProdukt3.getText());
  		   wNazwa3Kopia.setText(podajNazweProdukt3.getText());
  		}
  		else
  		{
		   wNazwa3.setText("popraw dane");
		   wNazwa3Kopia.setText("popraw dane");
  		}
	   
  		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowMiara3 = podajMiareProduktu3.getText().length();
   		if(liczbaZnakowMiara3 > 0 && liczbaZnakowMiara3 <= 4) {
   		wMiara3.setText(podajMiareProduktu3.getText());
   		wMiara3Kopia.setText(podajMiareProduktu3.getText());		
   		}
   		else
   		{
   			wMiara3.setText("xxx");
		   	wMiara3Kopia.setText("xxx");
   		}	
	    
   		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowIlosc3 = podajIloscProdukt3.getText().length();
   		String sprawdzenieIlosc3 = podajIloscProdukt3.getText();
   		boolean testIlosc3 = sprawdzenieIlosc3.chars().allMatch( Character::isDigit );
   		if(testIlosc3 == true && liczbaZnakowIlosc3<=6) {
   		   wIlosc3.setText(String.valueOf(podajIloscProdukt3.getText()));
   		   wIlosc3Kopia.setText(String.valueOf(podajIloscProdukt3.getText())); 
   		}
   		else
   		{
   			wIlosc3.setText("xxxx");
   		 	wIlosc3Kopia.setText("xxxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena3 =podajCeneProdukt3Zl.getText().length();
   		String sprawdzenieCena3 = podajCeneProdukt3Zl.getText();
   		boolean testCena3 = sprawdzenieCena3.chars().allMatch( Character::isDigit );
   		if(testCena3 == true && liczbaZnakowCena3 > 0 && liczbaZnakowCena3 <= 5) {
   		   wCena3.setText(String.valueOf(podajCeneProdukt3Zl.getText()));
   		   wCena3Kopia.setText(String.valueOf(podajCeneProdukt3Zl.getText()));
   		}
   		else
   		{
   			wCena3.setText("xxx");
   			wCena3Kopia.setText("xxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena3GR =podajCeneProdukt3GR.getText().length();
   		String sprawdzenieCena3GR = podajCeneProdukt3GR.getText();
   		boolean testCena3GR = sprawdzenieCena3GR.chars().allMatch( Character::isDigit );
   		if(testCena3GR == true && liczbaZnakowCena3GR == 2) {
   		   wCena3GR.setText(String.valueOf(podajCeneProdukt3GR.getText()));
   		   wCena3GRKopia.setText(String.valueOf(podajCeneProdukt3GR.getText()));
   		}
   		else
   		{
   			wCena3GR.setText("xx");
   			wCena3GRKopia.setText("xx");
   		}

  		//----------------------------------------------------------------------------// 

 		
 		int liczbaZnakowStawka3 =stawkaPodatku3.getText().length();
 		//System.out.print(liczbaZnakowStawka3);
   		int sprawdzenieStawka3 = Integer.valueOf(stawkaPodatku3.getText());
  
   		if(sprawdzenieStawka3 == 0 || sprawdzenieStawka3 == 5 || sprawdzenieStawka3 == 8 || sprawdzenieStawka3 == 23) {
   		   wStawka3.setText(String.valueOf(stawkaPodatku3.getText()));	   
   		   wStawka3Kopia.setText(String.valueOf(stawkaPodatku3.getText()));
   		}
   		else
   		{
   			wStawka3.setText("xx");
   			wStawka3Kopia.setText("xx");
   		} 
  
	   
	   	iloscProduktu3 = Integer.parseInt(podajIloscProdukt3.getText());
		   
	   	wartoscTowarow3ZL=iloscProduktu3*((Double.parseDouble(podajCeneProdukt3Zl.getText()))+(Double.parseDouble(podajCeneProdukt3GR.getText())/100));
	   	wartoscTowarow3ZL=BigDecimal.valueOf(wartoscTowarow3ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	
	   	wartoscTowarow3GR=BigDecimal.valueOf(wartoscTowarow3ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow3GR=BigDecimal.valueOf(wartoscTowarow3GR).setScale(4, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow3GR=BigDecimal.valueOf(wartoscTowarow3GR).setScale(3, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow3GR=(BigDecimal.valueOf(wartoscTowarow3GR).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
	
	   	wyswietlWartoscTowarow3ZL.setText(String.valueOf((int)wartoscTowarow3ZL));
		wyswietlWartoscTowarow3ZLKopia.setText(String.valueOf((int)wartoscTowarow3ZL));
	   	
		stawkaPodatku3Wartosc = Integer.parseInt(stawkaPodatku3.getText());
		
	   	wartoscTowarow3GR=Math.round(wartoscTowarow3GR); //zaokraglenie
	   	
		if(wartoscTowarow3GR==1) {
			wyswietlWartoscTowarow3GR.setText("01");
			wyswietlWartoscTowarow3GRKopia.setText("01");
			
		}else if(wartoscTowarow3GR==2) {
			wyswietlWartoscTowarow3GR.setText("02");
			wyswietlWartoscTowarow3GRKopia.setText("02");
						
		}else if(wartoscTowarow3GR==3) {
			wyswietlWartoscTowarow3GR.setText("03");
			wyswietlWartoscTowarow3GRKopia.setText("03");
						
		}else if(wartoscTowarow3GR==4) {
			wyswietlWartoscTowarow3GR.setText("04");
			wyswietlWartoscTowarow3GRKopia.setText("04");
			
		}else if(wartoscTowarow3GR==5) {
			wyswietlWartoscTowarow3GR.setText("05");
			wyswietlWartoscTowarow3GRKopia.setText("05");
			
		}else if(wartoscTowarow3GR==6) {
			wyswietlWartoscTowarow3GR.setText("06");
			wyswietlWartoscTowarow3GRKopia.setText("06");
			
		}else if(wartoscTowarow3GR==7) {
			wyswietlWartoscTowarow3GR.setText("07");
			wyswietlWartoscTowarow3GRKopia.setText("07");

		}else if(wartoscTowarow3GR==8) {
			wyswietlWartoscTowarow3GR.setText("08");
			wyswietlWartoscTowarow3GRKopia.setText("08");
	
		}else if(wartoscTowarow3GR==9) {
			wyswietlWartoscTowarow3GR.setText("09");
			wyswietlWartoscTowarow3GRKopia.setText("09");
			
		}else if(wartoscTowarow3GR==0) {
			wyswietlWartoscTowarow3GR.setText(" -");
			wyswietlWartoscTowarow3GRKopia.setText(" -");
			
		}else {
			wyswietlWartoscTowarow3GR.setText(String.valueOf(Math.round(wartoscTowarow3GR)));
			wyswietlWartoscTowarow3GRKopia.setText(String.valueOf(Math.round(wartoscTowarow3GR)));
			
		}
		
//---------SUMOWANIE KWOTY BEZ PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
			if(stawkaPodatku3Wartosc==23)
			{					
				Kwota_bez_podatku_23 += (int)wartoscTowarow3ZL + (wartoscTowarow3GR/100);		
			}
			else if(stawkaPodatku3Wartosc==8)
			{
				Kwota_bez_podatku_8 += (int)wartoscTowarow3ZL + (wartoscTowarow3GR/100);		
			}
			else if(stawkaPodatku3Wartosc==5)
			{
				Kwota_bez_podatku_5 += (int)wartoscTowarow3ZL + (wartoscTowarow3GR/100);		
			}
			else if(stawkaPodatku3Wartosc==0)
			{
				Kwota_bez_podatku_0 += (int)wartoscTowarow3ZL + (wartoscTowarow3GR/100);		
			}

		//--------------------------------------------------------------// 
		//KOLUMNA WARTOSC PODATKU
		//--------------------------------------------------------------//
			if(stawkaPodatku3Wartosc==23)
			{					
				wartoscPodatku3ZL= wartoscTowarow3ZL * 0.23;	
			}
			else if(stawkaPodatku3Wartosc==8)
			{
				wartoscPodatku3ZL= wartoscTowarow3ZL * 0.08;	
			}
			else if(stawkaPodatku3Wartosc==5)
			{
				wartoscPodatku3ZL= wartoscTowarow3ZL * 0.05;	
			}
			else if(stawkaPodatku3Wartosc==0)
			{
				wartoscPodatku3ZL= 0;	
			}
					
		   	
		wyswietlWartoscPodatku3ZL.setText(String.valueOf((int)wartoscPodatku3ZL));
		wyswietlWartoscPodatku3ZLKopia.setText(String.valueOf((int)wartoscPodatku3ZL));	
			
		Kwota_podatku kwota_podatku_3 = new Kwota_podatku(wartoscPodatku3ZL);
		wartoscPodatku3GR = kwota_podatku_3.oblicz_podatek();

				   	
		wartoscPodatku3GR=Math.round(wartoscPodatku3GR); //zaokraglenie
				   	
		if(wartoscPodatku3GR==1) {
			wyswietlWartoscPodatku3GR.setText("01");
			wyswietlWartoscPodatku3GRKopia.setText("01");		
							
		}else if(wartoscPodatku3GR==2) {
			wyswietlWartoscPodatku3GR.setText("02");						
			wyswietlWartoscPodatku3GRKopia.setText("02");

		}else if(wartoscPodatku3GR==3) {
			wyswietlWartoscPodatku3GR.setText("03");	
			wyswietlWartoscPodatku3GRKopia.setText("03");

		}else if(wartoscPodatku3GR==4) {
			wyswietlWartoscPodatku3GR.setText("04");		
			wyswietlWartoscPodatku3GRKopia.setText("04");

		}else if(wartoscPodatku3GR==5) {
			wyswietlWartoscPodatku3GR.setText("05");			
			wyswietlWartoscPodatku3GRKopia.setText("05");
						
		}else if(wartoscPodatku3GR==6) {
			wyswietlWartoscPodatku3GR.setText("06");
			wyswietlWartoscPodatku3GRKopia.setText("06");
						
		}else if(wartoscPodatku3GR==7) {
			wyswietlWartoscPodatku3GR.setText("07");
			wyswietlWartoscPodatku3GRKopia.setText("07");
						
		}else if(wartoscPodatku3GR==8) {
			wyswietlWartoscPodatku3GR.setText("08");
			wyswietlWartoscPodatku3GRKopia.setText("08");
					
		}else if(wartoscPodatku3GR==9) {
			wyswietlWartoscPodatku3GR.setText("09");	
			wyswietlWartoscPodatku3GRKopia.setText("09");
						
		}else if(wartoscPodatku3GR==0) {
			wyswietlWartoscPodatku3GR.setText(" -");						
			wyswietlWartoscPodatku3GRKopia.setText(" -");
						
		}else {
			wyswietlWartoscPodatku3GR.setText(String.valueOf(Math.round(wartoscPodatku3GR)));			
			wyswietlWartoscPodatku3GRKopia.setText(String.valueOf(Math.round(wartoscPodatku3GR)));
		}
		//------------------------------------------------------------------------------------//
		//---------SUMOWANIE KWOTY PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//			
		if(stawkaPodatku3Wartosc==23)
		{					
		  	Kwota_podatku_23 += (int)wartoscPodatku3ZL + (wartoscPodatku3GR/100);	
		}
		else if(stawkaPodatku3Wartosc==8)
		{
		  	Kwota_podatku_8 += (int)wartoscPodatku3ZL + (wartoscPodatku3GR/100);
		}
		else if(stawkaPodatku3Wartosc==5)
		{
		  	Kwota_podatku_5 += (int)wartoscPodatku3ZL + (wartoscPodatku3GR/100);
		}
		else if(stawkaPodatku3Wartosc==0)
		{
		  	Kwota_podatku_0 += (int)wartoscPodatku3ZL + (wartoscPodatku3GR/100);
		}
		
		//------------------------------------------------------------------------------------//
		//------------------------------KWOTA WRAZ PODATKIEM----------------------------------//		
		wartoscZPodatkiem3Zl = (int)wartoscTowarow3ZL + (wartoscTowarow3GR/100) + (int)wartoscPodatku3ZL + (wartoscPodatku3GR/100);
		wyswietlWartoscZPodatkiem3ZL.setText(String.valueOf((int)wartoscZPodatkiem3Zl));
		wyswietlWartoscZPodatkiem3ZLKopia.setText(String.valueOf((int)wartoscZPodatkiem3Zl));
		  		
		Kwota_wraz_z_podatkiem kwota_wraz_z_podatkiem = new Kwota_wraz_z_podatkiem(wartoscZPodatkiem3Zl);
		wartoscZPodatkiem3GR = kwota_wraz_z_podatkiem.oblicz_kwote_wraz_z_podatekiem();
			   	
		wartoscZPodatkiem3GR=Math.round(wartoscZPodatkiem3GR); //zaokraglenie
			   	
		if(wartoscZPodatkiem3GR==1) {
			wyswietlWartoscZPodatkiem3GR.setText("01");
			wyswietlWartoscZPodatkiem3GRKopia.setText("01");		
						
		}else if(wartoscZPodatkiem3GR==2) {
			wyswietlWartoscZPodatkiem3GR.setText("02");						
			wyswietlWartoscZPodatkiem3GRKopia.setText("02");

		}else if(wartoscZPodatkiem3GR==3) {
			wyswietlWartoscZPodatkiem3GR.setText("03");	
			wyswietlWartoscZPodatkiem3GRKopia.setText("03");

		}else if(wartoscZPodatkiem3GR==4) {
			wyswietlWartoscZPodatkiem3GR.setText("04");		
			wyswietlWartoscZPodatkiem3GRKopia.setText("04");

		}else if(wartoscZPodatkiem3GR==5) {
			wyswietlWartoscZPodatkiem3GR.setText("05");			
			wyswietlWartoscZPodatkiem3GRKopia.setText("05");
					
		}else if(wartoscZPodatkiem3GR==6) {
			wyswietlWartoscZPodatkiem3GR.setText("06");
			wyswietlWartoscZPodatkiem3GRKopia.setText("06");
					
		}else if(wartoscZPodatkiem3GR==7) {
			wyswietlWartoscZPodatkiem3GR.setText("07");
			wyswietlWartoscZPodatkiem3GRKopia.setText("07");
					
		}else if(wartoscZPodatkiem3GR==8) {
			wyswietlWartoscZPodatkiem3GR.setText("08");
			wyswietlWartoscZPodatkiem3GRKopia.setText("08");
					
		}else if(wartoscZPodatkiem3GR==9) {
			wyswietlWartoscZPodatkiem3GR.setText("09");	
			wyswietlWartoscZPodatkiem3GRKopia.setText("09");
					
		}else if(wartoscZPodatkiem3GR==0) {
			wyswietlWartoscZPodatkiem3GR.setText(" -");						
			wyswietlWartoscZPodatkiem3GRKopia.setText(" -");
					
		}else {
			wyswietlWartoscZPodatkiem3GR.setText(String.valueOf(Math.round(wartoscZPodatkiem3GR)));			
			wyswietlWartoscZPodatkiem3GRKopia.setText(String.valueOf(Math.round(wartoscZPodatkiem3GR)));
		}
		
		//---------SUMOWANIE KWOTY WRAZ Z PODATKIEM W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
		if(stawkaPodatku3Wartosc==23)
		{					
			wartoscZPodatkiemZl23 += (int)wartoscZPodatkiem3Zl + (wartoscZPodatkiem3GR/100);	
		}
		else if(stawkaPodatku3Wartosc==8)
		{
			wartoscZPodatkiemZl8 += (int)wartoscZPodatkiem3Zl + (wartoscZPodatkiem3GR/100);	
		}
		else if(stawkaPodatku3Wartosc==5)
		{
			wartoscZPodatkiemZl5 += (int)wartoscZPodatkiem3Zl + (wartoscZPodatkiem3GR/100);	
		}
		else if(stawkaPodatku3Wartosc==0)
		{
			wartoscZPodatkiemZl0 += (int)wartoscZPodatkiem3Zl + (wartoscZPodatkiem3GR/100);	
		}
   }
   
 	 else if(zrodlo == listaProdukt4){
		String czwartyWybranyProdukt = listaProdukt4.getSelectedItem().toString(); //ODCZYTANIE WYNRANEJ OPCJI Z COMBOBOXA
		
	    File wartosc = new File("Produkty\\Licznik\\licznik.txt");
	    Scanner in = null;
		try {
			in = new Scanner(wartosc);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	    String zawartosc = in.nextLine();
	    int numer = Integer.valueOf(zawartosc);
	    //System.out.println(numer); //TEST				
	    
	   
	    
	    for(int i = 1; i<numer; i++) {
		if(czwartyWybranyProdukt.equals("Produkt " + i))
		{
			File daneProduktu = new File("Produkty\\" + i +"-produkt.txt");
			
			Scanner dane = null;
			try {
				in = new Scanner(daneProduktu);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		    String daneTekstNazwa = in.nextLine();
		    String daneTekstMiara = in.nextLine();
		    //System.out.println(daneTekst2);

			wNazwa4.setText(daneTekstNazwa);	      
			wMiara4.setText(daneTekstMiara);
			
			wNazwa4Kopia.setText(daneTekstNazwa);	      
			wMiara4Kopia.setText(daneTekstMiara);
		}
	    }
		
 	 }
   	 
   else if(zrodlo == bWyswietl4) 
   {	   
	   lp4.setText("4");
	   lp4Kopia.setText("4");  
	   
	   //----------------------------------------------------------------------------// 
	   
  		int liczbaZnakowNazwa4 = podajNazweProdukt4.getText().length();
  		if(liczbaZnakowNazwa4>0 && liczbaZnakowNazwa4<70) {	
  		   wNazwa4.setText(podajNazweProdukt4.getText());
  		   wNazwa4Kopia.setText(podajNazweProdukt4.getText());
  		}
  		else
  		{
		   wNazwa4.setText("popraw dane");
		   wNazwa4Kopia.setText("popraw dane");
  		}
	    
  		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowMiara4 = podajMiareProduktu4.getText().length();
   		if(liczbaZnakowMiara4 > 0 && liczbaZnakowMiara4 <= 4) {
   		wMiara4.setText(podajMiareProduktu4.getText());
   		wMiara4Kopia.setText(podajMiareProduktu4.getText());		
   		}
   		else
   		{
   			wMiara4.setText("xxx");
		   	wMiara4Kopia.setText("xxx");
   		}	
   		
   		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowIlosc4 = podajIloscProdukt4.getText().length();
   		String sprawdzenieIlosc4 = podajIloscProdukt4.getText();
   		boolean testIlosc4 = sprawdzenieIlosc4.chars().allMatch( Character::isDigit );
   		if(testIlosc4 == true && liczbaZnakowIlosc4<=6) {
   		   wIlosc4.setText(String.valueOf(podajIloscProdukt4.getText()));
   		   wIlosc4Kopia.setText(String.valueOf(podajIloscProdukt4.getText())); 
   		}
   		else
   		{
   			wIlosc4.setText("xxxx");
   		 	wIlosc4Kopia.setText("xxxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena4 =podajCeneProdukt4Zl.getText().length();
   		String sprawdzenieCena4 = podajCeneProdukt4Zl.getText();
   		boolean testCena4 = sprawdzenieCena4.chars().allMatch( Character::isDigit );
   		if(testCena4 == true && liczbaZnakowCena4 > 0 && liczbaZnakowCena4 <= 5) {
   		   wCena4.setText(String.valueOf(podajCeneProdukt4Zl.getText()));
   		   wCena4Kopia.setText(String.valueOf(podajCeneProdukt4Zl.getText()));
   		}
   		else
   		{
   			wCena4.setText("xxx");
   			wCena4Kopia.setText("xxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena4GR =podajCeneProdukt4GR.getText().length();
   		String sprawdzenieCena4GR = podajCeneProdukt4GR.getText();
   		boolean testCena4GR = sprawdzenieCena4GR.chars().allMatch( Character::isDigit );
   		if(testCena4GR == true && liczbaZnakowCena4GR == 2) {
   		   wCena4GR.setText(String.valueOf(podajCeneProdukt4GR.getText()));
   		   wCena4GRKopia.setText(String.valueOf(podajCeneProdukt4GR.getText()));
   		}
   		else
   		{
   			wCena4GR.setText("xx");
   			wCena4GRKopia.setText("xx");
   		}

  		//----------------------------------------------------------------------------// 

 		
 		int liczbaZnakowStawka4 =stawkaPodatku4.getText().length();
 		//System.out.print(liczbaZnakowStawka4);
   		int sprawdzenieStawka4 = Integer.valueOf(stawkaPodatku4.getText());
  
   		if(sprawdzenieStawka4 == 0 || sprawdzenieStawka4 == 5 || sprawdzenieStawka4 == 8 || sprawdzenieStawka4 == 23) {
   		   wStawka4.setText(String.valueOf(stawkaPodatku4.getText()));	   
   		   wStawka4Kopia.setText(String.valueOf(stawkaPodatku4.getText()));
   		}
   		else
   		{
   			wStawka4.setText("xx");
   			wStawka4Kopia.setText("xx");
   		} 
  
	   	iloscProduktu4 = Integer.parseInt(podajIloscProdukt4.getText());
		   
	   	wartoscTowarow4ZL=iloscProduktu4*((Double.parseDouble(podajCeneProdukt4Zl.getText()))+(Double.parseDouble(podajCeneProdukt4GR.getText())/100));
	   	wartoscTowarow4ZL=BigDecimal.valueOf(wartoscTowarow4ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	
	   	wartoscTowarow4GR=BigDecimal.valueOf(wartoscTowarow4ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow4GR=BigDecimal.valueOf(wartoscTowarow4GR).setScale(4, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow4GR=BigDecimal.valueOf(wartoscTowarow4GR).setScale(3, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow4GR=(BigDecimal.valueOf(wartoscTowarow4GR).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
	
	   	wyswietlWartoscTowarow4ZL.setText(String.valueOf((int)wartoscTowarow4ZL));
		wyswietlWartoscTowarow4ZLKopia.setText(String.valueOf((int)wartoscTowarow4ZL));
	   	
		stawkaPodatku4Wartosc = Integer.parseInt(stawkaPodatku4.getText());
		
	   	wartoscTowarow4GR=Math.round(wartoscTowarow4GR); //zaokraglenie
	   	
		if(wartoscTowarow4GR==1) {
			wyswietlWartoscTowarow4GR.setText("01");
			wyswietlWartoscTowarow4GRKopia.setText("01");
			
		}else if(wartoscTowarow4GR==2) {
			wyswietlWartoscTowarow4GR.setText("02");
			wyswietlWartoscTowarow4GRKopia.setText("02");
						
		}else if(wartoscTowarow4GR==3) {
			wyswietlWartoscTowarow4GR.setText("03");
			wyswietlWartoscTowarow4GRKopia.setText("03");
						
		}else if(wartoscTowarow4GR==4) {
			wyswietlWartoscTowarow4GR.setText("04");
			wyswietlWartoscTowarow4GRKopia.setText("04");
			
		}else if(wartoscTowarow4GR==5) {
			wyswietlWartoscTowarow4GR.setText("05");
			wyswietlWartoscTowarow4GRKopia.setText("05");
			
		}else if(wartoscTowarow4GR==6) {
			wyswietlWartoscTowarow4GR.setText("06");
			wyswietlWartoscTowarow4GRKopia.setText("06");
			
		}else if(wartoscTowarow4GR==7) {
			wyswietlWartoscTowarow4GR.setText("07");
			wyswietlWartoscTowarow4GRKopia.setText("07");

		}else if(wartoscTowarow4GR==8) {
			wyswietlWartoscTowarow4GR.setText("08");
			wyswietlWartoscTowarow4GRKopia.setText("08");
	
		}else if(wartoscTowarow4GR==9) {
			wyswietlWartoscTowarow4GR.setText("09");
			wyswietlWartoscTowarow4GRKopia.setText("09");
			
		}else if(wartoscTowarow4GR==0) {
			wyswietlWartoscTowarow4GR.setText(" -");
			wyswietlWartoscTowarow4GRKopia.setText(" -");
			
		}else {
			wyswietlWartoscTowarow4GR.setText(String.valueOf(Math.round(wartoscTowarow4GR)));
			wyswietlWartoscTowarow4GRKopia.setText(String.valueOf(Math.round(wartoscTowarow4GR)));
			
		}
		
//---------SUMOWANIE KWOTY BEZ PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
		if(stawkaPodatku4Wartosc==23)
		{					
			Kwota_bez_podatku_23 += (int)wartoscTowarow4ZL + (wartoscTowarow4GR/100);	
		}
		else if(stawkaPodatku4Wartosc==8)
		{
			Kwota_bez_podatku_8 += (int)wartoscTowarow4ZL + (wartoscTowarow4GR/100);
		}
		else if(stawkaPodatku4Wartosc==5)
		{
			Kwota_bez_podatku_5 += (int)wartoscTowarow4ZL + (wartoscTowarow4GR/100);
		}
		else if(stawkaPodatku4Wartosc==0)
		{
			Kwota_bez_podatku_0 += (int)wartoscTowarow4ZL + (wartoscTowarow4GR/100);
		}
		//--------------------------------------------------------------// 
		//KOLUMNA WARTOSC PODATKU
		//--------------------------------------------------------------//
		if(stawkaPodatku4Wartosc==23)
		{					
		wartoscPodatku4ZL= wartoscTowarow4ZL * 0.23;	
		}
		else if(stawkaPodatku4Wartosc==8)
		{
			wartoscPodatku4ZL= wartoscTowarow4ZL * 0.08;	
		}
		else if(stawkaPodatku4Wartosc==5)
		{
			wartoscPodatku4ZL= wartoscTowarow4ZL * 0.05;	
		}
		else if(stawkaPodatku4Wartosc==0)
		{
			wartoscPodatku4ZL = 0;	
		}
			
		wyswietlWartoscPodatku4ZL.setText(String.valueOf((int)wartoscPodatku4ZL));
		wyswietlWartoscPodatku4ZLKopia.setText(String.valueOf((int)wartoscPodatku4ZL));
		
		Kwota_podatku kwota_podatku_4 = new Kwota_podatku(wartoscPodatku4ZL);
		wartoscPodatku4GR = kwota_podatku_4.oblicz_podatek();
						   					   	
		wartoscPodatku4GR=Math.round(wartoscPodatku4GR); //zaokraglenie
						   	
		if(wartoscPodatku4GR==1) {
			wyswietlWartoscPodatku4GR.setText("01");
			wyswietlWartoscPodatku4GRKopia.setText("01");		
									
		}else if(wartoscPodatku4GR==2) {
			wyswietlWartoscPodatku4GR.setText("02");						
			wyswietlWartoscPodatku4GRKopia.setText("02");

		}else if(wartoscPodatku4GR==3) {
			wyswietlWartoscPodatku4GR.setText("03");	
			wyswietlWartoscPodatku4GRKopia.setText("03");

		}else if(wartoscPodatku4GR==4) {
			wyswietlWartoscPodatku4GR.setText("04");		
			wyswietlWartoscPodatku4GRKopia.setText("04");

		}else if(wartoscPodatku4GR==5) {
			wyswietlWartoscPodatku4GR.setText("05");			
			wyswietlWartoscPodatku4GRKopia.setText("05");
								
		}else if(wartoscPodatku4GR==6) {
			wyswietlWartoscPodatku4GR.setText("06");
			wyswietlWartoscPodatku4GRKopia.setText("06");
								
		}else if(wartoscPodatku4GR==7) {
			wyswietlWartoscPodatku4GR.setText("07");
			wyswietlWartoscPodatku4GRKopia.setText("07");
								
		}else if(wartoscPodatku4GR==8) {
			wyswietlWartoscPodatku4GR.setText("08");
			wyswietlWartoscPodatku4GRKopia.setText("08");
							
		}else if(wartoscPodatku4GR==9) {
			wyswietlWartoscPodatku4GR.setText("09");	
			wyswietlWartoscPodatku4GRKopia.setText("09");
								
		}else if(wartoscPodatku4GR==0) {
			wyswietlWartoscPodatku4GR.setText(" -");						
			wyswietlWartoscPodatku4GRKopia.setText(" -");
								
		}else {
			wyswietlWartoscPodatku4GR.setText(String.valueOf(Math.round(wartoscPodatku4GR)));			
			wyswietlWartoscPodatku4GRKopia.setText(String.valueOf(Math.round(wartoscPodatku4GR)));
		}
		//------------------------------------------------------------------------------------//
		//---------SUMOWANIE KWOTY PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//			
		if(stawkaPodatku4Wartosc==23)
		{					
		  	Kwota_podatku_23 += (int)wartoscPodatku4ZL + (wartoscPodatku4GR/100);		
		}
		else if(stawkaPodatku4Wartosc==8)
		{
		  	Kwota_podatku_8 += (int)wartoscPodatku4ZL + (wartoscPodatku4GR/100);
		}
		else if(stawkaPodatku4Wartosc==5)
		{
		  	Kwota_podatku_5 += (int)wartoscPodatku4ZL + (wartoscPodatku4GR/100);
		}
		else if(stawkaPodatku4Wartosc==0)
		{
		  	Kwota_podatku_0 += (int)wartoscPodatku4GR + (wartoscPodatku4GR/100);
		}
		
		//------------------------------------------------------------------------------------//
		//------------------------------KWOTA WRAZ PODATKIEM----------------------------------//		
		wartoscZPodatkiem4Zl = (int)wartoscTowarow4ZL + (wartoscTowarow4GR/100) + (int)wartoscPodatku4ZL + (wartoscPodatku4GR/100);
		wyswietlWartoscZPodatkiem4ZL.setText(String.valueOf((int)wartoscZPodatkiem4Zl));
		wyswietlWartoscZPodatkiem4ZLKopia.setText(String.valueOf((int)wartoscZPodatkiem4Zl));
		  		
		Kwota_wraz_z_podatkiem kwota_wraz_z_podatkiem = new Kwota_wraz_z_podatkiem(wartoscZPodatkiem4Zl);
		wartoscZPodatkiem4GR = kwota_wraz_z_podatkiem.oblicz_kwote_wraz_z_podatekiem();
			   	
		wartoscZPodatkiem4GR=Math.round(wartoscZPodatkiem4GR); //zaokraglenie
			   	
		if(wartoscZPodatkiem4GR==1) {
			wyswietlWartoscZPodatkiem4GR.setText("01");
			wyswietlWartoscZPodatkiem4GRKopia.setText("01");		
						
		}else if(wartoscZPodatkiem4GR==2) {
			wyswietlWartoscZPodatkiem4GR.setText("02");						
			wyswietlWartoscZPodatkiem4GRKopia.setText("02");

		}else if(wartoscZPodatkiem4GR==3) {
			wyswietlWartoscZPodatkiem4GR.setText("03");	
			wyswietlWartoscZPodatkiem4GRKopia.setText("03");

		}else if(wartoscZPodatkiem4GR==4) {
			wyswietlWartoscZPodatkiem4GR.setText("04");		
			wyswietlWartoscZPodatkiem4GRKopia.setText("04");

		}else if(wartoscZPodatkiem4GR==5) {
			wyswietlWartoscZPodatkiem4GR.setText("05");			
			wyswietlWartoscZPodatkiem4GRKopia.setText("05");
					
		}else if(wartoscZPodatkiem4GR==6) {
			wyswietlWartoscZPodatkiem4GR.setText("06");
			wyswietlWartoscZPodatkiem4GRKopia.setText("06");
					
		}else if(wartoscZPodatkiem4GR==7) {
			wyswietlWartoscZPodatkiem4GR.setText("07");
			wyswietlWartoscZPodatkiem4GRKopia.setText("07");
					
		}else if(wartoscZPodatkiem4GR==8) {
			wyswietlWartoscZPodatkiem4GR.setText("08");
			wyswietlWartoscZPodatkiem4GRKopia.setText("08");
					
		}else if(wartoscZPodatkiem4GR==9) {
			wyswietlWartoscZPodatkiem4GR.setText("09");	
			wyswietlWartoscZPodatkiem4GRKopia.setText("09");
					
		}else if(wartoscZPodatkiem4GR==0) {
			wyswietlWartoscZPodatkiem4GR.setText(" -");						
			wyswietlWartoscZPodatkiem4GRKopia.setText(" -");
					
		}else {
			wyswietlWartoscZPodatkiem4GR.setText(String.valueOf(Math.round(wartoscZPodatkiem4GR)));			
			wyswietlWartoscZPodatkiem4GRKopia.setText(String.valueOf(Math.round(wartoscZPodatkiem4GR)));
		}
		
		//---------SUMOWANIE KWOTY WRAZ Z PODATKIEM W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
		if(stawkaPodatku4Wartosc==23)
		{					
			wartoscZPodatkiemZl23 += (int)wartoscZPodatkiem4Zl + (wartoscZPodatkiem4GR/100);	
		}
		else if(stawkaPodatku4Wartosc==8)
		{
			wartoscZPodatkiemZl8 += (int)wartoscZPodatkiem4Zl + (wartoscZPodatkiem4GR/100);	
		}
		else if(stawkaPodatku4Wartosc==5)
		{
			wartoscZPodatkiemZl5 += (int)wartoscZPodatkiem4Zl + (wartoscZPodatkiem4GR/100);	
		}
		else if(stawkaPodatku4Wartosc==0)
		{
			wartoscZPodatkiemZl0 += (int)wartoscZPodatkiem4Zl + (wartoscZPodatkiem4GR/100);	
		}
   }
   	 
   	 
 	 else if(zrodlo == listaProdukt5){
		String piatyWybranyProdukt = listaProdukt5.getSelectedItem().toString(); //ODCZYTANIE WYNRANEJ OPCJI Z COMBOBOXA
		
	    File wartosc = new File("Produkty\\Licznik\\licznik.txt");
	    Scanner in = null;
		try {
			in = new Scanner(wartosc);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	    String zawartosc = in.nextLine();
	    int numer = Integer.valueOf(zawartosc);
	    //System.out.println(numer); //TEST				
	    
	   
	    
	    for(int i = 1; i<numer; i++) {
		if(piatyWybranyProdukt.equals("Produkt " + i))
		{
			File daneProduktu = new File("Produkty\\" + i +"-produkt.txt");
			
			Scanner dane = null;
			try {
				in = new Scanner(daneProduktu);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		    String daneTekstNazwa = in.nextLine();
		    String daneTekstMiara = in.nextLine();
		    //System.out.println(daneTekst2);

			wNazwa5.setText(daneTekstNazwa);	      
			wMiara5.setText(daneTekstMiara);
			
			wNazwa5Kopia.setText(daneTekstNazwa);	      
			wMiara5Kopia.setText(daneTekstMiara);
		}
	    }
		
 	 }
   
   else if(zrodlo == bWyswietl5) 
   {
	   lp5.setText("5");
	   lp5Kopia.setText("5");  
	   
	   //----------------------------------------------------------------------------// 
	  
  		int liczbaZnakowNazwa5 = podajNazweProdukt5.getText().length();
  		if(liczbaZnakowNazwa5>0 && liczbaZnakowNazwa5<70) {	
  		   wNazwa5.setText(podajNazweProdukt5.getText());
  		   wNazwa5Kopia.setText(podajNazweProdukt5.getText());
  		}
  		else
  		{
		   wNazwa5.setText("popraw dane");
		   wNazwa5Kopia.setText("popraw dane");
  		}
	   
  		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowMiara5 = podajMiareProduktu5.getText().length();
   		if(liczbaZnakowMiara5> 0 && liczbaZnakowMiara5 <= 4) {
   		wMiara5.setText(podajMiareProduktu5.getText());
   		wMiara5Kopia.setText(podajMiareProduktu5.getText());		
   		}
   		else
   		{
   			wMiara5.setText("xxx");
		   	wMiara5Kopia.setText("xxx");
   		}	
	    
   		//----------------------------------------------------------------------------// 		
   		
   		int liczbaZnakowIlosc5 = podajIloscProdukt5.getText().length();
   		String sprawdzenieIlosc5 = podajIloscProdukt5.getText();
   		boolean testIlosc5 = sprawdzenieIlosc5.chars().allMatch( Character::isDigit );
   		if(testIlosc5 == true && liczbaZnakowIlosc5<=6) {
   		   wIlosc5.setText(String.valueOf(podajIloscProdukt5.getText()));
   		   wIlosc5Kopia.setText(String.valueOf(podajIloscProdukt5.getText())); 
   		}
   		else
   		{
   			wIlosc5.setText("xxxx");
   		 	wIlosc5Kopia.setText("xxxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena5 =podajCeneProdukt5Zl.getText().length();
   		String sprawdzenieCena5 = podajCeneProdukt5Zl.getText();
   		boolean testCena5 = sprawdzenieCena5.chars().allMatch( Character::isDigit );
   		if(testCena5 == true && liczbaZnakowCena5 > 0 && liczbaZnakowCena5 <= 5) {
   		   wCena5.setText(String.valueOf(podajCeneProdukt5Zl.getText()));
   		   wCena5Kopia.setText(String.valueOf(podajCeneProdukt5Zl.getText()));
   		}
   		else
   		{
   			wCena5.setText("xxx");
   			wCena5Kopia.setText("xxx");
   		}

  		//----------------------------------------------------------------------------// 

 		int liczbaZnakowCena5GR =podajCeneProdukt5GR.getText().length();
   		String sprawdzenieCena5GR = podajCeneProdukt5GR.getText();
   		boolean testCena5GR = sprawdzenieCena5GR.chars().allMatch( Character::isDigit );
   		if(testCena5GR == true && liczbaZnakowCena5GR == 2) {
   		   wCena5GR.setText(String.valueOf(podajCeneProdukt5GR.getText()));
   		   wCena5GRKopia.setText(String.valueOf(podajCeneProdukt5GR.getText()));
   		}
   		else
   		{
   			wCena5GR.setText("xx");
   			wCena5GRKopia.setText("xx");
   		}

  		//----------------------------------------------------------------------------// 

 		
 		int liczbaZnakowStawka5 =stawkaPodatku5.getText().length();
 		//System.out.print(liczbaZnakowStawka5);
   		int sprawdzenieStawka5 = Integer.valueOf(stawkaPodatku5.getText());
  
   		if(sprawdzenieStawka5 == 0 || sprawdzenieStawka5 == 5 || sprawdzenieStawka5 == 8 || sprawdzenieStawka5 == 23) {
   		   wStawka5.setText(String.valueOf(stawkaPodatku5.getText()));	   
   		   wStawka5Kopia.setText(String.valueOf(stawkaPodatku5.getText()));
   		}
   		else
   		{
   			wStawka5.setText("xx");
   			wStawka5Kopia.setText("xx");
   		} 
  
	   
	   	iloscProduktu5 = Integer.parseInt(podajIloscProdukt5.getText());
		   
	   	wartoscTowarow5ZL=iloscProduktu5*((Double.parseDouble(podajCeneProdukt5Zl.getText()))+(Double.parseDouble(podajCeneProdukt5GR.getText())/100));
	   	wartoscTowarow5ZL=BigDecimal.valueOf(wartoscTowarow5ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	
	   	wartoscTowarow5GR=BigDecimal.valueOf(wartoscTowarow5ZL).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow5GR=BigDecimal.valueOf(wartoscTowarow5GR).setScale(4, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow5GR=BigDecimal.valueOf(wartoscTowarow5GR).setScale(3, RoundingMode.HALF_UP).doubleValue();
	   	wartoscTowarow5GR=(BigDecimal.valueOf(wartoscTowarow5GR).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
	
	   	wyswietlWartoscTowarow5ZL.setText(String.valueOf((int)wartoscTowarow5ZL));
		wyswietlWartoscTowarow5ZLKopia.setText(String.valueOf((int)wartoscTowarow5ZL));
	   	
		stawkaPodatku5Wartosc = Integer.parseInt(stawkaPodatku5.getText());
		
	   	wartoscTowarow5GR=Math.round(wartoscTowarow5GR); //zaokraglenie
	   	
		if(wartoscTowarow5GR==1) {
			wyswietlWartoscTowarow5GR.setText("01");
			wyswietlWartoscTowarow5GRKopia.setText("01");
			
		}else if(wartoscTowarow5GR==2) {
			wyswietlWartoscTowarow5GR.setText("02");
			wyswietlWartoscTowarow5GRKopia.setText("02");
						
		}else if(wartoscTowarow5GR==3) {
			wyswietlWartoscTowarow5GR.setText("03");
			wyswietlWartoscTowarow5GRKopia.setText("03");
						
		}else if(wartoscTowarow5GR==4) {
			wyswietlWartoscTowarow5GR.setText("04");
			wyswietlWartoscTowarow5GRKopia.setText("04");
			
		}else if(wartoscTowarow5GR==5) {
			wyswietlWartoscTowarow5GR.setText("05");
			wyswietlWartoscTowarow5GRKopia.setText("05");
			
		}else if(wartoscTowarow5GR==6) {
			wyswietlWartoscTowarow5GR.setText("06");
			wyswietlWartoscTowarow5GRKopia.setText("06");
			
		}else if(wartoscTowarow5GR==7) {
			wyswietlWartoscTowarow5GR.setText("07");
			wyswietlWartoscTowarow5GRKopia.setText("07");

		}else if(wartoscTowarow5GR==8) {
			wyswietlWartoscTowarow5GR.setText("08");
			wyswietlWartoscTowarow5GRKopia.setText("08");
	
		}else if(wartoscTowarow5GR==9) {
			wyswietlWartoscTowarow5GR.setText("09");
			wyswietlWartoscTowarow5GRKopia.setText("09");
			
		}else if(wartoscTowarow5GR==0) {
			wyswietlWartoscTowarow5GR.setText(" -");
			wyswietlWartoscTowarow5GRKopia.setText(" -");
			
		}else {
			wyswietlWartoscTowarow5GR.setText(String.valueOf(Math.round(wartoscTowarow5GR)));
			wyswietlWartoscTowarow5GRKopia.setText(String.valueOf(Math.round(wartoscTowarow5GR)));
			
		}
		
		//---------SUMOWANIE KWOTY BEZ PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
		if(stawkaPodatku5Wartosc==23)
		{					
			Kwota_bez_podatku_23 += (int)wartoscTowarow5ZL + (wartoscTowarow5GR/100);
		}
		else if(stawkaPodatku5Wartosc==8)
		{
			Kwota_bez_podatku_8 += (int)wartoscTowarow5ZL + (wartoscTowarow5GR/100);
		}
		else if(stawkaPodatku5Wartosc==5)
		{
			Kwota_bez_podatku_5 += (int)wartoscTowarow5ZL + (wartoscTowarow5GR/100);
		}
		else if(stawkaPodatku5Wartosc==0)
		{
			Kwota_bez_podatku_0 += (int)wartoscTowarow5ZL + (wartoscTowarow5GR/100);
		}
	//--------------------------------------------------------------// 
	//KOLUMNA WARTOSC PODATKU
	//--------------------------------------------------------------//
	if(stawkaPodatku5Wartosc==23)
	{					
		wartoscPodatku5ZL= wartoscTowarow5ZL * 0.23;	
	}
	else if(stawkaPodatku5Wartosc==8)
	{
		wartoscPodatku5ZL= wartoscTowarow5ZL * 0.08;	
	}
	else if(stawkaPodatku5Wartosc==5)
	{
		wartoscPodatku5ZL= wartoscTowarow5ZL * 0.05;	
	}
	else if(stawkaPodatku5Wartosc==0)
	{
		wartoscPodatku5ZL= 0;	
	}
	
	wyswietlWartoscPodatku5ZL.setText(String.valueOf((int)wartoscPodatku5ZL));
	wyswietlWartoscPodatku5ZLKopia.setText(String.valueOf((int)wartoscPodatku5ZL));						
	
	Kwota_podatku kwota_podatku_5 = new Kwota_podatku(wartoscPodatku5ZL);
	wartoscPodatku5GR = kwota_podatku_5.oblicz_podatek();
						   	
	wartoscPodatku5GR=Math.round(wartoscPodatku5GR); //zaokraglenie
								   	
	if(wartoscPodatku5GR==1) {
		wyswietlWartoscPodatku5GR.setText("01");
		wyswietlWartoscPodatku5GRKopia.setText("01");		
											
	}else if(wartoscPodatku5GR==2) {
		wyswietlWartoscPodatku5GR.setText("02");						
		wyswietlWartoscPodatku5GRKopia.setText("02");

	}else if(wartoscPodatku5GR==3) {
		wyswietlWartoscPodatku5GR.setText("03");	
		wyswietlWartoscPodatku5GRKopia.setText("03");

	}else if(wartoscPodatku5GR==4) {
		wyswietlWartoscPodatku5GR.setText("04");		
		wyswietlWartoscPodatku5GRKopia.setText("04");

	}else if(wartoscPodatku5GR==5) {
		wyswietlWartoscPodatku5GR.setText("05");			
		wyswietlWartoscPodatku5GRKopia.setText("05");
										
	}else if(wartoscPodatku5GR==6) {
		wyswietlWartoscPodatku5GR.setText("06");
		wyswietlWartoscPodatku5GRKopia.setText("06");
										
	}else if(wartoscPodatku5GR==7) {
		wyswietlWartoscPodatku5GR.setText("07");
		wyswietlWartoscPodatku5GRKopia.setText("07");
										
	}else if(wartoscPodatku5GR==8) {
		wyswietlWartoscPodatku5GR.setText("08");
		wyswietlWartoscPodatku5GRKopia.setText("08");
									
	}else if(wartoscPodatku5GR==9) {
		wyswietlWartoscPodatku5GR.setText("09");	
		wyswietlWartoscPodatku5GRKopia.setText("09");
										
	}else if(wartoscPodatku5GR==0) {
		wyswietlWartoscPodatku5GR.setText(" -");						
		wyswietlWartoscPodatku5GRKopia.setText(" -");
										
	}else {
		wyswietlWartoscPodatku5GR.setText(String.valueOf(Math.round(wartoscPodatku5GR)));			
		wyswietlWartoscPodatku5GRKopia.setText(String.valueOf(Math.round(wartoscPodatku5GR)));
	}
	//------------------------------------------------------------------------------------//
	//---------SUMOWANIE KWOTY PODATKU W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//			
	if(stawkaPodatku5Wartosc==23)
	{					
	  	Kwota_podatku_23 += (int)wartoscPodatku5ZL + (wartoscPodatku5GR/100);	
	}
	else if(stawkaPodatku5Wartosc==8)
	{
	  	Kwota_podatku_8 += (int)wartoscPodatku5ZL + (wartoscPodatku5GR/100);
	}
	else if(stawkaPodatku5Wartosc==5)
	{
	  	Kwota_podatku_5 += (int)wartoscPodatku5ZL + (wartoscPodatku5GR/100);
	}
	else if(stawkaPodatku5Wartosc==0)
	{
	  	Kwota_podatku_0 += (int)wartoscPodatku5ZL + (wartoscPodatku5GR/100);
	}	
	
	//------------------------------------------------------------------------------------//
	//------------------------------KWOTA WRAZ PODATKIEM----------------------------------//		
	wartoscZPodatkiem5Zl = (int)wartoscTowarow5ZL + (wartoscTowarow5GR/100) + (int)wartoscPodatku5ZL + (wartoscPodatku5GR/100);
	wyswietlWartoscZPodatkiem5ZL.setText(String.valueOf((int)wartoscZPodatkiem5Zl));
	wyswietlWartoscZPodatkiem5ZLKopia.setText(String.valueOf((int)wartoscZPodatkiem5Zl));
	  		
	Kwota_wraz_z_podatkiem kwota_wraz_z_podatkiem = new Kwota_wraz_z_podatkiem(wartoscZPodatkiem5Zl);
	wartoscZPodatkiem5GR = kwota_wraz_z_podatkiem.oblicz_kwote_wraz_z_podatekiem();
		   	
	wartoscZPodatkiem5GR=Math.round(wartoscZPodatkiem5GR); //zaokraglenie
		   	
	if(wartoscZPodatkiem5GR==1) {
		wyswietlWartoscZPodatkiem5GR.setText("01");
		wyswietlWartoscZPodatkiem5GRKopia.setText("01");		
					
	}else if(wartoscZPodatkiem5GR==2) {
		wyswietlWartoscZPodatkiem5GR.setText("02");						
		wyswietlWartoscZPodatkiem5GRKopia.setText("02");

	}else if(wartoscZPodatkiem5GR==3) {
		wyswietlWartoscZPodatkiem5GR.setText("03");	
		wyswietlWartoscZPodatkiem5GRKopia.setText("03");

	}else if(wartoscZPodatkiem5GR==4) {
		wyswietlWartoscZPodatkiem5GR.setText("04");		
		wyswietlWartoscZPodatkiem5GRKopia.setText("04");

	}else if(wartoscZPodatkiem5GR==5) {
		wyswietlWartoscZPodatkiem5GR.setText("05");			
		wyswietlWartoscZPodatkiem5GRKopia.setText("05");
				
	}else if(wartoscZPodatkiem5GR==6) {
		wyswietlWartoscZPodatkiem5GR.setText("06");
		wyswietlWartoscZPodatkiem5GRKopia.setText("06");
				
	}else if(wartoscZPodatkiem5GR==7) {
		wyswietlWartoscZPodatkiem5GR.setText("07");
		wyswietlWartoscZPodatkiem5GRKopia.setText("07");
				
	}else if(wartoscZPodatkiem5GR==8) {
		wyswietlWartoscZPodatkiem5GR.setText("08");
		wyswietlWartoscZPodatkiem5GRKopia.setText("08");
				
	}else if(wartoscZPodatkiem5GR==9) {
		wyswietlWartoscZPodatkiem5GR.setText("09");	
		wyswietlWartoscZPodatkiem5GRKopia.setText("09");
				
	}else if(wartoscZPodatkiem5GR==0) {
		wyswietlWartoscZPodatkiem5GR.setText(" -");						
		wyswietlWartoscZPodatkiem5GRKopia.setText(" -");
				
	}else {
		wyswietlWartoscZPodatkiem5GR.setText(String.valueOf(Math.round(wartoscZPodatkiem5GR)));			
		wyswietlWartoscZPodatkiem5GRKopia.setText(String.valueOf(Math.round(wartoscZPodatkiem5GR)));
	}
	
	//---------SUMOWANIE KWOTY WRAZ Z PODATKIEM W ZALEZNOSCI OD WYSOKOSCI OPODATKOWANIA------------//		
	if(stawkaPodatku5Wartosc==23)
	{					
		wartoscZPodatkiemZl23 += (int)wartoscZPodatkiem5Zl + (wartoscZPodatkiem5GR/100);	
	}
	else if(stawkaPodatku5Wartosc==8)
	{
		wartoscZPodatkiemZl8 += (int)wartoscZPodatkiem5Zl + (wartoscZPodatkiem5GR/100);	
	}
	else if(stawkaPodatku5Wartosc==5)
	{
		wartoscZPodatkiemZl5 += (int)wartoscZPodatkiem5Zl + (wartoscZPodatkiem5GR/100);	
	}
	else if(stawkaPodatku5Wartosc==0)
	{
		wartoscZPodatkiemZl0 += (int)wartoscZPodatkiem5Zl + (wartoscZPodatkiem5GR/100);	
	}
   } 
   
   //-------------------------------------------------------------------------------------------------//		
   //-------------------------------WYKONANIE OBLICZEN------------------------------------------------//
   //-------------------------------------------------------------------------------------------------//
   
   else if(zrodlo == bOblicz) 
   {
   //------------------------------------------23%----------------------------------------------------//
	wyswietlWartoscTowarow1ZL23.setText(String.valueOf((int)Kwota_bez_podatku_23));
	wyswietlWartoscTowarow1ZLKopia23.setText(String.valueOf((int)Kwota_bez_podatku_23));
	
    Kwota_bez_podatku kwota_bez_podatku = new Kwota_bez_podatku(Kwota_bez_podatku_23, Kwota_bez_podatku_8, Kwota_bez_podatku_5, Kwota_bez_podatku_0);
    
    double obliczona_kwota_bez_podatku_23=kwota_bez_podatku.oblicz_23();
       
    if(obliczona_kwota_bez_podatku_23==1) {
		wyswietlWartoscTowarow1GR23.setText("01");
		wyswietlWartoscTowarow1GRKopia23.setText("01");
				
	}else if(obliczona_kwota_bez_podatku_23==2) {
		wyswietlWartoscTowarow1GR23.setText("02");
		wyswietlWartoscTowarow1GRKopia23.setText("02");

	}else if(obliczona_kwota_bez_podatku_23==3) {
		wyswietlWartoscTowarow1GR23.setText("03");
		wyswietlWartoscTowarow1GRKopia23.setText("03");

	}else if(obliczona_kwota_bez_podatku_23==4) {
		wyswietlWartoscTowarow1GR23.setText("04");
		wyswietlWartoscTowarow1GRKopia23.setText("04");

	}else if(obliczona_kwota_bez_podatku_23==5) {
		wyswietlWartoscTowarow1GR23.setText("05");
		wyswietlWartoscTowarow1GRKopia23.setText("05");
		
	}else if(obliczona_kwota_bez_podatku_23==6) {
		wyswietlWartoscTowarow1GR23.setText("06");
		wyswietlWartoscTowarow1GRKopia23.setText("06");

	}else if(obliczona_kwota_bez_podatku_23==7) {
		wyswietlWartoscTowarow1GR23.setText("07");
		wyswietlWartoscTowarow1GRKopia23.setText("07");

	}else if(obliczona_kwota_bez_podatku_23==8) {
		wyswietlWartoscTowarow1GR23.setText("08");
		wyswietlWartoscTowarow1GRKopia23.setText("08");

	}else if(obliczona_kwota_bez_podatku_23==9) {
		wyswietlWartoscTowarow1GR23.setText("09");
		wyswietlWartoscTowarow1GRKopia23.setText("09");

	}else if(obliczona_kwota_bez_podatku_23==0) {
		wyswietlWartoscTowarow1GR23.setText(" -");
		wyswietlWartoscTowarow1GRKopia23.setText(" -");
		
	
	}else {
		wyswietlWartoscTowarow1GR23.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku_23)));
		wyswietlWartoscTowarow1GRKopia23.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku_23)));
		
	}	
   //-------------------------------------------8%----------------------------------------------------//  
    wyswietlWartoscTowarow1ZL8.setText(String.valueOf((int)Kwota_bez_podatku_8));
	wyswietlWartoscTowarow1ZLKopia8.setText(String.valueOf((int)Kwota_bez_podatku_8));
    
    double obliczona_kwota_bez_podatku_8=kwota_bez_podatku.oblicz_8();
    
    if(obliczona_kwota_bez_podatku_8==1) {
		wyswietlWartoscTowarow1GR8.setText("01");
		wyswietlWartoscTowarow1GRKopia8.setText("01");
				
	}else if(obliczona_kwota_bez_podatku_8==2) {
		wyswietlWartoscTowarow1GR8.setText("02");
		wyswietlWartoscTowarow1GRKopia8.setText("02");

	}else if(obliczona_kwota_bez_podatku_8==3) {
		wyswietlWartoscTowarow1GR8.setText("03");
		wyswietlWartoscTowarow1GRKopia8.setText("03");

	}else if(obliczona_kwota_bez_podatku_8==4) {
		wyswietlWartoscTowarow1GR8.setText("04");
		wyswietlWartoscTowarow1GRKopia8.setText("04");

	}else if(obliczona_kwota_bez_podatku_8==5) {
		wyswietlWartoscTowarow1GR8.setText("05");
		wyswietlWartoscTowarow1GRKopia8.setText("05");
		
	}else if(obliczona_kwota_bez_podatku_8==6) {
		wyswietlWartoscTowarow1GR8.setText("06");
		wyswietlWartoscTowarow1GRKopia8.setText("06");

	}else if(obliczona_kwota_bez_podatku_8==7) {
		wyswietlWartoscTowarow1GR8.setText("07");
		wyswietlWartoscTowarow1GRKopia8.setText("07");

	}else if(obliczona_kwota_bez_podatku_8==8) {
		wyswietlWartoscTowarow1GR8.setText("08");
		wyswietlWartoscTowarow1GRKopia8.setText("08");

	}else if(obliczona_kwota_bez_podatku_8==9) {
		wyswietlWartoscTowarow1GR8.setText("09");
		wyswietlWartoscTowarow1GRKopia8.setText("09");

	}else if(obliczona_kwota_bez_podatku_8==0) {
		wyswietlWartoscTowarow1GR8.setText(" -");
		wyswietlWartoscTowarow1GRKopia8.setText(" -");
		
	
	}else {
		wyswietlWartoscTowarow1GR8.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku_8)));
		wyswietlWartoscTowarow1GRKopia8.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku_8)));
		
	}	
  //-------------------------------------------5%----------------------------------------------------//
    wyswietlWartoscTowarow1ZL5.setText(String.valueOf((int)Kwota_bez_podatku_5));
	wyswietlWartoscTowarow1ZLKopia5.setText(String.valueOf((int)Kwota_bez_podatku_5));
    
    double obliczona_kwota_bez_podatku_5=kwota_bez_podatku.oblicz_5();
    
    if(obliczona_kwota_bez_podatku_5==1) {
		wyswietlWartoscTowarow1GR5.setText("01");
		wyswietlWartoscTowarow1GRKopia5.setText("01");
				
	}else if(obliczona_kwota_bez_podatku_5==2) {
		wyswietlWartoscTowarow1GR5.setText("02");
		wyswietlWartoscTowarow1GRKopia5.setText("02");

	}else if(obliczona_kwota_bez_podatku_5==3) {
		wyswietlWartoscTowarow1GR5.setText("03");
		wyswietlWartoscTowarow1GRKopia5.setText("03");

	}else if(obliczona_kwota_bez_podatku_5==4) {
		wyswietlWartoscTowarow1GR5.setText("04");
		wyswietlWartoscTowarow1GRKopia5.setText("04");

	}else if(obliczona_kwota_bez_podatku_5==5) {
		wyswietlWartoscTowarow1GR5.setText("05");
		wyswietlWartoscTowarow1GRKopia5.setText("05");
		
	}else if(obliczona_kwota_bez_podatku_5==6) {
		wyswietlWartoscTowarow1GR5.setText("06");
		wyswietlWartoscTowarow1GRKopia5.setText("06");

	}else if(obliczona_kwota_bez_podatku_5==7) {
		wyswietlWartoscTowarow1GR5.setText("07");
		wyswietlWartoscTowarow1GRKopia5.setText("07");

	}else if(obliczona_kwota_bez_podatku_5==8) {
		wyswietlWartoscTowarow1GR5.setText("08");
		wyswietlWartoscTowarow1GRKopia5.setText("08");

	}else if(obliczona_kwota_bez_podatku_5==9) {
		wyswietlWartoscTowarow1GR5.setText("09");
		wyswietlWartoscTowarow1GRKopia5.setText("09");

	}else if(obliczona_kwota_bez_podatku_5==0) {
		wyswietlWartoscTowarow1GR5.setText(" -");
		wyswietlWartoscTowarow1GRKopia5.setText(" -");
		
	
	}else {
		wyswietlWartoscTowarow1GR5.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku_5)));
		wyswietlWartoscTowarow1GRKopia5.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku_5)));
		
	}	
   //-------------------------------------------0%----------------------------------------------------//
    wyswietlWartoscTowarow1ZL0.setText(String.valueOf((int)Kwota_bez_podatku_0));
	wyswietlWartoscTowarow1ZLKopia0.setText(String.valueOf((int)Kwota_bez_podatku_0));
    
    double obliczona_kwota_bez_podatku_0=kwota_bez_podatku.oblicz_0();
    
    if(obliczona_kwota_bez_podatku_0==1) {
		wyswietlWartoscTowarow1GR0.setText("01");
		wyswietlWartoscTowarow1GRKopia0.setText("01");
				
	}else if(obliczona_kwota_bez_podatku_0==2) {
		wyswietlWartoscTowarow1GR0.setText("02");
		wyswietlWartoscTowarow1GRKopia0.setText("02");

	}else if(obliczona_kwota_bez_podatku_0==3) {
		wyswietlWartoscTowarow1GR0.setText("03");
		wyswietlWartoscTowarow1GRKopia0.setText("03");

	}else if(obliczona_kwota_bez_podatku_0==4) {
		wyswietlWartoscTowarow1GR0.setText("04");
		wyswietlWartoscTowarow1GRKopia0.setText("04");

	}else if(obliczona_kwota_bez_podatku_0==5) {
		wyswietlWartoscTowarow1GR0.setText("05");
		wyswietlWartoscTowarow1GRKopia0.setText("05");
		
	}else if(obliczona_kwota_bez_podatku_0==6) {
		wyswietlWartoscTowarow1GR0.setText("06");
		wyswietlWartoscTowarow1GRKopia0.setText("06");

	}else if(obliczona_kwota_bez_podatku_0==7) {
		wyswietlWartoscTowarow1GR0.setText("07");
		wyswietlWartoscTowarow1GRKopia0.setText("07");

	}else if(obliczona_kwota_bez_podatku_0==8) {
		wyswietlWartoscTowarow1GR0.setText("08");
		wyswietlWartoscTowarow1GRKopia0.setText("08");

	}else if(obliczona_kwota_bez_podatku_0==9) {
		wyswietlWartoscTowarow1GR0.setText("09");
		wyswietlWartoscTowarow1GRKopia0.setText("09");

	}else if(obliczona_kwota_bez_podatku_0==0) {
		wyswietlWartoscTowarow1GR0.setText(" -");
		wyswietlWartoscTowarow1GRKopia0.setText(" -");
		
	
	}else {
		wyswietlWartoscTowarow1GR0.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku_0)));
		wyswietlWartoscTowarow1GRKopia0.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku_0)));
		
	}	
  //-----------------------------------LACZNA KWOTA BEZ PODATKU----------------------------------------------//
    razem_Kwota_bez_podatku = Kwota_bez_podatku_23 + Kwota_bez_podatku_8 + Kwota_bez_podatku_5 + Kwota_bez_podatku_0;
    
    wyswietlRazemWartoscTowarow1ZL3.setText(String.valueOf((int)razem_Kwota_bez_podatku));
    wyswietlRazemWartoscTowarow1ZLKopia3.setText(String.valueOf((int)razem_Kwota_bez_podatku));
        
    Razem_Kwota_bez_podatku Razem_Kwota_bez_podatku = new Razem_Kwota_bez_podatku(razem_Kwota_bez_podatku);
    
    double obliczona_kwota_bez_podatku=Razem_Kwota_bez_podatku.oblicz_razem();
       
    if(obliczona_kwota_bez_podatku==1) {
		wyswietlRazemWartoscTowarow1GR3.setText("01");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("01");
				
	}else if(obliczona_kwota_bez_podatku==2) {
		wyswietlRazemWartoscTowarow1GR3.setText("02");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("02");

	}else if(obliczona_kwota_bez_podatku==3) {
		wyswietlRazemWartoscTowarow1GR3.setText("03");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("03");

	}else if(obliczona_kwota_bez_podatku==4) {
		wyswietlRazemWartoscTowarow1GR3.setText("04");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("04");

	}else if(obliczona_kwota_bez_podatku==5) {
		wyswietlRazemWartoscTowarow1GR3.setText("05");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("05");
		
	}else if(obliczona_kwota_bez_podatku==6) {
		wyswietlRazemWartoscTowarow1GR3.setText("06");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("06");

	}else if(obliczona_kwota_bez_podatku==7) {
		wyswietlRazemWartoscTowarow1GR3.setText("07");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("07");

	}else if(obliczona_kwota_bez_podatku==8) {
		wyswietlRazemWartoscTowarow1GR3.setText("08");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("08");

	}else if(obliczona_kwota_bez_podatku_23==9) {
		wyswietlRazemWartoscTowarow1GR3.setText("09");
		wyswietlRazemWartoscTowarow1GRKopia3.setText("09");

	}else if(obliczona_kwota_bez_podatku==0) {
		wyswietlRazemWartoscTowarow1GR3.setText(" -");
		wyswietlRazemWartoscTowarow1GRKopia3.setText(" -");
			
	}else {
		wyswietlRazemWartoscTowarow1GR3.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku)));
		wyswietlRazemWartoscTowarow1GRKopia3.setText(String.valueOf(Math.round(obliczona_kwota_bez_podatku)));
		
	}	

    
  //---------------------------------KWOTA PODATKU 23%----------------------------------------------------//
    wyswietlWartoscPodatkuZL23.setText(String.valueOf((int)Kwota_podatku_23));
    wyswietlWartoscPodatkuZL23Kopia.setText(String.valueOf((int)Kwota_podatku_23));

    Kwota_podatku_sumowanie_poszczegolnych Kwota_podaktu_poszczegolnych = new Kwota_podatku_sumowanie_poszczegolnych(Kwota_podatku_23, Kwota_podatku_8, Kwota_podatku_5);
        
     double obliczona_kwota_podatku_23=Kwota_podaktu_poszczegolnych.oblicz_23();
        
        if(obliczona_kwota_podatku_23==1) {
        	wyswietlWartoscPodatkuGR23.setText("01");
        	wyswietlWartoscPodatkuGR23Kopia.setText("01");
    				
    	}else if(obliczona_kwota_podatku_23==2) {
    		wyswietlWartoscPodatkuGR23.setText("02");
    		wyswietlWartoscPodatkuGR23Kopia.setText("02");

    	}else if(obliczona_kwota_podatku_23==3) {
    		wyswietlWartoscPodatkuGR23.setText("03");
    		wyswietlWartoscPodatkuGR23Kopia.setText("03");

    	}else if(obliczona_kwota_podatku_23==4) {
    		wyswietlWartoscPodatkuGR23.setText("04");
    		wyswietlWartoscPodatkuGR23Kopia.setText("04");

    	}else if(obliczona_kwota_podatku_23==5) {
    		wyswietlWartoscPodatkuGR23.setText("05");
    		wyswietlWartoscPodatkuGR23Kopia.setText("05");
    		
    	}else if(obliczona_kwota_podatku_23==6) {
    		wyswietlWartoscPodatkuGR23.setText("06");
    		wyswietlWartoscPodatkuGR23Kopia.setText("06");

    	}else if(obliczona_kwota_podatku_23==7) {
    		wyswietlWartoscPodatkuGR23.setText("07");
    		wyswietlWartoscPodatkuGR23Kopia.setText("07");

    	}else if(obliczona_kwota_podatku_23==8) {
    		wyswietlWartoscPodatkuGR23.setText("08");
    		wyswietlWartoscPodatkuGR23Kopia.setText("08");

    	}else if(obliczona_kwota_podatku_23==9) {
    		wyswietlWartoscPodatkuGR23.setText("09");
    		wyswietlWartoscPodatkuGR23Kopia.setText("09");

    	}else if(obliczona_kwota_podatku_23==0) {
    		wyswietlWartoscPodatkuGR23.setText(" -");
    		wyswietlWartoscPodatkuGR23Kopia.setText(" -");		
    	
    	}else {
    		wyswietlWartoscPodatkuGR23.setText(String.valueOf(Math.round(obliczona_kwota_podatku_23)));
    		wyswietlWartoscPodatkuGR23Kopia.setText(String.valueOf(Math.round(obliczona_kwota_podatku_23)));
    		
    	}	
        
      //---------------------------------KWOTA PODATKU 8%----------------------------------------------------//
        wyswietlWartoscPodatkuZL8.setText(String.valueOf((int)Kwota_podatku_8));
        wyswietlWartoscPodatkuZL8Kopia.setText(String.valueOf((int)Kwota_podatku_8));   
            
        double obliczona_kwota_podatku_8=Kwota_podaktu_poszczegolnych.oblicz_8();
            
            if(obliczona_kwota_podatku_8==1) {
            	wyswietlWartoscPodatkuGR8.setText("01");
            	wyswietlWartoscPodatkuGR8Kopia.setText("01");
        				
        	}else if(obliczona_kwota_podatku_8==2) {
        		wyswietlWartoscPodatkuGR8.setText("02");
        		wyswietlWartoscPodatkuGR8Kopia.setText("02");

        	}else if(obliczona_kwota_podatku_8==3) {
        		wyswietlWartoscPodatkuGR8.setText("03");
        		wyswietlWartoscPodatkuGR8Kopia.setText("03");

        	}else if(obliczona_kwota_podatku_8==4) {
        		wyswietlWartoscPodatkuGR8.setText("04");
        		wyswietlWartoscPodatkuGR8Kopia.setText("04");

        	}else if(obliczona_kwota_podatku_8==5) {
        		wyswietlWartoscPodatkuGR8.setText("05");
        		wyswietlWartoscPodatkuGR8Kopia.setText("05");
        		
        	}else if(obliczona_kwota_podatku_8==6) {
        		wyswietlWartoscPodatkuGR8.setText("06");
        		wyswietlWartoscPodatkuGR8Kopia.setText("06");

        	}else if(obliczona_kwota_podatku_8==7) {
        		wyswietlWartoscPodatkuGR8.setText("07");
        		wyswietlWartoscPodatkuGR8Kopia.setText("07");

        	}else if(obliczona_kwota_podatku_8==8) {
        		wyswietlWartoscPodatkuGR8.setText("08");
        		wyswietlWartoscPodatkuGR8Kopia.setText("08");

        	}else if(obliczona_kwota_podatku_8==9) {
        		wyswietlWartoscPodatkuGR8.setText("09");
        		wyswietlWartoscPodatkuGR8Kopia.setText("09");

        	}else if(obliczona_kwota_podatku_8==0) {
        		wyswietlWartoscPodatkuGR8.setText(" -");
        		wyswietlWartoscPodatkuGR8Kopia.setText(" -");		
        	
        	}else {
        		wyswietlWartoscPodatkuGR8.setText(String.valueOf(Math.round(obliczona_kwota_podatku_8)));
        		wyswietlWartoscPodatkuGR8Kopia.setText(String.valueOf(Math.round(obliczona_kwota_podatku_8)));
        		
        	}	
          //---------------------------------KWOTA PODATKU 5%----------------------------------------------------//
            wyswietlWartoscPodatkuZL5.setText(String.valueOf((int)Kwota_podatku_5));
            wyswietlWartoscPodatkuZL5Kopia.setText(String.valueOf((int)Kwota_podatku_5));   
                
            double obliczona_kwota_podatku_5=Kwota_podaktu_poszczegolnych.oblicz_5();
                
            if(obliczona_kwota_podatku_5==1) {
                wyswietlWartoscPodatkuGR5.setText("01");
                wyswietlWartoscPodatkuGR5Kopia.setText("01");
            				
            }else if(obliczona_kwota_podatku_5==2) {
            	wyswietlWartoscPodatkuGR5.setText("02");
            	wyswietlWartoscPodatkuGR5Kopia.setText("02");

            }else if(obliczona_kwota_podatku_5==3) {
            	wyswietlWartoscPodatkuGR5.setText("03");
            	wyswietlWartoscPodatkuGR5Kopia.setText("03");

            }else if(obliczona_kwota_podatku_5==4) {
            	wyswietlWartoscPodatkuGR5.setText("04");
            	wyswietlWartoscPodatkuGR5Kopia.setText("04");

            }else if(obliczona_kwota_podatku_5==5) {
            	wyswietlWartoscPodatkuGR5.setText("05");
            	wyswietlWartoscPodatkuGR5Kopia.setText("05");
            		
            }else if(obliczona_kwota_podatku_5==6) {
            	wyswietlWartoscPodatkuGR5.setText("06");
            	wyswietlWartoscPodatkuGR5Kopia.setText("06");

            }else if(obliczona_kwota_podatku_5==7) {
            	wyswietlWartoscPodatkuGR5.setText("07");
            	wyswietlWartoscPodatkuGR5Kopia.setText("07");

            }else if(obliczona_kwota_podatku_5==8) {
            	wyswietlWartoscPodatkuGR5.setText("08");
            	wyswietlWartoscPodatkuGR5Kopia.setText("08");

            }else if(obliczona_kwota_podatku_5==9) {
            	wyswietlWartoscPodatkuGR5.setText("09");
            	wyswietlWartoscPodatkuGR5Kopia.setText("09");

           	}else if(obliczona_kwota_podatku_5==0) {
            	wyswietlWartoscPodatkuGR5.setText(" -");
            	wyswietlWartoscPodatkuGR5Kopia.setText(" -");		
            	
           	}else {
            	wyswietlWartoscPodatkuGR5.setText(String.valueOf(Math.round(obliczona_kwota_podatku_5)));
            	wyswietlWartoscPodatkuGR5Kopia.setText(String.valueOf(Math.round(obliczona_kwota_podatku_5)));
            		
            	}
            
            //-----------------------------------LACZNA KWOTA PODATKU----------------------------------------------//
            razem_Kwota_podatku = Kwota_podatku_23 + Kwota_podatku_8 + Kwota_podatku_5;
            
            wyswietlRazemWartoscPodatkuZL.setText(String.valueOf((int)razem_Kwota_podatku));
            wyswietlRazemWartoscPodatkuZLKopia.setText(String.valueOf((int)razem_Kwota_podatku));
                
            Razem_Kwota_podatku Razem_Kwota_podatku = new Razem_Kwota_podatku(razem_Kwota_podatku);
            
            double obliczona_kwota_podatku=Razem_Kwota_podatku.oblicz_razem_podatek();
               
            if(obliczona_kwota_podatku==1) {
            	wyswietlRazemWartoscPodatkuGR.setText("01");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("01");
        				
        	}else if(obliczona_kwota_podatku==2) {
        		wyswietlRazemWartoscPodatkuGR.setText("02");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("02");

        	}else if(obliczona_kwota_podatku==3) {
        		wyswietlRazemWartoscPodatkuGR.setText("03");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("03");

        	}else if(obliczona_kwota_podatku==4) {
        		wyswietlRazemWartoscPodatkuGR.setText("04");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("04");

        	}else if(obliczona_kwota_podatku==5) {
        		wyswietlRazemWartoscPodatkuGR.setText("05");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("05");
        		
        	}else if(obliczona_kwota_podatku==6) {
        		wyswietlRazemWartoscPodatkuGR.setText("06");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("06");

        	}else if(obliczona_kwota_podatku==7) {
        		wyswietlRazemWartoscPodatkuGR.setText("07");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("07");

        	}else if(obliczona_kwota_podatku==8) {
        		wyswietlRazemWartoscPodatkuGR.setText("08");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("08");

        	}else if(obliczona_kwota_podatku_23==9) {
        		wyswietlRazemWartoscPodatkuGR.setText("09");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("09");

        	}else if(obliczona_kwota_podatku==0) {
        		wyswietlRazemWartoscPodatkuGR.setText(" -");
            	wyswietlRazemWartoscPodatkuGRKopia.setText(" -");
        			
        	}else {
        		wyswietlRazemWartoscPodatkuGR.setText(String.valueOf(Math.round(obliczona_kwota_podatku)));
            	wyswietlRazemWartoscPodatkuGRKopia.setText(String.valueOf(Math.round(obliczona_kwota_podatku)));
        		
        	}	
            
          //-----------------------------KWOTA WRAZ Z PODATKIEM W ZALEZNOSCI OD PODATKU---------------------------//
          //----------------------------------------KWOTA PODATKU 23%---------------------------------------------//
            wyswietlRazemWartoscPodatkuZL23.setText(String.valueOf((int)wartoscZPodatkiemZl23));
            wyswietlRazemWartoscPodatkuZL23Kopia.setText(String.valueOf((int)wartoscZPodatkiemZl23));

            Kwota_wraz_z_podatkiem_sumowanie_poszczegolnych kwota_wraz_z_podatkiem_sumowanie_poszczegolnych = new Kwota_wraz_z_podatkiem_sumowanie_poszczegolnych(wartoscZPodatkiemZl23, wartoscZPodatkiemZl8, wartoscZPodatkiemZl5, wartoscZPodatkiemZl0);
                
            double obliczona_kwota_wraz_z_podatkiem_23=kwota_wraz_z_podatkiem_sumowanie_poszczegolnych.oblicz_23();
                
            if(obliczona_kwota_wraz_z_podatkiem_23==1) {
            	wyswietlRazemWartoscPodatkuGR23.setText("01");
            	wyswietlRazemWartoscPodatkuGR23Kopia.setText("01");
            				
            }else if(obliczona_kwota_wraz_z_podatkiem_23==2) {
            	wyswietlRazemWartoscPodatkuGR23.setText("02");
            	wyswietlRazemWartoscPodatkuGR23Kopia.setText("02");

            }else if(obliczona_kwota_wraz_z_podatkiem_23==3) {
            	wyswietlRazemWartoscPodatkuGR23.setText("03");
            	wyswietlRazemWartoscPodatkuGR23Kopia.setText("03");

            }else if(obliczona_kwota_wraz_z_podatkiem_23==4) {
            	wyswietlRazemWartoscPodatkuGR23.setText("04");
            	wyswietlRazemWartoscPodatkuGR23Kopia.setText("04");

           	}else if(obliczona_kwota_wraz_z_podatkiem_23==5) {
           		wyswietlRazemWartoscPodatkuGR23.setText("05");
           		wyswietlRazemWartoscPodatkuGR23Kopia.setText("05");
            		
           	}else if(obliczona_kwota_wraz_z_podatkiem_23==6) {
           		wyswietlRazemWartoscPodatkuGR23.setText("06");
           		wyswietlRazemWartoscPodatkuGR23Kopia.setText("06");

            }else if(obliczona_kwota_wraz_z_podatkiem_23==7) {
            	wyswietlRazemWartoscPodatkuGR23.setText("07");
            	wyswietlRazemWartoscPodatkuGR23Kopia.setText("07");

            }else if(obliczona_kwota_wraz_z_podatkiem_23==8) {
            	wyswietlRazemWartoscPodatkuGR23.setText("08");
            	wyswietlRazemWartoscPodatkuGR23Kopia.setText("08");

           	}else if(obliczona_kwota_wraz_z_podatkiem_23==9) {
           		wyswietlRazemWartoscPodatkuGR23.setText("09");
           		wyswietlRazemWartoscPodatkuGR23Kopia.setText("09");

           	}else if(obliczona_kwota_wraz_z_podatkiem_23==0) {
           		wyswietlRazemWartoscPodatkuGR23.setText(" -");
           		wyswietlRazemWartoscPodatkuGR23Kopia.setText(" -");		
            	
           	}else {
           		wyswietlRazemWartoscPodatkuGR23.setText(String.valueOf(Math.round(obliczona_kwota_wraz_z_podatkiem_23)));
           		wyswietlRazemWartoscPodatkuGR23Kopia.setText(String.valueOf(Math.round(obliczona_kwota_wraz_z_podatkiem_23)));
            		
           	}	
            
            
          //----------------------------------------KWOTA PODATKU 8%---------------------------------------------//
            wyswietlRazemWartoscPodatkuZL8.setText(String.valueOf((int)wartoscZPodatkiemZl8));
            wyswietlRazemWartoscPodatkuZL8Kopia.setText(String.valueOf((int)wartoscZPodatkiemZl8));     
                
            double obliczona_kwota_wraz_z_podatkiem_8=kwota_wraz_z_podatkiem_sumowanie_poszczegolnych.oblicz_8();
                
            if(obliczona_kwota_wraz_z_podatkiem_8==1) {
            	wyswietlRazemWartoscPodatkuGR8.setText("01");
            	wyswietlRazemWartoscPodatkuGR8Kopia.setText("01");
            				
            }else if(obliczona_kwota_wraz_z_podatkiem_8==2) {
            	wyswietlRazemWartoscPodatkuGR8.setText("02");
            	wyswietlRazemWartoscPodatkuGR8Kopia.setText("02");

            }else if(obliczona_kwota_wraz_z_podatkiem_8==3) {
            	wyswietlRazemWartoscPodatkuGR8.setText("03");
            	wyswietlRazemWartoscPodatkuGR8Kopia.setText("03");

            }else if(obliczona_kwota_wraz_z_podatkiem_8==4) {
            	wyswietlRazemWartoscPodatkuGR8.setText("04");
            	wyswietlRazemWartoscPodatkuGR8Kopia.setText("04");

           	}else if(obliczona_kwota_wraz_z_podatkiem_8==5) {
           		wyswietlRazemWartoscPodatkuGR8.setText("05");
           		wyswietlRazemWartoscPodatkuGR8Kopia.setText("05");
            		
           	}else if(obliczona_kwota_wraz_z_podatkiem_8==6) {
           		wyswietlRazemWartoscPodatkuGR8.setText("06");
           		wyswietlRazemWartoscPodatkuGR8Kopia.setText("06");

            }else if(obliczona_kwota_wraz_z_podatkiem_8==7) {
            	wyswietlRazemWartoscPodatkuGR8.setText("07");
            	wyswietlRazemWartoscPodatkuGR8Kopia.setText("07");

            }else if(obliczona_kwota_wraz_z_podatkiem_8==8) {
            	wyswietlRazemWartoscPodatkuGR8.setText("08");
            	wyswietlRazemWartoscPodatkuGR8Kopia.setText("08");

           	}else if(obliczona_kwota_wraz_z_podatkiem_8==9) {
           		wyswietlRazemWartoscPodatkuGR8.setText("09");
           		wyswietlRazemWartoscPodatkuGR8Kopia.setText("09");

           	}else if(obliczona_kwota_wraz_z_podatkiem_8==0) {
           		wyswietlRazemWartoscPodatkuGR8.setText(" -");
           		wyswietlRazemWartoscPodatkuGR8Kopia.setText(" -");		
            	
           	}else {
           		wyswietlRazemWartoscPodatkuGR8.setText(String.valueOf(Math.round(obliczona_kwota_wraz_z_podatkiem_8)));
           		wyswietlRazemWartoscPodatkuGR8Kopia.setText(String.valueOf(Math.round(obliczona_kwota_wraz_z_podatkiem_8)));
            		
           	}	
            
            //----------------------------------------KWOTA PODATKU 5%---------------------------------------------//
            wyswietlRazemWartoscPodatkuZL5.setText(String.valueOf((int)wartoscZPodatkiemZl5));
            wyswietlRazemWartoscPodatkuZL5Kopia.setText(String.valueOf((int)wartoscZPodatkiemZl5));     
                
            double obliczona_kwota_wraz_z_podatkiem_5=kwota_wraz_z_podatkiem_sumowanie_poszczegolnych.oblicz_5();
                
            if(obliczona_kwota_wraz_z_podatkiem_5==1) {
            	wyswietlRazemWartoscPodatkuGR5.setText("01");
            	wyswietlRazemWartoscPodatkuGR5Kopia.setText("01");
            				
            }else if(obliczona_kwota_wraz_z_podatkiem_5==2) {
            	wyswietlRazemWartoscPodatkuGR5.setText("02");
            	wyswietlRazemWartoscPodatkuGR5Kopia.setText("02");

            }else if(obliczona_kwota_wraz_z_podatkiem_5==3) {
            	wyswietlRazemWartoscPodatkuGR5.setText("03");
            	wyswietlRazemWartoscPodatkuGR5Kopia.setText("03");

            }else if(obliczona_kwota_wraz_z_podatkiem_5==4) {
            	wyswietlRazemWartoscPodatkuGR5.setText("04");
            	wyswietlRazemWartoscPodatkuGR5Kopia.setText("04");

           	}else if(obliczona_kwota_wraz_z_podatkiem_5==5) {
           		wyswietlRazemWartoscPodatkuGR5.setText("05");
           		wyswietlRazemWartoscPodatkuGR5Kopia.setText("05");
            		
           	}else if(obliczona_kwota_wraz_z_podatkiem_5==6) {
           		wyswietlRazemWartoscPodatkuGR5.setText("06");
           		wyswietlRazemWartoscPodatkuGR5Kopia.setText("06");

            }else if(obliczona_kwota_wraz_z_podatkiem_5==7) {
            	wyswietlRazemWartoscPodatkuGR5.setText("07");
            	wyswietlRazemWartoscPodatkuGR5Kopia.setText("07");

            }else if(obliczona_kwota_wraz_z_podatkiem_5==8) {
            	wyswietlRazemWartoscPodatkuGR5.setText("08");
            	wyswietlRazemWartoscPodatkuGR5Kopia.setText("08");

           	}else if(obliczona_kwota_wraz_z_podatkiem_5==9) {
           		wyswietlRazemWartoscPodatkuGR5.setText("09");
           		wyswietlRazemWartoscPodatkuGR5Kopia.setText("09");

           	}else if(obliczona_kwota_wraz_z_podatkiem_5==0) {
           		wyswietlRazemWartoscPodatkuGR5.setText(" -");
           		wyswietlRazemWartoscPodatkuGR5Kopia.setText(" -");		
            	
           	}else {
           		wyswietlRazemWartoscPodatkuGR5.setText(String.valueOf(Math.round(obliczona_kwota_wraz_z_podatkiem_5)));
           		wyswietlRazemWartoscPodatkuGR5Kopia.setText(String.valueOf(Math.round(obliczona_kwota_wraz_z_podatkiem_5)));
            		
           	}	
            
            //----------------------------------------KWOTA PODATKU 0%---------------------------------------------//
            wyswietlRazemWartoscPodatkuZL0.setText(String.valueOf((int)wartoscZPodatkiemZl0));
            wyswietlRazemWartoscPodatkuZL0Kopia.setText(String.valueOf((int)wartoscZPodatkiemZl0)); 
 
                
            double obliczona_kwota_wraz_z_podatkiem_0=kwota_wraz_z_podatkiem_sumowanie_poszczegolnych.oblicz_0();
                
            if(obliczona_kwota_wraz_z_podatkiem_0==1) {
            	wyswietlRazemWartoscPodatkuGR0.setText("01");
            	wyswietlRazemWartoscPodatkuGR0Kopia.setText("01");
            				
            }else if(obliczona_kwota_wraz_z_podatkiem_0==2) {
            	wyswietlRazemWartoscPodatkuGR0.setText("02");
            	wyswietlRazemWartoscPodatkuGR0Kopia.setText("02");     

            }else if(obliczona_kwota_wraz_z_podatkiem_0==3) {
            	wyswietlRazemWartoscPodatkuGR0.setText("03");
            	wyswietlRazemWartoscPodatkuGR0Kopia.setText("03");

            }else if(obliczona_kwota_wraz_z_podatkiem_0==4) {
            	wyswietlRazemWartoscPodatkuGR0.setText("04");
            	wyswietlRazemWartoscPodatkuGR0Kopia.setText("04");;

           	}else if(obliczona_kwota_wraz_z_podatkiem_0==5) {
           		wyswietlRazemWartoscPodatkuGR0.setText("05");
           		wyswietlRazemWartoscPodatkuGR0Kopia.setText("05");
            		
           	}else if(obliczona_kwota_wraz_z_podatkiem_0==6) {
           		wyswietlRazemWartoscPodatkuGR0.setText("06");
           		wyswietlRazemWartoscPodatkuGR0Kopia.setText("06");

            }else if(obliczona_kwota_wraz_z_podatkiem_0==7) {
            	wyswietlRazemWartoscPodatkuGR0.setText("07");
            	wyswietlRazemWartoscPodatkuGR0Kopia.setText("07");

            }else if(obliczona_kwota_wraz_z_podatkiem_0==8) {
            	wyswietlRazemWartoscPodatkuGR0.setText("08");
            	wyswietlRazemWartoscPodatkuGR0Kopia.setText("08");

           	}else if(obliczona_kwota_wraz_z_podatkiem_0==9) {
           		wyswietlRazemWartoscPodatkuGR0.setText("09");
           		wyswietlRazemWartoscPodatkuGR0Kopia.setText("09");

           	}else if(obliczona_kwota_wraz_z_podatkiem_0==0) {
           		wyswietlRazemWartoscPodatkuGR0.setText(" -");
           		wyswietlRazemWartoscPodatkuGR0Kopia.setText(" -");	
            	
           	}else {
           		wyswietlRazemWartoscPodatkuGR0.setText(String.valueOf(Math.round(obliczona_kwota_wraz_z_podatkiem_0)));
           		wyswietlRazemWartoscPodatkuGR0Kopia.setText(String.valueOf(Math.round(obliczona_kwota_wraz_z_podatkiem_0)));
           		
           	}	
               
          //-----------------------------------LACZNA KWOTA WRAZ Z PODATKIEM ORAZ KWOTA DO ZAPLATY----------------------------------------------//
            laczna_Kwota_Wraz_Z_Podatkiem = wartoscZPodatkiemZl23 + wartoscZPodatkiemZl8 + wartoscZPodatkiemZl5 + wartoscZPodatkiemZl0;
            
            wyswietlLacznaWartoscWrazZPodatkiemZl.setText(String.valueOf((int)laczna_Kwota_Wraz_Z_Podatkiem));
            wyswietlLacznaWartoscWrazZPodatkiemZlKopia.setText(String.valueOf((int)laczna_Kwota_Wraz_Z_Podatkiem));
            
            //DO ZAPLATY ZL
            doZaplatyZl.setText(String.valueOf((int)laczna_Kwota_Wraz_Z_Podatkiem));
            doZaplatyZlKopia.setText(String.valueOf((int)laczna_Kwota_Wraz_Z_Podatkiem));
                
            Laczna_kwota_wraz_z_podatkiem laczna_kwota_wraz_z_podatkiem = new Laczna_kwota_wraz_z_podatkiem(laczna_Kwota_Wraz_Z_Podatkiem);
            
            double obliczona_laczna_kwota_wraz_z_podatkiem=laczna_kwota_wraz_z_podatkiem.oblicz_laczna_wartosc_wraz_z_podatekiem();
               
            if(obliczona_laczna_kwota_wraz_z_podatkiem==1) {
            	wyswietlLacznaWartoscWrazZPodatkiemGR.setText("01");
            	wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText("01");
            	
            	//DO ZAPLATY
            	doZaplatyGR.setText("01");
            	doZaplatyGRKopia.setText("01");
        				
        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==2) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText("02");
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText("02");
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText("02");
            	doZaplatyGRKopia.setText("02");;

        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==3) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText("03");
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText("03");
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText("03");
            	doZaplatyGRKopia.setText("03");

        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==4) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText("04");
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText("04");
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText("04");
            	doZaplatyGRKopia.setText("04");

        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==5) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText("05");
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText("05");
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText("05");
            	doZaplatyGRKopia.setText("05");
        		
        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==6) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText("06");
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText("06");
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText("06");
            	doZaplatyGRKopia.setText("06");

        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==7) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText("07");
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText("07");
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText("07");
            	doZaplatyGRKopia.setText("07");

        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==8) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText("08");
            	wyswietlRazemWartoscPodatkuGRKopia.setText("08");
            	
            	//DO ZAPLATY
            	doZaplatyGR.setText("08");
            	doZaplatyGRKopia.setText("08");

        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==9) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText("09");
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText("09");
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText("09");
            	doZaplatyGRKopia.setText("09");

        	}else if(obliczona_laczna_kwota_wraz_z_podatkiem==0) {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText(" -");
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText(" -");
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText(" -");
            	doZaplatyGRKopia.setText(" -");
        			
        	}else {
        		wyswietlLacznaWartoscWrazZPodatkiemGR.setText(String.valueOf(Math.round(obliczona_laczna_kwota_wraz_z_podatkiem)));
        		wyswietlLacznaWartoscWrazZPodatkiemGRKopia.setText(String.valueOf(Math.round(obliczona_laczna_kwota_wraz_z_podatkiem)));
        		
            	//DO ZAPLATY
            	doZaplatyGR.setText(String.valueOf(Math.round(obliczona_laczna_kwota_wraz_z_podatkiem)));
            	doZaplatyGRKopia.setText(String.valueOf(Math.round(obliczona_laczna_kwota_wraz_z_podatkiem)));
        		
        	}	           
            
          //LICZBY NA SOWA                                                                               
        	zlotowkiJednosci = (int)laczna_Kwota_Wraz_Z_Podatkiem%10;                                
                                                                                                
        	zlotowkiDziesiatki=(int)laczna_Kwota_Wraz_Z_Podatkiem%100/10;                            
                                                                                                
        	zlotowkiSetki=(int)laczna_Kwota_Wraz_Z_Podatkiem%1000/100;                               
        	                                                                                    
        	zlotowkiTysiace=(int)laczna_Kwota_Wraz_Z_Podatkiem%10000/1000;                           
                                                                                                
        	                                                                                    
        	if(zlotowkiJednosci>0 && zlotowkiDziesiatki==0) {                               
        		slownieZlotowkiJednosci.setText(jednosci[zlotowkiJednosci]);                
        		slownieZlotowkiJednosciKopia.setText(jednosci[zlotowkiJednosci]);           
        		slownieZlotowkiDziesiatki.setText("");                                      
        		slownieZlotowkiDziesiatkiKopia.setText("");                                 
        	}else if(zlotowkiDziesiatki==1 && zlotowkiJednosci>0) {                         
        		slownieZlotowkiJednosci.setText("");                                        
        		slownieZlotowkiJednosciKopia.setText("");                                   
        		zlotowkiDziesiatki=zlotowkiJednosci;                                        
        		slownieZlotowkiDziesiatki.setText(dziesiatkiV1[zlotowkiDziesiatki]);        
        		slownieZlotowkiDziesiatkiKopia.setText(dziesiatkiV1[zlotowkiDziesiatki]);   
        	}else if(zlotowkiDziesiatki>1) {                                                
        		slownieZlotowkiDziesiatki.setText(dziesiatki[zlotowkiDziesiatki]);          
        		slownieZlotowkiDziesiatkiKopia.setText(dziesiatki[zlotowkiDziesiatki]);     
        		slownieZlotowkiJednosci.setText(jednosci[zlotowkiJednosci]);                
        		slownieZlotowkiJednosciKopia.setText(jednosci[zlotowkiJednosci]);           
        	}else if(zlotowkiDziesiatki==1 & zlotowkiJednosci==0) {                         
        		slownieZlotowkiDziesiatki.setText(dziesiatki[1]);                           
        		slownieZlotowkiDziesiatkiKopia.setText(dziesiatki[1]);                      
        	}                                                                               
        		                                                                                
        		slownieZlotowkiSetki.setText(setki[zlotowkiSetki]);                             
        		slownieZlotowkiSetkiKopia.setText(setki[zlotowkiSetki]);                        
        	if(zlotowkiTysiace==1) {                                                        
        		slownieTysiaceKoncowki.setText(tysiaceKoncowki[1]);                         
        		slownieTysiaceKoncowkiKopia.setText(tysiaceKoncowki[1]);                    
        		slownieZlotowkiTysiace.setText(jednosci[0]);                                
        		slownieZlotowkiTysiaceKopia.setText(jednosci[0]);                           
        	}                                                                               
        	else if(zlotowkiTysiace>=5 && zlotowkiTysiace<=9) {                             
        		slownieZlotowkiTysiace.setText(jednosci[zlotowkiTysiace]);                  
        		slownieZlotowkiTysiaceKopia.setText(jednosci[zlotowkiTysiace]);             
        		slownieTysiaceKoncowki.setText(tysiaceKoncowki[3]);                         
        		slownieTysiaceKoncowkiKopia.setText(tysiaceKoncowki[3]);                    
        	}                                                                               
        	else if(zlotowkiTysiace>1 && zlotowkiTysiace<5) {                               
        		slownieZlotowkiTysiace.setText(jednosci[zlotowkiTysiace]);                  
        		slownieZlotowkiTysiaceKopia.setText(jednosci[zlotowkiTysiace]);             
        		slownieTysiaceKoncowki.setText(tysiaceKoncowki[2]);                         
        		slownieTysiaceKoncowkiKopia.setText(tysiaceKoncowki[2]);                    
        	}                                                                               
        	else if(zlotowkiTysiace>9) {                                                    
        		slownieZlotowkiTysiace.setText(jednosci[zlotowkiTysiace]);                  
        		slownieZlotowkiTysiaceKopia.setText(jednosci[zlotowkiTysiace]);             
        		slownieTysiaceKoncowki.setText(tysiaceKoncowki[3]);                         
        		slownieTysiaceKoncowkiKopia.setText(tysiaceKoncowki[3]);                    
        	}                                                                               
        	else if(zlotowkiTysiace==0) {                                                   
        		slownieTysiaceKoncowki.setText("");                                         
        		slownieTysiaceKoncowkiKopia.setText("");                                    
        		slownieZlotowkiTysiace.setText("");                                         
        		slownieZlotowkiTysiaceKopia.setText("");                                    
        	}                                                                               
        //SOWO ZOTE				                                                                    
        		zlote.setText("z");                                                            
        		zloteKopia.setText("z");  
        //-------------------------------------GROSZE------------------------------------------//		
        		groszeSlownie.setText(String.valueOf(Math.round(obliczona_laczna_kwota_wraz_z_podatkiem)+"/100 gr"));
            	groszeSlownieKopia.setText(String.valueOf(Math.round(obliczona_laczna_kwota_wraz_z_podatkiem)+"/100 gr"));
            		
              		
   }
   //ZAPISANIE ZAPISANIE ZAPISANIE ZAPISANIE ZAPISANIE ZAPISANIE ZAPISANIE ZAPISANIE ZAPISANIE ZAPISANIE ZAPISANIE
   		else if(zrodlo == bSave)
   		{		
   			Date obecnaData = new Date();
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
			String data = sdf1.format(obecnaData);
			
   			//zrodlo: https://docs.oracle.com/
   			BufferedImage generowanyObraz = new BufferedImage(598, 824, BufferedImage.TYPE_4BYTE_ABGR); //BufferedImage - sluzy do obslugi i przetwarzania danych obrazu ; BufferedImage.TYPE_4BYTE_ABGR - typ obrazu
   			Graphics gotowyObraz = generowanyObraz.createGraphics();
   			paint(gotowyObraz); //narysuj przekazany obraz  
   			
   			File outputfile = new File("Historia Faktur\\" + data + ".png");
   			try{
   				ImageIO.write(generowanyObraz, "png", outputfile); //zapisanie obrazu do pliku png
   			}catch (IOException e1) {
   				
   			}
   		}  
   	 
   //DRUKOWANIE   DRUKOWANIE   DRUKOWANIE    DRUKOWANIE    DRUKOWANIE    DRUKOWANIE    DRUKOWANIE    DRUKOWANIE  
   		else
   	           	 {
   		PrinterJob job = PrinterJob.getPrinterJob();
   		PageFormat pf = job.pageDialog(job.defaultPage());
   		job.setPrintable(this);
   		boolean ok = job.printDialog();                      //wywoanie dialogu zatwierdzajcego drukowanie
   		if (ok) 
   		{
              try 
              {

                  job.print();
              } catch (PrinterException ex) 
              {
               /* The job did not successfully complete */
             }
          } 
      	}      	
       }
       
       public Jednorazowa_faktura(JPanel f) {
           panelToPrint = f;
       }
   }

