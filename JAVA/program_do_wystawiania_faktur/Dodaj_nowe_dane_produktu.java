package pl.programfaktury.first;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Dodaj_nowe_dane_produktu extends JFrame implements ActionListener{
	
	JLabel produktNaglowek;
	
	//DANE PRODUKT
	JLabel produktNazwaTekst;
	JTextField produktNazwa;
	JLabel produktMiaraTekst;
	JTextField produktMiara;
	JLabel podajMiareProduktuTekst;
	JTextField podajMiareProduktu;
//PRZYCISKI I LISTA ROZWIJANA	
	JButton bZapisz;
	
	
    public Dodaj_nowe_dane_produktu() {
//PARAMETRY OKNA
		setTitle("Dodaj nowe dane produktu");
		setSize(500, 400);
		
		setLayout(null);
		//---------------------------PIATY PRODUKT---------------------------------------------//
		produktNaglowek = new JLabel("Podaj dane nowego produktu");
	    produktNaglowek.setBounds(50,50,200,20);
	    add(produktNaglowek);
	    
		/*LISTA PRODUKTOW
	    produkt = new JComboBox();
	    produkt.setBounds(910,750,200,20);
	    produkt.addItem("WYBIERZ PIATY PRODUKT");
	    produkt.addItem("Produkt 1");
	    produkt.addItem("Produkt 2");
	    produkt.addItem("Produkt 3");
		add(produkt);			
	   	*/
	    //--------------------------------------------------------------//
	   	
		produktNazwaTekst = new JLabel("Podaj nazw� produktu:");
		produktNazwaTekst.setBounds(50,75,200,20);
	   	add(produktNazwaTekst);
	    	    	
	   	produktNazwa = new JTextField("");
	   	produktNazwa.setBounds(230,75,200,20);
	   	add(produktNazwa);
	    //--------------------------------------------------------------//
	    	    	    	    	
	    			
	    //--------------------------------------------------------------//
	   	produktMiaraTekst = new JLabel("Podaj miar� produktu:");
	   	produktMiaraTekst.setBounds(50,100,230,20);
	    add(produktMiaraTekst);
	    	
	    podajMiareProduktu = new JTextField("");
	    podajMiareProduktu.setBounds(230,100,200,20);
	    add(podajMiareProduktu);
	    //--------------------------------------------------------------//
	    
		//--------------------------------------------------------------//
		bZapisz = new JButton("ZAPISZ DANE");
		bZapisz .setBounds(50,175,120,20);
		add(bZapisz);
		bZapisz.addActionListener(this);
		//--------------------------------------------------------------//
		//--------------------------------------------------------------//
    }
	@Override
	public void actionPerformed(ActionEvent e) {
		Object zrodlo = e.getSource();
		
		if(zrodlo == bZapisz) {
			
		}
			int liczbaZnakowProduktNazwa = produktNazwa.getText().length();
			int liczbaZnakowMiaraProduktu = podajMiareProduktu.getText().length();		
			
	  		
			
			if(liczbaZnakowProduktNazwa > 0  && liczbaZnakowProduktNazwa < 55 && liczbaZnakowMiaraProduktu > 0  && liczbaZnakowMiaraProduktu <= 4) {
			
		     File file = new File("Produkty\\Licznik\\licznik.txt");
		     Scanner in = null;
			try {
				in = new Scanner(file);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		    String zawartosc = in.nextLine();
		    int numer = Integer.valueOf(zawartosc);
		    //System.out.println(numer); //TEST
			
			String nazwaProduktuDoPliku = produktNazwa.getText();
			//System.out.println(produktNazwa.getText().length());
			String miaraProduktuDoPliku = podajMiareProduktu.getText();

			
			try {
				BufferedWriter produkt = new BufferedWriter(new FileWriter(new File("Produkty\\" + numer + "-produkt.txt"), true));
				produkt.write(nazwaProduktuDoPliku);
				produkt.newLine();
				produkt.write(miaraProduktuDoPliku);
				produkt.newLine();
				produkt.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			numer = ++numer;
			zawartosc = Integer.toString(numer);
			System.out.println();
			
			try {
				BufferedWriter uaktualnionyLicznik = new BufferedWriter(new FileWriter(new File("Produkty\\Licznik\\licznik.txt")));
				uaktualnionyLicznik.write(zawartosc);
				uaktualnionyLicznik.close();
			} catch(IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
		
			
			//setVisible(true);
		}
	}
}
