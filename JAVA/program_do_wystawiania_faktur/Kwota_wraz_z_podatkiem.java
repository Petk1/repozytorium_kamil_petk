package pl.programfaktury.first;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Kwota_wraz_z_podatkiem {
	private double Kwota_wraz_z_podatkiem;

	public Kwota_wraz_z_podatkiem(double  Kwota_wraz_z_podatkiem) {
		this.Kwota_wraz_z_podatkiem =  Kwota_wraz_z_podatkiem;

	}
	
	public double oblicz_kwote_wraz_z_podatekiem() {
		double wynik;
		
		//wykonanie niezbednych zaokraglen
	   	wynik=BigDecimal.valueOf(Kwota_wraz_z_podatkiem).setScale(5, RoundingMode.HALF_UP).doubleValue();
	   	wynik=BigDecimal.valueOf(wynik).setScale(4, RoundingMode.HALF_UP).doubleValue();
	   	wynik=BigDecimal.valueOf(wynik).setScale(3, RoundingMode.HALF_UP).doubleValue();
	   	wynik=(BigDecimal.valueOf(wynik).setScale(2, RoundingMode.HALF_UP).doubleValue()*100)%100;
		
		return wynik;//zwrocenie zaokraglonej wartosci GR podatku
		
	}
}
