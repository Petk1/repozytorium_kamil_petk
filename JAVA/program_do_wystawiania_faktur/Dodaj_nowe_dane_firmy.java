package pl.programfaktury.first;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.Printable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.util.Date;
import java.util.Scanner;
import java.text.SimpleDateFormat;

public class Dodaj_nowe_dane_firmy extends JFrame implements ActionListener{
		
		JLabel firmaNaglowek;
	
//DANE FIRMY
		JLabel nazwaFirmyTekst;
		JLabel adresFirmyTekst;
		JLabel nipTekst;
		JTextField nazwaFirmy;
		JTextField adresFirmy;
		JTextField nip;

//PRZYCISKI
		JButton bZapisz;
		
//LISTA ROZWIJANA ZAPISANE FIRMY
		JComboBox zapisaneFirmy;
		
    public Dodaj_nowe_dane_firmy() {
//PARAMETRY OKNA
		setTitle("Dodaj nowe dane firmy");
		setSize(500, 400);
		setLayout(null);
		
		
//DANE FIRMY
		//--------------------------------------------------------------//
		firmaNaglowek = new JLabel("Podaj nowe dane firmy");
		firmaNaglowek.setBounds(50,50,150,20);
		add(firmaNaglowek);
		
		nazwaFirmyTekst = new JLabel("Podaj nazw� firmy:");
		nazwaFirmyTekst.setBounds(50,75,150,20);
		add(nazwaFirmyTekst);
			    
		nazwaFirmy = new JTextField("");
		nazwaFirmy.setBounds(180,75,200,20);
		add(nazwaFirmy);
		//--------------------------------------------------------------//
					    
		//--------------------------------------------------------------//
		nipTekst = new JLabel("Podaj NIP firmy:");
		nipTekst.setBounds(50,100,150,20);
		add(nipTekst);
					    	
		nip= new JTextField("");
		nip.setBounds(180,100,200,20);
		add(nip);
		 //--------------------------------------------------------------//	
		
		//--------------------------------------------------------------//
		adresFirmyTekst = new JLabel("Podaj adres firmy:");
		adresFirmyTekst.setBounds(50,125,150,20);
		add(adresFirmyTekst);
					    	
		adresFirmy= new JTextField("");
		adresFirmy.setBounds(180,125,200,20);
		add(adresFirmy);
		
		//--------------------------------------------------------------//
		bZapisz = new JButton("ZAPISZ DANE");
		bZapisz .setBounds(50,175,120,20);
		add(bZapisz);
		bZapisz.addActionListener(this);
		//--------------------------------------------------------------//		
		//--------------------------------------------------------------//
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		Object zrodlo = e.getSource();
		
		if(zrodlo == bZapisz) {

			int liczbaZnakowNazwaFirmy = nazwaFirmy.getText().length();
			int liczbaZnakowAdresFirmy = adresFirmy.getText().length();		
			//int liczbaZnakowNipFirmy = nip.getText().length();
	   		String sprawdzenieNipFirmy = nip.getText();
	   		//boolean testNipFirmy = sprawdzenieNipFirmy.chars().allMatch( Character::isDigit );	   		
			
			if(liczbaZnakowNazwaFirmy > 0 && liczbaZnakowNazwaFirmy <= 75 && liczbaZnakowAdresFirmy > 0 && liczbaZnakowAdresFirmy < 75) {
		    File file = new File("Firmy\\Licznik\\licznik.txt");
		    Scanner in = null;
			try {
				in = new Scanner(file);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

		    String zawartosc = in.nextLine();
		    int numer = Integer.valueOf(zawartosc);
		    //System.out.println(numer); //TEST
			
			String nazwaFirmyDoPliku = nazwaFirmy.getText();
			String adresFirmyDoPliku = adresFirmy.getText();
			String nipDoPliku = nip.getText();
		
			
			try {
				BufferedWriter firma = new BufferedWriter(new FileWriter(new File("Firmy\\" + numer + "-firma.txt"), true));
				firma.write(nazwaFirmyDoPliku);
				firma.newLine();
				firma.write(nipDoPliku);
				firma.newLine();
				firma.write(adresFirmyDoPliku);
				firma.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			numer = ++numer;
			zawartosc = Integer.toString(numer);
			System.out.println();
			
			try {
				BufferedWriter uaktualnionyLicznik = new BufferedWriter(new FileWriter(new File("Firmy\\Licznik\\licznik.txt")));
				uaktualnionyLicznik.write(zawartosc);
				uaktualnionyLicznik.close();
			} catch(IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
		}
		}	
	}
}
